/* eslint-disable no-undef */
// import { MockComponentContext } from './src/tests/testutils';

const crypto = require("crypto");
const dotenv = require("dotenv");

const path = require("path");
// Mocking crypto because it's not built into jsdom
Object.defineProperty(global.self, "crypto", {
  value: {
    getRandomValues: (arr) => crypto.randomBytes(arr.length),
  },
});

jest.mock(
  "react-dnd",
  () => ({
    useDrag: () => [{}, () => {}, () => {}],
    useDrop: () => [{}, () => {}, () => {}],
    DragPreviewImage: () => null,
    DndProvider: ({ children }) => children,
    DnDProvider: ({ children }) => children,
  }),
  {
    virtual: true,
  }
);

// jest.mock('componentLibrary/TenantContext', () => MockComponentContext, {
//   virtual: true,
// });

// For Apollo mocked tests
// window.fetch = () => {};

// Random numbers are one now.  One was picked randomly by a dice roll.
// For some errors this causes a stack overflow when sorting the messages, because errors get a random ID in jest.  That took 7 months of debugging to find that this one line needs to be removed.
// Math.random = () => 1;

// global.console = {
//   ...console,
//   log: jest.fn(),
//   debug: jest.fn(),
//   info: jest.fn(),
//   warn: jest.fn(),
//   // Mocking this so things like PropTypes will break tests
//   // error: (err) => {
//   //   throw new Error(`Console logged error: ${err}`);
//   // },
//   time: jest.fn(),
// };

// If a test console.errors, raise it at the end
const errors = [];
const logs = [];

const pushLog = (...log) => logs.push(log);
const pushError = (...err) => errors.push(err);

beforeEach(() => {
  jest.spyOn(global.Math, "random").mockReturnValue(0.9);
  // Silences the console during tests
  jest.spyOn(console, "log").mockImplementation(pushLog);
  jest.spyOn(console, "debug").mockImplementation(pushLog);
  jest.spyOn(console, "warn").mockImplementation(pushLog);
  jest.spyOn(console, "error").mockImplementation(pushError);
  jest.spyOn(console, "time").mockImplementation(() => {});

  // jest.spyOn(global.console, "error").mock()
  // global.console.error = (err) => errors.push(err);
});

afterEach(() => {
  jest.spyOn(global.Math, "random").mockRestore();
  jest.spyOn(console, "log").mockRestore();
  jest.spyOn(console, "debug").mockRestore();
  jest.spyOn(console, "warn").mockRestore();
  jest.spyOn(console, "error").mockRestore();

  if (errors.length > 0) console.log("Errors:");
  errors.forEach((error) => {
    console.log(...error);
  });

  logs.forEach((log) => console.log(...log));
});

// Populating env vars
const envPath = path.join(__dirname, "environments", ".env.prod");
dotenv.config({ path: envPath });
