const version = {
  name: `${process.env.APP_NAME}`,
  version: `${process.env.APP_VERSION}`,
  stage: `${process.env.APP_STAGE}`,
};

export default version;
