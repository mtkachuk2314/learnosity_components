const config = {
  // verbose is set to true to see the test names in the console
  verbose: true,
  // transformIgnorePatterns is set to ignore react-dnd and its dependencies
  testEnvironment: "jest-environment-jsdom",
  transformIgnorePatterns: [
    "/node_modules/(?!react-dnd|core-dnd|@react-dnd|dnd-core|react-dnd-html5-backend)",
  ],
  // moduleNameMapper is set to ignore css and less files, and to mock images and other files
  moduleNameMapper: {
    "\\.(png|jpg|jpeg|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$":
      "<rootDir>/__mocks__/fileMock.js",
    "\\.(css|less|scss)$": "identity-obj-proxy",
    "react-dnd": "react-dnd",
    "react-dnd-html5-backend": "react-dnd-html5-backend-cjs",
    "dnd-core": "dnd-core-cjs",
    //     '^react-dnd$': '<rootDir>/node_modules/react-dnd/dist/cjs',
    // '^react-dnd-html5-backend$':
    //   '<rootDir>/node_modules/react-dnd-html5-backend/dist/cjs',
    // '^dnd-core$': '<rootDir>/node_modules/dnd-core/dist/cjs',
  },
  // modulePathIgnorePatterns is set to ignore the Old test folder
  modulePathIgnorePatterns: ["<rootDir>/src/OLD__tests__OLD/"],

  // setupFilesAfterEnv is set to the testSetupFile.js file
  setupFilesAfterEnv: ["<rootDir>/testSetupFile.js"],
  // globals is set to NODE_ENV: 'production'
  globals: {
    NODE_ENV: "production",
  },
  // collectCoverage will collect coverage data and generate reports in the reports folder
  coverageDirectory: "reports",
  // coverageReporters is set to generate reports in clover, text, and cobertura formats
  // json and html are options as well
  coverageReporters: ["clover", "text", "cobertura", "html"],
  // The threshold at which it fails
  // coverageThreshold: {
  //   global: {
  //     lines: 85,
  //   },
  // },
  // Places to ignore when collecting code coverage data
  collectCoverageFrom: [
    "src/**/*.{js,jsx}",
    "!**/node_modules/**",
    "!**/vendor/**",
    "!**/index.js",
    "!**/App.jsx",
    "!**/__tests__/**",
    "!**/contexts/**",
    "!src/apolloQueries.js",
    "!**/Mock**.js",
    "!**/Debug**.js",
  ],
  // reporters is set to generate a junit report
  reporters: [
    "default",
    [
      "jest-junit",
      {
        suiteName: "component application tests",
        // Further config for junit in package.json
      },
    ],
  ],
};
module.exports = config;
