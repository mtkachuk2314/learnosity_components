import styled from "@emotion/styled";

const baseStyles = {
  position: "relative",
  fontWeight: 400,
  fontFamily: ["Inter", "sans-serif"].join(","),
};

const isInfoBoxStyles = {
  padding: "1rem",
  minHeight: "6.5rem",
  fontSize: "1rem",
};

const isVideoOrImageStyles = {
  padding: "0px",
  minHeight: "20px",
  fontSize: "12px",
};

const isNeitherStyles = {
  padding: "1rem 6.5rem",
  minHeight: "6.25rem",
  fontSize: "1rem",
};

const getParentSectionStyles = (parentSection) => ({
  color: parentSection === "credit" ? "#636363" : "rgba(35, 35, 35, 1)",
  fontStyle: parentSection === "credit" ? "italic" : "normal",
});

const getContainerStyles = ({ isInfoBox, isVideo, isImage }) => {
  let styles;

  if (isInfoBox) {
    styles = isInfoBoxStyles;
  } else if (isVideo || isImage) {
    styles = isVideoOrImageStyles;
  } else {
    styles = isNeitherStyles;
  }

  return styles;
};

export const ReactQuillContainer = styled("div")(
  ({ theme, isInfoBox, isVideo, isImage, parentSection }) => ({
    ...baseStyles,
    "& .ql-container": {
      background: "rgba(255, 255, 255, 1)",
      ...getContainerStyles({ isInfoBox, isVideo, isImage }),
    },
    "& .ql-editor": {
      padding: "0px",
      border: `none`,
      borderRadius: `none`,
      boxShadow: `none`,
      letterSpacing: "0.15px",
      whiteSpace: "normal",
      fontSize: (isVideo || isImage) && "12px",
      ...getParentSectionStyles(parentSection),
    },
    "& .quill > .ql-container > .ql-editor.ql-blank::before": {
      padding: !isVideo && !isImage && !isInfoBox ? "0px 104px" : "0px",
      overflow: "hidden",
      left: isInfoBox ? "" : "0px !important",
      fontSize: (isVideo || isImage) && "12px",
      fontStyle: parentSection === "credit" ? "italic" : "normal",
    },
    "& .ql-editor.ql-blank::after": {
      background: (isVideo || isImage) && "rgba(255, 255, 255, 1)",
      fontStyle: parentSection === "credit" ? "italic" : "normal",
      fontSize: (isVideo || isImage) && "12px",
      marginTop: isVideo || isImage ? "-1.38rem" : "-1.42rem",
    },
    "& .ql-editor.ql-blank": {
      height: "auto",
      fontSize: (isVideo || isImage) && "12px",
    },
    "& .ql-toolbar .MuiPaper-root": {
      background: !isVideo && !isImage ? "rgba(255, 255, 255, 1)" : "#FAFAFA",
    },
  })
);

export default ReactQuillContainer;
