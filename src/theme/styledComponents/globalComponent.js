import { Box } from "@mui/material";
import styled from "@emotion/styled";

// globar wrapper aroung MAin components to add 16px around each

export const MainWrapper = styled(Box)(({ theme }) => ({
  padding: "1.25rem 1rem",
}));

export const MainInsideWrapper = styled(Box)(({ theme }) => ({
  paddingTop: "0.625rem", // component label height + 16px
}));
export const MainInsideWrapperInfoBox = styled(Box)(({ theme }) => ({
  paddingTop: "1rem", // component label height + 16px
}));

export const MainInsideWrapperMultiColumn = styled(Box)(({ theme }) => ({
  paddingTop: "0rem", // component label height + 16px
}));
