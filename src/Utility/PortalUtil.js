import { createPortal } from "react-dom";

const PortalUtil = ({ portalRef, children }) => {
  if (!portalRef?.current) {
    console.error("No target provided for portal component");

    return null;
  }

  return createPortal(children, portalRef.current);
};

export default PortalUtil;
