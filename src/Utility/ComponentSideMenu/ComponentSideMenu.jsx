import React from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { IconButton, Tooltip } from "@mui/material";
import icons from "./icons/icons";
// import component sidemenu styles
import styles from "./ComponentSideMenu.module.scss";

const ConditionalTooltip = ({ children, disabled, ...props }) => {
  if (disabled) return children;
  return <Tooltip {...props}>{children}</Tooltip>;
};

// Menu is visible on hover, responsible for the drag handle, the move up/down functionality, delete, duplicate
const ComponentSideMenu = ({
  isDragging,
  onDelete,
  onDuplicate,
  onMoveUp,
  onMoveDown,
  canMoveDown = true,
  canMoveUp = true,
  dragRef,
  type, // type here is component name which is getting used for Localization
}) => {
  const { t } = useTranslation();

  return (
    <div
      className={styles.componentSideMenuContainer}
      style={{
        ...(isDragging ? { opacity: 0 } : {}),
      }}
    >
      <div className={styles.componentSideMenuNestedContainer}>
        <span
          ref={dragRef}
          data-testid="component-drag"
          style={{
            display: "inline-flex",
            justifyContent: "center",
            cursor: "move",
          }}
        >
          {icons.dragIcon}
        </span>
        <Tooltip
          arrow
          title={t("Duplicate")}
          placement="left"
          classes={styles.tooltipNestedSideMenu}
        >
          <IconButton
            disableFocusRipple
            disableRipple
            onClick={onDuplicate}
            data-testid="duplicate-component-button"
            aria-label={t("Duplicate Container", {
              type,
            })}
            size="small"
            sx={{ fontSize: "0.9em", borderRadius: "0.25rem !important" }}
            className={styles.sideMenuIconButton}
          >
            {icons.copyIcon}
          </IconButton>
        </Tooltip>
        <Tooltip
          arrow
          title={t("Delete")}
          placement="left"
          classes={styles.tooltipNestedSideMenu}
        >
          <IconButton
            disableFocusRipple
            disableRipple
            onClick={onDelete}
            data-testid="delete-component-button"
            aria-label={t("Delete Container", {
              type,
            })}
            size="small"
            sx={{ fontSize: "0.9em", borderRadius: "0.25rem !important" }}
            className={styles.sideMenuDeleteIconButton}
          >
            {icons.deleteIcon}
          </IconButton>
        </Tooltip>
        <ConditionalTooltip
          disabled={!canMoveUp}
          arrow
          title={t("Move up")}
          placement="left"
          classes={styles.tooltipNestedSideMenu}
        >
          <IconButton
            disableFocusRipple
            disableRipple
            onClick={onMoveUp}
            data-testid="move-component-up-button"
            aria-label={t("Container Move Up", {
              type,
            })}
            size="small"
            sx={{ fontSize: "0.9em", borderRadius: "0.25rem !important" }}
            disabled={!canMoveUp}
            aria-disabled={!canMoveUp}
            className={styles.sideMenuIconButton}
          >
            {icons.moveUp}
          </IconButton>
        </ConditionalTooltip>
        <ConditionalTooltip
          disabled={!canMoveDown}
          arrow
          title={t("Move down")}
          placement="left"
          classes={styles.tooltipNestedSideMenu}
        >
          <IconButton
            disableFocusRipple
            disableRipple
            onClick={onMoveDown}
            data-testid="move-component-down-button"
            aria-label={t("Container Move Down", {
              type,
            })}
            size="small"
            sx={{ fontSize: "0.9em", borderRadius: "0.25rem !important" }}
            disabled={!canMoveDown}
            aria-disabled={!canMoveDown}
            className={styles.sideMenuIconButton}
          >
            {icons.moveDown}
          </IconButton>
        </ConditionalTooltip>
      </div>
    </div>
  );
};

ComponentSideMenu.propTypes = {
  isDragging: PropTypes.bool.isRequired,
  onDelete: PropTypes.func.isRequired,
  onDuplicate: PropTypes.func.isRequired,
  onMoveUp: PropTypes.func.isRequired,
  onMoveDown: PropTypes.func.isRequired,
  canMoveDown: PropTypes.bool,
  canMoveUp: PropTypes.bool,
  dragRef: PropTypes.func,
  type: PropTypes.string,
};

ComponentSideMenu.defaultProps = {
  canMoveDown: true,
  canMoveUp: true,
  dragRef: null,
  type: "",
};

ConditionalTooltip.propTypes = {
  children: PropTypes.node.isRequired,
  disabled: PropTypes.bool.isRequired,
};

export default ComponentSideMenu;
