/* eslint-disable no-nested-ternary */
import React, {
  useContext,
  useRef,
  useState,
  useCallback,
  useEffect,
  useMemo,
} from "react";
import PropTypes from "prop-types";

import { ErrorBoundary } from "react-error-boundary";

import styled from "@emotion/styled";

import { IconButton, Typography } from "@mui/material";

import componentIndex from "../components/componentIndex";
import { MockAuthoringContext } from "./MockAuthoringContext";

const styledOptions = {
  shouldForwardProp: (prop) =>
    ![
      "draggingSelf",
      "showSelf",
      "hoverActive",
      "viewOnly",
      "isDragging",
      "isHoverActive",
      "isActive",
      "isActiveComponent",
    ].includes(prop),
};

const ListItem = styled("li")({
  position: "relative",
  listStyle: "none",
  marginLeft: 0,
  paddingLeft: 0,
});

const Wrapper = styled(
  "div",
  styledOptions
)(({ showSelf, hoverActive }) => ({
  outline: showSelf
    ? `3px solid blue`
    : hoverActive
    ? `3px solid #DAE3EE`
    : null,
  opacity: 1,
}));

const StaticLabel = styled(
  "span",
  styledOptions
)(() => ({
  background: "#DAE3EE",
  width: "fit-content",
  height: "100%",
  borderRadius: "4px 4px 0px 0px",
  color: "blue",
  display: "flex",
  alignItems: "center",
  flexDirection: "row",
}));

export const ComponentLabelContainer = styled(
  "div",
  styledOptions
)(({ draggingSelf, showSelf, offset = -28 }) => {
  const style = {
    width: "fit-content",
    position: "absolute",
    left: "-4px",
    top: `${offset}px`,
    padding: "0 1px",
    display: "flex",
    alignItems: "center",
  };

  if (draggingSelf && !showSelf) style.opacity = 0.4;

  return style;
});

export const SmallIconButton = styled(IconButton)(() => ({
  color: "#FFF",
}));

export const ToolbarContainer = styled("div")(() => ({
  gridColumn: "1 / span 1",
  gridRow: 1,
  marginTop: "-3rem",
  position: "sticky",
  top: `65px`,
  alignSelf: "start",
  zIndex: 1000 - 100,
  height: "auto",
}));

const ErrorFallbackComponent = ({ error, resetErrorBoundary }) => {
  return (
    <div>
      <h1>An error occurred: {error.message}</h1>
      <button type="button" onClick={resetErrorBoundary}>
        Try again
      </button>
    </div>
  );
};

const MockComponentWrapper = ({
  id,
  componentName,
  componentProps: componentPropsProp,
}) => {
  const [isHoverActive, setIsHoverActive] = useState(false);
  const [isActiveComponent, setIsActiveComponent] = useState(false);
  const dropRef = useRef();
  const toolbarContainerRef = useRef();

  const componentDetails = componentIndex[componentName];

  const { Component, readableName, defaultProps } = componentDetails;

  const componentProps = useMemo(
    () => ({
      ...defaultProps,
      ...(componentPropsProp ?? {}),
    }),
    [componentPropsProp, defaultProps]
  );

  const { dispatch } = useContext(MockAuthoringContext);

  const updateAuthoringContext = useCallback(
    async (newComponentState) => {
      dispatch({
        type: "UPDATE_COMPONENT",
        id,
        updatedState: newComponentState,
      });
    },
    [id, dispatch]
  );

  const setActiveComponent = useCallback((active) => {
    setIsActiveComponent(active);
    setIsHoverActive(false);
  }, []);

  const setActiveComponentListener = useCallback(
    (event) => {
      if (!dropRef.current) return;
      if (dropRef.current.contains(event.target)) {
        setActiveComponent(true);
        return;
      }
      setActiveComponent(false);
    },
    [dropRef, setActiveComponent]
  );

  const setHoverState = () => {
    if (!isActiveComponent) setIsHoverActive(true);
  };

  const setHoverMenu = useCallback((state) => {
    setIsHoverActive(state);
  }, []);

  useEffect(() => {
    if (!dropRef.current) return () => {};

    if (isActiveComponent) {
      window.addEventListener("mousedown", setActiveComponentListener);
    } else {
      window.removeEventListener("mousedown", setActiveComponentListener);
    }

    return () => {
      window.removeEventListener("mousedown", setActiveComponentListener);
    };
  }, [isActiveComponent, dropRef, setActiveComponentListener]);

  return (
    <ErrorBoundary FallbackComponent={ErrorFallbackComponent}>
      <ListItem
        style={{
          position: "relative",
        }}
        id={id}
        data-testid="component-dropzone"
        data-testtype={componentName}
        ref={dropRef}
        onMouseEnter={() => {
          setHoverState(true);
        }}
        onMouseLeave={() => {
          setHoverMenu(false);
        }}
        onClick={() => {
          if (id) setActiveComponent(true);
        }}
        onBlur={(e) => {
          if (e.relatedTarget === null) return;
          if (!dropRef.current.contains(e.relatedTarget)) {
            setIsActiveComponent(false);
          }
        }}
      >
        {isHoverActive && !isActiveComponent && (
          <ComponentLabelContainer
            showSelf={isActiveComponent}
            hoverActive={isHoverActive}
            data-testid="component-label-container"
          >
            <StaticLabel
              data-testid="static-label"
              isHoverActive={isHoverActive}
            >
              <Typography
                variant="body2"
                component="span"
                sx={{
                  padding: " 4px 10px",
                  marginRight: "5px",
                }}
                data-testid="component-label-name"
              >
                {readableName}
              </Typography>
            </StaticLabel>
          </ComponentLabelContainer>
        )}
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "repeat(7, 1fr)",
            gridTemplateRows: "auto auto",
          }}
        >
          <ToolbarContainer>
            <div
              ref={toolbarContainerRef}
              style={{
                zIndex: "auto",
                top: "0px",
                position: "relative",
                marginBottom: "0.5rem",
                display: isActiveComponent ? "block" : "none",
              }}
              data-testid="parent-wrapper-toolbar-container"
            />
          </ToolbarContainer>
          <Wrapper
            style={{
              gridColumn: "span 7",
              gridRow: 2,
            }}
            showSelf={isActiveComponent}
            hoverActive={isHoverActive}
            data-testid="component-wrapper"
          >
            <ErrorBoundary FallbackComponent={ErrorFallbackComponent}>
              <Component
                {...componentProps}
                setProp={updateAuthoringContext}
                componentID={id}
                setActiveComponent={setActiveComponent}
                isActiveComponent={isActiveComponent}
                setHoverMenu={setHoverMenu}
                toolbarContainerRef={toolbarContainerRef}
              />
            </ErrorBoundary>
          </Wrapper>
        </div>
      </ListItem>
    </ErrorBoundary>
  );
};

MockComponentWrapper.propTypes = {
  id: PropTypes.string,
  componentName: PropTypes.string.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  componentProps: PropTypes.any.isRequired,
};

MockComponentWrapper.defaultProps = {
  id: null,
};

export default MockComponentWrapper;
export const ComponentWrapperMemo = React.memo(MockComponentWrapper);
