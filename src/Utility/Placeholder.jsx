import React from "react";
import styled from "@emotion/styled";
import { useTranslation } from "react-i18next";

const PlaceholderContainer = styled("div")(({ showError }) => ({
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  height: "auto",
  border: showError ? "3px dashed #D32F2F" : "3px dashed #1565c0",
  padding: "1.375rem 1.25rem",
}));

const Title = styled("h3")(({ showError }) => ({
  color: showError ? "#D32F2F" : "#636363",
  fontWeight: "400",
  fontSize: "1.5rem",
  lineHeight: "1.5rem",
  margin: "0",
  alignItems: "center",
  textAlign: "center",
}));

const Paragraph = styled("p")(({ placeholderType }) => ({
  fontSize: "0.875rem",
  lineHeight: "1.374rem",
  margin: "0.625rem 0 0 0",
  fontWeight: "500",
  alignItems: "center",
  textAlign: "center",
}));

const SubParagraph = styled(Paragraph)(() => ({
  marginTop: "0",
  lineHeight: "1.374rem",
  color: "#1565c0",
  alignItems: "center",
  textAlign: "center",
}));

const Placeholder = ({ isOver, showError, placeholderType, components }) => {
  const { t } = useTranslation();
  const acceptedComponent = () => {
    switch (placeholderType) {
      case "infobox":
        return `${t("text, header, image and video")}`;
      case "multiColumn":
        return `${t("text and image")}`;
      default:
        return `${t("header, text, image, video, and table")}`;
    }
  };
  let backgroundColorMultipleColumn = "";

  if (placeholderType === "multiColumn") {
    if (showError && components.length === 0) {
      backgroundColorMultipleColumn = "rgba(211, 47, 47, 0.04)";
    } else if (isOver && components.length === 0) {
      backgroundColorMultipleColumn = "rgba(21, 101, 192, 0.04)";
    } else {
      backgroundColorMultipleColumn = "#ffffff";
    }
  }
  return (
    <PlaceholderContainer
      className={placeholderType === "multiColumn" && "placeholderContainer"}
      style={{ backgroundColor: backgroundColorMultipleColumn }}
      isOver={isOver}
      showError={showError}
      placeholderType={placeholderType}
    >
      <Title showError={showError}>
        {showError
          ? t("Error: not compatible", { component: showError })
          : t("Add a component here")}
      </Title>
      <Paragraph
        placeholderType={placeholderType}
        className={placeholderType === "multiColumn" && "placeHolder"}
      >
        {t(
          "Drag and drop a component from the left panel or use your keyboard to insert a component"
        )}
      </Paragraph>
      <SubParagraph
        className={placeholderType === "multiColumn" && "subPlaceHolder"}
      >
        {`${t("Accepted components")}: ${acceptedComponent()}.`}
      </SubParagraph>
    </PlaceholderContainer>
  );
};
export default Placeholder;
