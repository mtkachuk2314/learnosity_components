import React, { useContext, useRef, useState, useEffect } from "react";
import { useDrag, useDrop } from "react-dnd";
import { getEmptyImage } from "react-dnd-html5-backend";
import { useTranslation } from "react-i18next";

import styled from "@emotion/styled";
import { IconButton, Typography } from "@mui/material";

import TenantContext from "../Context/TenantContext";
import { LayoutContext as InteractivesContext } from "../Context/InteractivesContext";
import { LayoutContext as TabContext } from "../components/Tabs/TabContext";
import { MultiColumnContext } from "../components/MultiColumn/MultiColumnContext";
import { InfoBoxContext } from "../components/InfoBox/InfoBoxContext";

import DropIndicator from "./DropIndicator";
import { useOnClickOutside } from "../hooks/useOnClickOutside";
import componentIndex from "../components/componentIndex";
import ComponentSideMenu from "./ComponentSideMenu/ComponentSideMenu";

export const SmallIconButton = styled(IconButton)(
  ({ draggingOver, draggingSelf, showSelf, hoverActive, viewOnly }) => ({
    color: draggingOver ? "transparent" : "#FFF",
    display:
      showSelf && !draggingSelf
        ? viewOnly && `none`
        : hoverActive && !draggingSelf && `none`,
  })
);

const BlueBox = styled("div")(
  ({ draggingSelf, showSelf, hoverActive, viewOnly }) => ({
    outline: viewOnly
      ? hoverActive
        ? `3px solid #DAE3EE`
        : null
      : hoverActive && !draggingSelf && !showSelf
      ? `3px solid #DAE3EE`
      : showSelf
      ? `3px solid #1466C0`
      : null,
    borderRadius: "0rem 0.25rem 0.25rem 0.25rem",
    opacity: draggingSelf ? 0.4 : 1,
    '& [data-id="callout"]': {
      margin: 0,
    },
  })
);

const StaticLabel = styled("span")(() => ({
  background: "#DAE3EE",
  width: "fit-content",
  borderRadius: "4px 4px 0px 0px",
  height: "1.875rem",
  display: "flex",
  alignItems: "center",
  flexDirection: "row",
  opacity: 1,
  color: "#1466C0",
}));

export const ComponentLabelContainer = styled("div")(() => {
  const style = {
    display: "flex",
    width: "fit-content",
    marginLeft: "-0.25rem",
    padding: "0 1px",
    opacity: 1,
    alignItems: "center",
    zIndex: "auto",
    top: "1.125rem",
    position: "relative",
    height: "auto",
  };

  return style;
});

const NestedComponentWrapper = ({
  componentType,
  // TODO: Why passing component and subsets of component data?
  component,
  compIndex,
  componentProps,
  // Formats of components, eg. Print/Digital; contains preferences for specific component
  formats,
  tabIndex,
  numOfComponent,
  droppedIndex,
  setDroppedIndex,
  setActiveComp,
  activeComp,
  setShowError,
  setShowDropError,
  multiColumn,
  setHoverMenu = () => {},
  setActiveComponent = () => {},
}) => {
  const dropRef = useRef(null);
  const nestedCompRef = useRef();

  let context;
  if (componentType === "tabs") {
    context = TabContext;
  } else if (componentType === "multi-column") {
    context = MultiColumnContext;
  } else if (componentType === "infobox") {
    context = InfoBoxContext;
  } else {
    context = InteractivesContext;
  }

  const [state, dispatch] = useContext(context);

  const [showSelf, setShowSelf] = useState(false);
  const [isHover, setIsHover] = useState(false);
  const [dropIndexOffset, setDropIndexOffset] = useState(null);
  const [direction, setDirection] = useState(null);
  const componentDetails = componentIndex[component.componentName];

  // Observer Mode
  const authoringData = useContext(TenantContext);
  const viewOnly = !authoringData?.viewOnlyComponents;

  const { Component } = componentDetails;
  const labelRef = useRef(null);

  // remove active border and label if you click outside component
  useOnClickOutside(dropRef, () => {
    setShowSelf(false);
    setShowError(false);
    setShowDropError(false);
    setIsHover(false);
  });

  // use translation to localize component name
  const { t } = useTranslation();

  // List of accepted into tab componenets
  const acceptListComp = (item) => {
    let acceptedComp = [];
    if (multiColumn) {
      acceptedComp = ["Text", "Image"];
    } else if (componentType === "infobox") {
      acceptedComp = ["Text", "Video", "Image", "Header"];
    } else {
      acceptedComp = ["Text", "Table", "Video", "Image", "Header"];
    }
    return acceptedComp.indexOf(item?.componentName) >= 0;
  };

  const [{ isOver, getItem }, drop] = useDrop({
    accept: [
      "Text",
      "Image",
      "Video",
      "Table",
      "InfoBox",
      "Header",
      "QuoteBox",
      "IFrame",
      "Accordion",
      "Tab",
      "section",
      "MultiColumn",
      "Reveal",
      "MultipleChoice",
    ],
    collect: (monitor) => ({
      isOver: !!monitor.isOver(),
      canDrop: !!monitor.canDrop(),
      isOverCurrent: monitor.isOver({ shallow: true }),
      droppedInContainer: monitor.didDrop(),
      getItem: monitor.getItem(),
    }),
    drop: async (item, monitor) => {
      const currentTab = item?.tabIndex === tabIndex;
      let hoverIndex;
      if (currentTab || componentType === "infobox") {
        if (dropIndexOffset === 0) {
          hoverIndex = item.compIndex < compIndex ? compIndex : compIndex + 1;
        } else {
          hoverIndex = item.compIndex < compIndex ? compIndex - 1 : compIndex;
        }
      } else {
        hoverIndex = dropIndexOffset === 0 ? compIndex + 1 : compIndex;
      }

      if (componentType === "infobox") {
        dropIndexOffset === 0 ? setDirection("down") : setDirection("up");
      }
      const dragIndex =
        currentTab || componentType === "infobox" ? item?.compIndex : undefined;
      if (acceptListComp(item)) {
        setDroppedIndex(hoverIndex);
        setDropIndexOffset(null);
        if (componentType === "infobox") {
          dispatch({
            func:
              item.compIndex !== undefined
                ? "DRAG_COMPONENT"
                : "DRAG_ADD_NEW_COMPONENT",
            ...(item.compIndex !== undefined && {
              dragIndex,
            }),
            hoverIndex,
            direction,
            wrapperId: item.wrapperId,
            component: {
              componentName: item.componentName,
              componentProps: JSON.parse(item?.componentProps),
              formats,
            },
          });
          if (item.within === undefined && typeof item?.delete == "function") {
            item.delete();
          }
        } else {
          dispatch({
            func:
              item.compIndex !== undefined && currentTab
                ? "DRAG_COMPONENT"
                : "DRAG_ADD_NEW_COMPONENT",
            tabIndex,
            ...((item.compIndex !== undefined || !currentTab) && {
              dragIndex,
            }),
            hoverIndex,
            ...((item.compIndex === undefined || !currentTab) && {
              component: {
                componentName: item.componentName,
                componentProps: JSON.parse(item?.componentProps),
                formats,
              },
            }),
          });
          !currentTab && item?.delete && item?.delete();
        }
      }
    },
    canDrop: (item) => {
      if (
        item.compIndex === compIndex &&
        item?.within &&
        componentType !== "infobox" &&
        item.tabIndex === tabIndex
      ) {
        return false;
      }
      return true;
    },

    hover: (item, monitor) => {
      // Only show highlights if it's a droppable region (not the item itself, not one of the regions that means the index won't change to prevent meaningless mutations)
      // Removed because monitor.canDrop only works with old i.e inital value setted it doesnt works on new updated value
      // if (!monitor.canDrop()) return;
      // Determine rectangle on screen
      const hoverBoundingRect = dropRef.current?.getBoundingClientRect();
      // Get vertical middle
      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
      // Determine mouse position
      const clientOffset = monitor.getClientOffset();
      // Get pixels to the top
      const hoverClientY = clientOffset.y - hoverBoundingRect.top;
      // Only perform the move when the mouse has crossed half of the items height
      // When dragging downwards, only move when the cursor is below 50%
      // When dragging upwards, only move when the cursor is above 50%
      // Dragging downwards
      if (hoverClientY > hoverMiddleY) {
        setDropIndexOffset(0);

        return;
      }
      // Dragging upwards
      if (hoverClientY < hoverMiddleY) {
        setDropIndexOffset(1);

        return;
      }

      setDropIndexOffset(null);
    },
  });

  const [{ isDragging }, drag, dragPreview] = useDrag({
    type: component.componentName,
    item: () => ({
      componentName: component.componentName,
      componentProps: JSON.stringify(componentProps),
      formats,
      compIndex,
      tabIndex,
      delete: () => {
        dispatch({
          func: "DELETE_COMPONENT",
          tabIndex,
          compIndex,
          add: true,
        });
      },
      within: true,
      new: true,
      source: "component",
      wrapperId: state.id,
      type: component.componentName,
    }),
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
      didDrop: monitor.didDrop(),
      droppedItem: monitor.getItem(),
    }),
  });
  // remove html5 default drag image
  useEffect(() => {
    if (typeof getEmptyImage !== "function") return;
    dragPreview(getEmptyImage(), { captureDraggingState: true });
  }, [showSelf, isHover, dragPreview]);

  if (typeof getEmptyImage == "function") getEmptyImage(drop(dropRef));

  useEffect(() => {
    if (droppedIndex === compIndex) {
      setDroppedIndex(null);
    }
  }, [droppedIndex, setDroppedIndex]);

  useEffect(() => {
    if (activeComp !== null) {
      if (activeComp === compIndex) {
        setShowSelf(true);
        setActiveComp(null);
      } else {
        setShowSelf(false);
      }
    }
  }, [activeComp]);

  const onDelete = () => {
    dispatch({
      func: "DELETE_COMPONENT",
      compIndex,
      tabIndex,
    });
  };

  const onDuplicate = () => {
    dispatch({
      func: "DUPLICATE_COMPONENT",
      compIndex,
      tabIndex,
    });
  };
  const onMoveUp = () => {
    dispatch({
      func: "MOVE_COMPONENT_UP",
      compIndex,
      tabIndex,
    });
    setActiveComp(compIndex - 1);
  };

  const onMoveDown = () => {
    dispatch({
      func: "MOVE_COMPONENT_DOWN",
      compIndex,
      tabIndex,
    });
    setActiveComp(compIndex + 1);
  };

  return (
    <div
      data-testid="div-before-drop-indicator"
      data-nested="true" // used by authoring to check if click in nested component
      key={`nested-component-${compIndex}`}
      ref={dropRef}
      onMouseEnter={() => {
        setIsHover(true);
        setHoverMenu(false);
      }}
      onMouseLeave={() => {
        setIsHover(false);
        setHoverMenu(true);
      }}
      onFocus={() => {
        setShowSelf(true);
        setIsHover(true);
        setHoverMenu(false);
        setActiveComponent(false);
      }}
      onClick={() => {
        setShowSelf(true);
        setActiveComponent(false);
      }}
      style={{
        position: "relative",
        marginTop: "0px",
        marginBottom: "3px",
      }}
      onBlur={(e) => {
        if (e.relatedTarget === null) return;
        if (!dropRef.current.contains(e.relatedTarget)) {
          setShowSelf(false);
          setIsHover(false);
        }
      }}
    >
      <div
        style={{
          display: "grid",
          alignItems: "start",
          position: "static",
          gridTemplateColumns: "repeat(7, 1fr)",
          gridTemplateAreas: `
    'toolbar . . . . . .'
    'compBody compBody compBody compBody compBody compBody compBody'
  `,
        }}
      >
        <div
          role="toolbar"
          aria-orientation="vertical"
          aria-label="component controls"
          tabIndex={0}
          data-testid="component-activate"
          ref={labelRef}
          onFocus={() => {
            setActiveComp(true);
          }}
          onBlur={(e) => {
            if (!dropRef.current?.contains(e.relatedTarget))
              setActiveComp(false);
          }}
        >
          {!viewOnly && isHover && (
            <ComponentSideMenu
              tabIndex={0}
              data-testid="component-sidemenu-container"
              onDelete={onDelete}
              onDuplicate={onDuplicate}
              onMoveUp={onMoveUp}
              onMoveDown={onMoveDown}
              canMoveDown={compIndex !== numOfComponent - 1}
              canMoveUp={compIndex !== 0}
              dragRef={drag}
              isDragging={isDragging}
              type={component?.componentName}
            />
          )}
        </div>
        <div
          style={{
            gridArea: "toolbar",
            marginTop: "-3rem",
            position: isHover && !showSelf ? "relative" : "sticky",
            // top >= height of header in authoring side wich is 64
            top: isHover && !showSelf ? "0rem" : "4rem",
            alignSelf: "start",
            height: "auto",
            zIndex: isHover ? "999" : "1000",
          }}
        >
          {viewOnly && isHover && (
            <ComponentLabelContainer data-testid="component-component-label-container">
              <StaticLabel data-testid="static-label">
                <Typography
                  variant="body2"
                  component="span"
                  sx={{
                    padding: "0.1875rem 0.625rem",
                    marginRight: "0.3125rem",
                  }}
                  data-testid="component-label-name"
                >
                  {t(component.componentName)}
                </Typography>
              </StaticLabel>
            </ComponentLabelContainer>
          )}
          {!viewOnly && isHover && !showSelf && (
            <ComponentLabelContainer data-testid="component-component-label-container">
              <StaticLabel data-testid="static-label">
                <Typography
                  variant="body2"
                  component="span"
                  sx={{
                    padding: "0.1875rem 0.625rem",
                    marginRight: "0.3125rem",
                  }}
                  data-testid="component-label-name"
                >
                  {t(component.componentName)}
                </Typography>
              </StaticLabel>
            </ComponentLabelContainer>
          )}
          {!viewOnly && (
            <div
              ref={nestedCompRef}
              style={{
                zIndex: "auto",
                bottom: "0rem",
                position: "relative",
                marginBottom: "0.5rem",
                display: showSelf ? "block" : "none",
                left: "-0.1875rem",
              }}
              data-testid="parent-wrapper-toolbar-container"
            />
          )}
        </div>
        <div
          style={{
            gridArea: "compBody",
          }}
        >
          <BlueBox
            showSelf={showSelf}
            hoverActive={isHover}
            draggingSelf={isDragging}
            viewOnly={viewOnly}
          >
            <Component
              {...componentProps}
              showSelf={showSelf}
              role="listitem"
              toolbarContainerRef={nestedCompRef}
              componentType={componentType}
              parentComponent={componentType}
              setProp={(stateUpdate) => {
                dispatch({
                  func: "UPDATE_COMPONENT",
                  compIndex,
                  tabIndex,
                  stateUpdate,
                });
              }}
            />
          </BlueBox>
        </div>
      </div>
      <DropIndicator
        data-testid="drop-indicator"
        offsetLine={dropIndexOffset - 1}
        showLine={dropIndexOffset === 1 && isOver}
        item={getItem}
        offsetDown={0}
        offsetUp={-5.8}
        multiColumn={multiColumn}
        infobox={componentType === "infobox"}
      />
      {compIndex + 1 === numOfComponent && (
        <DropIndicator
          data-testid="drop-indicator"
          offsetLine={dropIndexOffset - 1}
          showLine={dropIndexOffset === 0 && isOver}
          item={getItem}
          offsetDown={0}
          offsetUp={compIndex !== numOfComponent - 1 ? 15 : 5}
          multiColumn={multiColumn}
          infobox={componentType === "infobox"}
        />
      )}
    </div>
  );
};

// TODO: PropTypes

export default NestedComponentWrapper;
