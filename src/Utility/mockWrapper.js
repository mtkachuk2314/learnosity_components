import React, {
  useState,
  useContext,
  createContext,
  useEffect,
  useRef,
  useCallback,
  useMemo,
} from "react";
import { ErrorBoundary } from "react-error-boundary";
import componentIndex from "../components/componentIndex";

import TenantContext from "../Context/TenantContext";

// mockWrapper.js is code to mock the Lesson-builder environment.

// Mocking a shared context living within CraftJS
const WidgetContext = createContext();

// For testing, mocking an initial canvas state from the DB (add all props for a component even if empty)
const mockedSavedCanvas = [
  { name: "Callout", heading: "saved heading", body: "", calloutType: "" },
];

export const WidgetContextProvider = ({ children }) => {
  // Mocking what the AuthApp's canvas will do to create the initial state
  const initialState = {
    // Setting selection to null, will be ID of element, just a mock of what AuthApp does
    selectedComponentId: null,
  };
  mockedSavedCanvas.forEach((component, index) => {
    // Creating entry in state with initial component props
    Object.assign(initialState, { [`id-${index}`]: { ...component } });
    if (!initialState.ROOT) initialState.ROOT = [];
    // Setting order of components in ROOT
    initialState.ROOT.push(`id-${index}`);
  });

  // TODO: Alter setter, add selectedUUID for testing on this page, or add to config panel wrapper?
  const [canvasState, setCanvasState] = useState(initialState);

  const [tenantData, setTenantData] = useState(null);

  useEffect(() => {
    // TODO: You could fetch a token and make an authorized call to the tenant table if you'd like here: (that's why it's fake async)
    // const defaultTVOEndPointSBX = 'https://api-sbx.tvopublishing.com/tenants/tvo'
    const tenantDataFakeFetch = {
      brightcoveAccountId: "23648095001",
      grades: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
      tenantId: "76876a56",
      courseMediaBucket: "s3-coursemedia-76876a56-sbx01-cac1-01",
      subjects: [
        "American Sign Language as a Second Language",
        "Arts",
        "Business Studies",
        "Canadian and World Studies",
        "English",
        "First Nations, Métis & Inuit Studies",
        "French",
        "French as a Second Language",
        "Guidance and Career Education",
        "Health and Physical Education",
        "Mathematics",
        "Science",
        "Social Sciences and Humanities",
        "Technological Education",
      ],
      fullName: "TV Ontario",
      clientId: "63f8bib1vv74porqvub754832h",
      language: "en",
      tenantName: "tvo",
      brightcovePolicyKey:
        "BCpkADawqM0aUZtfCvrLXpdoRcRxqMw2xSWMkTuGLEdbP4URyJRFB643tzqk1xFEmnnA8aSBdxgvSZ4FFM2W-r3kGe8feBtXCfuM1w3SpjmEaI8_VwxsnjcFCfU",
      courseMediaCFrontUrl: "https://d3lultcesgitj9.cloudfront.net",
      userpoolId: "ca-central-1_RY8TV7pK3",
      brightcovePlayer: "dykQDka9h",
      mediaRootURL: "https://d3lultcesgitj9.cloudfront.net",
      viewOnlyComponents: true,
    };

    setTenantData(tenantDataFakeFetch);
  }, []);

  const value = useMemo(() => [canvasState, setCanvasState], [canvasState]);

  if (!tenantData) return null;

  return (
    <TenantContext.Provider value={tenantData}>
      <WidgetContext.Provider value={value}>{children}</WidgetContext.Provider>
    </TenantContext.Provider>
  );
};

// error boundary is added to each component
const OurFallbackComponent = ({ error, resetErrorBoundary }) => {
  return (
    <div>
      <h1>An error occurred: {error.message}</h1>
      <button type="button" onClick={resetErrorBoundary}>
        Try again
      </button>
    </div>
  );
};

export const ComponentStateWrapper = ({ id, name, ...componentState }) => {
  const [state, setState] = useContext(WidgetContext);
  const [isActiveComponent, setIsActiveComponent] = useState(false);
  const toolbarContainerRef = useRef();
  const dropRef = useRef();

  const setActiveComponentListener = useCallback(
    (event) => {
      if (!dropRef.current) return;
      if (dropRef.current.contains(event.target)) {
        setIsActiveComponent(true);
        return;
      }
      setIsActiveComponent(false);
    },
    [dropRef]
  );

  useEffect(() => {
    if (!dropRef.current) return () => {};

    if (isActiveComponent) {
      window.addEventListener("mousedown", setActiveComponentListener);
    } else {
      window.removeEventListener("mousedown", setActiveComponentListener);
    }

    return () => {
      window.removeEventListener("mousedown", setActiveComponentListener);
    };
  }, [isActiveComponent, dropRef, setActiveComponentListener]);

  const handleChange = (newState) => {
    console.log(`Updating state for ${id} ->`, state, newState);
    setState((prevState) => ({
      ...prevState,
      [id]: { ...prevState[id], ...newState },
    }));
  };

  const Component = componentIndex[name]?.Component;
  if (!Component) {
    console.warn(`Component name "${name}" not present in componentIndex`);
    return null;
  }

  return (
    <ErrorBoundary FallbackComponent={OurFallbackComponent}>
      {/* eslint-disable-next-line jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events */}
      <div
        style={{ position: "relative" }}
        ref={dropRef}
        onClick={() => {
          if (id) setIsActiveComponent(true);
        }}
        onBlur={(e) => {
          if (e.relatedTarget === null) return;
          if (!dropRef.current.contains(e.relatedTarget)) {
            setIsActiveComponent(false);
          }
        }}
      >
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "repeat(7, 1fr)",
            gridTemplateRows: "auto auto",
          }}
        >
          <div
            style={{
              gridColumn: "1 / span 1",
              gridRow: 1,
              marginTop: "-3rem",
              position: "sticky",
              top: `65px`,
              alignSelf: "start",
              zIndex: 1000 - 100,
              height: "auto",
            }}
          >
            <div
              ref={toolbarContainerRef}
              style={{
                zIndex: "auto",
                top: "0px",
                position: "relative",
                marginBottom: "0.5rem",
                display: "block",
              }}
              data-testid="parent-wrapper-toolbar-container"
            />
          </div>
          <div style={{ gridColumn: "span 7", gridRow: 2 }}>
            <Component
              setProp={handleChange}
              toolbarContainerRef={toolbarContainerRef}
              setActiveComponent={(s) => setIsActiveComponent(s)}
              isActiveComponent={isActiveComponent}
              setHoverMenu={() => console.log("setHoverMenu")}
              {...componentState}
            />
          </div>
        </div>
      </div>
    </ErrorBoundary>
  );
};

export const ConfigStateWrapper = () => {
  /*
  Wrapper mocking the wrapper in Authoring Application that passes in a setter and state from context,
  the wrapper handles which component is "selected" and passes in a setter that only affects one component
  TODO: If Components have to be able to "select" themselves for configuration, a second setter that sets the selected component could be passed in.  Current understanding is that "selected" state would be managed by parent application
  */
  const [state, setState] = useContext(WidgetContext);

  if (!state.selectedComponentId) return null;
  console.log("Selected component", state.selectedComponentId);

  const selectedComponentState = state[state.selectedComponentId];

  const ComponentConfigPanel =
    componentIndex[selectedComponentState.name].ConfigPanel;
  if (!ComponentConfigPanel) {
    console.log(
      `Selected component "${selectedComponentState.name}" has no config panel`
    );
    return null;
  }

  const setComponentState = (stateUpdate) => {
    console.log("Updating state", stateUpdate);
    setState((prevState) => ({
      ...prevState,
      [prevState.selectedComponentId]: {
        ...prevState[prevState.selectedComponentId],
        ...stateUpdate,
      },
    }));
  };

  return (
    <ComponentConfigPanel
      componentState={selectedComponentState}
      setState={setComponentState}
    />
  );
};

export const ComponentSelector = () => {
  /*
  Mock for testing to allow selected state of component changing, populating the config panel with that component
  */
  const [state, setState] = useContext(WidgetContext);

  return (
    <div>
      <label htmlFor="component-selector">Select a component:</label>
      <select
        name="component selector"
        id="component-selector"
        onChange={(e) =>
          setState((state) => ({
            ...state,
            selectedComponentId: e.target.value,
          }))
        }
      >
        {state.ROOT.map((componentId, index) => (
          <option key={index} value={componentId}>
            {state[componentId].name}
          </option>
        ))}
      </select>
    </div>
  );
};

export const Canvas = ({ unwrappedComponents = null }) => {
  // Pass any components to this that you'd like rendered for now but without state management through "unwrappedComponents"

  const [state, setState] = useContext(WidgetContext);

  if (!state.ROOT || !state.ROOT.length > 0)
    throw new Error(`Canvas created with no components present in ROOT`);

  const addComponent = (componentName) => () => {
    console.log(`Adding component to mock canvas: ${componentName}`);
    setState((state) => {
      const newComponentId = `id-${state.ROOT.length}`;
      const ROOT = [...state.ROOT];
      ROOT.push(newComponentId);
      state[newComponentId] = {
        name: componentName,
        ...componentIndex[componentName].defaultProps,
      };
      return { ...state, ROOT };
    });
  };

  return (
    <div
      className="canvas"
      style={{ border: "2px solid black", minWidth: "650px" }}
    >
      {state.ROOT.map((componentId) => {
        const { name, ...props } = state[componentId];
        return (
          <ComponentStateWrapper
            key={componentId}
            id={componentId}
            name={name}
            {...props}
          />
        );
      })}
      {unwrappedComponents}
      {Object.keys(componentIndex).map((componentName, index) => {
        return (
          <button
            type="button"
            key={index}
            onClick={addComponent(componentName)}
          >
            Add {componentIndex[componentName].readableName}
          </button>
        );
      })}
    </div>
  );
};
