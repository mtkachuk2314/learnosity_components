import React from "react";

import styled from "@emotion/styled";

const triangleSize = 9;

const DropIndicator = styled("div")(
  ({
    offsetLine,
    showLine,
    centerLine,
    item,
    offsetDown = 4,
    offsetUp = -5.8,
    multiColumn,
    infobox,
  }) => {
    let dashColor = "#1565C0";

    let acceptedComp;
    if (multiColumn) {
      acceptedComp = ["Text", "Image"];
    } else if (infobox) {
      acceptedComp = ["Text", "Image", "Video", "Header"];
    } else {
      acceptedComp = ["Text", "Table", "Video", "Image", "Header"];
    }
    if (acceptedComp.indexOf(item?.componentName) === -1) {
      dashColor = "#D32F2F";
    }

    const style = {
      position: "absolute",
      display: showLine ? "block" : "none",
      left: "2px",
      right: "2px",
      // Caution: Magic numbers
      top: offsetLine === 0 ? `${offsetUp}px` : null,
      bottom: offsetLine === 1 ? `${offsetDown}px` : null,
      overflow: "visible",
      borderTop: `3px dashed ${dashColor}`,
      transition: "top 1s linear, bottom 1s linear, position 1s linear",
      zIndex: "1000",
      "&::before": {
        content: "''",
        position: "absolute",
        borderStyle: "solid",
        borderRight: `${triangleSize}px solid transparent`,
        borderTop: `${triangleSize / 2}px solid transparent`,
        borderBottom: `${triangleSize / 2}px solid transparent`,
        borderLeft: `${triangleSize}px solid ${dashColor}`,
        display: "block",
        width: "10px",
        height: "10px",
        left: "-12.5px",
        top: "-7px",
        zIndex: "1100",
        marginRight: `${triangleSize}px`,
        marginLeft: `${triangleSize}px`,
      },
      "&::after": {
        content: "''",
        position: "absolute",
        borderStyle: "solid",
        borderLeft: `${triangleSize}px solid transparent`,
        borderTop: `${triangleSize / 2}px solid transparent`,
        borderBottom: `${triangleSize / 2}px solid transparent`,
        borderRight: `${triangleSize}px solid ${dashColor}`,
        display: "block",
        width: "10px",
        height: "10px",
        right: "-12.5px",
        top: "-7px",
        zIndex: "1000",
        marginRight: `${triangleSize}px`,
        marginLeft: `${triangleSize}px`,
      },
    };

    if (centerLine === true) {
      delete style.bottom;
      style.top = "8px";
    }

    return style;
  }
);

// TODO: Add logic to other indicator as prop
export const CenterDropIndicator = () => (
  <hr
    style={{
      height: "0px",
      borderTop: "2px dashed blue",
      position: "absolute",
      top: "-10px",
      left: 0,
      right: 0,
    }}
  />
);

export default DropIndicator;
