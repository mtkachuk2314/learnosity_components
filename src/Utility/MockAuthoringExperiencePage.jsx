/* eslint-disable no-param-reassign */
import React, { useContext, useState, useEffect, useCallback } from "react";

import componentIndex from "../components/componentIndex";
import MockComponentWrapper from "./MockComponentWrapper";
import MockAuthoringContextProvider, {
  MockAuthoringContext,
} from "./MockAuthoringContext";

const ComponentAddList = () => {
  const { dispatch } = useContext(MockAuthoringContext);

  const components = Object.keys(componentIndex);

  const addComponent = (componentName) => () => {
    dispatch({ type: "ADD_COMPONENT", componentName });
  };

  return (
    <div style={{ display: "flex", flexDirection: "row" }}>
      {components.map((componentName) => (
        <button
          type="button"
          key={componentName}
          onClick={addComponent(componentName)}
        >
          Add {componentIndex[componentName].readableName}
        </button>
      ))}
    </div>
  );
};

const ComponentList = () => {
  const { state } = useContext(MockAuthoringContext);

  return (
    <div>
      {state.components.map((component) => (
        <MockComponentWrapper
          key={component.id}
          id={component.id}
          componentName={component.componentName}
          componentProps={component.props}
        />
      ))}
    </div>
  );
};

const PropKey = ({ keyName, type, value, childKeys, defaultProp }) => {
  console.log({ value, childKeys });
  return (
    <div>
      <div style={{ marginLeft: "1rem" }}>
        <div
          style={{
            color: typeof value === typeof defaultProp ? "green" : "red",
          }}
        >
          {keyName}: {type}
        </div>
        <div style={{ marginLeft: "1rem" }}>
          {childKeys.map((childKey) => (
            <PropKey
              key={childKey}
              keyName={childKey}
              type={
                Array.isArray(value[childKey])
                  ? "array"
                  : typeof value[childKey]
              }
              value={value[childKey]}
              childKeys={
                value[childKey] &&
                typeof value[childKey] === "object" &&
                !Array.isArray(value[childKey])
                  ? Object.keys(value[childKey])
                  : []
              }
              defaultProp={defaultProp?.[childKey]}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

const ComponentDefaultPropsSection = ({ defaultProps, props }) => {
  return (
    <div style={{ margin: "2rem" }}>
      <h3>Default Props</h3>
      {Object.keys(props).map((key) => (
        <PropKey
          key={key}
          keyName={key}
          type={Array.isArray(props[key]) ? "array" : typeof props[key]}
          value={props[key]}
          childKeys={
            props[key] &&
            typeof props[key] === "object" &&
            !Array.isArray(props[key])
              ? Object.keys(props[key])
              : []
          }
          defaultProp={defaultProps?.[key]}
        />
      ))}
    </div>
  );
};

const ComponentTestWrapper = () => {
  const { state, dispatch } = useContext(MockAuthoringContext);

  const component = state.components[0];

  const { defaultProps } = componentIndex[component.componentName];

  const [fieldState, setFieldState] = useState(
    JSON.stringify(component.props, undefined, 2)
  );

  const [isValidJSON, setIsValidJSON] = useState(true);

  useEffect(() => {
    setFieldState(JSON.stringify(component.props, undefined, 2));
  }, [component.props]);

  const handleChange = useCallback((e) => {
    const { value } = e.target;

    setFieldState(value);

    try {
      JSON.parse(value);
      setIsValidJSON(true);
    } catch {
      setIsValidJSON(false);
    }
  }, []);

  const handleSubmit = useCallback(
    (e) => {
      e.preventDefault();

      let updatedState;
      try {
        updatedState = JSON.parse(fieldState);
      } catch {
        return;
      }

      dispatch({
        type: "UPDATE_COMPONENT",
        id: component.id,
        updatedState,
      });
    },
    [component.id, dispatch, fieldState]
  );

  return (
    <div style={{ margin: "3rem" }}>
      <h1>{component.componentName} Playground</h1>
      <MockComponentWrapper
        key={component.id}
        id={component.id}
        componentName={component.componentName}
        componentProps={component.props}
      />
      <textarea
        style={{ marginTop: "1rem", minWidth: "100%" }}
        value={fieldState}
        onChange={handleChange}
        // set to  min of 10 rows
        rows={Math.max(fieldState.split("\n").length, 10)}
      />
      <button
        type="button"
        onClick={handleSubmit}
        disabled={!isValidJSON}
        style={{ marginTop: "1rem", display: "block" }}
      >
        Set Component Data
      </button>
      <div style={{ marginTop: "1rem" }}>
        <h2>Component Schema</h2>
      </div>
      {/* <ComponentDefaultPropsSection
        defaultProps={defaultProps}
        props={component.props}
      /> */}
    </div>
  );
};

// Wrapper that exposes the props and allows for custom objects to be passed in
export const SingleComponentTestPage = ({ componentName }) => {
  const initialState = {
    components: [
      {
        props: componentIndex[componentName].defaultProps,
        componentName,
        id: "1",
      },
    ],
  };

  return (
    <MockAuthoringContextProvider initState={initialState}>
      <ComponentTestWrapper />
    </MockAuthoringContextProvider>
  );
};

/*
Renders components in a list
*/
const SimpleAuthoringPage = () => {
  return (
    <div style={{ padding: "5rem" }}>
      <MockAuthoringContextProvider>
        <ComponentList />
        <hr />
        <ComponentAddList />
      </MockAuthoringContextProvider>
    </div>
  );
};

export default SimpleAuthoringPage;
