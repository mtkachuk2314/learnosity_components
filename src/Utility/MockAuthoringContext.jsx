/* eslint-disable no-param-reassign */
import React, { createContext, useReducer, useMemo, useEffect } from "react";

import produce, { enablePatches, produceWithPatches } from "immer";

import TenantContext from "../Context/TenantContext";
import componentIndex from "../components/componentIndex";

export const MockAuthoringContext = createContext();

const initialState = { components: [] };

const curriedReducer = (prevState, action) => {
  switch (action.type) {
    case "ADD_COMPONENT": {
      const { componentName } = action;
      const component = componentIndex[componentName];
      if (!component) return prevState;

      return produce(prevState, (draft) => {
        draft.components.push({
          id: `id-${Math.floor(Math.random() * 1000000)}`,
          componentName,
          props: component.defaultProps,
        });
      });
    }
    case "UPDATE_COMPONENT": {
      const { id, updatedState } = action;

      const index = prevState.components.findIndex(
        (component) => component.id === id
      );

      if (index === -1) return prevState;

      const [newState, patches] = produceWithPatches(prevState, (draft) => {
        draft.components[index].props = {
          ...draft.components[index].props,
          ...updatedState,
        };
      });

      console.log("Changes", patches);

      return newState;
    }
    default:
      throw new Error(`Invalid type passed: ${action.type}`);
  }
};

const tenantDataFakeFetch = {
  brightcoveAccountId: "23648095001",
  grades: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
  tenantId: "76876a56",
  courseMediaBucket: "s3-coursemedia-76876a56-sbx01-cac1-01",
  subjects: [
    "American Sign Language as a Second Language",
    "Arts",
    "Business Studies",
    "Canadian and World Studies",
    "English",
    "First Nations, Métis & Inuit Studies",
    "French",
    "French as a Second Language",
    "Guidance and Career Education",
    "Health and Physical Education",
    "Mathematics",
    "Science",
    "Social Sciences and Humanities",
    "Technological Education",
  ],
  fullName: "TV Ontario",
  clientId: "63f8bib1vv74porqvub754832h",
  language: "en",
  tenantName: "tvo",
  brightcovePolicyKey:
    "BCpkADawqM0aUZtfCvrLXpdoRcRxqMw2xSWMkTuGLEdbP4URyJRFB643tzqk1xFEmnnA8aSBdxgvSZ4FFM2W-r3kGe8feBtXCfuM1w3SpjmEaI8_VwxsnjcFCfU",
  courseMediaCFrontUrl: "https://d3lultcesgitj9.cloudfront.net",
  userpoolId: "ca-central-1_RY8TV7pK3",
  brightcovePlayer: "dykQDka9h",
  mediaRootURL: "https://d3lultcesgitj9.cloudfront.net",
  viewOnlyComponents: true,
  openAssetManager: () =>
    new Promise((resolve) => {
      setTimeout(
        () =>
          resolve({
            fileName: "2_3_juno_1.jpeg",
            fileEnding: "jpg",
          }),
        1000
      );
    }),
};

const MockAuthoringContextProvider = ({
  children,
  initState = initialState,
}) => {
  const [state, dispatch] = useReducer(curriedReducer, initState);

  const value = useMemo(() => ({ state, dispatch }), [state]);

  useEffect(() => {
    enablePatches();
  }, []);

  return (
    <MockAuthoringContext.Provider value={value}>
      <TenantContext.Provider value={tenantDataFakeFetch}>
        {children}
      </TenantContext.Provider>
    </MockAuthoringContext.Provider>
  );
};

export default MockAuthoringContextProvider;
