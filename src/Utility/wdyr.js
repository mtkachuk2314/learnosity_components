import React from 'react';

// why-did-you-render file, import it only in development mode
// import as first import in file you want to track
// updates on re-renders in console

if (process.env.NODE_ENV === 'development') {
  const whyDidYouRender = require('@welldone-software/why-did-you-render');
  whyDidYouRender(React, {
    trackAllPureComponents: true,
  });
}