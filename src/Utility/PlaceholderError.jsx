import React from "react";
import styled from "@emotion/styled";
import { useTranslation } from "react-i18next";

// error style message
import ErrorOutlineIcon from "@mui/icons-material/ErrorOutline";

const PlaceholderErrorContainer = styled("div")(
  ({ errorType, errorComponent }) => ({
    display: "flex",
    justifyContent: errorComponent !== "table" ? "flex-start" : "center",
    alignItems: errorComponent !== "table" ? "flex-start" : "center",
    borderRadius: "0.25rem",
    height: errorType === "infoboxError" ? "48px" : "auto",
    background:
      "linear-gradient(0deg, rgba(255, 255, 255, 0.9), rgba(255, 255, 255, 0.9)), #D32F2F",
    padding: "0.875rem",
  })
);

const Paragraph = styled("p")(({ errorComponent }) => ({
  display: "flex",
  alignItems: errorComponent === "table" ? "flex-start" : "center",
  color: "#d32f2f",
  lineHeight: "143%",
  fontFamily: '"Inter", sans-serif',
  fontSize: "0.875rem",
  margin: "0",
}));

const PlaceholderError = ({ errorType, errorComponent }) => {
  const { t } = useTranslation();
  let errorMessage = "";
  switch (errorType) {
    case "infoboxError":
      errorMessage = t(
        "Error: component not compatible. Only header, text, image and video are allowed."
      );
      break;
    case "mathImageUpload":
      errorMessage = "This file type is not supported";
      break;
    case "convertMathImage":
      errorMessage =
        "Error: Image couldn't be processed. Please upload and try again";
      break;
    case "minimumColumnsTableError":
      errorMessage = "Table requires at least 2 columns.";
      break;
    case "maximumColumnsTableError":
      errorMessage = "The number of columns cannot exceed 6.";
      break;
    case "minimumRowsTableError":
      errorMessage = "Table requires at least 2 rows.";
      break;
    case "maximumRowsTableError":
      errorMessage = "The number of rows cannot exceed 10.";
      break;
    case "invalidColumnsRowsError":
      errorMessage =
        "The input is not within range of 2-10 rows or 2-6 columns.";
      break;
    case "equationKeyboard":
      errorMessage =
        "Error: The equation is either incomplete or invalid. Please add a valid equation.";
      break;
    case "addComponent":
      errorMessage = t(
        "Error: component not compatible. Only header, text, image, video, and table are allowed."
      );
      break;
    default:
      break;
  }
  return (
    <PlaceholderErrorContainer
      errorComponent={errorComponent}
      errorType={errorType}
    >
      <Paragraph errorComponent={errorComponent}>
        <ErrorOutlineIcon sx={{ marginRight: "0.875rem" }} />
        {errorMessage}
      </Paragraph>
    </PlaceholderErrorContainer>
  );
};
export default PlaceholderError;
