import { useDebouncedCallback } from "use-debounce";

const DEBOUNCE_TIME = 500;
const DEBOUNCE_TIME_MAX = 1000;

const useDebounce = (updateFunc) => {
  const debounceFunction = useDebouncedCallback(
    updateFunc,
    DEBOUNCE_TIME, // Debounce wait time of 500 milliseconds
    {
      maxWait: DEBOUNCE_TIME_MAX, // Throttle maximum wait time of 3 seconds
    }
  );

  return debounceFunction;
};

export default useDebounce;
