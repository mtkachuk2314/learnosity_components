import React from "react";
import { render } from "@testing-library/react";

import TenantContext from "../Context/TenantContext"; // global TenantContext
import FormatContextProvider, {
  FORMATS,
} from "../authoring/context/AuthoringExperienceFormatContext";

const mockTenantContextValue = {
  viewOnlyComponents: true,
  brightcoveAccountId: "23648095001",
  grades: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
  tenantId: "76876a56",
  courseMediaBucket: "s3-coursemedia-76876a56-sbx01-cac1-01",
  subjects: [
    "American Sign Language as a Second Language",
    "Arts",
    "Business Studies",
    "Canadian and World Studies",
    "English",
    "First Nations, Métis & Inuit Studies",
    "French",
    "French as a Second Language",
    "Guidance and Career Education",
    "Health and Physical Education",
    "Mathematics",
    "Science",
    "Social Sciences and Humanities",
    "Technological Education",
  ],
  fullName: "TV Ontario",
  clientId: "63f8bib1vv74porqvub754832h",
  language: "en",
  tenantName: "tvo",
  brightcovePolicyKey:
    "BCpkADawqM0aUZtfCvrLXpdoRcRxqMw2xSWMkTuGLEdbP4URyJRFB643tzqk1xFEmnnA8aSBdxgvSZ4FFM2W-r3kGe8feBtXCfuM1w3SpjmEaI8_VwxsnjcFCfU",
  courseMediaCFrontUrl: "https://d3lultcesgitj9.cloudfront.net",
  userpoolId: "ca-central-1_RY8TV7pK3",
  brightcovePlayer: "dykQDka9h",
  mediaRootURL: "https://d3lultcesgitj9.cloudfront.net",
  openAssetManager: () => {},
};

// this provider is defined in authoring --> so we need to mock that for all components
const ComponentTenantProvider = ({
  children,
  selectedFormat = FORMATS.DIGITAL,
}) => (
  <TenantContext.Provider value={mockTenantContextValue}>
    <FormatContextProvider initSelectedFormat={selectedFormat}>
      {children}
    </FormatContextProvider>
  </TenantContext.Provider>
);

export default ComponentTenantProvider;

const renderWithWrapper = (ui, options = {}) => {
  return render(ui, {
    wrapper: (props) => (
      <ComponentTenantProvider {...props} {...options.wrapperProps} />
    ),
    ...options,
  });
};

// re-export everything
export * from "@testing-library/react";

// re-export renderWrapper as render
export { renderWithWrapper as render };
