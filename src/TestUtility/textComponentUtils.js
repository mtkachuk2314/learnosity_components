export function randomInt(max) {
  return Math.floor(Math.random() * max);
}

export function choose(choices) {
  return choices[randomInt(choices.length)];
}

export function runFuzz(testCase) {
  const start = performance.now();
  do {
    testCase();
  } while (performance.now() - start < 30 * 1000);
}
