import { createContext, useContext } from "react";

const TenantContext = createContext();

export const useTenantContext = () => useContext(TenantContext);

export default TenantContext;
