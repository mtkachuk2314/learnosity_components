import React, {
  createContext,
  useReducer,
  useEffect,
  useState,
  useMemo,
} from "react";
import produce from "immer";
import isEqual from "lodash/isEqual";

// state of tabs & accordion data stored in LayoutContext
export const LayoutContext = createContext();
export const ActivePaneContext = createContext();

export const layoutConfig = (draft, action) => {
  switch (action.func) {
    case "UPDATE_STATE":
      return action.data;
    case "ADD_LAYER":
      draft.splice(action.paneIndex + 1, 0, {
        id: action.id,
        title: "",
        components: [],
        expanded: action.expanded,
      });
      return draft;
    case "REMOVE_LAYER":
      draft.splice(action.paneIndex, 1);
      return draft;
    case "ADD_COMPONENT":
      draft[action.tabIndex].components.push({
        ...action.component,
      });
      return draft;
    case "MOVE_PANE_UP":
      // eslint-disable-next-line no-case-declarations
      if (action.paneIndex !== 0) {
        const elementL = draft[action.paneIndex];
        draft.splice(action.paneIndex, 1);
        draft.splice(action.paneIndex - 1, 0, elementL);
      } else {
        const elementL = draft[action.paneIndex];
        draft.splice(0, 1);
        draft.splice(draft.length, 0, elementL);
      }
      return draft;
    case "MOVE_PANE_DOWN":
      // eslint-disable-next-line no-case-declarations
      if (action.paneIndex !== draft.length - 1) {
        const elementR = draft[action.paneIndex];
        draft.splice(action.paneIndex, 1);
        draft.splice(action.paneIndex + 1, 0, elementR);
      } else {
        const elementR = draft[action.paneIndex];
        draft.pop();
        draft.unshift(elementR);
      }
      return draft;
    case "UPDATE_COMPONENT":
      draft[action.tabIndex].components[action.compIndex].componentProps = {
        ...action.stateUpdate,
      };
      return draft;
    case "DELETE_COMPONENT":
      draft[action.tabIndex].components.splice(action.compIndex, 1);
      return draft;
    case "MOVE_COMPONENT_DOWN":
      // eslint-disable-next-line no-case-declarations
      const elementCR = draft[action.tabIndex].components[action.compIndex];
      draft[action.tabIndex].components.splice(action.compIndex, 1);
      draft[action.tabIndex].components.splice(
        action.compIndex + 1,
        0,
        elementCR
      );
      return draft;
    case "MOVE_COMPONENT_UP":
      // eslint-disable-next-line no-case-declarations
      const elementCL = draft[action.tabIndex].components[action.compIndex];
      draft[action.tabIndex].components.splice(action.compIndex, 1);
      draft[action.tabIndex].components.splice(
        action.compIndex - 1,
        0,
        elementCL
      );
      return draft;
    case "DUPLICATE_COMPONENT":
      draft[action.tabIndex].components.splice(
        action.compIndex + 1,
        0,
        draft[action.tabIndex].components[action.compIndex]
      );
      return draft;
    case "DRAG_COMPONENT":
      // eslint-disable-next-line no-case-declarations
      const dragElement = draft[action.tabIndex].components[action.dragIndex];
      draft[action.tabIndex].components.splice(action.dragIndex, 1);
      draft[action.tabIndex].components.splice(
        action.hoverIndex,
        0,
        dragElement
      );
      return draft;
    case "DRAG_ADD_NEW_COMPONENT":
      draft[action.tabIndex].components.splice(
        action.hoverIndex,
        0,
        action.component
      );
      return draft;
    case "CHANGE_TITLE":
      draft[action.layerIndex].title = action.title;
      return draft;
    case "CHANGE_CLOSED_TITLE":
      draft[action.layerIndex].closedTitle = action.closedTitle;
      return draft;
    case "TOGGLE_PANE":
      draft[action.paneIndex].expanded = !draft[action.paneIndex].expanded;
      return draft;
    case "EXPAND_ALL_PANE":
      draft.forEach((item) => (item.expanded = true));
      return draft;
    case "COLLAPSE_ALL_PANE":
      draft.forEach((item) => (item.expanded = false));
      return draft;
    case "DELETE_PLACEHOLDER":
      draft.map((ele) => {
        return delete ele.placeholderTitle;
      });
      return draft;
    case "DELETE_REVEAL_PLACEHOLDERS":
      draft.map((ele) => {
        delete ele.activeTitle;
        delete ele.inActiveTitle;
        return draft;
      });
    default:
      return draft;
  }
};

export const paneConfig = (draft, action) => {
  switch (action.func) {
    case "TOGGLE_PANE":
      // Check if the specified pane index and the `expanded` property exist in the state array.
      if (draft && draft[action.paneIndex]) {
        // If the `expanded` property is currently `true`, set it to `false`, and vice versa.
        draft[action.paneIndex].expanded = !draft[action.paneIndex].expanded;
      }
      return draft;
    case "UPDATE_STATE":
      return action.state;
    default:
      return draft;
  }
};

// layout provider wraps the tab & accordion component to access reducer
export const LayoutProvider = ({ children, setProp, layoutState }) => {
  const [state, dispatch] = useReducer(produce(layoutConfig), layoutState);
  // const childEffect = children?._owner?.alternate?.firstEffect?.alternate?.elementType;

  const [activePane, setActivePane] = useReducer(
    produce(paneConfig),
    layoutState.map((item) => {
      return { expanded: item.expanded };
    })
  );

  const [mounted, setMounted] = useState(false);
  const diff = !isEqual(state, layoutState);

  useEffect(() => {
    if (!mounted) {
      dispatch({ func: "UPDATE_STATE", data: layoutState });
      state.forEach(
        (tab, index) => tab.activeTab === true && setActivePane(index)
      );
      setMounted(true);
    }
    diff && mounted && setProp({ layoutState: state });
  }, [state]);

  useEffect(() => {
    diff && mounted && dispatch({ func: "UPDATE_STATE", data: layoutState });
  }, [layoutState]);

  const layoutValue = useMemo(() => [state, dispatch], [state]);
  const activePaneValue = useMemo(
    () => [activePane, setActivePane],
    [activePane]
  );
  return (
    <LayoutContext.Provider value={layoutValue}>
      <ActivePaneContext.Provider value={activePaneValue}>
        {children}
      </ActivePaneContext.Provider>
    </LayoutContext.Provider>
  );
};
