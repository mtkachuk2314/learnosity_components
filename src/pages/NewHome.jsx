import React from "react";

import Header from "../components/Header";
import SimpleAuthoringPage from "../Utility/MockAuthoringExperiencePage";

const NewHome = () => {
  return (
    <>
      <Header title="component-library" backgroundColor="DarkSlateGray" />
      <div>
        <SimpleAuthoringPage />
      </div>
    </>
  );
};

export default NewHome;
