import React from "react";

import { screen, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";

import componentIndex, { categories } from "../components/componentIndex";
import { render } from "../TestUtility/renderUtils";

const config = {
  RENDER_TIME_SINGLE_COMPONENT: 50,
  NUMBER_MANY_COMPONENTS: 50,
  RENDER_TIME_MANY_COMPONENTS: 1000,
};

const components = Object.keys(componentIndex).map(
  (componentName) => componentIndex[componentName]
);

const ToolbarRefWrapper = ({ Component, ...props }) => {
  const ref = React.useRef(null);
  return (
    <>
      <div ref={ref}></div>
      <Component {...props} toolbarContainerRef={ref} />
    </>
  );
};

describe("Generic Component Tests", () => {
  components.forEach((component) => {
    const {
      Component,
      readableName,
      defaultProps,
      componentIcon,
      category,
      compatibleFormats,
    } = component;

    // Should have required properties in index
    test(`${
      readableName || "Unknown"
    }: should have an icon, readable name, compatible formats, and category as expected by consumer`, () => {
      expect(componentIcon).toBeDefined();
      expect(readableName).toBeDefined();
      expect(category).toBeDefined();
      expect(compatibleFormats.digital).toBeDefined();
      expect(typeof compatibleFormats.digital).toBe("boolean");
      expect(compatibleFormats.print).toBeDefined();
      expect(typeof compatibleFormats.print).toBe("boolean");
    });

    test(`${readableName}: should render component with default props`, () => {
      render(<Component {...defaultProps} />);
    });

    test(`${readableName}: renders in less than ${config.RENDER_TIME_SINGLE_COMPONENT}`, () => {
      const start = performance.now();
      render(<Component {...defaultProps} />);
      const end = performance.now();
      const time = end - start;
      // console.log("Render time for ", readableName, time);
      expect(time).toBeLessThan(config.RENDER_TIME_SINGLE_COMPONENT);
    });

    test(`${readableName} does not call setProp on load with default props`, async () => {
      const setProp = jest.fn();
      render(<Component {...defaultProps} setProp={setProp} />);

      // Wait 1 second
      await new Promise((r) => setTimeout(r, 1000));

      expect(setProp).not.toHaveBeenCalled();
    });

    test(`${readableName}: does not call setActiveComponent on load with default props`, async () => {
      const setActiveComponent = jest.fn();

      render(
        <Component {...defaultProps} setActiveComponent={setActiveComponent} />
      );

      await new Promise((r) => setTimeout(r, 100));

      expect(setActiveComponent).not.toHaveBeenCalled();
    });

    test(`${readableName}: does not call setActiveComponent or setProp when click outside component registered`, async () => {
      const setActiveComponent = jest.fn();
      const setProp = jest.fn();

      render(
        <div>
          <div data-testid="outside-component" style={{ margin: "10em" }} />
          <div style={{ margin: "10em" }} />
          <Component
            {...defaultProps}
            setActiveComponent={setActiveComponent}
            setProp={setProp}
          />
        </div>
      );

      fireEvent(
        screen.getByTestId("outside-component"),
        new MouseEvent("click", {
          bubbles: true,
          cancelable: true,
        })
      );

      await new Promise((r) => setTimeout(r, 100));

      expect(setProp).not.toHaveBeenCalled();
      expect(setActiveComponent).not.toHaveBeenCalled();
    });

    test(`${readableName}: renders when set to active and passed ref for potential toolbar`, async () => {
      const setActiveComponent = jest.fn();
      const setProp = jest.fn();

      render(
        <ToolbarRefWrapper
          {...defaultProps}
          setActiveComponent={setActiveComponent}
          isActiveComponent
          setProp={setProp}
          Component={Component}
        />
      );
    });

    test(`${readableName}: ${config.NUMBER_MANY_COMPONENTS} instances don't conflict, and render within ${config.RENDER_TIME_MANY_COMPONENTS}ms`, async () => {
      const setActiveComponent = jest.fn();
      const setProp = jest.fn();

      const start = performance.now();
      render(
        <div>
          {[...Array(config.NUMBER_MANY_COMPONENTS)].map((_, index) => (
            <Component
              key={index}
              {...defaultProps}
              setActiveComponent={setActiveComponent}
              setProp={setProp}
            />
          ))}
        </div>
      );
      const end = performance.now();
      const time = end - start;
      expect(time).toBeLessThan(config.RENDER_TIME_MANY_COMPONENTS);

      await new Promise((r) => setTimeout(r, 100));

      expect(setActiveComponent).not.toHaveBeenCalled();
      expect(setProp).not.toHaveBeenCalled();
    });

    // Test tab into component then tab back out
    test(`${readableName}: should be tabbable without errors`, async () => {
      const user = userEvent.setup();

      const setProp = jest.fn();
      const setActiveComponent = jest.fn();

      render(
        <Component
          {...defaultProps}
          setProp={setProp}
          setActiveComponent={setActiveComponent}
        />
      );

      await user.tab();
      await user.tab();
      await user.tab({ shift: true });
      await user.tab({ shift: true });
    });
  });
});

const categoriesArray = Object.keys(categories).map(
  (category) => categories[category]
);

describe("Generic Category Tests", () => {
  categoriesArray.forEach((category) => {
    test(`${
      category.readableName || "Unknown category"
    }: should have a readable name and icon as expected by consumer`, () => {
      expect(category.readableName).toBeDefined();
      expect(category.icon).toBeDefined();
    });
  });
});

/*
TODO:
- Generic tests when in view only that there are no fields/buttons
- Generic tests pulling from JSON file with mock data for Components to render props sampled from production
- Write tests for existing bugs to catch them

*/
