/**
 *  Accordion Unit Tests
 *
 *
 *  Test categories:
 *         ** Functionality --> ensure the component is functioning as expected
 *         ** Performance --> test the speed and efficiency of the component
 *         ** Security --> test the security of the component
 *         ** Accessibility --> test accessible for screen readers and keyboard navigation
 *
 */

import "@testing-library/jest-dom";
import { render, screen, fireEvent, act } from "../TestUtility/renderUtils";
import { v4 as uuidv4 } from "uuid";
import Accordions from "../components/Accordion/subComponent/Accordions";
import { LayoutProvider } from "../Context/InteractivesContext";

// import { logRoles } from "@testing-library/dom";

// Mocked uuid library
jest.mock("uuid", () => ({
  v4: () => "123456789",
}));

// Mocked context values
let mockState = [
  {
    id: uuidv4(),
    title: "",
    components: [],
    expanded: true,
  },
];

let timeline = {
  mockInversePatches: [],
  current: mockState,
  mockForwardPatches: [],
};

// should mock undo base on inverse array
const undoMock = jest.fn(() => {
  if (timeline.mockInversePatches.length > 0) {
    const { mockInversePatches, current, mockForwardPatches } = timeline;
    const length = mockInversePatches.length;
    const lastItem = mockInversePatches[length - 1];
    const restOfArr = mockInversePatches.slice(0, length - 1);
    timeline = {
      mockInversePatches: restOfArr,
      current: lastItem,
      mockForwardPatches: [...mockForwardPatches, current],
    };
  }
});

// should mock redo base on inverse array
const redoMock = jest.fn(() => {
  if (timeline.mockForwardPatches.length > 0) {
    const { mockInversePatches, current, mockForwardPatches } = timeline;
    const length = mockForwardPatches.length;
    const lastItem = mockForwardPatches[length - 1];
    const restOfArr = mockForwardPatches.slice(0, length - 1);
    timeline = {
      mockInversePatches: [...mockInversePatches, current],
      current: lastItem,
      mockForwardPatches: restOfArr,
    };
  }
});

// should mock updating state in authoring - which can change layoutState base on inverse and forward patches arrays
const setPropMock = jest.fn((newState) => {
  const { layoutState } = newState;
  const { mockInversePatches, current, mockForwardPatches } = timeline;
  timeline = {
    mockInversePatches:
      JSON.stringify(current) === JSON.stringify(layoutState)
        ? [...mockInversePatches]
        : [...mockInversePatches, current],
    current: layoutState,
    // this seems to be the idiomatic approach for most applications
    mockForwardPatches: [...mockForwardPatches],
  };
});

const MockProvider = ({ children, layoutState }) => {
  return (
    <LayoutProvider layoutState={layoutState} setProp={setPropMock}>
      {children}
    </LayoutProvider>
  );
};

describe("Accordion Component", () => {
  describe("Functionality", () => {
    beforeEach(() => {
      jest.useFakeTimers();
      timeline = {
        mockInversePatches: [],
        current: mockState,
        mockForwardPatches: [],
      };
    });

    afterEach(() => {
      jest.clearAllMocks();
      jest.clearAllTimers();
      timeline = {
        mockInversePatches: [],
        current: mockState,
        mockForwardPatches: [],
      };
    });
    // <------------Testing updates - debounced & none debounced & undo/redo & race condition------------>
    test("Changing accordion title should group fast actions and undo action removes a group of characters", () => {
      // Render the component
      render(
        <MockProvider layoutState={mockState}>
          <Accordions />
        </MockProvider>
      );

      // make pane active - otherwise it wont find title element
      const accordionPaneElement = screen.getByTestId("pane-0");
      fireEvent.click(accordionPaneElement);

      // Get the input element using a data-testid or other selector
      let accordionTitleElement = screen.getByLabelText(
        "accordion title input"
      );
      // Simulate user typing - check if setProp debounce properlly
      fireEvent.change(accordionTitleElement, { target: { value: "1" } });

      setTimeout(() => {}, 200);
      jest.advanceTimersByTime(200);

      fireEvent.change(accordionTitleElement, {
        target: { value: `${accordionTitleElement.value}:` },
      });
      fireEvent.change(accordionTitleElement, {
        target: { value: `${accordionTitleElement.value})` },
      });

      //need delay of a second
      setTimeout(() => {
        fireEvent.change(accordionTitleElement, {
          target: { value: `${accordionTitleElement.value}H` },
        });
        fireEvent.change(accordionTitleElement, {
          target: { value: `${accordionTitleElement.value}I` },
        });
      }, 1500);

      jest.advanceTimersByTime(1500);

      fireEvent.change(accordionTitleElement, {
        target: { value: `${accordionTitleElement.value}2` },
      });

      //need delay of a second
      // so following sequence should consider as a group of chars
      setTimeout(() => {
        fireEvent.change(accordionTitleElement, {
          target: { value: `${accordionTitleElement.value}H` },
        });
        fireEvent.change(accordionTitleElement, {
          target: { value: `${accordionTitleElement.value}E` },
        });
        fireEvent.change(accordionTitleElement, {
          target: { value: `${accordionTitleElement.value}L` },
        });
        fireEvent.change(accordionTitleElement, {
          target: { value: `${accordionTitleElement.value}L` },
        });
        fireEvent.change(accordionTitleElement, {
          target: { value: `${accordionTitleElement.value}O` },
        });
      }, 1000);

      jest.advanceTimersByTime(1000);

      //need delay of half second
      setTimeout(() => {
        fireEvent.change(accordionTitleElement, {
          target: { value: `${accordionTitleElement.value}!` },
        });
      }, 1000);
      jest.advanceTimersByTime(1000);

      setTimeout(() => {}, 1000);
      jest.advanceTimersByTime(1000);

      // console.log(timeline.mockForwardPatches);
      // console.log(timeline.mockInversePatches);
      // console.log("----->", timeline.current);
      // console.log(mockState);

      // ----------------------------undo-----------------------------------//
      // //undo act -> check debouncing and undo functionality
      // // - hit undo by user - call undo mock
      undoMock();

      // // // mockState has been updtaed - checking for body
      // act(() => {
      //   render(
      //     <MockProvider layoutState={timeline.current}>
      //       <Accordions />
      //     </MockProvider>
      //   );
      // });

      fireEvent.change(accordionTitleElement, {
        target: { value: timeline.current[0].title },
      });

      setTimeout(() => {}, 1000);
      jest.advanceTimersByTime(1000);

      // // //assertion
      expect(accordionTitleElement).toHaveValue("1:)HI2HELLO");

      undoMock();

      fireEvent.change(accordionTitleElement, {
        target: { value: timeline.current[0].title },
      });

      //assertion
      expect(accordionTitleElement).toHaveValue("1:)HI2");

      undoMock();

      fireEvent.change(accordionTitleElement, {
        target: { value: timeline.current[0].title },
      });

      //assertion
      expect(accordionTitleElement).toHaveValue("1:)");
      expect(accordionTitleElement).not.toHaveValue("1:)H");

      // ----------------------------redo-----------------------------------//
      redoMock();

      fireEvent.change(accordionTitleElement, {
        target: { value: timeline.current[0].title },
      });
      //assertion
      expect(accordionTitleElement).toHaveValue("1:)HI2");

      redoMock();

      fireEvent.change(accordionTitleElement, {
        target: { value: timeline.current[0].title },
      });

      //assertion
      expect(accordionTitleElement).toHaveValue("1:)HI2HELLO");

      // ----------------------------undo/redo-----------------------------------//

      undoMock();

      fireEvent.change(accordionTitleElement, {
        target: { value: timeline.current[0].title },
      });

      //assertion
      expect(accordionTitleElement).toHaveValue("1:)HI2");

      redoMock();

      fireEvent.change(accordionTitleElement, {
        target: { value: timeline.current[0].title },
      });

      //assertion
      expect(accordionTitleElement).toHaveValue("1:)HI2HELLO");

      redoMock();

      fireEvent.change(accordionTitleElement, {
        target: { value: timeline.current[0].title },
      });

      //assertion
      expect(accordionTitleElement).toHaveValue("1:)HI2HELLO!");

      redoMock();

      fireEvent.change(accordionTitleElement, {
        target: { value: timeline.current[0].title },
      });

      //assertion
      expect(accordionTitleElement).toHaveValue("1:)HI2HELLO!");
    });

    test("Changing accordin title should qroup user input character and debounce state update", () => {
      // Render the component
      render(
        <MockProvider layoutState={mockState}>
          <Accordions />
        </MockProvider>
      );

      // make pane active - otherwise it wont find title element
      const accordionPaneElement = screen.getByTestId("pane-0");
      fireEvent.click(accordionPaneElement);

      // Get the input element using a data-testid or other selector
      let accordionTitleElement = screen.getByLabelText(
        "accordion title input"
      );
      // Simulate user typing - check if setProp debounce properlly

      // grouping sequence : 1:) HI2 Hello  !
      fireEvent.change(accordionTitleElement, { target: { value: "1" } });

      // setprop shoul not call before passing 500 milisec
      expect(setPropMock).not.toHaveBeenCalled();

      setTimeout(() => {}, 200);
      jest.advanceTimersByTime(200);

      fireEvent.change(accordionTitleElement, {
        target: { value: `${accordionTitleElement.value}:` },
      });
      fireEvent.change(accordionTitleElement, {
        target: { value: `${accordionTitleElement.value})` },
      });

      expect(setPropMock).not.toHaveBeenCalled();

      //need delay of a second
      setTimeout(() => {
        fireEvent.change(accordionTitleElement, {
          target: { value: `${accordionTitleElement.value}H` },
        });
        fireEvent.change(accordionTitleElement, {
          target: { value: `${accordionTitleElement.value}I` },
        });
      }, 1000);

      jest.advanceTimersByTime(1000);
      expect(setPropMock).toHaveBeenCalledTimes(1);

      fireEvent.change(accordionTitleElement, {
        target: { value: `${accordionTitleElement.value}2` },
      });

      expect(setPropMock).toHaveBeenCalledTimes(1);

      //need delay of a second
      // so following sequence should consider as a group of chars
      setTimeout(() => {
        fireEvent.change(accordionTitleElement, {
          target: { value: `${accordionTitleElement.value}H` },
        });
        fireEvent.change(accordionTitleElement, {
          target: { value: `${accordionTitleElement.value}E` },
        });
        fireEvent.change(accordionTitleElement, {
          target: { value: `${accordionTitleElement.value}L` },
        });
        fireEvent.change(accordionTitleElement, {
          target: { value: `${accordionTitleElement.value}L` },
        });
        fireEvent.change(accordionTitleElement, {
          target: { value: `${accordionTitleElement.value}O` },
        });
      }, 1500);

      jest.advanceTimersByTime(1500);
      expect(setPropMock).toHaveBeenCalledTimes(2);

      //need delay of a second
      setTimeout(() => {
        fireEvent.change(accordionTitleElement, {
          target: { value: `${accordionTitleElement.value}!` },
        });
      }, 500);
      jest.advanceTimersByTime(500);
      expect(setPropMock).toHaveBeenCalledTimes(3);

      // make delay to have latest version in console.log
      setTimeout(() => {}, 500);
      jest.advanceTimersByTime(500);

      // console.log(timeline.mockForwardPatches);
      // console.log(timeline.mockInversePatches);
      // console.log("----->", timeline.current);
      // console.log(mockState);
    });
  });
});
