/**
 *  Video Unit Tests
 *
 *
 *  Test categories:
 *         ** Functionality --> ensure the component is functioning as expected
 *         ** Performance --> test the speed and efficiency of the component
 *         ** Security --> test the security of the component
 *         ** Accessibility --> test accessible for screen readers and keyboard navigation
 *
 */

import "@testing-library/jest-dom";
import { render, screen, fireEvent } from "../TestUtility/renderUtils";
import userEvent from "@testing-library/user-event";
import { v4 as uuidv4 } from "uuid";
import VideoMain from "../components/Video/VideoMain";
import { VideoProvider } from "../components/Video/VideoContext";
import NestedComponentWrapper from "../Utility/NestedComponentWrapper";
import { LayoutProvider } from "../Context/InteractivesContext";
// import { logRoles } from "@testing-library/dom";

//context provider mocks
// Video is using TenantContext and VideoContext:
// TenantContext is defined as a custom wrapper ---> ../TestUtility/testing-library-render-utils
// HeaderContext

// Mocked uuid library
jest.mock("uuid", () => ({
  v4: () => "123456789",
}));

// Mocked context values
const mockStateNull = {
  videoState: {
    id: uuidv4(),
    videoSource: null,
    videoURL: null,
    videoDescription: null,
    videoCredit: null,
    videoId: null,
    videoTextSettings: {
      description: true,
      credit: true,
    },
  },
};

const componentTypeMock = "accordion";

const childComponentMock = {
  id: "64a6dc8231b4696a03739df4",
  componentName: "Video",
  props: mockStateNull,
  position: "w",
  __typename: "Component",
};

const setterFunctionMock = jest.fn();

//accordion default props is used as mockstate
const parentStateMock = [
  {
    id: uuidv4(),
    title: "",
    components: [
      {
        componentName: "Video",
        componentProps: mockStateNull,
      },
    ],
    expanded: true,
  },
];

const MockInteractiveProvider = ({ children }) => {
  return (
    <LayoutProvider layoutState={parentStateMock} setProp={setPropMock}>
      {children}
    </LayoutProvider>
  );
};

let timeline = {
  mockInversePatches: [],
  current: mockStateNull,
  mockForwardPatches: [],
};

// should mock undo based on inverse array
const undoMock = jest.fn(() => {
  if (timeline.mockInversePatches.length > 0) {
    const { mockInversePatches, current, mockForwardPatches } = timeline;
    const length = mockInversePatches.length;
    const lastItem = mockInversePatches[length - 1];
    const restOfArr = mockInversePatches.slice(0, length - 1);
    timeline = {
      mockInversePatches: restOfArr,
      current: lastItem,
      mockForwardPatches: [...mockForwardPatches, current],
    };
  }
});

// should mock redo based on inverse array
const redoMock = jest.fn(() => {
  if (timeline.mockForwardPatches.length > 0) {
    const { mockInversePatches, current, mockForwardPatches } = timeline;
    const length = mockForwardPatches.length;
    const lastItem = mockForwardPatches[length - 1];
    const restOfArr = mockForwardPatches.slice(0, length - 1);
    timeline = {
      mockInversePatches: [...mockInversePatches, current],
      current: lastItem,
      mockForwardPatches: restOfArr,
    };
  }
});

// should mock updating state in authoring - which can change videoState based on inverse and forward patches arrays
const setPropMock = jest.fn((newState) => {
  const { videoState } = newState;
  const { mockInversePatches, current, mockForwardPatches } = timeline;
  timeline = {
    mockInversePatches:
      JSON.stringify(current) === JSON.stringify(videoState)
        ? [...mockInversePatches]
        : [...mockInversePatches, current],
    current: newState,
    // this seems to be the idiomatic approach for most applications
    mockForwardPatches: [...mockForwardPatches],
  };
});

const MockProvider = ({ children }) => {
  return (
    <VideoProvider videoState={mockStateNull} setProp={setPropMock}>
      {children}
    </VideoProvider>
  );
};

describe("Video Component", () => {
  describe("General functionality whether Brightcove or YouTube", () => {
    test("The component renders correctly with given (null/empty) placeholders", () => {
      // Render the component
      render(
        <MockProvider>
          <VideoMain
            videoState={mockStateNull.videoState}
            setProp={setPropMock}
          />
        </MockProvider>
      );

      const videoContainer = screen.getByTestId("video-container");
      const videoPlaceholderContainer = screen.getByTestId("video");
      const videoPlaceholderImage = screen.getByAltText(/play img/i);
      const videoDescriptionContainer = screen.getByTestId(
        "video-description-text-container"
      );
      const videoDescriptionField = screen.getByText(/video description/i);
      const videoCreditContainer = screen.getByTestId(
        "video-credit-text-container"
      );
      const videoCreditField = screen.getByText(/credit/i);
      const transcriptButton = screen.getByRole("button", {
        name: /no transcript/i,
      });

      expect(videoContainer).toBeInTheDocument();
      expect(videoPlaceholderContainer).toBeInTheDocument();
      expect(videoPlaceholderImage).toBeInTheDocument();
      expect(videoDescriptionContainer).toBeInTheDocument();
      expect(videoDescriptionField).toBeInTheDocument();
      expect(videoCreditContainer).toBeInTheDocument();
      expect(videoCreditField).toBeInTheDocument();
      expect(transcriptButton).toBeInTheDocument();
    });

    // TODO: Get help!
    xtest("Should allow the user to type text into the description field", async () => {
      const user = userEvent.setup();
      // Render the component
      render(
        <MockProvider>
          <VideoMain
            videoState={mockStateNull.videoState}
            setProp={setPropMock}
          />
        </MockProvider>
      );

      const videoDescriptionContainer = screen.getByTestId(
        "video-description-text-container"
      );
      const videoDescriptionPlaceholder =
        screen.getByText(/video description/i);

      expect(videoDescriptionContainer).toBeInTheDocument();
      expect(videoDescriptionPlaceholder).toBeInTheDocument();

      await user.click(videoDescriptionPlaceholder);

      const videoDescriptionInput = screen.getByTestId("text-editor-component");
      expect(videoDescriptionInput).toBeInTheDocument();

      await user.type(videoDescriptionInput, "Hello, world!");

      expect(mockStateNull.videoState.videoDescription).toBe({
        ops: [{ insert: "Hello, world!" }],
      });
    });

    xtest("Should allow the user to type into the credit field", () => {
      // Render the component
      render(
        <MockProvider>
          <VideoMain videoState={mockStateNull} setProp={setPropMock} />
        </MockProvider>
      );

      // TODO: Fill this in with tests once we pull in Video unit test action item
    });
  });

  describe("General Toolbar functionality whether Brightcove or YouTube", () => {
    //<------------Testing Video toolbar functionality------------>
    test("The component is clickable and shows toolbar when Video component is clicked", async () => {
      const user = userEvent.setup();
      // Render the component
      render(
        <MockInteractiveProvider>
          <NestedComponentWrapper
            componentType={componentTypeMock}
            component={childComponentMock}
            compIndex={0}
            componentProps={parentStateMock[0].components[0].componentProps}
            tabIndex={0}
            numOfComponent={1}
            droppedIndex={0}
            setDroppedIndex={setterFunctionMock}
            setActiveComp={setterFunctionMock}
            activeComp={setterFunctionMock}
            setShowError={setterFunctionMock}
            setShowDropError={setterFunctionMock}
            setHoverMenu={setterFunctionMock}
            setActiveComponent={setterFunctionMock}
          />
        </MockInteractiveProvider>
      );

      const videoContainer = screen.getByTestId("video-container");

      await user.click(videoContainer);

      const videoToolbar = screen.getByTestId("video-toolbar");
      const addVideoButton = screen.getByRole("button", { name: /add video/i });
      const transcriptButton = screen.getByRole("button", {
        name: /download transcript/i,
      });
      const textFormattingButton = screen.getByRole("button", {
        name: /bold dropdown button/i,
      });
      const textAlignmentButton = screen.getByRole("button", {
        name: /alignment dropdown button/i,
      });
      const textListButton = screen.getByRole("button", {
        name: /list dropdown button/i,
      });
      const textLinkButton = screen.getByRole("button", {
        name: /link button/i,
      });
      const textSettingsButton = screen.getByRole("button", {
        name: /configure video description/i,
      });

      expect(videoToolbar).toBeInTheDocument();
      expect(addVideoButton).toBeInTheDocument();
      expect(addVideoButton).not.toBeDisabled();
      expect(transcriptButton).toBeInTheDocument();
      expect(transcriptButton).toBeDisabled();
      expect(textFormattingButton).toBeInTheDocument();
      expect(textFormattingButton).toBeDisabled();
      expect(textAlignmentButton).toBeInTheDocument();
      expect(textAlignmentButton).toBeDisabled();
      expect(textListButton).toBeInTheDocument();
      expect(textListButton).toBeDisabled();
      expect(textLinkButton).toBeInTheDocument();
      expect(textLinkButton).toBeDisabled();
      expect(textSettingsButton).toBeInTheDocument();
      expect(textSettingsButton).not.toBeDisabled();
    });

    test("The component hides the toolbar when user clicks outside of component", async () => {
      const user = userEvent.setup();
      // Render the component
      render(
        <MockInteractiveProvider>
          <NestedComponentWrapper
            componentType={componentTypeMock}
            component={childComponentMock}
            compIndex={0}
            componentProps={parentStateMock[0].components[0].componentProps}
            tabIndex={0}
            numOfComponent={1}
            droppedIndex={0}
            setDroppedIndex={setterFunctionMock}
            setActiveComp={setterFunctionMock}
            activeComp={setterFunctionMock}
            setShowError={setterFunctionMock}
            setShowDropError={setterFunctionMock}
            setHoverMenu={setterFunctionMock}
            setActiveComponent={setterFunctionMock}
          />
        </MockInteractiveProvider>
      );

      const videoContainer = screen.getByTestId("video-container");

      await user.click(videoContainer);

      const videoToolbar = screen.getByTestId("video-toolbar");
      expect(videoToolbar).toBeInTheDocument();

      const clickEvent = new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      });
      // Dispatch the event on the document
      document.dispatchEvent(clickEvent);

      // Trigger blur event- simulate clicking outside of component because above mock doesn't remove focus
      videoContainer.blur();
      expect(videoContainer).not.toHaveFocus();
      expect(videoToolbar).not.toBeInTheDocument();

      const addVideoButton = screen.queryByRole("button", {
        name: /add video/i,
      });
      const transcriptButton = screen.queryByRole("button", {
        name: /download transcript/i,
      });
      const textFormattingButton = screen.queryByRole("button", {
        name: /bold dropdown button/i,
      });
      const textAlignmentButton = screen.queryByRole("button", {
        name: /alignment dropdown button/i,
      });
      const textListButton = screen.queryByRole("button", {
        name: /list dropdown button/i,
      });
      const textLinkButton = screen.queryByRole("button", {
        name: /link button/i,
      });

      expect(addVideoButton).not.toBeInTheDocument();
      expect(transcriptButton).not.toBeInTheDocument();
      expect(textFormattingButton).not.toBeInTheDocument();
      expect(textAlignmentButton).not.toBeInTheDocument();
      expect(textListButton).not.toBeInTheDocument();
      expect(textLinkButton).not.toBeInTheDocument();
    });

    test("Dropdown options display when user clicks 'Add Video' button from toolbar", async () => {
      const user = userEvent.setup();
      // Render the component
      render(
        <MockInteractiveProvider>
          <NestedComponentWrapper
            componentType={componentTypeMock}
            component={childComponentMock}
            compIndex={0}
            componentProps={parentStateMock[0].components[0].componentProps}
            tabIndex={0}
            numOfComponent={1}
            droppedIndex={0}
            setDroppedIndex={setterFunctionMock}
            setActiveComp={setterFunctionMock}
            activeComp={setterFunctionMock}
            setShowError={setterFunctionMock}
            setShowDropError={setterFunctionMock}
            setHoverMenu={setterFunctionMock}
            setActiveComponent={setterFunctionMock}
          />
        </MockInteractiveProvider>
      );

      const videoContainer = screen.getByTestId("video-container");

      await user.click(videoContainer);

      const videoToolbar = screen.getByTestId("video-toolbar");
      expect(videoToolbar).toBeInTheDocument();

      const addVideoButton = screen.getByRole("button", { name: /add video/i });
      expect(addVideoButton).toBeInTheDocument();

      await user.click(addVideoButton);

      const addBrightcoveButton = screen.getByRole("menuitem", {
        name: /add brightcove video/i,
      });
      const addYouTubeButton = screen.getByRole("menuitem", {
        name: /add youtube video/i,
      });
      expect(addBrightcoveButton).toBeInTheDocument();
      expect(addYouTubeButton).toBeInTheDocument();
    });

    xtest("User is able to format description", async () => {
      const user = userEvent.setup();
      // Render the component
      render(
        <MockProvider>
          <VideoMain videoState={mockStateNull} setProp={setPropMock} />
        </MockProvider>
      );

      // TODO: Fill this in with tests once we pull in Video unit test action item
    });

    xtest("User is able to format credit (alignment, add link)", async () => {
      const user = userEvent.setup();
      // Render the component
      render(
        <MockProvider>
          <VideoMain videoState={mockStateNull} setProp={setPropMock} />
        </MockProvider>
      );

      // TODO: Fill this in with tests once we pull in Video unit test action item
    });

    xtest("User is able to hide video description with kebab-menu option", async () => {
      const user = userEvent.setup();
      // Render the component
      render(
        <MockProvider>
          <VideoMain videoState={mockStateNull} setProp={setPropMock} />
        </MockProvider>
      );

      // TODO: Fill this in with tests once we pull in Video unit test action item
    });

    xtest("User is able to hide video credit with kebab-menu option", async () => {
      const user = userEvent.setup();
      // Render the component
      render(
        <MockProvider>
          <VideoMain videoState={mockStateNull} setProp={setPropMock} />
        </MockProvider>
      );

      // TODO: Fill this in with tests once we pull in Video unit test action item
    });
  });

  describe("Brightcove", () => {
    describe("Functionality", () => {
      // <------------Testing default behavior------------>
      xtest("Should allow the user to change videoSource to 'brightcove'", () => {
        // Render the component
        render(
          <MockProvider>
            <VideoMain videoState={mockStateNull} setProp={setPropMock} />
          </MockProvider>
        );

        // TODO: Fill this in with tests once we pull in Video unit test action item
      });

      xtest("Video renders when user types in an Brightcove ID and presses 'Add' button", async () => {
        const user = userEvent.setup();
        // Render the component
        render(
          <MockProvider>
            <VideoMain videoState={mockStateNull} setProp={setPropMock} />
          </MockProvider>
        );

        // TODO: Fill this in with tests once we pull in Video unit test action item
      });

      xtest("Video description dynamically added from brightcove API fetch and displaying in credit field", async () => {
        const user = userEvent.setup();
        // Render the component
        render(
          <MockProvider>
            <VideoMain videoState={mockStateNull} setProp={setPropMock} />
          </MockProvider>
        );

        // TODO: Fill this in with tests once we pull in Video unit test action item
      });
    });
  });

  describe("YouTube", () => {
    describe("Functionality", () => {
      // <------------Testing default behavior------------>
      test("Should allow the user to change videoSource to 'youTube'", async () => {
        const user = userEvent.setup();
        // Render the component
        render(
          <MockInteractiveProvider>
            <NestedComponentWrapper
              componentType={componentTypeMock}
              component={childComponentMock}
              compIndex={0}
              componentProps={parentStateMock[0].components[0].componentProps}
              tabIndex={0}
              numOfComponent={1}
              droppedIndex={0}
              setDroppedIndex={setterFunctionMock}
              setActiveComp={setterFunctionMock}
              activeComp={setterFunctionMock}
              setShowError={setterFunctionMock}
              setShowDropError={setterFunctionMock}
              setHoverMenu={setterFunctionMock}
              setActiveComponent={setterFunctionMock}
            />
          </MockInteractiveProvider>
        );

        const videoContainer = screen.getByTestId("video-container");

        await user.click(videoContainer);

        const videoToolbar = screen.getByTestId("video-toolbar");
        expect(videoToolbar).toBeInTheDocument();

        const addVideoButton = screen.getByRole("button", {
          name: /add video/i,
        });
        expect(addVideoButton).toBeInTheDocument();

        await user.click(addVideoButton);

        const addBrightcoveButton = screen.getByRole("menuitem", {
          name: /add brightcove video/i,
        });
        const addYouTubeButton = screen.getByRole("menuitem", {
          name: /add youtube video/i,
        });
        expect(addBrightcoveButton).toBeInTheDocument();
        expect(addYouTubeButton).toBeInTheDocument();
        await user.click(addYouTubeButton);
        const youTubeTextElement = screen.getByPlaceholderText(
          /paste unique identifier/i
        );
        expect(youTubeTextElement).toHaveAttribute(
          "data-testid",
          "youtube-input-field"
        );
        expect(youTubeTextElement).toBeInTheDocument();
        // TODO (maybe): Check data in props to confirm videoSource == 'youTube'
      });

      test("Video renders when user types in a YouTube URL and presses 'Add' button", async () => {
        const user = userEvent.setup();
        // Render the component
        render(
          <MockInteractiveProvider>
            <NestedComponentWrapper
              componentType={componentTypeMock}
              component={childComponentMock}
              compIndex={0}
              componentProps={parentStateMock[0].components[0].componentProps}
              tabIndex={0}
              numOfComponent={1}
              droppedIndex={0}
              setDroppedIndex={setterFunctionMock}
              setActiveComp={setterFunctionMock}
              activeComp={setterFunctionMock}
              setShowError={setterFunctionMock}
              setShowDropError={setterFunctionMock}
              setHoverMenu={setterFunctionMock}
              setActiveComponent={setterFunctionMock}
            />
          </MockInteractiveProvider>
        );

        const videoContainer = screen.getByTestId("video-container");

        await user.click(videoContainer);

        const videoToolbar = screen.getByTestId("video-toolbar");
        expect(videoToolbar).toBeInTheDocument();

        const addVideoButton = screen.getByRole("button", {
          name: /add video/i,
        });
        expect(addVideoButton).toBeInTheDocument();

        await user.click(addVideoButton);

        const addBrightcoveButton = screen.getByRole("menuitem", {
          name: /add brightcove video/i,
        });
        const addYouTubeButton = screen.getByRole("menuitem", {
          name: /add youtube video/i,
        });
        expect(addBrightcoveButton).toBeInTheDocument();
        expect(addYouTubeButton).toBeInTheDocument();
        await user.click(addYouTubeButton);
        const youTubeTextElement = screen.getByTestId("youtube-input-field");

        expect(youTubeTextElement).toBeInTheDocument();
        await user.click(youTubeTextElement);
        fireEvent.change(youTubeTextElement, {
          target: { value: "https://www.youtube.com/watch?v=c3ZTc_JoNgs" },
        });
        expect(youTubeTextElement).toHaveValue(
          "https://www.youtube.com/watch?v=c3ZTc_JoNgs"
        );

        const addButton = screen.getByRole("button", {
          name: /youtube video submit button/i,
        });
        expect(addButton).toBeInTheDocument();
        await user.click(addButton);

        const youTubePlayer = screen.getByTestId("youtube-video-player");
        expect(youTubePlayer).toBeInTheDocument();
        expect(youTubePlayer).toHaveAttribute(
          "videoId",
          "c3ZTc_JoNgsc3ZTc_JoNgs"
        );
        // TODO: Confirm props changed - stretch goal
      });

      // This might not be real - (waiting on results of spike this sprint)
      xtest("Video description dynamically added from youTube API fetch", async () => {
        const user = userEvent.setup();
        // Render the component
        render(
          <MockProvider>
            <VideoMain videoState={mockStateNull} setProp={setPropMock} />
          </MockProvider>
        );

        // TODO: Fill this in with tests once we pull in Video unit test action item
      });
    });
  });

  xdescribe("Debouncing-Undo/Redo", () => {
    // TODO: Fill this in with tests once we pull in Video unit test action item
  });

  xdescribe("Performance", () => {
    // TODO: Fill this in with tests once we pull in Video unit test action item
  });

  xdescribe("Security", () => {
    // TODO: Fill this in with tests once we pull in Video unit test action item
  });

  xdescribe("Accessibility", () => {
    // TODO: Fill this in with tests once we pull in Video unit test action item
  });
});
