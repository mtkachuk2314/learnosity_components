/**
 *  Text Unit Tests
 *
 *
 *  Test categories:
 *         ** Functionality --> ensure the component is functioning as expected
 *         ** Performance --> test the speed and efficiency of the component
 *         ** Security --> test the security of the component
 *         ** Accessibility --> test accessible for screen readers and keyboard navigation
 *
 */

import "@testing-library/jest-dom";
import userEvent from "@testing-library/user-event";
import { render, screen, fireEvent } from "../TestUtility/renderUtils";
import NestedComponentWrapper from "../Utility/NestedComponentWrapper";
import { LayoutProvider } from "../Context/InteractivesContext";
import { v4 as uuidv4 } from "uuid";
import Text from "../components/Text/Text";
import { Quill } from "react-quill";
import "react-quill/dist/quill.snow.css";

// import { logRoles } from "@testing-library/dom";

// Mocked uuid library
jest.mock("uuid", () => ({
  v4: () => "123456789",
}));

// Mocked context values
let mockState = { ops: [{ insert: "" }] };
const setTabActive = jest.fn();
const setActiveComponent = jest.fn();
const selectedIcon = jest.fn();
const componentType = "text";
const setSelectedIcon = jest.fn();
const setTextRef = jest.fn();
const portal = null;

let timeline = {
  mockInversePatches: [],
  current: mockState,
  mockForwardPatches: [],
};

// should mock undo base on inverse array
const undoMock = jest.fn(() => {
  if (timeline.mockInversePatches.length > 0) {
    const { mockInversePatches, current, mockForwardPatches } = timeline;
    const length = mockInversePatches.length;
    const lastItem = mockInversePatches[length - 1];
    const restOfArr = mockInversePatches.slice(0, length - 1);
    timeline = {
      mockInversePatches: restOfArr,
      current: lastItem,
      mockForwardPatches: [...mockForwardPatches, current],
    };
  }
});

// should mock redo base on inverse array
const redoMock = jest.fn(() => {
  if (timeline.mockForwardPatches.length > 0) {
    const { mockInversePatches, current, mockForwardPatches } = timeline;
    const length = mockForwardPatches.length;
    const lastItem = mockForwardPatches[length - 1];
    const restOfArr = mockForwardPatches.slice(0, length - 1);
    timeline = {
      mockInversePatches: [...mockInversePatches, current],
      current: lastItem,
      mockForwardPatches: restOfArr,
    };
  }
});

// should mock updating state in authoring - which can change layoutState base on inverse and forward patches arrays
const setPropMock = jest.fn((newState) => {
  const { layoutState } = newState;
  const { mockInversePatches, current, mockForwardPatches } = timeline;
  timeline = {
    mockInversePatches:
      JSON.stringify(current) === JSON.stringify(layoutState)
        ? [...mockInversePatches]
        : [...mockInversePatches, current],
    current: layoutState,
    // this seems to be the idiomatic approach for most applications
    mockForwardPatches: [...mockForwardPatches],
  };
});

describe("Text Component", () => {
  describe("Functionality", () => {
    // <------------Testing updates - debounced & none debounced & undo/redo & race condition------------>
    describe.skip("Undo/Redo testing", () => {
      beforeEach(() => {
        jest.useFakeTimers();
        jest.setTimeout(10000);
        timeline = {
          mockInversePatches: [],
          current: mockState,
          mockForwardPatches: [],
        };
      });

      afterEach(() => {
        jest.clearAllMocks();
        jest.clearAllTimers();
        jest.useRealTimers();
        timeline = {
          mockInversePatches: [],
          current: mockState,
          mockForwardPatches: [],
        };
      });
      test.skip("Changing text context should qroup user input character and debounce state update", async () => {
        const user = userEvent.setup();
        // Render the component
        const { container } = render(
          <Text
            body={mockState}
            setProp={setPropMock}
            setTabActive={setTabActive}
            setActiveComponent={setActiveComponent}
            selectedIcon={selectedIcon}
            componentType={componentType}
            setSelectedIcon={setSelectedIcon}
            setTextRef={setTextRef}
            portal={portal}
          />
        );

        // make editor active - otherwise it will show placeholder
        const textPlaceholderElement = screen.getByTestId("text-component");

        // Assert that the element exist
        expect(textPlaceholderElement).toBeInTheDocument();
        expect(textPlaceholderElement).toHaveTextContent(
          /^Lorem ipsum dolor sit amet,/
        );

        fireEvent.click(textPlaceholderElement);
        // fireEvent.focus(textPlaceholderElement);

        expect(textPlaceholderElement).not.toBeInTheDocument();

        const quilltextEditorElement = container.querySelector(".ql-editor");

        expect(quilltextEditorElement).toBeInTheDocument();

        fireEvent.change(quilltextEditorElement, {
          target: { innerHTML: "<p>1</p>" },
        });

        // Simulate user typing - check if setProp debounce properlly

        // grouping sequence : 1:) HI2 Hello  !

        // setprop should not call before passing 500 milisec
        expect(setPropMock).not.toHaveBeenCalled();

        setTimeout(() => {}, 2000);
        jest.advanceTimersByTime(2000);
        expect(setPropMock).toHaveBeenCalled(1);

        fireEvent.change(quilltextEditorElement, {
          target: {
            innerHTML: `<p>${
              quilltextEditorElement.querySelector("p").textContent
            }:</p>`,
          },
        });
        fireEvent.change(quilltextEditorElement, {
          target: {
            innerHTML: `<p>${
              quilltextEditorElement.querySelector("p").textContent
            })</p>`,
          },
        });

        expect(setPropMock).not.toHaveBeenCalled();

        //need delay of a second
        setTimeout(() => {
          fireEvent.change(quilltextEditorElement, {
            target: {
              innerHTML: `<p>${
                quilltextEditorElement.querySelector("p").textContent
              }H</p>`,
            },
          });
          fireEvent.change(quilltextEditorElement, {
            target: {
              innerHTML: `<p>${
                quilltextEditorElement.querySelector("p").textContent
              }I</p>`,
            },
          });
        }, 1000);

        jest.advanceTimersByTime(1000);
        expect(setPropMock).toHaveBeenCalledTimes(1);

        //need delay of a second
        // so following sequence should consider as a group of chars
        setTimeout(() => {
          fireEvent.change(quilltextEditorElement, {
            target: {
              innerHTML: `<p>${
                quilltextEditorElement.querySelector("p").textContent
              }H</p>`,
            },
          });
          fireEvent.change(quilltextEditorElement, {
            target: {
              innerHTML: `<p>${
                quilltextEditorElement.querySelector("p").textContent
              }E</p>`,
            },
          });
          fireEvent.change(quilltextEditorElement, {
            target: {
              innerHTML: `<p>${
                quilltextEditorElement.querySelector("p").textContent
              }L</p>`,
            },
          });
          fireEvent.change(quilltextEditorElement, {
            target: {
              innerHTML: `<p>${
                quilltextEditorElement.querySelector("p").textContent
              }L</p>`,
            },
          });
          fireEvent.change(quilltextEditorElement, {
            target: {
              innerHTML: `<p>${
                quilltextEditorElement.querySelector("p").textContent
              }O</p>`,
            },
          });
        }, 1000);

        jest.advanceTimersByTime(1000);
        expect(setPropMock).toHaveBeenCalledTimes(2);

        //need delay of a second
        setTimeout(() => {
          fireEvent.change(quilltextEditorElement, {
            target: {
              innerHTML: `<p>${
                quilltextEditorElement.querySelector("p").textContent
              }!</p>`,
            },
          });
        }, 1000);
        jest.advanceTimersByTime(1000);
        expect(setPropMock).toHaveBeenCalledTimes(3);

        //changing formats should not get debounced

        fireEvent.change(quilltextEditorElement, {
          target: {
            innerHTML: `<p><em>${
              quilltextEditorElement.querySelector("p").textContent
            }</em></p>`,
          },
        });
        fireEvent.change(quilltextEditorElement, {
          target: {
            innerHTML: `<p><strong>${
              quilltextEditorElement.querySelector("p").textContent
            }</strong></p>`,
          },
        });
        expect(setPropMock).toHaveBeenCalledTimes(5);

        //changing format using toolbar

        const textToolbarElement = screen.getByTestId("text-toolbar");
        expect(textToolbarElement).toBeInTheDocument();
        const boldButton = screen.getByTestId("bold-toolbar");
        expect(boldButton).toBeInTheDocument();
        fireEvent.click(boldButton);

        fireEvent.change(quilltextEditorElement, {
          target: {
            innerHTML: `<p>${
              quilltextEditorElement.querySelector("p").textContent
            }:))</p>`,
          },
        });
        expect(setPropMock).toHaveBeenCalledTimes(6);
        // make delay to have latest version in console.log
        setTimeout(() => {}, 1000);
        jest.advanceTimersByTime(1000);

        console.log(timeline.mockForwardPatches);
        console.log(timeline.mockInversePatches);
        console.log("----->", timeline.current);
        console.log(mockState);
      });

      test.skip("Changing text title should group fast actions and undo action removes a group of characters", () => {
        // Render the component
        const { container } = render(
          <Text
            body={mockState}
            setProp={setPropMock}
            setTabActive={setTabActive}
            setActiveComponent={setActiveComponent}
            selectedIcon={selectedIcon}
            componentType={componentType}
            setSelectedIcon={setSelectedIcon}
            setTextRef={setTextRef}
            portal={portal}
          />
        );

        // make editor active - otherwise it will show placeholder
        const textPlaceholderElement = screen.getByTestId("text-component");

        // Assert that the element exist
        expect(textPlaceholderElement).toBeInTheDocument();
        expect(textPlaceholderElement).toHaveTextContent(
          /^Lorem ipsum dolor sit amet,/
        );

        fireEvent.click(textPlaceholderElement);
        // fireEvent.focus(textPlaceholderElement);

        expect(textPlaceholderElement).not.toBeInTheDocument();

        const textToolbarElement = screen.getByTestId("text-toolbar");
        const quilltextEditorElement = container.querySelector(".ql-editor");

        expect(quilltextEditorElement).toBeInTheDocument();

        fireEvent.change(quilltextEditorElement, {
          target: { innerHTML: "<p>1</p>" },
        });

        // Simulate user typing - check if setProp debounce properlly

        // grouping sequence : 1:) HI2 Hello  !

        setTimeout(() => {}, 200);
        jest.advanceTimersByTime(200);

        fireEvent.change(quilltextEditorElement, {
          target: {
            innerHTML: `<p>${
              quilltextEditorElement.querySelector("p").textContent
            }:</p>`,
          },
        });
        fireEvent.change(quilltextEditorElement, {
          target: {
            innerHTML: `<p>${
              quilltextEditorElement.querySelector("p").textContent
            })</p>`,
          },
        });

        //need delay of a second
        setTimeout(() => {
          fireEvent.change(quilltextEditorElement, {
            target: {
              innerHTML: `<p>${
                quilltextEditorElement.querySelector("p").textContent
              }H</p>`,
            },
          });
          fireEvent.change(quilltextEditorElement, {
            target: {
              innerHTML: `<p>${
                quilltextEditorElement.querySelector("p").textContent
              }I</p>`,
            },
          });
        }, 1000);

        jest.advanceTimersByTime(1000);

        //need delay of a second
        // so following sequence should consider as a group of chars
        setTimeout(() => {
          fireEvent.change(quilltextEditorElement, {
            target: {
              innerHTML: `<p>${
                quilltextEditorElement.querySelector("p").textContent
              }H</p>`,
            },
          });
          fireEvent.change(quilltextEditorElement, {
            target: {
              innerHTML: `<p>${
                quilltextEditorElement.querySelector("p").textContent
              }E</p>`,
            },
          });
          fireEvent.change(quilltextEditorElement, {
            target: {
              innerHTML: `<p>${
                quilltextEditorElement.querySelector("p").textContent
              }L</p>`,
            },
          });
          fireEvent.change(quilltextEditorElement, {
            target: {
              innerHTML: `<p>${
                quilltextEditorElement.querySelector("p").textContent
              }L</p>`,
            },
          });
          fireEvent.change(quilltextEditorElement, {
            target: {
              innerHTML: `<p>${
                quilltextEditorElement.querySelector("p").textContent
              }O</p>`,
            },
          });
        }, 1000);

        jest.advanceTimersByTime(1000);

        //need delay of a second
        setTimeout(() => {
          fireEvent.change(quilltextEditorElement, {
            target: {
              innerHTML: `<p>${
                quilltextEditorElement.querySelector("p").textContent
              }!</p>`,
            },
          });
        }, 1000);
        jest.advanceTimersByTime(1000);

        // make delay to have latest version in console.log
        setTimeout(() => {}, 1000);
        jest.advanceTimersByTime(1000);

        console.log(timeline.mockForwardPatches);
        console.log(timeline.mockInversePatches);
        console.log("----->", timeline.current);
        console.log(mockState);

        // ----------------------------undo-----------------------------------//
        // //undo act -> check debouncing and undo functionality
        // // - hit undo by user - call undo mock
        undoMock();

        // // // mockState has been updtaed - checking for body
        // act(() => {
        //   render(
        //     <MockProvider layoutState={timeline.current}>
        //       <Accordions />
        //     </MockProvider>
        //   );
        // });

        fireEvent.change(quilltextEditorElement, {
          target: { innerHTML: timeline.current[0].body },
        });

        setTimeout(() => {}, 1000);
        jest.advanceTimersByTime(1000);

        // // //assertion
        expect(quilltextEditorElement).toHaveValue("1:)HI2HELLO");

        undoMock();

        fireEvent.change(quilltextEditorElement, {
          target: { innerHTML: timeline.current[0].title },
        });

        //assertion
        expect(quilltextEditorElement).toHaveValue("1:)HI2");

        undoMock();

        fireEvent.change(quilltextEditorElement, {
          target: { innerHTML: timeline.current[0].title },
        });

        //assertion
        expect(quilltextEditorElement).toHaveValue("1:)");
        expect(quilltextEditorElement).not.toHaveValue("1:)H");

        // ----------------------------redo-----------------------------------//
        redoMock();

        fireEvent.change(quilltextEditorElement, {
          target: { innerHTML: timeline.current[0].title },
        });
        //assertion
        expect(quilltextEditorElement).toHaveValue("1:)HI2");

        redoMock();

        fireEvent.change(quilltextEditorElement, {
          target: { innerHTML: timeline.current[0].title },
        });

        //assertion
        expect(quilltextEditorElement).toHaveValue("1:)HI2HELLO");

        // ----------------------------undo/redo-----------------------------------//

        undoMock();

        fireEvent.change(quilltextEditorElement, {
          target: { innerHTML: timeline.current[0].title },
        });

        //assertion
        expect(quilltextEditorElement).toHaveValue("1:)HI2");

        redoMock();

        fireEvent.change(quilltextEditorElement, {
          target: { innerHTML: timeline.current[0].title },
        });

        //assertion
        expect(quilltextEditorElement).toHaveValue("1:)HI2HELLO");

        redoMock();

        fireEvent.change(quilltextEditorElement, {
          target: { innerHTML: timeline.current[0].title },
        });

        //assertion
        expect(quilltextEditorElement).toHaveValue("1:)HI2HELLO!");

        redoMock();

        fireEvent.change(quilltextEditorElement, {
          target: { innerHTML: timeline.current[0].title },
        });

        //assertion
        expect(quilltextEditorElement).toHaveValue("1:)HI2HELLO!");
      });
    });

    // <------------Testing default behavior------------>
    test("The component renders correctly with the given placeholder", () => {
      // Render the component
      const { container } = render(
        <Text
          body={mockState}
          setProp={setPropMock}
          setTabActive={setTabActive}
          setActiveComponent={setActiveComponent}
          selectedIcon={selectedIcon}
          componentType={componentType}
          setSelectedIcon={setSelectedIcon}
          setTextRef={setTextRef}
          portal={portal}
        />
      );
      // Get the element using a PlaceholderText
      // make editor active - otherwise it will show placeholder
      const textPlaceholderElement = screen.getByTestId("text-component");

      // Assert that the element exist
      expect(textPlaceholderElement).toBeInTheDocument();
      expect(textPlaceholderElement).toHaveTextContent(
        /^Lorem ipsum dolor sit amet,/
      );

      fireEvent.click(textPlaceholderElement);
      // fireEvent.focus(textPlaceholderElement);

      expect(textPlaceholderElement).not.toBeInTheDocument();
    });

    test("user can type text into the  component", async () => {
      // Render your component
      const { container } = render(
        <Text
          body={mockState}
          setProp={setPropMock}
          setTabActive={setTabActive}
          setActiveComponent={setActiveComponent}
          selectedIcon={selectedIcon}
          componentType={componentType}
          setSelectedIcon={setSelectedIcon}
          setTextRef={setTextRef}
          portal={portal}
        />
      );

      // make editor active - otherwise it will show placeholder
      const textPlaceholderElement = screen.getByTestId("text-component");

      // Assert that the element exist
      expect(textPlaceholderElement).toBeInTheDocument();
      expect(textPlaceholderElement).toHaveTextContent(
        /^Lorem ipsum dolor sit amet,/
      );

      fireEvent.click(textPlaceholderElement);
      // fireEvent.focus(textPlaceholderElement);

      expect(textPlaceholderElement).not.toBeInTheDocument();

      const quilltextEditorElement = container.querySelector(".ql-editor");

      expect(quilltextEditorElement).toBeInTheDocument();

      fireEvent.change(quilltextEditorElement, {
        target: { innerHTML: "<p>H</p>" },
      });
      //   expect(quilltextEditorElement).toHaveValue("H");
      expect(quilltextEditorElement).toContainHTML("<p>H</p>");
    });

    // <------------Testing Text toolbar functionality------------>
    describe("Toolbar", () => {
      const childStateMock = { ops: [{ insert: "" }] };

      const componentTypeMock = "accordion";

      const childComponentMock = {
        id: "64a6dc8231b4696a03739df4",
        componentName: "Text",
        props: childStateMock,
        position: "w",
        __typename: "Component",
      };

      const setterFunctionMock = jest.fn();

      const setPropMock = jest.fn(() => {});

      //accordion default props is used as mockstate
      const parentStateMock = [
        {
          id: uuidv4(),
          title: "",
          components: [
            {
              componentName: "Text",
              componentProps: {
                body: { ops: [{ insert: "" }] },
              },
            },
          ],
          expanded: true,
        },
      ];

      const MockInteractivateProvider = ({ children }) => {
        return (
          <LayoutProvider layoutState={parentStateMock} setProp={setPropMock}>
            {children}
          </LayoutProvider>
        );
      };
      test("The component is clickable and show toolbar when clicked", async () => {
        const user = userEvent.setup();
        // Render the component
        render(
          <MockInteractivateProvider>
            <NestedComponentWrapper
              componentType={componentTypeMock}
              component={childComponentMock}
              compIndex={0}
              componentProps={parentStateMock[0].components[0].componentProps}
              tabIndex={0}
              numOfComponent={1}
              droppedIndex={0}
              setDroppedIndex={setterFunctionMock}
              setActiveComp={setterFunctionMock}
              activeComp={setterFunctionMock}
              setShowError={setterFunctionMock}
              setShowDropError={setterFunctionMock}
              setHoverMenu={setterFunctionMock}
              setActiveComponent={setterFunctionMock}
            />
          </MockInteractivateProvider>
        );
        // make editor active - otherwise it will show placeholder
        const textPlaceholderElement = screen.getByTestId("text-component");

        fireEvent.click(textPlaceholderElement);
        // fireEvent.focus(textPlaceholderElement);

        const textToolbar = screen.getByTestId("text-toolbar");
        expect(textToolbar).toBeInTheDocument();
      });
      test("The component hide toolbar when clicked outside", async () => {
        const user = userEvent.setup();
        // Render the component
        render(
          <MockInteractivateProvider>
            <NestedComponentWrapper
              componentType={componentTypeMock}
              component={childComponentMock}
              compIndex={0}
              componentProps={parentStateMock[0].components[0].componentProps}
              tabIndex={0}
              numOfComponent={1}
              droppedIndex={0}
              setDroppedIndex={setterFunctionMock}
              setActiveComp={setterFunctionMock}
              activeComp={setterFunctionMock}
              setShowError={setterFunctionMock}
              setShowDropError={setterFunctionMock}
              setHoverMenu={setterFunctionMock}
              setActiveComponent={setterFunctionMock}
            />
          </MockInteractivateProvider>
        );
        // make editor active - otherwise it will show placeholder
        const textPlaceholderElement = screen.getByTestId("text-component");

        fireEvent.click(textPlaceholderElement);
        // fireEvent.focus(textPlaceholderElement);

        const textToolbar = screen.getByTestId("text-toolbar");
        expect(textToolbar).toBeInTheDocument();
        const clickEvent = new MouseEvent("click", {
          bubbles: true,
          cancelable: true,
        });
        // Dispatch the event on the document
        document.dispatchEvent(clickEvent);

        // Trigger blur event- simulate clickoutside because above clickoutside mock dont lose focus
        textPlaceholderElement.blur();
        expect(textPlaceholderElement).not.toHaveFocus();

        expect(textPlaceholderElement).not.toBeInTheDocument();
      });

      test.skip("The text changes to bold when bold icon is clicked", async () => {
        const user = userEvent.setup();
        // Render the component
        render(
          <MockInteractivateProvider>
            <NestedComponentWrapper
              componentType={componentTypeMock}
              component={childComponentMock}
              compIndex={0}
              componentProps={parentStateMock[0].components[0].componentProps}
              tabIndex={0}
              numOfComponent={1}
              droppedIndex={0}
              setDroppedIndex={setterFunctionMock}
              setActiveComp={setterFunctionMock}
              activeComp={setterFunctionMock}
              setShowError={setterFunctionMock}
              setShowDropError={setterFunctionMock}
              setHoverMenu={setterFunctionMock}
              setActiveComponent={setterFunctionMock}
            />
          </MockInteractivateProvider>
        );

        // make editor active - otherwise it will show placeholder
        const textPlaceholderElement = screen.getByTestId("text-component");

        // Assert that the element exist
        expect(textPlaceholderElement).toBeInTheDocument();

        await user.click(textPlaceholderElement);
        // Assert that the element does not exist
        expect(textPlaceholderElement).not.toBeInTheDocument();

        const quilltextEditorElement = container
          .querySelector(".ql-editor")
          .getElementsByTagName("p")
          .item(0);

        expect(quilltextEditorElement).toBeInTheDocument();
        expect(quilltextEditorElement.innerHTML).toEqual("<br>");

        const textToolbar = screen.getByTestId("text-toolbar");
        expect(textToolbar).toBeInTheDocument();

        //--------------------------
        // Get the B option element using a data-testid
        const formattingButton = screen.getByTestId("formatting-dropdown");
        // Assert dropdown submenu is shown
        expect(formattingButton).toBeInTheDocument();

        await user.click(formattingButton);
        // click on B button to shwo drop-down menu
        // Assert dropdown toolbar for B button is shown
        const boldButton = screen.getByLabelText("bold");
        expect(boldButton).toBeInTheDocument();

        await user.click(boldButton);

        expect(quilltextEditorElement.innerHTML).toEqual(
          `<strong><span class="ql-cursor"></span></strong>`
        );

        //   // Assert that the text element has the expected style

        const quillEditor = new Quill(quilltextEditorElement);

        quillEditor.insertText(0, "Hello, world!");

        // fireEvent.change(targetElement, { target: { textContent: "Hello Sai" } });

        expect(quillEditor.root.innerHTML).toEqual(
          `<p><strong>Hello, world!</strong></p>`
        );
      });

      test.skip("The text changes to italic when italic icon is clicked", async () => {
        const user = userEvent.setup();
        // Render the component
        const { container } = render(
          <Text
            body={mockState}
            setProp={setPropMock}
            setTabActive={setTabActive}
            setActiveComponent={setActiveComponent}
            selectedIcon={selectedIcon}
            componentType={componentType}
            setSelectedIcon={setSelectedIcon}
            setTextRef={setTextRef}
            portal={portal}
          />
        );

        // make editor active - otherwise it will show placeholder
        const textPlaceholderElement = screen.getByTestId("text-component");

        // Assert that the element exist
        expect(textPlaceholderElement).toBeInTheDocument();

        await user.click(textPlaceholderElement);
        // Assert that the element does not exist
        expect(textPlaceholderElement).not.toBeInTheDocument();

        const quilltextEditorElement = container
          .querySelector(".ql-editor")
          .getElementsByTagName("p")
          .item(0);

        expect(quilltextEditorElement).toBeInTheDocument();
        expect(quilltextEditorElement.innerHTML).toEqual("<br>");

        const textToolbar = screen.getByTestId("text-toolbar");
        expect(textToolbar).toBeInTheDocument();

        //--------------------------
        // Get the B option element using a data-testid
        const formattingButton = screen.getByTestId("formatting-dropdown");
        // Assert dropdown submenu is shown
        expect(formattingButton).toBeInTheDocument();

        await user.click(formattingButton);
        // click on B button to shwo drop-down menu
        // Assert dropdown toolbar for B button is shown
        const italicButton = screen.getByLabelText("italic");
        expect(italicButton).toBeInTheDocument();

        await user.click(italicButton);

        expect(quilltextEditorElement.innerHTML).toContain(
          '<em><span class="ql-cursor"></span></em>'
        );

        //   // Assert that the text element has the expected style

        const quillEditor = new Quill(quilltextEditorElement);

        quillEditor.insertText(0, "Hello, world!");

        // fireEvent.change(targetElement, { target: { textContent: "Hello Sai" } });

        expect(quillEditor.root.innerHTML).toEqual(
          `<p><em>Hello, world!</em></p>`
        );
      });

      test.skip("The text changes to underline when underline icon is clicked", async () => {
        const user = userEvent.setup();
        // Render the component
        const { container } = render(
          <Text
            body={mockState}
            setProp={setPropMock}
            setTabActive={setTabActive}
            setActiveComponent={setActiveComponent}
            selectedIcon={selectedIcon}
            componentType={componentType}
            setSelectedIcon={setSelectedIcon}
            setTextRef={setTextRef}
            portal={portal}
          />
        );

        // make editor active - otherwise it will show placeholder
        const textPlaceholderElement = screen.getByTestId("text-component");

        // Assert that the element exist
        expect(textPlaceholderElement).toBeInTheDocument();

        await user.click(textPlaceholderElement);
        // Assert that the element does not exist
        expect(textPlaceholderElement).not.toBeInTheDocument();

        const quilltextEditorElement = container
          .querySelector(".ql-editor")
          .getElementsByTagName("p")
          .item(0);

        expect(quilltextEditorElement).toBeInTheDocument();
        expect(quilltextEditorElement.innerHTML).toEqual("<br>");

        const textToolbar = screen.getByTestId("text-toolbar");
        expect(textToolbar).toBeInTheDocument();

        //--------------------------
        // Get the B option element using a data-testid
        const formattingButton = screen.getByTestId("formatting-dropdown");
        // Assert dropdown submenu is shown
        expect(formattingButton).toBeInTheDocument();

        await user.click(formattingButton);
        // click on B button to shwo drop-down menu
        // Assert dropdown toolbar for B button is shown
        const underlineButton = screen.getByLabelText("underline");
        expect(underlineButton).toBeInTheDocument();

        await user.click(underlineButton);

        expect(quilltextEditorElement.innerHTML).toContain(
          '<u><span class="ql-cursor"></span></u>'
        );

        //   // Assert that the text element has the expected style

        const quillEditor = new Quill(quilltextEditorElement);

        quillEditor.insertText(0, "Hello, world!");

        // fireEvent.change(targetElement, { target: { textContent: "Hello Sai" } });

        expect(quillEditor.root.innerHTML).toEqual(`<u>Hello, world!</u>`);
      });

      test.skip("The text changes to sub script when sub script is clicked", async () => {
        const user = userEvent.setup();
        // Render the component
        const { container } = render(
          <Text
            body={mockState}
            setProp={setPropMock}
            setTabActive={setTabActive}
            setActiveComponent={setActiveComponent}
            selectedIcon={selectedIcon}
            componentType={componentType}
            setSelectedIcon={setSelectedIcon}
            setTextRef={setTextRef}
            portal={portal}
          />
        );

        // make editor active - otherwise it will show placeholder
        const textPlaceholderElement = screen.getByTestId("text-component");

        // Assert that the element exist
        expect(textPlaceholderElement).toBeInTheDocument();

        await user.click(textPlaceholderElement);
        // Assert that the element does not exist
        expect(textPlaceholderElement).not.toBeInTheDocument();

        const quilltextEditorElement = container
          .querySelector(".ql-editor")
          .getElementsByTagName("p")
          .item(0);

        expect(quilltextEditorElement).toBeInTheDocument();
        expect(quilltextEditorElement.innerHTML).toEqual("<br>");

        const textToolbar = screen.getByTestId("text-toolbar");
        expect(textToolbar).toBeInTheDocument();

        //--------------------------
        // Get the B option element using a data-testid
        const formattingButton = screen.getByTestId("formatting-dropdown");
        // Assert dropdown submenu is shown
        expect(formattingButton).toBeInTheDocument();

        await user.click(formattingButton);
        // click on B button to shwo drop-down menu
        // Assert dropdown toolbar for B button is shown
        const subScriptButton = screen.getByLabelText("sub script");
        expect(subScriptButton).toBeInTheDocument();

        await user.click(subScriptButton);

        expect(quilltextEditorElement.innerHTML).toContain(
          '<span class="ql-cursor"></span>'
        );

        //   // Assert that the text element has the expected style

        const quillEditor = new Quill(quilltextEditorElement);

        quillEditor.insertText(0, "Hello, world!");

        // fireEvent.change(targetElement, { target: { textContent: "Hello Sai" } });

        expect(quillEditor.root.innerHTML).toEqual(
          `<p><sub>Hello, world!</sub></p>`
        );
      });

      test.skip("The text changes to super script when super script icon is clicked", async () => {
        const user = userEvent.setup();
        // Render the component
        const { container } = render(
          <Text
            body={mockState}
            setProp={setPropMock}
            setTabActive={setTabActive}
            setActiveComponent={setActiveComponent}
            selectedIcon={selectedIcon}
            componentType={componentType}
            setSelectedIcon={setSelectedIcon}
            setTextRef={setTextRef}
            portal={portal}
          />
        );

        // make editor active - otherwise it will show placeholder
        const textPlaceholderElement = screen.getByTestId("text-component");

        // Assert that the element exist
        expect(textPlaceholderElement).toBeInTheDocument();

        await user.click(textPlaceholderElement);
        // Assert that the element does not exist
        expect(textPlaceholderElement).not.toBeInTheDocument();

        const quilltextEditorElement = container
          .querySelector(".ql-editor")
          .getElementsByTagName("p")
          .item(0);

        expect(quilltextEditorElement).toBeInTheDocument();
        expect(quilltextEditorElement.innerHTML).toEqual("<br>");

        const textToolbar = screen.getByTestId("text-toolbar");
        expect(textToolbar).toBeInTheDocument();

        //--------------------------
        // Get the B option element using a data-testid
        const formattingButton = screen.getByTestId("formatting-dropdown");
        // Assert dropdown submenu is shown
        expect(formattingButton).toBeInTheDocument();

        await user.click(formattingButton);
        // click on B button to shwo drop-down menu
        // Assert dropdown toolbar for B button is shown
        const superScriptButton = screen.getByLabelText("super script");
        expect(superScriptButton).toBeInTheDocument();

        await user.click(superScriptButton);

        expect(quilltextEditorElement.innerHTML).toContain(
          '<span class="ql-cursor"></span>'
        );

        //   // Assert that the text element has the expected style

        const quillEditor = new Quill(quilltextEditorElement);

        quillEditor.insertText(0, "Hello, world!");

        // fireEvent.change(targetElement, { target: { textContent: "Hello Sai" } });

        expect(quillEditor.root.innerHTML).toEqual(
          `<p><sup>Hello, world!</sup></p>`
        );
      });

      test.skip("The text changes to strike when strike icon is clicked", async () => {
        const user = userEvent.setup();
        // Render the component
        const { container } = render(
          <Text
            body={mockState}
            setProp={setPropMock}
            setTabActive={setTabActive}
            setActiveComponent={setActiveComponent}
            selectedIcon={selectedIcon}
            componentType={componentType}
            setSelectedIcon={setSelectedIcon}
            setTextRef={setTextRef}
            portal={portal}
          />
        );

        // make editor active - otherwise it will show placeholder
        const textPlaceholderElement = screen.getByTestId("text-component");

        // Assert that the element exist
        expect(textPlaceholderElement).toBeInTheDocument();

        await user.click(textPlaceholderElement);
        // Assert that the element does not exist
        expect(textPlaceholderElement).not.toBeInTheDocument();

        const quilltextEditorElement = container
          .querySelector(".ql-editor")
          .getElementsByTagName("p")
          .item(0);

        expect(quilltextEditorElement).toBeInTheDocument();
        expect(quilltextEditorElement.innerHTML).toEqual("<br>");

        const textToolbar = screen.getByTestId("text-toolbar");
        expect(textToolbar).toBeInTheDocument();

        //--------------------------
        // Get the B option element using a data-testid
        const formattingButton = screen.getByTestId("formatting-dropdown");
        // Assert dropdown submenu is shown
        expect(formattingButton).toBeInTheDocument();

        await user.click(formattingButton);
        // click on B button to shwo drop-down menu
        // Assert dropdown toolbar for B button is shown
        const strikeButton = screen.getByLabelText("strike");
        expect(strikeButton).toBeInTheDocument();

        await user.click(strikeButton);

        expect(quilltextEditorElement.innerHTML).toContain(
          '<s><span class="ql-cursor"></span></s>'
        );

        //   // Assert that the text element has the expected style

        const quillEditor = new Quill(quilltextEditorElement);

        quillEditor.insertText(0, "Hello, world!");

        // fireEvent.change(targetElement, { target: { textContent: "Hello Sai" } });

        expect(quillEditor.root.innerHTML).toEqual(
          `<p><s>Hello, world!</s></p>`
        );
      });

      test("The text content is displayed with the correct center alignment when alignment icon is clicked", async () => {
        const user = userEvent.setup();
        // Render the component
        const { container } = render(
          <MockInteractivateProvider>
            <NestedComponentWrapper
              componentType={componentTypeMock}
              component={childComponentMock}
              compIndex={0}
              componentProps={parentStateMock[0].components[0].componentProps}
              tabIndex={0}
              numOfComponent={1}
              droppedIndex={0}
              setDroppedIndex={setterFunctionMock}
              setActiveComp={setterFunctionMock}
              activeComp={setterFunctionMock}
              setShowError={setterFunctionMock}
              setShowDropError={setterFunctionMock}
              setHoverMenu={setterFunctionMock}
              setActiveComponent={setterFunctionMock}
            />
          </MockInteractivateProvider>
        );

        // make editor active - otherwise it will show placeholder
        const textPlaceholderElement = screen.getByTestId("text-component");

        // Assert that the element exist
        expect(textPlaceholderElement).toBeInTheDocument();

        await user.click(textPlaceholderElement);
        // Assert that the element does not exist
        expect(textPlaceholderElement).not.toBeInTheDocument();

        const quilltextEditorElement = container
          .querySelector(".ql-editor")
          .getElementsByTagName("p")
          .item(0);

        expect(quilltextEditorElement).toBeInTheDocument();
        expect(quilltextEditorElement.innerHTML).toEqual("<br>");

        const textToolbar = screen.getByTestId("text-toolbar");
        expect(textToolbar).toBeInTheDocument();

        //--------------------------
        // Get the B option element using a data-testid
        const alignmentButton = screen.getByLabelText("alignment dropdown");
        // Assert dropdown submenu is shown
        expect(alignmentButton).toBeInTheDocument();
        // click on alignment button to shwo drop-down menu
        await user.click(alignmentButton);

        // Assert dropdown toolbar for center button is shown
        const centerButton = screen.getByLabelText("align center");
        expect(centerButton).toBeInTheDocument();

        // click on center alignment button to change alignment
        await user.click(centerButton);

        const centerPElement = container.querySelector(".ql-align-center");

        expect(centerPElement).toBeInTheDocument();
      });

      test("The text content is displayed with the correct right alignment when alignment icon is clicked", async () => {
        const user = userEvent.setup();
        // Render the component
        const { container } = render(
          <MockInteractivateProvider>
            <NestedComponentWrapper
              componentType={componentTypeMock}
              component={childComponentMock}
              compIndex={0}
              componentProps={parentStateMock[0].components[0].componentProps}
              tabIndex={0}
              numOfComponent={1}
              droppedIndex={0}
              setDroppedIndex={setterFunctionMock}
              setActiveComp={setterFunctionMock}
              activeComp={setterFunctionMock}
              setShowError={setterFunctionMock}
              setShowDropError={setterFunctionMock}
              setHoverMenu={setterFunctionMock}
              setActiveComponent={setterFunctionMock}
            />
          </MockInteractivateProvider>
        );

        // make editor active - otherwise it will show placeholder
        const textPlaceholderElement = screen.getByTestId("text-component");

        // Assert that the element exist
        expect(textPlaceholderElement).toBeInTheDocument();

        await user.click(textPlaceholderElement);
        // Assert that the element does not exist
        expect(textPlaceholderElement).not.toBeInTheDocument();

        const quilltextEditorElement = container
          .querySelector(".ql-editor")
          .getElementsByTagName("p")
          .item(0);

        expect(quilltextEditorElement).toBeInTheDocument();
        expect(quilltextEditorElement.innerHTML).toEqual("<br>");

        const textToolbar = screen.getByTestId("text-toolbar");
        expect(textToolbar).toBeInTheDocument();

        //--------------------------
        // Get the B option element using a data-testid
        const alignmentButton = screen.getByLabelText("alignment dropdown");
        // Assert dropdown submenu is shown
        expect(alignmentButton).toBeInTheDocument();
        // click on alignment button to shwo drop-down menu
        await user.click(alignmentButton);

        // Assert dropdown toolbar for center button is shown
        const rightButton = screen.getByLabelText("align right");
        expect(rightButton).toBeInTheDocument();

        // click on center alignment button to change alignment
        await user.click(rightButton);

        const rightPElement = container.querySelector(".ql-align-right");

        expect(rightPElement).toBeInTheDocument();
      });

      test("The text content is displayed with the correct left alignment when alignment icon is clicked", async () => {
        const user = userEvent.setup();
        // Render the component
        const { container } = render(
          <MockInteractivateProvider>
            <NestedComponentWrapper
              componentType={componentTypeMock}
              component={childComponentMock}
              compIndex={0}
              componentProps={parentStateMock[0].components[0].componentProps}
              tabIndex={0}
              numOfComponent={1}
              droppedIndex={0}
              setDroppedIndex={setterFunctionMock}
              setActiveComp={setterFunctionMock}
              activeComp={setterFunctionMock}
              setShowError={setterFunctionMock}
              setShowDropError={setterFunctionMock}
              setHoverMenu={setterFunctionMock}
              setActiveComponent={setterFunctionMock}
            />
          </MockInteractivateProvider>
        );

        // make editor active - otherwise it will show placeholder
        const textPlaceholderElement = screen.getByTestId("text-component");

        // Assert that the element exist
        expect(textPlaceholderElement).toBeInTheDocument();

        await user.click(textPlaceholderElement);
        // Assert that the element does not exist
        expect(textPlaceholderElement).not.toBeInTheDocument();

        const quilltextEditorElement = container
          .querySelector(".ql-editor")
          .getElementsByTagName("p")
          .item(0);

        expect(quilltextEditorElement).toBeInTheDocument();
        expect(quilltextEditorElement.innerHTML).toEqual("<br>");

        const textToolbar = screen.getByTestId("text-toolbar");
        expect(textToolbar).toBeInTheDocument();

        //--------------------------
        // Get the B option element using a data-testid
        const alignmentButton = screen.getByLabelText("alignment dropdown");
        // Assert dropdown submenu is shown
        expect(alignmentButton).toBeInTheDocument();
        // click on alignment button to shwo drop-down menu
        await user.click(alignmentButton);

        // Assert dropdown toolbar for center button is shown
        const leftButton = screen.getByLabelText("align left");
        expect(leftButton).toBeInTheDocument();

        // click on center alignment button to change alignment
        await user.click(leftButton);

        const rightPElement = container.querySelector(".ql-align-left");
        const centerPElement = container.querySelector(".ql-align-left");

        expect(rightPElement).not.toBeInTheDocument();
        expect(centerPElement).not.toBeInTheDocument();
      });

      test("The text content is turned to unordered list when list dropdown icon is clicked", async () => {
        const user = userEvent.setup();
        // Render the component
        const { container } = render(
          <MockInteractivateProvider>
            <NestedComponentWrapper
              componentType={componentTypeMock}
              component={childComponentMock}
              compIndex={0}
              componentProps={parentStateMock[0].components[0].componentProps}
              tabIndex={0}
              numOfComponent={1}
              droppedIndex={0}
              setDroppedIndex={setterFunctionMock}
              setActiveComp={setterFunctionMock}
              activeComp={setterFunctionMock}
              setShowError={setterFunctionMock}
              setShowDropError={setterFunctionMock}
              setHoverMenu={setterFunctionMock}
              setActiveComponent={setterFunctionMock}
            />
          </MockInteractivateProvider>
        );

        // make editor active - otherwise it will show placeholder
        const textPlaceholderElement = screen.getByTestId("text-component");

        // Assert that the element exist
        expect(textPlaceholderElement).toBeInTheDocument();

        await user.click(textPlaceholderElement);
        // Assert that the element does not exist
        expect(textPlaceholderElement).not.toBeInTheDocument();

        const quilltextEditorElement = container
          .querySelector(".ql-editor")
          .getElementsByTagName("p")
          .item(0);

        expect(quilltextEditorElement).toBeInTheDocument();
        expect(quilltextEditorElement.innerHTML).toEqual("<br>");

        const textToolbar = screen.getByTestId("text-toolbar");
        expect(textToolbar).toBeInTheDocument();

        //--------------------------
        // Get the B option element using a data-testid
        const listButton = screen.getByLabelText("list options");
        // Assert dropdown submenu is shown
        expect(listButton).toBeInTheDocument();
        // click on alignment button to shwo drop-down menu
        await user.click(listButton);

        // Assert dropdown toolbar for center button is shown
        const unOrderButton = screen.getByLabelText("bullet list");
        expect(unOrderButton).toBeInTheDocument();

        // click on center alignment button to change alignment
        await user.click(unOrderButton);
        // Check if there are <li> elements within the <ul> element
        const liElements = container.querySelectorAll("ul li");
        expect(liElements.length).toBeGreaterThan(0);
      });

      test("The text content is turned to ordered list when list dropdown icon is clicked", async () => {
        const user = userEvent.setup();
        // Render the component
        const { container } = render(
          <MockInteractivateProvider>
            <NestedComponentWrapper
              componentType={componentTypeMock}
              component={childComponentMock}
              compIndex={0}
              componentProps={parentStateMock[0].components[0].componentProps}
              tabIndex={0}
              numOfComponent={1}
              droppedIndex={0}
              setDroppedIndex={setterFunctionMock}
              setActiveComp={setterFunctionMock}
              activeComp={setterFunctionMock}
              setShowError={setterFunctionMock}
              setShowDropError={setterFunctionMock}
              setHoverMenu={setterFunctionMock}
              setActiveComponent={setterFunctionMock}
            />
          </MockInteractivateProvider>
        );

        // make editor active - otherwise it will show placeholder
        const textPlaceholderElement = screen.getByTestId("text-component");

        // Assert that the element exist
        expect(textPlaceholderElement).toBeInTheDocument();

        await user.click(textPlaceholderElement);
        // Assert that the element does not exist
        expect(textPlaceholderElement).not.toBeInTheDocument();

        const quilltextEditorElement = container
          .querySelector(".ql-editor")
          .getElementsByTagName("p")
          .item(0);

        expect(quilltextEditorElement).toBeInTheDocument();
        expect(quilltextEditorElement.innerHTML).toEqual("<br>");

        const textToolbar = screen.getByTestId("text-toolbar");
        expect(textToolbar).toBeInTheDocument();

        //--------------------------
        // Get the B option element using a data-testid
        const listButton = screen.getByLabelText("list options");
        // Assert dropdown submenu is shown
        expect(listButton).toBeInTheDocument();
        // click on alignment button to shwo drop-down menu
        await user.click(listButton);

        // Assert dropdown toolbar for center button is shown
        const OrderButton = screen.getByLabelText("numbered list");
        expect(OrderButton).toBeInTheDocument();

        // click on center alignment button to change alignment
        await user.click(OrderButton);
        // Check if there are <li> elements within the <ul> element
        const liElements = container.querySelectorAll("ol li");
        expect(liElements.length).toBeGreaterThan(0);
      });
    });
  });
});
