import "@testing-library/jest-dom";
import userEvent from "@testing-library/user-event";

import { v4 as uuidv4 } from "uuid";

import { render, screen, fireEvent, act } from "../TestUtility/renderUtils";
import InfoBox from "../components/InfoBox/subcomponents/InfoBox";
import InfoBoxMain, {
  defaultProps as infoboxDefaultProps,
} from "../components/InfoBox/InfoBoxMain";
import MultiColumnMain, {
  defaultProps as multicolumnDefaultProps,
} from "../components/MultiColumn/MultiColumnMain";

import Header from "../components/InfoBox/subcomponents/Header";
import Label from "../components/InfoBox/subcomponents/Label";
import { FORMATS } from "../authoring/context/AuthoringExperienceFormatContext";

describe("MultiColumn tests", () => {
  test("It renders with no children", () => {
    render(<MultiColumnMain {...multicolumnDefaultProps} />);
    // Confirm renders with no errors
    expect(screen.getByTestId("multi-column-component")).toBeInTheDocument();
    // Check for two dropzones
    expect(screen.queryAllByTestId("multi-column-dropzone")).toHaveLength(2);
  });
  test("It renders with image component children", () => {
    const mockMultiColumnProps = {
      columnWidth: "50:50",
      verticalAlignment: "top",
      layoutState: [
        {
          id: "c6062d01-f807-45cb-b3a7-4cb130af443c",
          components: [
            {
              componentName: "Image",
              componentProps: {
                imgSize: {
                  width: 100,
                  height: 100,
                },
                uploadedImg: null,
                imgLink: null,
                alt: "",
                longDesc: {
                  ops: [
                    {
                      insert: "I am a description\n",
                    },
                  ],
                },
                caption: null,
                credit: "",
                imageTextSettings: {
                  description: true,
                  credit: true,
                  fullWidth: false,
                },
                customAltTextOptionSelected: "Custom alt text",
              },
              formats: {
                digital: {
                  shown: true,
                },
                print: {
                  shown: true,
                },
              },
            },
            {
              componentName: "Image",
              componentProps: {
                imgSize: {
                  width: 100,
                  height: 100,
                },
                uploadedImg: null,
                alt: "",
                customAltTextOptionSelected: "Custom alt text",
                longDesc: "",
                credit: "",
                imageTextSettings: {
                  description: true,
                  credit: true,
                  fullWidth: false,
                },
                imgLink: null,
                caption: null,
              },
              formats: {
                digital: {
                  shown: true,
                },
                print: {
                  shown: true,
                },
              },
            },
          ],
        },
        {
          id: "04abf2d4-1622-4003-bc76-ff2dc905b47a",
          components: [],
        },
      ],
    };

    render(<MultiColumnMain {...mockMultiColumnProps} />);
    expect(screen.getByTestId("multi-column-component")).toBeInTheDocument();
    expect(screen.queryAllByTestId("multi-column-dropzone")).toHaveLength(2);

    // Check for the two image components being rendered
    expect(screen.queryAllByTestId("image-wrapper")).toHaveLength(2);
  });
});
