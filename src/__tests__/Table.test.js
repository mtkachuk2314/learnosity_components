/**
 *  Tables Unit Tests
 *
 *
 *  Test categories:
 *         ** Functionality --> ensure the component is functioning as expected
 *         ** Performance --> test the speed and efficiency of the component
 *         ** Security --> test the security of the component
 *         ** Accessibility --> test accessible for screen readers and keyboard navigation
 *
 */

import "@testing-library/jest-dom";
import { render, screen, fireEvent } from "../TestUtility/renderUtils";
import TableMain from "../components/Table/TableMain";
import Table from "../components/Table/subcomponents/Table";
import { LayoutProvider } from "../components/Table/TableContext";

// Mocked uuid library
jest.mock("uuid", () => ({
  v4: () => "123456789",
}));

// Mocked context values
let mockState = {
  headers: [
    { accessorKey: "column1", id: "column1", header: "" },
    { accessorKey: "column2", id: "column2", header: "" },
  ],
  data: [
    {
      column1: {
        value: "",
        type: "title",
        horizontalAlignment: "center-align",
        verticalAlignment: "middle-align",
      },
      column2: {
        value: "1",
        type: "title",
        horizontalAlignment: "center-align",
        verticalAlignment: "middle-align",
      },
    },
    {
      column1: {
        value: "2",
        type: "cell",
        horizontalAlignment: "left-align",
        verticalAlignment: "middle-align",
      },
      column2: {
        value: "2",
        type: "cell",
        horizontalAlignment: "left-align",
        verticalAlignment: "middle-align",
      },
    },
  ],
  headerType: "top-header",
  hideTopHeader: false,
  hideSideHeader: false,
  showStripes: false,
};

let timeline = {
  mockInversePatches: [],
  current: mockState,
  mockForwardPatches: [],
};

// should mock undo base on inverse array
const undoMock = jest.fn(() => {
  if (timeline.mockInversePatches.length > 0) {
    const { mockInversePatches, current, mockForwardPatches } = timeline;
    const length = mockInversePatches.length;
    const lastItem = mockInversePatches[length - 1];
    const restOfArr = mockInversePatches.slice(0, length - 1);
    timeline = {
      mockInversePatches: restOfArr,
      current: lastItem,
      mockForwardPatches: [...mockForwardPatches, current],
    };
  }
});

// should mock redo base on inverse array
const redoMock = jest.fn(() => {
  if (timeline.mockForwardPatches.length > 0) {
    const { mockInversePatches, current, mockForwardPatches } = timeline;
    const length = mockForwardPatches.length;
    const lastItem = mockForwardPatches[length - 1];
    const restOfArr = mockForwardPatches.slice(0, length - 1);
    timeline = {
      mockInversePatches: [...mockInversePatches, current],
      current: lastItem,
      mockForwardPatches: restOfArr,
    };
  }
});

// should mock updating state in authoring - which can change layoutState base on inverse and forward patches arrays
const setPropMock = jest.fn((newState) => {
  console.log("Wilson", newState);
  const { layoutState } = newState;
  const { mockInversePatches, current, mockForwardPatches } = timeline;
  timeline = {
    mockInversePatches:
      JSON.stringify(current) === JSON.stringify(layoutState)
        ? [...mockInversePatches]
        : [...mockInversePatches, current],
    current: layoutState,
    // this seems to be the idiomatic approach for most applications
    mockForwardPatches: [...mockForwardPatches],
  };
});

const MockProvider = ({ children, layoutState }) => {
  return (
    <LayoutProvider layoutState={layoutState} setProp={setPropMock}>
      {children}
    </LayoutProvider>
  );
};

describe("Tables Component", () => {
  describe("Functionality", () => {
    beforeEach(() => {
      jest.useFakeTimers();
      timeline = {
        mockInversePatches: [],
        current: mockState,
        mockForwardPatches: [],
      };
    });

    afterEach(() => {
      jest.clearAllMocks();
      jest.clearAllTimers();
      timeline = {
        mockInversePatches: [],
        current: mockState,
        mockForwardPatches: [],
      };
    });
    jest.setTimeout(10000);
    test("Changing table title should group fast actions and undo action removes a group of characters", () => {
      // Render the component
      render(<TableMain layoutState={mockState} setProp={setPropMock} />);

      // make pane active - otherwise it wont find title element
      const tablePaneElement = screen.getByTestId("row1-column1");
      fireEvent.click(tablePaneElement);

      // Get the input element using a data-testid or other selector
      const tableTitleElement = screen.getByTestId("row1-1");
      // Simulate user typing - check if setProp debounce properlly

      // grouping sequence : 1:) HI2 Hello  !

      fireEvent.change(tableTitleElement, { target: { value: "1" } });

      setTimeout(() => {}, 200);
      jest.advanceTimersByTime(200);

      fireEvent.change(tableTitleElement, {
        target: { value: `${tableTitleElement.value}:` },
      });
      fireEvent.change(tableTitleElement, {
        target: { value: `${tableTitleElement.value})` },
      });

      //need delay of a second
      setTimeout(() => {
        fireEvent.change(tableTitleElement, {
          target: { value: `${tableTitleElement.value}H` },
        });
        fireEvent.change(tableTitleElement, {
          target: { value: `${tableTitleElement.value}I` },
        });
      }, 1000);

      jest.advanceTimersByTime(1000);

      fireEvent.change(tableTitleElement, {
        target: { value: `${tableTitleElement.value}2` },
      });

      //need delay of a second
      // so following sequence should consider as a group of chars
      setTimeout(() => {
        fireEvent.change(tableTitleElement, {
          target: { value: `${tableTitleElement.value}H` },
        });
        fireEvent.change(tableTitleElement, {
          target: { value: `${tableTitleElement.value}E` },
        });
        fireEvent.change(tableTitleElement, {
          target: { value: `${tableTitleElement.value}L` },
        });
        fireEvent.change(tableTitleElement, {
          target: { value: `${tableTitleElement.value}L` },
        });
        fireEvent.change(tableTitleElement, {
          target: { value: `${tableTitleElement.value}O` },
        });
      }, 1000);

      jest.advanceTimersByTime(1000);

      //need delay of half second
      setTimeout(() => {
        fireEvent.change(tableTitleElement, {
          target: { value: `${tableTitleElement.value}!` },
        });
      }, 1000);
      jest.advanceTimersByTime(1000);

      setTimeout(() => {}, 1000);
      jest.advanceTimersByTime(1000);

      console.log(timeline.mockForwardPatches);
      console.log(timeline.mockInversePatches);
      console.log("----->", timeline.current.data[0].column1);
      console.log(mockState);

      // ----------------------------undo-----------------------------------//
      // //undo act -> check debouncing and undo functionality
      // // - hit undo by user - call undo mock
      undoMock();

      console.log(timeline.current.data);

      fireEvent.change(tableTitleElement, {
        target: { value: timeline.current.data[0].column1.value },
      });

      setTimeout(() => {}, 1000);
      jest.advanceTimersByTime(1000);

      // // //assertion
      expect(tableTitleElement).toHaveValue("1:)HI2HELLO");

      undoMock();

      fireEvent.change(tableTitleElement, {
        target: { value: timeline.current.data[0].column1.value },
      });

      //assertion
      expect(tableTitleElement).toHaveValue("1:)HI2");

      undoMock();

      fireEvent.change(tableTitleElement, {
        target: { value: timeline.current.data[0].column1.value },
      });

      //assertion
      expect(tableTitleElement).toHaveValue("1:)");
      expect(tableTitleElement).not.toHaveValue("1:)H");

      // ----------------------------redo-----------------------------------//
      redoMock();

      fireEvent.change(tableTitleElement, {
        target: { value: timeline.current.data[0].column1.value },
      });
      //assertion
      expect(tableTitleElement).toHaveValue("1:)HI2");

      redoMock();

      fireEvent.change(tableTitleElement, {
        target: { value: timeline.current.data[0].column1.value },
      });

      //assertion
      expect(tableTitleElement).toHaveValue("1:)HI2HELLO");

      // ----------------------------undo/redo-----------------------------------//

      undoMock();

      fireEvent.change(tableTitleElement, {
        target: { value: timeline.current.data[0].column1.value },
      });

      //assertion
      expect(tableTitleElement).toHaveValue("1:)HI2");

      redoMock();

      fireEvent.change(tableTitleElement, {
        target: { value: timeline.current.data[0].column1.value },
      });

      //assertion
      expect(tableTitleElement).toHaveValue("1:)HI2HELLO");

      redoMock();

      fireEvent.change(tableTitleElement, {
        target: { value: timeline.current.data[0].column1.value },
      });

      //assertion
      expect(tableTitleElement).toHaveValue("1:)HI2HELLO!");

      redoMock();

      fireEvent.change(tableTitleElement, {
        target: { value: timeline.current.data[0].column1.value },
      });

      //assertion
      expect(tableTitleElement).toHaveValue("1:)HI2HELLO!");
    });
    // <------------Testing updates - debounced & none debounced & undo/redo & race condition------------>
    test("Changing table title should qroup user input character and debounce state update", () => {
      // Render the component
      render(
        <MockProvider layoutState={mockState}>
          <Table />
        </MockProvider>
      );

      // make pane active - otherwise it wont find title element
      const tablePaneElement = screen.getByTestId("row1-column1");
      fireEvent.click(tablePaneElement);

      // Get the input element using a data-testid or other selector
      let tableTitleElement = screen.getByTestId("row1-1");
      // Simulate user typing - check if setProp debounce properlly

      // grouping sequence : 1:) HI2 Hello  !

      fireEvent.change(tableTitleElement, { target: { value: "1" } });

      // setprop shoul not call before passing 500 milisec
      expect(setPropMock).not.toHaveBeenCalled();

      setTimeout(() => {}, 200);
      jest.advanceTimersByTime(200);

      fireEvent.change(tableTitleElement, {
        target: { value: `${tableTitleElement.value}:` },
      });
      fireEvent.change(tableTitleElement, {
        target: { value: `${tableTitleElement.value})` },
      });

      expect(tableTitleElement).toHaveValue("1:)");

      expect(setPropMock).not.toHaveBeenCalled();

      //need delay of a second
      setTimeout(() => {
        fireEvent.change(tableTitleElement, {
          target: { value: `${tableTitleElement.value}H` },
        });
        fireEvent.change(tableTitleElement, {
          target: { value: `${tableTitleElement.value}I` },
        });
      }, 1000);

      jest.advanceTimersByTime(1000);
      expect(setPropMock).toHaveBeenCalledTimes(1);

      fireEvent.change(tableTitleElement, {
        target: { value: `${tableTitleElement.value}2` },
      });

      expect(setPropMock).toHaveBeenCalledTimes(1);

      //need delay of a second
      // so following sequence should consider as a group of chars
      setTimeout(() => {
        fireEvent.change(tableTitleElement, {
          target: { value: `${tableTitleElement.value}H` },
        });
        fireEvent.change(tableTitleElement, {
          target: { value: `${tableTitleElement.value}E` },
        });
        fireEvent.change(tableTitleElement, {
          target: { value: `${tableTitleElement.value}L` },
        });
        fireEvent.change(tableTitleElement, {
          target: { value: `${tableTitleElement.value}L` },
        });
        fireEvent.change(tableTitleElement, {
          target: { value: `${tableTitleElement.value}O` },
        });
      }, 1000);

      jest.advanceTimersByTime(1000);
      expect(setPropMock).toHaveBeenCalledTimes(2);

      //need delay of a second
      setTimeout(() => {
        fireEvent.change(tableTitleElement, {
          target: { value: `${tableTitleElement.value}!` },
        });
      }, 1000);
      jest.advanceTimersByTime(1000);
      expect(setPropMock).toHaveBeenCalledTimes(3);

      // make delay to have latest version in console.log
      setTimeout(() => {}, 1000);
      jest.advanceTimersByTime(1000);

      console.log(timeline.mockForwardPatches);
      console.log(timeline.mockInversePatches);
      console.log("----->", timeline.current);
      console.log(mockState);
    });
  });
});
