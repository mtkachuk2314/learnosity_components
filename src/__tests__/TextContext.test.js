/**
 *  Text context Unit Tests
 */
import React from "react";
import "@testing-library/jest-dom";
import { render, screen, waitFor } from "@testing-library/react";
import { renderHook } from "@testing-library/react-hooks";
import { choose, randomInt, runFuzz } from "../TestUtility/textComponentUtils";
import Delta, { AttributeMap, Op } from "quill-delta";
import Quill from "quill";
import userEvent from "@testing-library/user-event";
import TextProvider, {
  TextContext,
  useTextContext,
} from "../components/Text/TextContext";

// Mocked context values (null)
const mockState = { body: { ops: [{ insert: "" }] } };

const setPropMock = jest.fn();

const BLOCK_EMBED_NAME = "video";
const INLINE_EMBED_NAME = "image";

const attributeDefs = {
  text: [
    { name: "color", values: ["#ffffff", "#000000", "#ff0000", "#ffff00"] },
    { name: "bold", values: [true] },
    { name: "code", values: [true] },
  ],
  newline: [
    // @ts-expect-error
    { name: "align", values: "center" },
    { name: "header", values: [1, 2, 3, 4, 5] },
    { name: "blockquote", values: [true] },
    { name: "list", values: ["ordered", "bullet", "checked", "unchecked"] },
  ],
  inlineEmbed: [
    { name: "width", values: ["100", "200", "300"] },
    { name: "height", values: ["100", "200", "300"] },
  ],
  blockEmbed: [
    { name: "align", values: ["center", "right"] },
    { name: "width", values: ["100", "200", "300"] },
    { name: "height", values: ["100", "200", "300"] },
  ],
};

const isLineFinished = (delta) => {
  const lastOp = delta.ops[delta.ops.length - 1];
  if (!lastOp) return false;
  if (typeof lastOp.insert === "string") {
    return lastOp.insert.endsWith("\n");
  }
  if (typeof lastOp.insert === "object") {
    const key = Object.keys(lastOp.insert)[0];
    return key === BLOCK_EMBED_NAME;
  }
  throw new Error("invalid op");
};

// generate attribute
const generateAttributes = (scope) => {
  const attributeCount =
    scope === "newline"
      ? // Some block-level formats are exclusive so we only pick one for now for simplicity
        choose([0, 0, 1])
      : choose([0, 0, 0, 0, 0, 1, 2, 3, 4]);
  const attributes = {};
  for (let i = 0; i < attributeCount; i += 1) {
    const def = choose(attributeDefs[scope]);
    attributes[def.name] = choose(def.values);
  }
  return attributes;
};

// generate a random text
const generateRandomText = () => {
  return choose([
    "hi",
    "world",
    "Slab",
    " ",
    "Hello Mahboobeh",
    "this is a long text that contains spaces",
  ]);
};

const generateSingleInsertDelta = () => {
  const operation = choose(["text", "text", "text"]);

  let insert;
  switch (operation) {
    case "text":
      insert = generateRandomText();
      break;
    case "newline":
      insert = "\n";
      break;
    case "inlineEmbed":
      insert = { [INLINE_EMBED_NAME]: "https://example.com" };
      break;
    case "blockEmbed": {
      insert = { [BLOCK_EMBED_NAME]: "https://example.com" };
      break;
    }
  }
  const attributes = generateAttributes(operation);
  const op = { insert };
  if (Object.keys(attributes).length) {
    op.attributes = attributes;
  }
  return op;
};

const safePushInsert = (delta) => {
  const op = generateSingleInsertDelta();
  if (typeof op.insert === "object" && op.insert[BLOCK_EMBED_NAME]) {
    delta.insert("\n");
  }
  delta.push(op);
};

//create a random delta object
const generateDocument = () => {
  const delta = new Delta();
  const operationCount = 2 + randomInt(20);
  for (let i = 0; i < operationCount; i += 1) {
    safePushInsert(delta);
  }
  if (!isLineFinished(delta)) {
    delta.insert("\n");
  }
  return delta;
};
// make change
const generateChange = (
  doc,
  changeCount,
  allowedActions = ["insert", "delete", "retain"]
) => {
  const docLength = doc.length();
  const skipLength = allowedActions.includes("retain")
    ? randomInt(docLength)
    : 0;
  let change = new Delta().retain(skipLength);
  const action = choose(allowedActions);
  const nextOp = doc.slice(skipLength).ops[0];
  if (!nextOp) throw new Error("nextOp expected");
  const needNewline = !isLineFinished(doc.slice(0, skipLength));
  switch (action) {
    case "insert": {
      const delta = new Delta();
      const operationCount = randomInt(5) + 1;
      for (let i = 0; i < operationCount; i += 1) {
        safePushInsert(delta);
      }
      if (
        needNewline ||
        (typeof nextOp.insert === "object" && !!nextOp.insert[BLOCK_EMBED_NAME])
      ) {
        delta.insert("\n");
      }
      change = change.concat(delta);
      break;
    }
    case "delete": {
      const lengthToDelete = randomInt(docLength - skipLength - 1) + 1;
      const nextOpAfterDelete = doc.slice(skipLength + lengthToDelete).ops[0];
      if (
        needNewline &&
        (!nextOpAfterDelete ||
          (typeof nextOpAfterDelete.insert === "object" &&
            !!nextOpAfterDelete.insert[BLOCK_EMBED_NAME]))
      ) {
        change.insert("\n");
      }
      change.delete(lengthToDelete);
      break;
    }
    case "retain": {
      const retainLength =
        typeof nextOp.insert === "string"
          ? randomInt(nextOp.insert.length - 1) + 1
          : 1;
      if (typeof nextOp.insert === "string") {
        if (
          nextOp.insert.includes("\n") &&
          nextOp.insert.replace(/\n/g, "").length
        ) {
          break;
        }
        if (nextOp.insert.includes("\n")) {
          change.retain(
            retainLength,
            AttributeMap.diff(nextOp.attributes, generateAttributes("newline"))
          );
        } else {
          change.retain(
            retainLength,
            AttributeMap.diff(nextOp.attributes, generateAttributes("text"))
          );
        }
        break;
      }
      break;
    }
  }
  changeCount -= 1;
  return changeCount <= 0
    ? change
    : change.compose(
        generateChange(doc.compose(change), changeCount, allowedActions)
      );
};

describe("Text Component Context API", () => {
  describe("Test reducer function", () => {
    afterEach(() => {
      jest.clearAllMocks();
    });

    test("The initial state (null value) of component is correctly set (context-style-math equation)  - UPDATE_STATE", () => {
      // const quill = new Quill(document.createElement("div"));
      // quill.setContents(mockState.body);
      // console.log("---->", quill.getText());
      render(
        <TextProvider textState={mockState.body} setProp={setPropMock}>
          <TextContext.Consumer>
            {([state, dispatch]) => {
              const quill = new Quill(document.createElement("div"));
              quill.setContents(state);
              return (
                <>
                  <span>
                    The initial text is{" "}
                    {quill.getText() === "\n" ? "null" : quill.getText()}
                  </span>
                </>
              );
            }}
          </TextContext.Consumer>
        </TextProvider>
      );
      // context check assersion
      expect(screen.getByText(/The initial text is null/i)).toBeTruthy();
    });

    test("The initial state (none-null value) of component is correctly set (context-style-math equation)  - UPDATE_STATE", async () => {
      //return delta obj
      const randomDeltaObject = generateDocument();
      // const dumyQuill = new Quill(document.createElement("div"));
      // dumyQuill.setContents(randomDeltaObject);
      // const dumyText = dumyQuill.getText();
      render(
        <TextProvider textState={randomDeltaObject} setProp={setPropMock}>
          <TextContext.Consumer>
            {([state, dispatch]) => {
              const quill = new Quill(document.createElement("div"));
              quill.setContents(state);
              return (
                <>
                  <span>{quill.getText()}</span>
                </>
              );
            }}
          </TextContext.Consumer>
        </TextProvider>
      );
      // context check assersion
      expect(
        screen.getByText(
          /this is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spaces/i
        )
      ).toBeInTheDocument();
      // initial style assersion ??
      // mathpix assersion ??
    });

    test("Test UPDATE_STATE reducer", async () => {
      const user = userEvent.setup();
      render(
        <TextProvider textState={mockState.body} setProp={setPropMock}>
          <TextContext.Consumer>
            {([state, dispatch]) => {
              const quill = new Quill(document.createElement("div"));
              quill.setContents(state);
              return (
                <>
                  <span>
                    The initial text is{" "}
                    {quill.getText() === "\n" ? "null" : quill.getText()}
                  </span>
                  <button
                    onClick={() => {
                      //return delta obj
                      const randomDeltaObject = generateDocument();
                      dispatch({
                        func: "UPDATE_STATE",
                        data: randomDeltaObject,
                      });
                    }}
                  >
                    Change editor value
                  </button>
                </>
              );
            }}
          </TextContext.Consumer>
        </TextProvider>
      );

      expect(screen.getByText(/The initial text is null/i)).toBeTruthy();

      const changeButton = screen.getByRole("button", {
        name: /Change editor value/i,
      });
      await user.click(changeButton);
      // expect changing state value & trigger setProps
      expect(
        screen.getByText(
          /this is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spaces/i
        )
      ).toBeInTheDocument();

      expect(setPropMock).toHaveBeenCalledTimes(1);
    });

    test("Test Diff logic - empty update does not trigger setProp (update database)", async () => {
      const user = userEvent.setup();
      //return delta obj
      const randomDeltaObject = generateDocument();
      render(
        <TextProvider textState={randomDeltaObject} setProp={setPropMock}>
          <TextContext.Consumer>
            {([state, dispatch]) => {
              const quill = new Quill(document.createElement("div"));
              quill.setContents(state);
              return (
                <>
                  <span>
                    The initial text is{" "}
                    {quill.getText() === "\n" ? "null" : quill.getText()}
                  </span>
                  <button
                    onClick={() => {
                      dispatch({
                        func: "UPDATE_STATE",
                        data: randomDeltaObject,
                      });
                    }}
                  >
                    Change editor value
                  </button>
                </>
              );
            }}
          </TextContext.Consumer>
        </TextProvider>
      );

      expect(
        screen.getByText(
          /this is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spacesthis is a long text that contains spaces/i
        )
      ).toBeInTheDocument();

      const changeButton = screen.getByRole("button", {
        name: /Change editor value/i,
      });
      await user.click(changeButton);
      // no change - expect not trigger setProps
      expect(setPropMock).not.toHaveBeenCalled();
    });

    test("Calling context should throw an error if not called within TextProvider", () => {
      const user = userEvent.setup();
      //render hook outside provider
      const { result } = renderHook(() => useTextContext());
      expect(result.error).toEqual(
        new Error("useTextContext must be called inside TextProvider")
      );
    });
  });
});
