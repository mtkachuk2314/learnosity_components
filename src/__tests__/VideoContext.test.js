/**
 *  Video context Unit Tests
 */
import React from "react";
import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react-hooks";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { v4 as uuidv4 } from "uuid";
import Quill from "quill";
import {
  VideoContext,
  videoConfig,
  VideoProvider,
} from "../components/Video/VideoContext";

// Mocked uuid library
jest.mock("uuid", () => ({
  v4: () => "123456789",
}));

// Mocked context values
const mockStateNull = {
  videoState: {
    id: uuidv4(),
    videoSource: null,
    videoURL: null,
    videoDescription: null,
    videoCredit: null,
    videoId: null,
    videoTextSettings: {
      description: true,
      credit: true,
    },
  },
};

const mockState = {
  videoState: {
    id: uuidv4(),
    videoSource: "brightcove",
    videoURL:
      "https://edge.api.brightcove.com/playback/v1/accounts/23648095001/videos/5967111782001",
    videoDescription: {
      ops: [{ insert: "Exploring Rate, Ratio and Proportion" }],
    },
    videoCredit: {
      ops: [{ insert: "TVO" }],
    },
    videoId: "5967111782001",
    videoTextSettings: {
      description: true,
      credit: true,
    },
  },
};

const mockStateNewDescriptionCredit = {
  videoState: {
    id: uuidv4(),
    videoSource: "youtube",
    videoURL:
      "https://edge.api.brightcove.com/playback/v1/accounts/23648095001/videos/5967111782001",
    videoDescription: {
      ops: [{ insert: "This is a new description" }],
    },
    videoCredit: {
      ops: [{ insert: "This is a new credit" }],
    },
    videoId: "5967111782001",
    videoTextSettings: {
      description: false,
      credit: false,
    },
  },
};

const setPropMock = jest.fn();

describe("Video Component Context API", () => {
  describe("Test reducer function", () => {
    test("The initial state of component is set correctly - UPDATE_STATE", () => {
      render(
        <VideoProvider
          videoState={mockStateNull.videoState}
          setProp={setPropMock}
        >
          <VideoContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>The initial videoSource is {state.videoSource}</span>
                  <span>The initial videoURL is {state.videoURL}</span>
                  <span>
                    The initial videoDescription is
                    {state.videoDescription}
                  </span>
                  <span>The initial videoCredit is {state.videoCredit}</span>
                  <span>The initial videoId is {state.videoId}</span>
                  <span>
                    The initial setting to display the description is{" "}
                    {`${state.videoTextSettings.description}`}
                  </span>
                  <span>
                    The initial setting to display the credit is{" "}
                    {`${state.videoTextSettings.credit}`}
                  </span>
                </>
              );
            }}
          </VideoContext.Consumer>
        </VideoProvider>
      );
      expect(screen.getByText(/The initial videoSource is/i)).toBeTruthy();
      expect(screen.getByText(/The initial videoURL is/i)).toBeTruthy();
      expect(screen.getByText(/The initial videoDescription is/i)).toBeTruthy();
      expect(screen.getByText(/The initial videoCredit is/i)).toBeTruthy();
      expect(screen.getByText(/The initial videoId is/i)).toBeTruthy();
      expect(
        screen.getByText(
          /The initial setting to display the description is true/i
        )
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial setting to display the credit is true/i)
      ).toBeTruthy();
    });
    test("The initial state (non-null values) of component is correctly set - UPDATE_STATE", () => {
      render(
        <VideoProvider videoState={mockState.videoState} setProp={setPropMock}>
          <VideoContext.Consumer>
            {([state, dispatch]) => {
              const quillDescription = new Quill(
                document.createElement("span")
              );
              quillDescription.setContents(state.videoDescription);
              const quillCredit = new Quill(document.createElement("span"));
              quillCredit.setContents(state.videoCredit);
              return (
                <>
                  <span>The initial videoSource is {state.videoSource}</span>
                  <span>The initial videoURL is {state.videoURL}</span>
                  <span>
                    The initial videoDescription is
                    {quillDescription.getText()}
                  </span>
                  <span>
                    The initial videoCredit is {quillCredit.getText()}
                  </span>
                  <span>The initial videoId is {state.videoId}</span>
                  <span>
                    The initial setting to display the description is{" "}
                    {`${state.videoTextSettings.description}`}
                  </span>
                  <span>
                    The initial setting to display the credit is{" "}
                    {`${state.videoTextSettings.credit}`}
                  </span>
                </>
              );
            }}
          </VideoContext.Consumer>
        </VideoProvider>
      );

      expect(
        screen.getByText(/The initial videoSource is brightcove/i)
      ).toBeTruthy();
      expect(
        screen.getByText(
          "The initial videoURL is https://edge.api.brightcove.com/playback/v1/accounts/23648095001/videos/5967111782001"
        )
      ).toBeTruthy();
      expect(
        screen.getByText(/Exploring Rate, Ratio and Proportion/)
      ).toBeTruthy();
      expect(screen.getByText(/The initial videoCredit is TVO/i)).toBeTruthy();
      expect(
        screen.getByText(/The initial videoId is 5967111782001/i)
      ).toBeTruthy();
      expect(
        screen.getByText(
          /The initial setting to display the description is true/i
        )
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial setting to display the credit is true/i)
      ).toBeTruthy();
    });
    test("Test UPDATE_STATE reducer - Changing in state value will update state and triggers setProp", async () => {
      const user = userEvent.setup();
      render(
        <VideoProvider
          videoState={mockStateNull.videoState}
          setProp={setPropMock}
        >
          <VideoContext.Consumer>
            {([state, dispatch]) => {
              const quillDescription = new Quill(
                document.createElement("span")
              );
              quillDescription.setContents(state.videoDescription);
              const quillCredit = new Quill(document.createElement("span"));
              quillCredit.setContents(state.videoCredit);
              return (
                <>
                  <span>The initial videoSource is {state.videoSource}</span>
                  <span>The initial videoURL is {state.videoURL}</span>
                  <span>
                    The initial videoDescription is
                    {quillDescription.getText()}
                  </span>
                  <span>
                    The initial videoCredit is {quillCredit.getText()}
                  </span>
                  <span>The initial videoId is {state.videoId}</span>
                  <span>
                    The initial setting to display the description is{" "}
                    {`${state.videoTextSettings.description}`}
                  </span>
                  <span>
                    The initial setting to display the credit is{" "}
                    {`${state.videoTextSettings.credit}`}
                  </span>
                  <button
                    onClick={() =>
                      dispatch({
                        func: "UPDATE_STATE",
                        data: mockState.videoState,
                      })
                    }
                  >
                    Update State
                  </button>
                </>
              );
            }}
          </VideoContext.Consumer>
        </VideoProvider>
      );

      const changeStateButton = screen.getByRole("button", {
        name: /Update State/i,
      });
      await user.click(changeStateButton);
      expect(
        screen.getByText(/The initial videoSource is brightcove/i)
      ).toBeTruthy();
      expect(
        screen.getByText(
          "The initial videoURL is https://edge.api.brightcove.com/playback/v1/accounts/23648095001/videos/5967111782001"
        )
      ).toBeTruthy();
      expect(
        screen.getByText(/Exploring Rate, Ratio and Proportion/)
      ).toBeTruthy();
      expect(screen.getByText(/The initial videoCredit is TVO/i)).toBeTruthy();
      expect(
        screen.getByText(/The initial videoId is 5967111782001/i)
      ).toBeTruthy();
      expect(
        screen.getByText(
          /The initial setting to display the description is true/i
        )
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial setting to display the credit is true/i)
      ).toBeTruthy();
    });
    // TODO: Fix this and find out why using the same state returns an error, claiming setProp is called once rather than 0 times
    xtest("Test Diff logic - empty update does not trigger setProp (update database)", async () => {
      const user = userEvent.setup();
      render(
        <VideoProvider
          videoState={mockStateNull.videoState}
          setProp={setPropMock}
        >
          <VideoContext.Consumer>
            {([state, dispatch]) => {
              const quillDescription = new Quill(
                document.createElement("span")
              );
              quillDescription.setContents(state.videoDescription);
              const quillCredit = new Quill(document.createElement("span"));
              quillCredit.setContents(state.videoCredit);
              return (
                <>
                  <span>The initial videoSource is {state.videoSource}</span>
                  <span>The initial videoURL is {state.videoURL}</span>
                  <span>
                    The initial videoDescription is
                    {quillDescription.getText()}
                  </span>
                  <span>
                    The initial videoCredit is {quillCredit.getText()}
                  </span>
                  <span>The initial videoId is {state.videoId}</span>
                  <span>
                    The initial setting to display the description is{" "}
                    {`${state.videoTextSettings.description}`}
                  </span>
                  <span>
                    The initial setting to display the credit is{" "}
                    {`${state.videoTextSettings.credit}`}
                  </span>
                  <button
                    onClick={() =>
                      dispatch({
                        func: "UPDATE_STATE",
                        data: mockStateNull.videoState,
                      })
                    }
                  >
                    Update State
                  </button>
                </>
              );
            }}
          </VideoContext.Consumer>
        </VideoProvider>
      );

      const changeStateButton = screen.getByRole("button", {
        name: /Update State/i,
      });
      await user.click(changeStateButton);
      expect(setPropMock).not.toHaveBeenCalled();
    });
    test("Test SET_VIDEO_SOURCE reducer option", async () => {
      const user = userEvent.setup();
      render(
        <VideoProvider videoState={mockState.videoState} setProp={setPropMock}>
          <VideoContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>The initial videoSource is {state.videoSource}</span>
                  <button
                    onClick={() =>
                      dispatch({
                        func: "SET_VIDEO_SOURCE",
                        videoSource: "youtube",
                      })
                    }
                  >
                    Update videoSource
                  </button>
                </>
              );
            }}
          </VideoContext.Consumer>
        </VideoProvider>
      );

      const changeStateButton = screen.getByRole("button", {
        name: /Update videoSource/i,
      });
      await user.click(changeStateButton);
      expect(
        screen.getByText(/The initial videoSource is youtube/i)
      ).toBeTruthy();
    });
    test("Test SET_VIDEO_ID reducer option", async () => {
      const user = userEvent.setup();
      render(
        <VideoProvider videoState={mockState.videoState} setProp={setPropMock}>
          <VideoContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>The initial videoId is {state.videoId}</span>
                  <button
                    onClick={() =>
                      dispatch({
                        func: "SET_VIDEO_ID",
                        videoId: "5850064867001",
                      })
                    }
                  >
                    Update videoId
                  </button>
                </>
              );
            }}
          </VideoContext.Consumer>
        </VideoProvider>
      );

      const changeStateButton = screen.getByRole("button", {
        name: /Update videoId/i,
      });
      await user.click(changeStateButton);
      expect(
        screen.getByText(/The initial videoId is 5850064867001/i)
      ).toBeTruthy();
    });
    test("Test CHANGE_DESCRIPTION reducer option", async () => {
      const user = userEvent.setup();
      render(
        <VideoProvider videoState={mockState.videoState} setProp={setPropMock}>
          <VideoContext.Consumer>
            {([state, dispatch]) => {
              const quill = new Quill(document.createElement("span"));
              quill.setContents(state.videoDescription);
              return (
                <>
                  <span>
                    The initial videoDescription is
                    {quill.getText()}
                  </span>
                  <button
                    onClick={() =>
                      dispatch({
                        func: "CHANGE_DESCRIPTION",
                        description: quill.setContents(
                          mockStateNewDescriptionCredit.videoState
                            .videoDescription
                        ),
                      })
                    }
                  >
                    Update videoDescription
                  </button>
                </>
              );
            }}
          </VideoContext.Consumer>
        </VideoProvider>
      );

      const changeStateButton = screen.getByRole("button", {
        name: /Update videoDescription/i,
      });
      await user.click(changeStateButton);
      expect(screen.getByText(/This is a new description/i)).toBeTruthy();
    });
    test("Test CHANGE_CREDIT reducer option", async () => {
      const user = userEvent.setup();
      render(
        <VideoProvider videoState={mockState.videoState} setProp={setPropMock}>
          <VideoContext.Consumer>
            {([state, dispatch]) => {
              const quill = new Quill(document.createElement("span"));
              quill.setContents(state.videoCredit);
              return (
                <>
                  <span>
                    The initial videoCredit is
                    {quill.getText()}
                  </span>
                  <button
                    onClick={() =>
                      dispatch({
                        func: "CHANGE_CREDIT",
                        credit: quill.setContents(
                          mockStateNewDescriptionCredit.videoState.videoCredit
                        ),
                      })
                    }
                  >
                    Update videoCredit
                  </button>
                </>
              );
            }}
          </VideoContext.Consumer>
        </VideoProvider>
      );

      const changeStateButton = screen.getByRole("button", {
        name: /Update videoCredit/i,
      });
      await user.click(changeStateButton);
      expect(screen.getByText(/This is a new credit/i)).toBeTruthy();
    });
    // TODO: Fix this test to have textSettings
    test("Test CHANGE_TEXT_SETTINGS reducer option", async () => {
      const user = userEvent.setup();
      render(
        <VideoProvider videoState={mockState.videoState} setProp={setPropMock}>
          <VideoContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>
                    The initial setting to display the description is{" "}
                    {`${state.videoTextSettings.description}`}
                  </span>
                  <span>
                    The initial setting to display the credit is{" "}
                    {`${state.videoTextSettings.credit}`}
                  </span>
                  <button
                    onClick={() =>
                      dispatch({
                        func: "CHANGE_TEXT_SETTINGS",
                        textSettings:
                          mockStateNewDescriptionCredit.videoState
                            .videoTextSettings,
                      })
                    }
                  >
                    Update textSettings
                  </button>
                </>
              );
            }}
          </VideoContext.Consumer>
        </VideoProvider>
      );

      const changeStateButton = screen.getByRole("button", {
        name: /Update textSettings/i,
      });
      await user.click(changeStateButton);
      expect(
        screen.getByText(
          /The initial setting to display the description is false/i
        )
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial setting to display the credit is false/i)
      ).toBeTruthy();
      expect(
        screen.queryByText(
          /The initial setting to display the description is true/i
        )
      ).toBeFalsy();
      expect(
        screen.queryByText(/The initial setting to display the credit is true/i)
      ).toBeFalsy();
    });
  });
});
