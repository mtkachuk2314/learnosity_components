/**
 *  NestedWrapper Unit Tests
 *
 *
 *  Test categories:
 *         ** Functionality --> ensure the component is functioning as expected
 *         ** Performance --> test the speed and efficiency of the component
 *         ** Security --> test the security of the component
 *         ** Accessibility --> test accessible for screen readers and keyboard navigation
 *
 */

import "@testing-library/jest-dom";
import { render, screen, fireEvent } from "../TestUtility/renderUtils";
import userEvent from "@testing-library/user-event";
// import { logRoles } from "@testing-library/dom";

describe.skip("Header Component", () => {
  describe("Functionality", () => {
    // <------------Testing styling------------>this should be handle in compwrapper
    test("the component displays text content correctly and responsively on different screen sizes", () => {});
    test("The component container changes color and style when hovered over", () => {});
    test("The component container changes color or style when clicked ", () => {});

    //<------------Testing Header component label functionality------------> this should be handle in compwrapper
    test("The component is deleted when delete icon is clicked ", () => {});
    test("The component is duplicate when copy icon is clicked ", () => {});
    test("The component is moved one level down when down arrow icon is clicked ", () => {});
    test("The component is moved one level up when up arrow icon is clicked ", () => {});
  });
});
