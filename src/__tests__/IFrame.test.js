/**
 *  Header Unit Tests
 *
 *
 *  Test categories:
 *         ** Functionality --> ensure the component is functioning as expected
 *         ** Performance --> test the speed and efficiency of the component
 *         ** Security --> test the security of the component
 *         ** Accessibility --> test accessible for screen readers and keyboard navigation
 *
 */

import "@testing-library/jest-dom";
import { render, screen, fireEvent } from "../TestUtility/renderUtils";
import IFrame from "../components/IFrame/subcomponents/IFrame";
import { IFrameProvider } from "../components/IFrame/IFrameContext";

jest.mock("uuid", () => ({
  v4: () => "123456789",
}));
// Mocked context values
let mockState = {
  title: "",
  src: "",
  height: 500,
  width: 900,
};

// should mock updating state in authoring - which can change layoutState base on inverse and forward patches arrays
const setPropMock = jest.fn(() => {});
const MockProvider = ({ children }) => {
  return (
    <IFrameProvider iframeState={mockState} setProp={setPropMock}>
      {children}
    </IFrameProvider>
  );
};

describe("Header Component", () => {
  describe("Functionality", () => {
    // <------------Testing default behavior------------>
    test.skip("The component renders correctly with the given placeholder", () => {
      global.innerWidth = 500;
      global.innerHeight = 900;
      // Render the component
      render(
        <MockProvider>
          <IFrame />
        </MockProvider>
      );
      // Get the element using a PlaceholderText
      const IFrameButton = screen.getByTestId("addIFrameButton");
      fireEvent.click(IFrameButton);

      const iFrameModal = screen.getByTestId("iFrameModal");
      expect(iFrameModal).toBeInTheDocument();
    });
  });
});
