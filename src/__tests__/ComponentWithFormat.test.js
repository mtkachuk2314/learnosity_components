import React from "react";

import "@testing-library/jest-dom";

import { screen, render } from "../TestUtility/renderUtils";

import ComponentWithFormatMemo from "../authoring/components/ComponentWithFormat";
import AuthoringExperienceFormatProvider, {
  FORMATS,
} from "../authoring/context/AuthoringExperienceFormatContext";

describe("ComponentWithFormat tests", () => {
  // TODO: I'd prefer to mock this once globally by extending the object in the testSetup, it would impact other tests so I'm leaving it for now and using it
  // beforeEach(() => {
  //   // Mocking Component Index to create consistently "Invalid" and "Valid" components for each format
  //   jest.mock(
  //     "../src/components/componentIndex.js",
  //     () => ({
  //       ValidPrint: {
  //         readableName: "Header",
  //         compatibleFormats: {
  //           print: true,
  //           digital: true,
  //         },
  //       },
  //       InvalidPrint: {
  //         readableName: "Header but invalid now",
  //         compatibleFormats: {
  //           print: false,
  //           digital: true,
  //         },
  //       },
  //     }),
  //     {
  //       virtual: true,
  //     }
  //   );
  // });
  // afterAll(() => {
  //   jest.unmock("../components/componentIndex.js");
  // });

  const mockFormats = {
    [FORMATS.PRINT]: { shown: true },
    [FORMATS.DIGITAL]: { shown: true },
  };

  test("It renders children when component is compatible with selected format", () => {
    render(
      <AuthoringExperienceFormatProvider initSelectedFormat={FORMATS.PRINT}>
        <ComponentWithFormatMemo componentName="Text" formats={mockFormats}>
          <div data-testid="child">child</div>
        </ComponentWithFormatMemo>
      </AuthoringExperienceFormatProvider>
    );

    expect(screen.getByTestId("child")).toBeInTheDocument();
  });

  test("It does not render children when component is incompatible with selected format", () => {
    render(
      <AuthoringExperienceFormatProvider initSelectedFormat={FORMATS.PRINT}>
        <ComponentWithFormatMemo componentName="Video" formats={mockFormats}>
          <div data-testid="child">child</div>
        </ComponentWithFormatMemo>
      </AuthoringExperienceFormatProvider>
    );

    expect(screen.queryByTestId("child")).not.toBeInTheDocument();
  });

  test("It does not render child in selected format when formats choice is false", () => {
    render(
      <AuthoringExperienceFormatProvider initSelectedFormat={FORMATS.PRINT}>
        <ComponentWithFormatMemo
          componentName="Text"
          formats={{
            [FORMATS.PRINT]: { shown: false },
          }}
        >
          <div data-testid="child">child</div>
        </ComponentWithFormatMemo>
      </AuthoringExperienceFormatProvider>
    );

    expect(screen.queryByTestId("child")).not.toBeInTheDocument();
  });

  test("It does renders child in selected format when formats choice is true and component is compatible", () => {
    render(
      <AuthoringExperienceFormatProvider initSelectedFormat={FORMATS.PRINT}>
        <ComponentWithFormatMemo
          componentName="Text"
          formats={{
            [FORMATS.PRINT]: { shown: true },
          }}
        >
          <div data-testid="child">child</div>
        </ComponentWithFormatMemo>
      </AuthoringExperienceFormatProvider>
    );

    expect(screen.getByTestId("child")).toBeInTheDocument();
  });

  test("It does not fail when context is not provided, renders child", () => {
    render(
      <ComponentWithFormatMemo componentName="Text" formats={mockFormats}>
        <div data-testid="child">child</div>
      </ComponentWithFormatMemo>
    );

    expect(screen.getByTestId("child")).toBeInTheDocument();
  });

  test("It does not fail when invalid context value is provided", () => {
    render(
      <AuthoringExperienceFormatProvider initSelectedFormat="IamValidbutnotreally">
        <ComponentWithFormatMemo componentName="Text" formats={mockFormats}>
          <div data-testid="child">child</div>
        </ComponentWithFormatMemo>
      </AuthoringExperienceFormatProvider>
    );

    expect(screen.getByTestId("child")).toBeInTheDocument();
  });

  test("It handles missing components without failing", () => {
    render(
      <AuthoringExperienceFormatProvider initSelectedFormat={FORMATS.DIGITAL}>
        <ComponentWithFormatMemo
          componentName="NotAComponent"
          formats={mockFormats}
        >
          <div data-testid="child">child</div>
        </ComponentWithFormatMemo>
      </AuthoringExperienceFormatProvider>
    );

    expect(screen.getByTestId("child")).toBeInTheDocument();
  });
});
