/**
 *  Header Unit Tests
 *
 *
 *  Test categories:
 *         ** Functionality --> ensure the component is functioning as expected
 *         ** Performance --> test the speed and efficiency of the component
 *         ** Security --> test the security of the component
 *         ** Accessibility --> test accessible for screen readers and keyboard navigation
 *
 */

import "@testing-library/jest-dom";
import { render, screen, fireEvent } from "../TestUtility/renderUtils";
import userEvent from "@testing-library/user-event";
import { v4 as uuidv4 } from "uuid";
import NestedComponentWrapper from "../Utility/NestedComponentWrapper";
import { LayoutProvider } from "../Context/InteractivesContext";
import Header from "../components/Header/subcomponents/Header";
import { HeaderProvider } from "../components/Header/HeaderContext";
// import { logRoles } from "@testing-library/dom";

//context provider mocks
// header is using TenantContext and HeaderContext:
// TenantContext is defined as a custom wrapper ---> ../TestUtility/testing-library-render-utils
// HeaderContext

// Mocked uuid library
jest.mock("uuid", () => ({
  __esModule: true,
  v4: () => "123456789",
}));
// Mocked context values
let mockState = {
  size: "large",
  alignment: "left-align",
  heading: "",
};

let timeline = {
  mockInversePatches: [],
  current: mockState,
  mockForwardPatches: [],
};

// should mock undo base on inverse array
const undoMock = jest.fn(() => {
  if (timeline.mockInversePatches.length > 0) {
    const { mockInversePatches, current, mockForwardPatches } = timeline;
    const length = mockInversePatches.length;
    const lastItem = mockInversePatches[length - 1];
    const restOfArr = mockInversePatches.slice(0, length - 1);
    timeline = {
      mockInversePatches: restOfArr,
      current: lastItem,
      mockForwardPatches: [...mockForwardPatches, current],
    };
  }
});

// should mock redo base on inverse array
const redoMock = jest.fn(() => {
  if (timeline.mockForwardPatches.length > 0) {
    const { mockInversePatches, current, mockForwardPatches } = timeline;
    const length = mockForwardPatches.length;
    const lastItem = mockForwardPatches[length - 1];
    const restOfArr = mockForwardPatches.slice(0, length - 1);
    timeline = {
      mockInversePatches: [...mockInversePatches, current],
      current: lastItem,
      mockForwardPatches: restOfArr,
    };
  }
});

// should mock updating state in authoring - which can change layoutState base on inverse and forward patches arrays
const setPropMock = jest.fn((newState) => {
  const { layoutState } = newState;
  const { mockInversePatches, current, mockForwardPatches } = timeline;
  timeline = {
    mockInversePatches:
      JSON.stringify(current) === JSON.stringify(layoutState)
        ? [...mockInversePatches]
        : [...mockInversePatches, current],
    current: newState,
    // this seems to be the idiomatic approach for most applications
    mockForwardPatches: [...mockForwardPatches],
  };
});
const MockProvider = ({ children }) => {
  return (
    <HeaderProvider headerState={mockState} setProp={setPropMock}>
      {children}
    </HeaderProvider>
  );
};

describe("Header Component", () => {
  describe("Functionality", () => {
    // <------------Debouncing - Undo/Redo behavior------------>
    describe("Debouncing-Undo/Redo", () => {
      beforeEach(() => {
        jest.setTimeout(20000);
        jest.useFakeTimers();
        timeline = {
          mockInversePatches: [],
          current: mockState,
          mockForwardPatches: [],
        };
      });

      afterEach(() => {
        jest.clearAllMocks();
        jest.clearAllTimers();
        jest.useRealTimers();
        timeline = {
          mockInversePatches: [],
          current: mockState,
          mockForwardPatches: [],
        };
      });
      test("Changing header text should group fast actions and undo action removes a group of characters", () => {
        // Render the component
        render(
          <MockProvider headerState={mockState}>
            <Header />
          </MockProvider>
        );
        const headerInputArea = screen.getByLabelText("Header input field");
        // Simulate user typing - check if setProp debounce properlly

        // grouping sequence : 1:) HI2 Hello  !

        fireEvent.change(headerInputArea, { target: { value: "1" } });

        setTimeout(() => {}, 200);
        jest.advanceTimersByTime(200);

        fireEvent.change(headerInputArea, {
          target: { value: `${headerInputArea.value}:` },
        });
        fireEvent.change(headerInputArea, {
          target: { value: `${headerInputArea.value})` },
        });

        //need delay of a second
        setTimeout(() => {
          fireEvent.change(headerInputArea, {
            target: { value: `${headerInputArea.value}H` },
          });
          fireEvent.change(headerInputArea, {
            target: { value: `${headerInputArea.value}I` },
          });
        }, 1000);

        jest.advanceTimersByTime(1000);
        console.log(timeline.mockForwardPatches);
        console.log(timeline.mockInversePatches);
        console.log("----->", timeline.current);
        console.log(mockState);
        fireEvent.change(headerInputArea, {
          target: { value: `${headerInputArea.value}2` },
        });

        //need delay of a second
        // so following sequence should consider as a group of chars
        setTimeout(() => {
          fireEvent.change(headerInputArea, {
            target: { value: `${headerInputArea.value}H` },
          });
          fireEvent.change(headerInputArea, {
            target: { value: `${headerInputArea.value}E` },
          });
          fireEvent.change(headerInputArea, {
            target: { value: `${headerInputArea.value}L` },
          });
          fireEvent.change(headerInputArea, {
            target: { value: `${headerInputArea.value}L` },
          });
          fireEvent.change(headerInputArea, {
            target: { value: `${headerInputArea.value}O` },
          });
        }, 1000);

        jest.advanceTimersByTime(1000);

        //need delay of half second
        setTimeout(() => {
          fireEvent.change(headerInputArea, {
            target: { value: `${headerInputArea.value}!` },
          });
        }, 1000);
        jest.advanceTimersByTime(1000);

        setTimeout(() => {}, 1000);
        jest.advanceTimersByTime(1000);

        // ----------------------------undo-----------------------------------//
        // //undo act -> check debouncing and undo functionality
        // // - hit undo by user - call undo mock
        undoMock();

        fireEvent.change(headerInputArea, {
          target: { value: timeline.current.headerState.heading },
        });

        // setTimeout(() => {}, 1000);
        // jest.advanceTimersByTime(1000);

        // // //assertion
        expect(headerInputArea).toHaveValue("1:)HI2HELLO");

        undoMock();

        fireEvent.change(headerInputArea, {
          target: { value: timeline.current.headerState.heading },
        });

        //assertion
        expect(headerInputArea).toHaveValue("1:)HI2");

        undoMock();

        fireEvent.change(headerInputArea, {
          target: { value: timeline.current.headerState.heading },
        });

        //assertion
        expect(headerInputArea).toHaveValue("1:)");
        expect(headerInputArea).not.toHaveValue("1:)H");

        // ----------------------------redo-----------------------------------//
        redoMock();

        fireEvent.change(headerInputArea, {
          target: { value: timeline.current.headerState.heading },
        });
        //assertion
        expect(headerInputArea).toHaveValue("1:)HI2");

        redoMock();

        fireEvent.change(headerInputArea, {
          target: { value: timeline.current.headerState.heading },
        });

        //assertion
        expect(headerInputArea).toHaveValue("1:)HI2HELLO");

        // ----------------------------undo/redo-----------------------------------//

        undoMock();

        fireEvent.change(headerInputArea, {
          target: { value: timeline.current.headerState.heading },
        });

        //assertion
        expect(headerInputArea).toHaveValue("1:)HI2");

        redoMock();

        fireEvent.change(headerInputArea, {
          target: { value: timeline.current.headerState.heading },
        });

        //assertion
        expect(headerInputArea).toHaveValue("1:)HI2HELLO");

        redoMock();

        fireEvent.change(headerInputArea, {
          target: { value: timeline.current.headerState.heading },
        });

        //assertion
        expect(headerInputArea).toHaveValue("1:)HI2HELLO!");

        redoMock();

        fireEvent.change(headerInputArea, {
          target: { value: timeline.current.headerState.heading },
        });

        //assertion
        expect(headerInputArea).toHaveValue("1:)HI2HELLO!");
      });

      test("Changing header text should qroup user input character and debounce state update", () => {
        // Render the component
        render(
          <MockProvider>
            <Header />
          </MockProvider>
        );
        const headerInputArea = screen.getByLabelText("Header input field");
        // Simulate user typing - check if setProp debounce properlly

        // grouping sequence : 1:) HI2 Hello  !
        fireEvent.change(headerInputArea, { target: { value: "1" } });

        // setprop shoul not call before passing 500 milisec
        expect(setPropMock).not.toHaveBeenCalled();

        setTimeout(() => {}, 200);
        jest.advanceTimersByTime(200);

        fireEvent.change(headerInputArea, {
          target: { value: `${headerInputArea.value}:` },
        });
        fireEvent.change(headerInputArea, {
          target: { value: `${headerInputArea.value})` },
        });

        expect(setPropMock).not.toHaveBeenCalled();

        //need delay of a second
        setTimeout(() => {
          fireEvent.change(headerInputArea, {
            target: { value: `${headerInputArea.value}H` },
          });
          fireEvent.change(headerInputArea, {
            target: { value: `${headerInputArea.value}I` },
          });
        }, 1000);

        jest.advanceTimersByTime(1000);
        expect(setPropMock).toHaveBeenCalledTimes(1);

        fireEvent.change(headerInputArea, {
          target: { value: `${headerInputArea.value}2` },
        });

        expect(setPropMock).toHaveBeenCalledTimes(1);

        //need delay of a second
        // so following sequence should consider as a group of chars
        setTimeout(() => {
          fireEvent.change(headerInputArea, {
            target: { value: `${headerInputArea.value}H` },
          });
          fireEvent.change(headerInputArea, {
            target: { value: `${headerInputArea.value}E` },
          });
          fireEvent.change(headerInputArea, {
            target: { value: `${headerInputArea.value}L` },
          });
          fireEvent.change(headerInputArea, {
            target: { value: `${headerInputArea.value}L` },
          });
          fireEvent.change(headerInputArea, {
            target: { value: `${headerInputArea.value}O` },
          });
        }, 1000);

        jest.advanceTimersByTime(1000);
        expect(setPropMock).toHaveBeenCalledTimes(2);

        //need delay of a second
        setTimeout(() => {
          fireEvent.change(headerInputArea, {
            target: { value: `${headerInputArea.value}!` },
          });
        }, 1000);
        jest.advanceTimersByTime(1000);
        expect(setPropMock).toHaveBeenCalledTimes(3);

        // make delay to have latest version in console.log
        setTimeout(() => {}, 1000);
        jest.advanceTimersByTime(1000);
      });
    });

    // <------------Testing default behavior------------>
    test("The component renders correctly with the given placeholder", () => {
      // Render the component
      render(
        <MockProvider>
          <Header />
        </MockProvider>
      );
      // Get the element using a PlaceholderText
      const headerTextArea = screen.getByPlaceholderText("Header placeholder");
      // Assert that the element exist
      expect(headerTextArea).toBeInTheDocument();
    });

    test("Should allow the user to type text into the component", async () => {
      const user = userEvent.setup();
      // Render the component
      render(
        <MockProvider>
          <Header />
        </MockProvider>
      );
      // Get the input element using a data-testid or other selector
      const headerElement = screen.getByLabelText("Header input field");

      // Simulate user typing text into the input
      // await user.type(headerElement, "Test Text");
      fireEvent.change(headerElement, { target: { value: "Test Text" } });

      // Assert that the input value has been updated
      expect(headerElement).toHaveTextContent("Test Text");
    });

    test.skip("Should handle pasting of text from the clipboard", () => {
      // // Render the component
      // render(
      //   <MockProvider>
      //     <Header />
      //   </MockProvider>
      // );
      // // Get the input element using a data-testid or other selector
      // const headerElement = screen.getByLabelText("Header input field");
      // // Simulate user pasting text into the input
      // const textToPaste = "Pasted Text";
      // fireEvent.paste(headerElement, {
      //   clipboardData: { getData: () => textToPaste },
      // });
      // // Assert that the input value has been updated
      // expect(headerElement).toHaveTextContent("Pasted Text");
    });

    //<------------Testing Header toolbar functionality------------>
    describe("Toolbar", () => {
      const childStateMock = {
        size: "large",
        alignment: "left-align",
        heading: "",
      };

      const componentTypeMock = "accordion";

      const childComponentMock = {
        id: "64a6dc8231b4696a03739df4",
        componentName: "Header",
        props: childStateMock,
        position: "w",
        __typename: "Component",
      };

      const setterFunctionMock = jest.fn();

      const setPropMock = jest.fn(() => {});

      //accordion default props is used as mockstate
      const parentStateMock = [
        {
          id: uuidv4(),
          title: "",
          components: [
            {
              componentName: "Header",
              componentProps: {
                headerState: {
                  size: "large",
                  alignment: "left-align",
                  heading: "test",
                },
              },
            },
          ],
          expanded: true,
        },
      ];

      const MockInteractivateProvider = ({ children }) => {
        return (
          <LayoutProvider layoutState={parentStateMock} setProp={setPropMock}>
            {children}
          </LayoutProvider>
        );
      };

      test("The component is clickable and show toolbar when clicked", async () => {
        const user = userEvent.setup();
        render(
          <MockInteractivateProvider>
            <NestedComponentWrapper
              componentType={componentTypeMock}
              component={childComponentMock}
              compIndex={0}
              componentProps={parentStateMock[0].components[0].componentProps}
              tabIndex={0}
              numOfComponent={1}
              droppedIndex={0}
              setDroppedIndex={setterFunctionMock}
              setActiveComp={setterFunctionMock}
              activeComp={setterFunctionMock}
              setShowError={setterFunctionMock}
              setShowDropError={setterFunctionMock}
              setHoverMenu={setterFunctionMock}
              setActiveComponent={setterFunctionMock}
            />
          </MockInteractivateProvider>
        );

        //need to active nested wrapper - wich will get active after click on it's child (here header)

        // Get the input element using a data-testid or other selector
        const headerElement = screen.getByLabelText("Header input field");
        await user.click(headerElement);

        const headerToolbar = screen.getByTestId("header-toolbar");
        expect(headerToolbar).toBeInTheDocument();
      });

      test("The component hide toolbar when clicked outside", async () => {
        const user = userEvent.setup();

        // Render the component
        render(
          <MockInteractivateProvider>
            <NestedComponentWrapper
              componentType={componentTypeMock}
              component={childComponentMock}
              compIndex={0}
              componentProps={parentStateMock[0].components[0].componentProps}
              tabIndex={0}
              numOfComponent={1}
              droppedIndex={0}
              setDroppedIndex={setterFunctionMock}
              setActiveComp={setterFunctionMock}
              activeComp={setterFunctionMock}
              setShowError={setterFunctionMock}
              setShowDropError={setterFunctionMock}
              setHoverMenu={setterFunctionMock}
              setActiveComponent={setterFunctionMock}
            />
          </MockInteractivateProvider>
        );

        // Get the input element using a data-testid or other selector
        const headerElement = screen.getByLabelText("Header input field");
        await user.click(headerElement);

        const headerToolbar = screen.getByTestId("header-toolbar");
        expect(headerToolbar).toBeInTheDocument();

        const clickEvent = new MouseEvent("click", {
          bubbles: true,
          cancelable: true,
        });
        // Dispatch the event on the document
        document.dispatchEvent(clickEvent);

        // Trigger blur event- simulate clickoutside because above clickoutside mock dont lose focus
        headerElement.blur();
        expect(headerElement).not.toHaveFocus();

        expect(headerToolbar).not.toBeInTheDocument();
      });

      test("The text content is displayed with the correct font size", async () => {
        const user = userEvent.setup();
        // Render the component
        render(
          <MockInteractivateProvider>
            <NestedComponentWrapper
              componentType={componentTypeMock}
              component={childComponentMock}
              compIndex={0}
              componentProps={parentStateMock[0].components[0].componentProps}
              tabIndex={0}
              numOfComponent={1}
              droppedIndex={0}
              setDroppedIndex={setterFunctionMock}
              setActiveComp={setterFunctionMock}
              activeComp={setterFunctionMock}
              setShowError={setterFunctionMock}
              setShowDropError={setterFunctionMock}
              setHoverMenu={setterFunctionMock}
              setActiveComponent={setterFunctionMock}
            />
          </MockInteractivateProvider>
        );

        // Get the header element using a data-testid
        const headerElement = screen.getByLabelText("Header input field");
        await user.click(headerElement);

        // Assert that entire toolbar is showing
        const headerToolbar = screen.getByTestId("header-toolbar");
        expect(headerToolbar).toBeInTheDocument();

        // Get the header-select-dropdown element using a data-testid
        const toolbarSize = screen.getByTestId("header-toolbar-size");
        await user.click(toolbarSize);
        //--------------------------
        // Get the large option element using a data-testid
        const toolbarLargeOption = screen.getByTestId("large header");
        // Assert dropdown submenu is shown
        expect(toolbarLargeOption).toBeInTheDocument();

        // click on dropdown
        await user.click(toolbarLargeOption);

        // Assert that the text element has the expected style
        expect(headerElement).toHaveStyle({
          lineHeight: "49.01px",
          // letterSpacing: "-1.5px",
          fontSize: "42px",
        });
        //--------------------------
        // click on dropdown to show menu again
        await user.click(toolbarSize);

        // Get the large option element using a data-testid or other selector
        const toolbarMediumOption = screen.getByTestId("medium header");
        // cassert dropdown submenu is shown
        expect(toolbarMediumOption).toBeInTheDocument();

        await user.click(toolbarMediumOption);

        // Assert that the text element has the expected style
        expect(headerElement).toHaveStyle({
          lineHeight: "43.2px",
          // letterSpacing: "-0.5 px",
          fontSize: "36px",
        });
        //--------------------------
        // click on dropdown to show menu again
        await user.click(toolbarSize);

        // // Get the large option element using a data-testid or other selector
        const toolbarSmallOption = screen.getByTestId("small header");
        // cassert dropdown submenu is shown
        expect(toolbarSmallOption).toBeInTheDocument();

        await user.click(toolbarSmallOption);

        // Assert that the text element has the expected style
        expect(headerElement).toHaveStyle({
          lineHeight: "39.68px",
          fontSize: "34px",
        });
      });

      test("The text content is displayed with the correct font weight", async () => {
        const user = userEvent.setup();
        // Render the component
        render(
          <MockInteractivateProvider>
            <NestedComponentWrapper
              componentType={componentTypeMock}
              component={childComponentMock}
              compIndex={0}
              componentProps={parentStateMock[0].components[0].componentProps}
              tabIndex={0}
              numOfComponent={1}
              droppedIndex={0}
              setDroppedIndex={setterFunctionMock}
              setActiveComp={setterFunctionMock}
              activeComp={setterFunctionMock}
              setShowError={setterFunctionMock}
              setShowDropError={setterFunctionMock}
              setHoverMenu={setterFunctionMock}
              setActiveComponent={setterFunctionMock}
            />
          </MockInteractivateProvider>
        );

        // Get the header element using a data-testid
        const headerElement = screen.getByLabelText("Header input field");
        await user.click(headerElement);

        // Assert that entire toolbar is showing
        const headerToolbar = screen.getByTestId("header-toolbar");
        expect(headerToolbar).toBeInTheDocument();

        // Get the header-select-dropdown element using a data-testid
        const toolbarSize = screen.getByTestId("header-toolbar-size");
        await user.click(toolbarSize);
        //--------------------------
        // Get the large option element using a data-testid
        const toolbarLargeOption = screen.getByTestId("large header");
        // Assert dropdown submenu is shown
        expect(toolbarLargeOption).toBeInTheDocument();

        // click on dropdown
        await user.click(toolbarLargeOption);

        // Assert that the text element has the expected style
        expect(headerElement).toHaveStyle({
          fontWeight: 500,
        });
        //--------------------------
        // click on dropdown to show menu again
        await user.click(toolbarSize);

        // Get the large option element using a data-testid or other selector
        const toolbarMediumOption = screen.getByTestId("medium header");
        // cassert dropdown submenu is shown
        expect(toolbarMediumOption).toBeInTheDocument();

        await user.click(toolbarMediumOption);

        // Assert that the text element has the expected style
        expect(headerElement).toHaveStyle({
          fontWeight: 500,
        });
        //--------------------------
        // click on dropdown to show menu again
        await user.click(toolbarSize);

        // // Get the large option element using a data-testid or other selector
        const toolbarSmallOption = screen.getByTestId("small header");
        // cassert dropdown submenu is shown
        expect(toolbarSmallOption).toBeInTheDocument();

        await user.click(toolbarSmallOption);

        // Assert that the text element has the expected style
        expect(headerElement).toHaveStyle({
          fontWeight: 500,
        });
      });

      test("The text content is displayed with the correct text alignment ", async () => {
        const user = userEvent.setup();
        // Render the component
        render(
          <MockInteractivateProvider>
            <NestedComponentWrapper
              componentType={componentTypeMock}
              component={childComponentMock}
              compIndex={0}
              componentProps={parentStateMock[0].components[0].componentProps}
              tabIndex={0}
              numOfComponent={1}
              droppedIndex={0}
              setDroppedIndex={setterFunctionMock}
              setActiveComp={setterFunctionMock}
              activeComp={setterFunctionMock}
              setShowError={setterFunctionMock}
              setShowDropError={setterFunctionMock}
              setHoverMenu={setterFunctionMock}
              setActiveComponent={setterFunctionMock}
            />
          </MockInteractivateProvider>
        );

        // Get the header element using a data-testid
        const headerElement = screen.getByLabelText("Header input field");
        await user.click(headerElement);

        // Assert that entire toolbar is showing
        const headerToolbar = screen.getByTestId("header-toolbar");
        expect(headerToolbar).toBeInTheDocument();

        // Get the header-alignment-button element using a data-testid
        const toolbarAlign = screen.getByTestId("header-toolbar-align");
        await user.click(toolbarAlign);

        //--------------------------
        // Get the left option element using a data-testid
        const toolbarLeftOption = screen.getByLabelText("Header align left");
        // Assert dropdown submenu is shown
        expect(toolbarLeftOption).toBeInTheDocument();

        // click on left button
        await user.click(toolbarLeftOption);

        // Assert that the text element has the expected style
        expect(headerElement).toHaveStyle({
          textAlign: "left",
        });
        //--------------------------

        // Get the large option element using a data-testid or other selector
        const toolbarCenterOption = screen.getByLabelText("Header center text");
        // cassert dropdown submenu is shown
        expect(toolbarCenterOption).toBeInTheDocument();

        fireEvent.click(toolbarCenterOption);
        // Assert that the text element has the expected style
        expect(headerElement).toHaveStyle({
          textAlign: "center",
        });

        //--------------------------

        // // Get the large option element using a data-testid or other selector
        const toolbarRightOption = screen.getByLabelText("Header align right");
        // cassert dropdown submenu is shown
        expect(toolbarRightOption).toBeInTheDocument();

        fireEvent.click(toolbarRightOption);

        // Assert that the text element has the expected style
        expect(headerElement).toHaveStyle({
          textAlign: "right",
        });
      });
    });
  });

  describe("Performance", () => {
    test("should handle large amounts of text without slowing down", () => {
      // Render the component
      render(
        <MockProvider>
          <Header />
        </MockProvider>
      );
      const largeText = Array(10000)
        .fill("Lorem ipsum dolor sit amet, consectetur adipiscing elit.")
        .join(" ");
      const startTime = performance.now();
      // Get the input element using a data-testid or other selector
      const headerElement = screen.getByLabelText("Header input field");

      // Simulate user typing text into the input
      fireEvent.change(headerElement, { target: { value: largeText } });

      const endTime = performance.now();
      const renderingTime = endTime - startTime;

      expect(renderingTime).toBeLessThan(100);
    });
  });

  describe.skip("Security", () => {
    test("should sanitize input to prevent XSS attacks", () => {
      const user = userEvent.setup();
      // Render the component
      render(
        <MockProvider>
          <Header />
        </MockProvider>
      );
      // Get the input element using a data-testid or other selector
      const headerElement = screen.getByLabelText("Header input field");

      // Simulate user typing text into the input
      fireEvent.change(headerElement, {
        target: { value: '<script>alert("XSS attack")</script>' },
      });

      // Assert that the input value has been updated
      // expect(headerElement).toHaveTextContent("Test Text");
    });
  });

  describe.skip("Accessibility", () => {
    test("The component is accessible for screen readers and keyboard navigation", async () => {
      const user = userEvent.setup();
      // Render the component
      render(
        <MockProvider>
          <Header />
        </MockProvider>
      );
      // Get the header element using a data-testid
      const headerElement = screen.getByLabelText("Header input field");
      await user.click(headerElement);

      // Get all the buttons in the component
      const buttons = screen.getAllByRole("button");

      buttons.forEach((button) => {
        // Assert that the button has appropriate ARIA roles and attributes
        expect(button).toBeInTheDocument();

        // Assert that the button is keyboard navigable
        expect(button).toHaveFocus();

        // Assert that the button is accessible for screen readers
        expect(button).toHaveAccessibleName();
      });
    });
  });
});
