/**
 *  componentSideMenu Unit Tests
 *
 *
 *  Test categories:
 *         ** Functionality --> ensure the component is functioning as expected
 *         ** Performance --> test the speed and efficiency of the component
 *         ** Security --> test the security of the component
 *         ** Accessibility --> test accessible for screen readers and keyboard navigation
 *
 */

import React from "react";
import {
  render,
  screen,
  waitFor,
  fireEvent,
  waitForElementToBeRemoved,
} from "../TestUtility/renderUtils";
import "@testing-library/jest-dom";
import { v4 as uuidv4 } from "uuid";
import NestedComponentWrapper from "../Utility/NestedComponentWrapper";
import { LayoutProvider } from "../Context/InteractivesContext";
import ComponentSideMenu from "../Utility/ComponentSideMenu/ComponentSideMenu";

jest.mock("uuid", () => ({
  __esModule: true,
  v4: () => "123456789",
}));
const childStateMock = {
  size: "large",
  alignment: "left-align",
  heading: "",
};
const componentTypeMock = "accordion";
const childComponentMock = {
  id: "64a6dc8231b4696a03739df4",
  componentName: "Header",
  props: childStateMock,
  position: "w",
  __typename: "Component",
};

const setterFunctionMock = jest.fn();
const setPropMock = jest.fn(() => {});
//accordion default props is used as mockstate
const parentStateMock = [
  {
    id: uuidv4(),
    title: "",
    components: [],
    expanded: true,
  },
];

const MockProvider = ({ children }) => {
  return (
    <LayoutProvider layoutState={parentStateMock} setProp={setPropMock}>
      {children}
    </LayoutProvider>
  );
};

describe("ComponentSideMenu Component", () => {
  describe("Functionality", () => {
    //<------------Testing side menu component label functionality------------>
    describe("Default behavior", () => {
      test("the side menu icons should have proper styling (icon color - background color) in hover state (mouse hover)", () => {});

      test("the side menu icons should have proper styling (icon color - background color) in hover state (Tab focued)", () => {});

      test("the side menu tooltips should be shown when icon button is focused (Tab focued)", async () => {});

      test("the side menu tooltips should be shown when icon button is focused (mouse hover)", async () => {
        const onDelete = jest.fn();
        const onDuplicate = jest.fn();
        const onMoveUp = jest.fn();
        const onMoveDown = jest.fn();
        render(
          <ComponentSideMenu
            isHidden={false}
            onDelete={onDelete}
            onDuplicate={onDuplicate}
            onMoveUp={onMoveUp}
            onMoveDown={onMoveDown}
            type="text"
          />
        );

        // 1. Duplicate Button
        expect(screen.queryByRole(/tooltip/)).not.toBeInTheDocument();
        expect(screen.queryByText("Duplicate")).not.toBeInTheDocument();

        const duplicateButton = screen.getByTestId(
          "duplicate-component-button"
        );

        expect(duplicateButton).toBeInTheDocument();
        // Hover over the desktop breakpoint button
        fireEvent.mouseEnter(duplicateButton);
        // Wait for the tooltip to show up
        await screen.findByRole(/tooltip/);

        expect(screen.getByRole(/tooltip/)).toBeInTheDocument();
        expect(screen.getByText("Duplicate")).toBeInTheDocument();

        // Unhover
        fireEvent.mouseLeave(duplicateButton);
        // Wait for tooltip to hide
        await waitForElementToBeRemoved(() => screen.queryByText("Duplicate"));

        expect(screen.queryByRole(/tooltip/)).not.toBeInTheDocument();
        expect(screen.queryByText("Duplicate")).not.toBeInTheDocument();

        // 2. Delete Button
        const deleteButton = screen.getByTestId("delete-component-button");

        expect(deleteButton).toBeInTheDocument();
        // Hover over the desktop breakpoint button
        fireEvent.mouseEnter(deleteButton);
        // Wait for the tooltip to show up
        await screen.findByRole(/tooltip/);

        expect(screen.getByRole(/tooltip/)).toBeInTheDocument();
        expect(screen.getByText("Delete")).toBeInTheDocument();

        // Unhover
        fireEvent.mouseLeave(deleteButton);
        // Wait for tooltip to hide
        await waitForElementToBeRemoved(() => screen.queryByText("Delete"));

        expect(screen.queryByRole(/tooltip/)).not.toBeInTheDocument();
        expect(screen.queryByText("Delete")).not.toBeInTheDocument();

        // 3. Move up Button
        const moveUpButton = screen.getByTestId("move-component-up-button");

        expect(moveUpButton).toBeInTheDocument();
        // Hover over the desktop breakpoint button
        fireEvent.mouseEnter(moveUpButton);
        // Wait for the tooltip to show up
        await screen.findByRole(/tooltip/);

        expect(screen.getByRole(/tooltip/)).toBeInTheDocument();
        expect(screen.getByText("Move up")).toBeInTheDocument();

        // Unhover
        fireEvent.mouseLeave(moveUpButton);
        // Wait for tooltip to hide
        await waitForElementToBeRemoved(() => screen.queryByText("Move up"));

        expect(screen.queryByRole(/tooltip/)).not.toBeInTheDocument();
        expect(screen.queryByText("Move up")).not.toBeInTheDocument();

        // 4. Move down Button
        const moveDownButton = screen.getByTestId("move-component-down-button");

        expect(moveDownButton).toBeInTheDocument();
        // Hover over the desktop breakpoint button
        fireEvent.mouseEnter(moveDownButton);
        // Wait for the tooltip to show up
        await screen.findByRole(/tooltip/);

        expect(screen.getByRole(/tooltip/)).toBeInTheDocument();
        expect(screen.getByText("Move down")).toBeInTheDocument();

        // Unhover
        fireEvent.mouseLeave(moveDownButton);
        // Wait for tooltip to hide
        await waitForElementToBeRemoved(() => screen.queryByText("Move down"));

        expect(screen.queryByRole(/tooltip/)).not.toBeInTheDocument();
        expect(screen.queryByText("Move down")).not.toBeInTheDocument();
      });

      test("the component side menu is shown when component is in hover state (mouse hover)", async () => {
        render(
          <MockProvider>
            <NestedComponentWrapper
              componentType={componentTypeMock}
              component={childComponentMock}
              compIndex={0}
              componentProps={childStateMock}
              tabIndex={0}
              numOfComponent={1}
              droppedIndex={0}
              setDroppedIndex={setterFunctionMock}
              setActiveComp={setterFunctionMock}
              activeComp={setterFunctionMock}
              setShowError={setterFunctionMock}
              setShowDropError={setterFunctionMock}
              setHoverMenu={setterFunctionMock}
              setActiveComponent={setterFunctionMock}
            />
          </MockProvider>
        );

        //component should get focused - simulating focuse (hover or click)
        const nestedWrapperContainer = screen.getByTestId(
          "div-before-drop-indicator"
        );
        expect(nestedWrapperContainer).toBeInTheDocument();
        fireEvent.mouseEnter(nestedWrapperContainer);

        // sied menu options show up
        expect(
          await screen.findByTestId("duplicate-component-button")
        ).toBeInTheDocument();
        expect(
          await screen.findByTestId("delete-component-button")
        ).toBeInTheDocument();
        expect(
          await screen.findByTestId("move-component-up-button")
        ).toBeInTheDocument();
        expect(
          await screen.findByTestId("move-component-down-button")
        ).toBeInTheDocument();
      });

      test("the component side menu is hidden when component is not in hover state (mouse hover) or lost it's focus", async () => {
        render(
          <MockProvider>
            <NestedComponentWrapper
              componentType={componentTypeMock}
              component={childComponentMock}
              compIndex={0}
              componentProps={childStateMock}
              tabIndex={0}
              numOfComponent={1}
              droppedIndex={0}
              setDroppedIndex={setterFunctionMock}
              setActiveComp={setterFunctionMock}
              activeComp={setterFunctionMock}
              setShowError={setterFunctionMock}
              setShowDropError={setterFunctionMock}
              setHoverMenu={setterFunctionMock}
              setActiveComponent={setterFunctionMock}
            />
          </MockProvider>
        );

        //component should get focused - simulating focuse (hover or click)
        const nestedWrapperContainer = screen.getByTestId(
          "div-before-drop-indicator"
        );
        expect(nestedWrapperContainer).toBeInTheDocument();

        // sied menu options show up
        expect(screen.queryByTestId("duplicate-component-button")).toBeNull();
        expect(screen.queryByTestId("delete-component-button")).toBeNull();
        expect(screen.queryByTestId("move-component-up-button")).toBeNull();
        expect(screen.queryByTestId("move-component-down-button")).toBeNull();

        // simulationg component lose focus
        fireEvent.mouseEnter(nestedWrapperContainer);
      });

      test("clicking onDelete, onDuplicate, onMoveUp,onMoveDown button in the side menu triggers respective functions", async () => {
        const onDelete = jest.fn();
        const onDuplicate = jest.fn();
        const onMoveUp = jest.fn();
        const onMoveDown = jest.fn();

        render(
          <ComponentSideMenu
            isHidden={false}
            onDelete={onDelete}
            onDuplicate={onDuplicate}
            onMoveUp={onMoveUp}
            onMoveDown={onMoveDown}
            canMoveDown
            canMoveUp
            type="text"
          />
        );

        expect(
          screen.getByTestId("duplicate-component-button")
        ).toBeInTheDocument();
        expect(
          screen.getByTestId("delete-component-button")
        ).toBeInTheDocument();
        expect(
          screen.getByTestId("move-component-up-button")
        ).toBeInTheDocument();
        expect(
          screen.getByTestId("move-component-down-button")
        ).toBeInTheDocument();

        fireEvent.click(screen.getByTestId("duplicate-component-button"));
        expect(onDuplicate).toHaveBeenCalledTimes(1);

        fireEvent.click(screen.getByTestId("delete-component-button"));
        expect(onDelete).toHaveBeenCalledTimes(1);

        fireEvent.click(screen.getByTestId("move-component-up-button"));
        expect(onMoveUp).toHaveBeenCalledTimes(1);

        fireEvent.click(screen.getByTestId("move-component-down-button"));
        expect(onMoveDown).toHaveBeenCalledTimes(1);
      });

      test("up and down buttons are disabled when canMoveUp and canMoveDown are false", async () => {
        const onDelete = jest.fn();
        const onDuplicate = jest.fn();
        const onMoveUp = jest.fn();
        const onMoveDown = jest.fn();

        render(
          <ComponentSideMenu
            isHidden={false}
            onDelete={onDelete}
            onDuplicate={onDuplicate}
            onMoveUp={onMoveUp}
            onMoveDown={onMoveDown}
            canMoveDown={false}
            canMoveUp={false}
            type="text"
          />
        );

        expect(
          screen.getByTestId("duplicate-component-button")
        ).toBeInTheDocument();
        expect(
          screen.getByTestId("delete-component-button")
        ).toBeInTheDocument();
        expect(
          screen.getByTestId("move-component-up-button")
        ).toBeInTheDocument();
        expect(
          screen.getByTestId("move-component-down-button")
        ).toBeInTheDocument();

        expect(screen.getByTestId("move-component-up-button")).toBeDisabled();
        expect(screen.getByTestId("move-component-down-button")).toBeDisabled();
      });
    });

    // <------------testing styling------------>
    describe("Side Menu UI", () => {
      test("rendering side menu component", async () => {
        const onDelete = jest.fn();
        const onDuplicate = jest.fn();
        const onMoveUp = jest.fn();
        const onMoveDown = jest.fn();

        render(
          <ComponentSideMenu
            isHidden={false}
            onDelete={onDelete}
            onDuplicate={onDuplicate}
            onMoveUp={onMoveUp}
            onMoveDown={onMoveDown}
            type="text"
          />
        );

        expect(
          screen.getByTestId("duplicate-component-button")
        ).toBeInTheDocument();
        expect(
          screen.getByTestId("delete-component-button")
        ).toBeInTheDocument();
        expect(
          screen.getByTestId("move-component-up-button")
        ).toBeInTheDocument();
        expect(
          screen.getByTestId("move-component-down-button")
        ).toBeInTheDocument();
      });
      test.skip("the side menu icons should have different color when icon button is focused (mouse hover or Tab focused)", () => {});
      test.skip("the side menu tooltips should be shown when icon button is focused (mouse hover or Tab focused)", () => {});
    });
  });

  describe("Accessibility", () => {
    test("the side menu disable option should not be in keyboard access flow", () => {});
  });
});
