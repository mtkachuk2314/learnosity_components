/**
 *  Header context Unit Tests
 */
import React from "react";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import {
  HeaderProvider,
  HeaderContext,
} from "../components/Header/HeaderContext";

// Mocked context values
const mockState = {
  size: "large",
  alignment: "left-align",
  heading: "",
};

const setPropMock = jest.fn();

describe("Header Component Context API", () => {
  describe("Test reducer function", () => {
    test("The initial state of component is set correctly - UPDATE_STATE ", () => {
      render(
        <HeaderProvider headerState={mockState} setProp={setPropMock}>
          <HeaderContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>The initial size is {state.size}</span>
                  <span>The initial alignment is {state.alignment}</span>
                  <span>
                    The initial heading text is{" "}
                    {state.heading === "" ? "null" : state.heading}
                  </span>
                </>
              );
            }}
          </HeaderContext.Consumer>
        </HeaderProvider>
      );
      expect(screen.getByText(/The initial size is large/i)).toBeTruthy();
      expect(
        screen.getByText(/The initial alignment is left-align/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial heading text is null/i)
      ).toBeTruthy();
    });
    test("Test CHANGE_SIZE function", async () => {
      const user = userEvent.setup();
      render(
        <HeaderProvider headerState={mockState} setProp={setPropMock}>
          <HeaderContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>The initial size is {state.size}</span>
                  <span>The initial alignment is {state.alignment}</span>
                  <span>
                    The initial heading text is{" "}
                    {state.heading === "" ? "null" : state.heading}
                  </span>
                  <button
                    onClick={() =>
                      dispatch({ func: "CHANGE_SIZE", size: "small" })
                    }
                  >
                    Change size to small
                  </button>
                </>
              );
            }}
          </HeaderContext.Consumer>
        </HeaderProvider>
      );

      const changeSizeButton = screen.getByRole("button", {
        name: /Change size to small/i,
      });
      await user.click(changeSizeButton);
      expect(screen.getByText(/The initial size is small/i)).toBeTruthy();
      expect(
        screen.getByText(/The initial alignment is left-align/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial heading text is null/i)
      ).toBeTruthy();
    });
    test("Test CHANGE_ALIGNMENT function", async () => {
      const user = userEvent.setup();
      render(
        <HeaderProvider headerState={mockState} setProp={setPropMock}>
          <HeaderContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>The initial size is {state.size}</span>
                  <span>The initial alignment is {state.alignment}</span>
                  <span>
                    The initial heading text is{" "}
                    {state.heading === "" ? "null" : state.heading}
                  </span>
                  <button
                    onClick={() =>
                      dispatch({
                        func: "CHANGE_ALIGNMENT",
                        alignment: "center-align",
                      })
                    }
                  >
                    Change alignment to center
                  </button>
                </>
              );
            }}
          </HeaderContext.Consumer>
        </HeaderProvider>
      );

      const changeAlignButton = screen.getByRole("button", {
        name: /Change alignment to center/i,
      });
      await user.click(changeAlignButton);
      expect(screen.getByText(/The initial size is large/i)).toBeTruthy();
      expect(
        screen.getByText(/The initial alignment is center-align/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial heading text is null/i)
      ).toBeTruthy();
    });
    test("Test CHANGE_HEADING function", async () => {
      const user = userEvent.setup();
      render(
        <HeaderProvider headerState={mockState} setProp={setPropMock}>
          <HeaderContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>The initial size is {state.size}</span>
                  <span>The initial alignment is {state.alignment}</span>
                  <span>
                    The initial heading text is{" "}
                    {state.heading === "" ? "null" : state.heading}
                  </span>
                  <button
                    onClick={() =>
                      dispatch({
                        func: "CHANGE_HEADING",
                        heading: "new header text",
                      })
                    }
                  >
                    Change header text
                  </button>
                </>
              );
            }}
          </HeaderContext.Consumer>
        </HeaderProvider>
      );

      const changetextButton = screen.getByRole("button", {
        name: /Change header text/i,
      });
      await user.click(changetextButton);
      expect(screen.getByText(/The initial size is large/i)).toBeTruthy();
      expect(
        screen.getByText(/The initial alignment is left-align/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial heading text is new header text/i)
      ).toBeTruthy();
    });
  });
});
