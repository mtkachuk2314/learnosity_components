/**
 *  InfoBox context Unit Tests
 */
import React from "react";
import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react-hooks";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { v4 as uuidv4 } from "uuid";
import Quill from "quill";
import {
  InfoBoxProvider,
  InfoBoxContext,
  useInfoBoxContext,
} from "../components/InfoBox/InfoBoxContext";

// Mocked uuid library
jest.mock("uuid", () => ({
  v4: () => "123456789",
}));

// Mocked context values
const mockStateNull = {
  id: uuidv4(),
  infoBoxIcon: null,
  infoBoxLabel: "",
  infoBoxHeader: {
    heading: "",
    headingLevel: "",
  },
  body: null,
  infoBoxSettings: {
    header: true,
    label: true,
  },
  components: [],
};

const mockState = {
  id: uuidv4(),
  infoBoxIcon: "Definition",
  infoBoxLabel: "Label",
  infoBoxHeader: {
    heading: "Header",
    headingLevel: "H3",
  },
  body: null,
  infoBoxSettings: {
    header: false,
    label: false,
  },
  components: [
    {
      componentName: "Text",
      componentProps: {
        body: { ops: [{ insert: "Hi\n" }] },
      },
    },
  ],
};

const mockOldState = {
  infoBoxIcon: "Definition",
  infoBoxLabel: "Label",
  infoBoxHeader: {
    heading: "Header",
    headingLevel: "H3",
  },
  body: null,
  infoBoxSettings: {
    header: false,
    label: false,
  },
  components: [
    {
      componentName: "Text",
      componentProps: {
        body: { ops: [{ insert: "Hi\n" }] },
      },
    },
  ],
};

const textComponentMock = {
  componentName: "Text",
  componentProps: {
    body: { ops: [{ insert: "Hello\n" }] },
  },
};

const MockWrongProvider = ({ children }) => {
  let mockHeaderState = {
    size: "large",
    alignment: "left-align",
    heading: "",
  };
  return (
    <HeaderProvider headerState={mockHeaderState} setProp={setPropMock}>
      {children}
    </HeaderProvider>
  );
};

const imageComponentMock = {
  componentName: "Image",
  componentProps: {
    imgSize: {
      width: 100,
      height: 100,
    },
    uploadedImg: null,
    alt: "",
    customAltTextOptionSelected: "Custom alt text",
    longDesc: "",
    credit: "",
    imageTextSettings: {
      description: true,
      credit: true,
      fullWidth: false,
    },
    imgLink: null,
    caption: null,
  },
};

const setPropMock = jest.fn();

describe("InfoBox Component Context API", () => {
  describe("Test reducer function", () => {
    afterEach(() => {
      jest.clearAllMocks();
    });
    test(`The initial state (null value) of component is correctly set  - UPDATE_STATE`, () => {
      render(
        <InfoBoxProvider infoBoxState={mockStateNull} setProp={setPropMock}>
          <InfoBoxContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>
                    {`The initial infoBoxIcon is ${
                      state.infoBoxIcon === null ? "null" : state.infoBoxIcon
                    }`}
                  </span>
                  <span>
                    {`The initial infoBoxLabel is ${
                      state.infoBoxLabel === "" ? "null" : state.infoBoxLabel
                    }`}
                  </span>
                  <span>
                    {`The initial infobox heading is ${
                      state.infoBoxHeader.heading === ""
                        ? "null"
                        : state.infoBoxHeader.heading
                    }`}
                  </span>
                  <span>
                    {`The initial infobox headingLevel is ${
                      state.infoBoxHeader.headingLevel === ""
                        ? "null"
                        : state.infoBoxHeader.headingLevel
                    }`}
                  </span>
                  <span>
                    {`The initial infobox header is ${
                      state.infoBoxSettings.header === true ? "showing" : "hide"
                    }`}
                  </span>
                  <span>
                    {`The initial infobox label is ${
                      state.infoBoxSettings.label === true ? "showing" : "hide"
                    }`}
                  </span>
                  <span>
                    {`There is ${
                      state.components.length === 0
                        ? ` no`
                        : state.components.length
                    } component inside infobox`}
                  </span>
                </>
              );
            }}
          </InfoBoxContext.Consumer>
        </InfoBoxProvider>
      );
      expect(screen.getByText(/The initial infoBoxIcon is null/i)).toBeTruthy();
      expect(
        screen.getByText(/The initial infoBoxLabel is null/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial infobox heading is null/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial infobox headingLevel is null/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial infobox header is showing/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial infobox label is showing/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/There is no component inside infobox/i)
      ).toBeTruthy();
    });
    test(`The initial state (none-null value) of component is correctly set  - UPDATE_STATE`, () => {
      render(
        <InfoBoxProvider infoBoxState={mockState} setProp={setPropMock}>
          <InfoBoxContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>
                    {`The initial infoBoxIcon is ${
                      state.infoBoxIcon === null ? "null" : state.infoBoxIcon
                    }`}
                  </span>
                  <span>
                    {`The initial infoBoxLabel is ${
                      state.infoBoxLabel === "" ? "null" : state.infoBoxLabel
                    }`}
                  </span>
                  <span>
                    {`The initial infobox heading is ${
                      state.infoBoxHeader.heading === ""
                        ? "null"
                        : state.infoBoxHeader.heading
                    }`}
                  </span>
                  <span>
                    {`The initial infobox headingLevel is ${
                      state.infoBoxHeader.headingLevel === ""
                        ? "null"
                        : state.infoBoxHeader.headingLevel
                    }`}
                  </span>
                  <span>
                    {`The initial infobox header is ${
                      state.infoBoxSettings.header === true ? "showing" : "hide"
                    }`}
                  </span>
                  <span>
                    {`The initial infobox label is ${
                      state.infoBoxSettings.label === true ? "showing" : "hide"
                    }`}
                  </span>
                  <span>
                    {`There is ${
                      state.components.length === 0
                        ? ` no`
                        : state.components.length
                    } component inside infobox`}
                  </span>
                </>
              );
            }}
          </InfoBoxContext.Consumer>
        </InfoBoxProvider>
      );
      expect(
        screen.getByText(/The initial infoBoxIcon is Definition/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial infoBoxLabel is Label/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial infobox heading is Header/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial infobox headingLevel is H3/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial infobox header is hide/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial infobox label is hide/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/There is 1 component inside infobox/i)
      ).toBeTruthy();
    });
    test(`Test UPDATE_STATE reducer - Changing in state value will update state and triggers setProp`, async () => {
      const user = userEvent.setup();
      render(
        <InfoBoxProvider infoBoxState={mockStateNull} setProp={setPropMock}>
          <InfoBoxContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>
                    {`The initial infoBoxIcon is ${
                      state.infoBoxIcon === null ? "null" : state.infoBoxIcon
                    }`}
                  </span>
                  <span>
                    {`The initial infoBoxLabel is ${
                      state.infoBoxLabel === "" ? "null" : state.infoBoxLabel
                    }`}
                  </span>
                  <span>
                    {`The initial infobox heading is ${
                      state.infoBoxHeader.heading === ""
                        ? "null"
                        : state.infoBoxHeader.heading
                    }`}
                  </span>
                  <span>
                    {`The initial infobox headingLevel is ${
                      state.infoBoxHeader.headingLevel === ""
                        ? "null"
                        : state.infoBoxHeader.headingLevel
                    }`}
                  </span>
                  <span>
                    {`The initial infobox header is ${
                      state.infoBoxSettings.header === true ? "showing" : "hide"
                    }`}
                  </span>
                  <span>
                    {`The initial infobox label is ${
                      state.infoBoxSettings.label === true ? "showing" : "hide"
                    }`}
                  </span>
                  <span>
                    {`There is ${
                      state.components.length === 0
                        ? `no`
                        : state.components.length
                    } component inside infobox`}
                  </span>
                  <button
                    onClick={() => {
                      dispatch({ func: "UPDATE_STATE", data: mockState });
                    }}
                  >
                    Change infoBox label,header,and icon
                  </button>
                </>
              );
            }}
          </InfoBoxContext.Consumer>
        </InfoBoxProvider>
      );

      const changeButton = screen.getByRole("button", {
        name: /Change infoBox label,header,and icon/i,
      });
      await user.click(changeButton);
      expect(setPropMock).toHaveBeenCalledTimes(1);
      expect(
        screen.getByText(/The initial infoBoxIcon is Definition/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial infoBoxLabel is Label/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial infobox heading is Header/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial infobox headingLevel is H3/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial infobox header is hide/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/The initial infobox label is hide/i)
      ).toBeTruthy();
      expect(
        screen.getByText(/There is 1 component inside infobox/i)
      ).toBeTruthy();
    });
    test(`Test Diff logic - empty update does not trigger setProp (update database)`, async () => {
      const user = userEvent.setup();
      render(
        <InfoBoxProvider infoBoxState={mockState} setProp={setPropMock}>
          <InfoBoxContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>
                    The initial infoBoxLabel is{" "}
                    {state.infoBoxLabel === "" ? "null" : state.infoBoxLabel}
                  </span>
                  <button
                    onClick={() => {
                      dispatch({ func: "UPDATE_STATE", data: mockState });
                    }}
                  >
                    Change infoBox label,header,and icon
                  </button>
                </>
              );
            }}
          </InfoBoxContext.Consumer>
        </InfoBoxProvider>
      );

      const changeButton = screen.getByRole("button", {
        name: /Change infoBox label,header,and icon/i,
      });
      await user.click(changeButton);
      // no change - expect not trigger setProps
      expect(setPropMock).not.toHaveBeenCalled();
    });
    test(`Test CHANGE_LABEL reducer option`, async () => {
      const user = userEvent.setup();
      render(
        <InfoBoxProvider infoBoxState={mockState} setProp={setPropMock}>
          <InfoBoxContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>
                    {`The initial infoBoxLabel is ${
                      state.infoBoxLabel === "" ? "null" : state.infoBoxLabel
                    }`}
                  </span>
                  <button
                    onClick={() => {
                      dispatch({
                        func: "CHANGE_LABEL",
                        label: "I am a new label",
                      });
                    }}
                  >
                    Change infoBox label,header,and icon
                  </button>
                </>
              );
            }}
          </InfoBoxContext.Consumer>
        </InfoBoxProvider>
      );

      expect(
        screen.getByText(/The initial infoBoxLabel is Label/i)
      ).toBeTruthy();

      const changeButton = screen.getByRole("button", {
        name: /Change infoBox label,header,and icon/i,
      });
      await user.click(changeButton);
      expect(
        screen.getByText(/The initial infoBoxLabel is I am a new label/i)
      ).toBeTruthy();
    });
    test(`Test CHANGE_HEADER reducer option`, async () => {
      const user = userEvent.setup();
      render(
        <InfoBoxProvider infoBoxState={mockState} setProp={setPropMock}>
          <InfoBoxContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>
                    {`The initial infobox heading is ${
                      state.infoBoxHeader.heading === ""
                        ? "null"
                        : state.infoBoxHeader.heading
                    }`}
                  </span>
                  <button
                    onClick={() => {
                      dispatch({
                        func: "CHANGE_HEADER",
                        header: "I am a new header",
                      });
                    }}
                  >
                    Change infoBox label,header,and icon
                  </button>
                </>
              );
            }}
          </InfoBoxContext.Consumer>
        </InfoBoxProvider>
      );

      expect(
        screen.getByText(/The initial infobox heading is Header/i)
      ).toBeTruthy();

      const changeButton = screen.getByRole("button", {
        name: /Change infoBox label,header,and icon/i,
      });
      await user.click(changeButton);
      expect(
        screen.getByText(/The initial infobox heading is I am a new header/i)
      ).toBeTruthy();
    });
    test(`Test CHANGE_ICON reducer option`, async () => {
      const user = userEvent.setup();
      render(
        <InfoBoxProvider infoBoxState={mockState} setProp={setPropMock}>
          <InfoBoxContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>
                    {`The initial infoBoxIcon is ${
                      state.infoBoxIcon === null ? "null" : state.infoBoxIcon
                    }`}
                  </span>
                  <button
                    onClick={() => {
                      dispatch({
                        func: "CHANGE_ICON",
                        icon: "Try It",
                      });
                    }}
                  >
                    Change infoBox icon
                  </button>
                </>
              );
            }}
          </InfoBoxContext.Consumer>
        </InfoBoxProvider>
      );

      expect(
        screen.getByText(/The initial infoBoxIcon is Definition/i)
      ).toBeTruthy();

      const changeButton = screen.getByRole("button", {
        name: /Change infoBox icon/i,
      });
      await user.click(changeButton);
      expect(
        screen.getByText(/The initial infoBoxIcon is Try It/i)
      ).toBeTruthy();
    });
    test(`Test CHANGE_INFOBOX_HEADER_SETTING reducer option`, async () => {
      const user = userEvent.setup();
      render(
        <InfoBoxProvider infoBoxState={mockState} setProp={setPropMock}>
          <InfoBoxContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>
                    {`The initial infobox header is ${
                      state.infoBoxSettings.header === true ? "showing" : "hide"
                    }`}
                  </span>
                  <button
                    onClick={() => {
                      dispatch({
                        func: "CHANGE_INFOBOX_HEADER_SETTING",
                        infoHeaderSetting: true,
                      });
                    }}
                  >
                    Change infoBox label,header,and icon
                  </button>
                </>
              );
            }}
          </InfoBoxContext.Consumer>
        </InfoBoxProvider>
      );

      expect(
        screen.getByText(/The initial infobox header is hide/i)
      ).toBeTruthy();

      const changeButton = screen.getByRole("button", {
        name: /Change infoBox label,header,and icon/i,
      });
      await user.click(changeButton);
      expect(
        screen.getByText(/The initial infobox header is showing/i)
      ).toBeTruthy();
    });
    test(`Test CHANGE_INFOBOX_LABEL_SETTING reducer option`, async () => {
      const user = userEvent.setup();
      render(
        <InfoBoxProvider infoBoxState={mockState} setProp={setPropMock}>
          <InfoBoxContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>
                    {`The initial infobox label is ${
                      state.infoBoxSettings.label === true ? "showing" : "hide"
                    }`}
                  </span>
                  <button
                    onClick={() => {
                      dispatch({
                        func: "CHANGE_INFOBOX_LABEL_SETTING",
                        infoLabelSetting: true,
                      });
                    }}
                  >
                    Change infoBox label,header,and icon
                  </button>
                </>
              );
            }}
          </InfoBoxContext.Consumer>
        </InfoBoxProvider>
      );

      expect(
        screen.getByText(/The initial infobox label is hide/i)
      ).toBeTruthy();

      const changeButton = screen.getByRole("button", {
        name: /Change infoBox label,header,and icon/i,
      });
      await user.click(changeButton);
      expect(
        screen.getByText(/The initial infobox label is showing/i)
      ).toBeTruthy();
    });
    test(`Test ADD_COMPONENT_DROPZONE reducer option - add component at the end of child list`, async () => {
      const user = userEvent.setup();
      render(
        <InfoBoxProvider infoBoxState={mockState} setProp={setPropMock}>
          <InfoBoxContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>
                    {`There is ${
                      state.components.length === 0
                        ? ` no`
                        : state.components.length
                    } component inside infobox`}
                  </span>
                  {state.components.length !== 0 && (
                    <span>
                      {state.components
                        .map((comp) => comp.componentName)
                        .join(",")}
                    </span>
                  )}
                  <button
                    onClick={() => {
                      dispatch({
                        func: "ADD_COMPONENT_DROPZONE",
                        component: imageComponentMock,
                      });
                    }}
                  >
                    Add component to drop zone
                  </button>
                </>
              );
            }}
          </InfoBoxContext.Consumer>
        </InfoBoxProvider>
      );

      expect(
        screen.getByText(/There is 1 component inside infobox/i)
      ).toBeTruthy();

      const changeButton = screen.getByRole("button", {
        name: /Add component to drop zone/i,
      });
      await user.click(changeButton);
      // order should be Text,Image
      expect(screen.getByText(/Text,Image/i)).toBeTruthy();
    });
    test(`Test DRAG_ADD_NEW_COMPONENT reducer option - insert new component in specific index`, async () => {
      const user = userEvent.setup();
      render(
        <InfoBoxProvider infoBoxState={mockState} setProp={setPropMock}>
          <InfoBoxContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>
                    {`There is ${
                      state.components.length === 0
                        ? ` no`
                        : state.components.length
                    } component inside infobox`}
                  </span>
                  {state.components.length !== 0 && (
                    <span>
                      {state.components
                        .map((comp) => comp.componentName)
                        .join(",")}
                    </span>
                  )}
                  <button
                    onClick={() => {
                      dispatch({
                        func: "DRAG_ADD_NEW_COMPONENT",
                        component: imageComponentMock,
                        hoverIndex: 0,
                      });
                    }}
                  >
                    Add component in index 0
                  </button>
                </>
              );
            }}
          </InfoBoxContext.Consumer>
        </InfoBoxProvider>
      );

      expect(
        screen.getByText(/There is 1 component inside infobox/i)
      ).toBeTruthy();

      const changeButton = screen.getByRole("button", {
        name: /Add component in index 0/i,
      });
      await user.click(changeButton);
      // order should be Text,Image
      expect(screen.getByText(/Image,Text/i)).toBeTruthy();
    });
    test.skip(`Test DRAG_COMPONENT reducer option`, async () => {
      //
    });
    test(`Test DELETE_COMPONENT reducer option - remove component in a specific index`, async () => {
      const user = userEvent.setup();
      render(
        <InfoBoxProvider infoBoxState={mockState} setProp={setPropMock}>
          <InfoBoxContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>
                    {`There is ${
                      state.components.length === 0
                        ? ` no`
                        : state.components.length
                    } component inside infobox`}
                  </span>
                  {state.components.length !== 0 && (
                    <span>
                      {state.components
                        .map((comp) => comp.componentName)
                        .join(",")}
                    </span>
                  )}
                  <button
                    onClick={() => {
                      dispatch({
                        func: "DRAG_ADD_NEW_COMPONENT",
                        component: imageComponentMock,
                        hoverIndex: 0,
                      });
                    }}
                  >
                    Add component in index 0
                  </button>
                  <button
                    onClick={() => {
                      dispatch({
                        func: "DELETE_COMPONENT",
                        compIndex: 1,
                      });
                    }}
                  >
                    Remove component in index 1
                  </button>
                </>
              );
            }}
          </InfoBoxContext.Consumer>
        </InfoBoxProvider>
      );

      expect(
        screen.getByText(/There is 1 component inside infobox/i)
      ).toBeTruthy();

      const changeButton = screen.getByRole("button", {
        name: /Add component in index 0/i,
      });
      await user.click(changeButton);
      const deleteButton = screen.getByRole("button", {
        name: /Remove component in index 1/i,
      });
      await user.click(deleteButton);
      // Text should get removed
      expect(screen.getByText(/Image/i)).toBeTruthy();
    });
    test(`Test MOVE_COMPONENT_DOWN reducer option - shift component down`, async () => {
      const user = userEvent.setup();
      render(
        <InfoBoxProvider infoBoxState={mockState} setProp={setPropMock}>
          <InfoBoxContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>
                    {`There is ${
                      state.components.length === 0
                        ? ` no`
                        : state.components.length
                    } component inside infobox`}
                  </span>
                  {state.components.length !== 0 && (
                    <span>
                      {state.components
                        .map((comp) => comp.componentName)
                        .join(",")}
                    </span>
                  )}
                  <button
                    onClick={() => {
                      dispatch({
                        func: "DRAG_ADD_NEW_COMPONENT",
                        component: imageComponentMock,
                        hoverIndex: 0,
                      });
                    }}
                  >
                    Add component in index 0
                  </button>
                  <button
                    onClick={() => {
                      dispatch({
                        func: "MOVE_COMPONENT_DOWN",
                        compIndex: 0,
                      });
                    }}
                  >
                    Move first component down
                  </button>
                </>
              );
            }}
          </InfoBoxContext.Consumer>
        </InfoBoxProvider>
      );

      expect(
        screen.getByText(/There is 1 component inside infobox/i)
      ).toBeTruthy();

      const changeButton = screen.getByRole("button", {
        name: /Add component in index 0/i,
      });
      await user.click(changeButton);
      const moveButton = screen.getByRole("button", {
        name: /Move first component down/i,
      });
      await user.click(moveButton);
      // Text should get removed
      expect(screen.getByText(/Text,Image/i)).toBeTruthy();
    });
    test(`Test MOVE_COMPONENT_UP reducer option -shift component up`, async () => {
      const user = userEvent.setup();
      render(
        <InfoBoxProvider infoBoxState={mockState} setProp={setPropMock}>
          <InfoBoxContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>
                    {`There is ${
                      state.components.length === 0
                        ? ` no`
                        : state.components.length
                    } component inside infobox`}
                  </span>
                  {state.components.length !== 0 && (
                    <span>
                      {state.components
                        .map((comp) => comp.componentName)
                        .join(",")}
                    </span>
                  )}
                  <button
                    onClick={() => {
                      dispatch({
                        func: "DRAG_ADD_NEW_COMPONENT",
                        component: imageComponentMock,
                        hoverIndex: 0,
                      });
                    }}
                  >
                    Add component in index 0
                  </button>
                  <button
                    onClick={() => {
                      dispatch({
                        func: "MOVE_COMPONENT_UP",
                        compIndex: 1,
                      });
                    }}
                  >
                    Move second component up
                  </button>
                </>
              );
            }}
          </InfoBoxContext.Consumer>
        </InfoBoxProvider>
      );

      expect(
        screen.getByText(/There is 1 component inside infobox/i)
      ).toBeTruthy();

      const changeButton = screen.getByRole("button", {
        name: /Add component in index 0/i,
      });
      await user.click(changeButton);
      const moveButton = screen.getByRole("button", {
        name: /Move second component up/i,
      });
      await user.click(moveButton);
      // Text should get removed
      expect(screen.getByText(/Text,Image/i)).toBeTruthy();
    });
    test(`Test DUPLICATE_COMPONENT reducer option`, async () => {
      const user = userEvent.setup();
      render(
        <InfoBoxProvider infoBoxState={mockState} setProp={setPropMock}>
          <InfoBoxContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>
                    {`There is ${
                      state.components.length === 0
                        ? ` no`
                        : state.components.length
                    } component inside infobox`}
                  </span>
                  {state.components.length !== 0 && (
                    <span>
                      {state.components
                        .map((comp) => comp.componentName)
                        .join(",")}
                    </span>
                  )}
                  <button
                    onClick={() => {
                      dispatch({
                        func: "DRAG_ADD_NEW_COMPONENT",
                        component: imageComponentMock,
                        hoverIndex: 0,
                      });
                    }}
                  >
                    Add component in index 0
                  </button>
                  <button
                    onClick={() => {
                      dispatch({
                        func: "DRAG_ADD_NEW_COMPONENT",
                        component: textComponentMock,
                        hoverIndex: 0,
                      });
                    }}
                  >
                    Add second omponent in index 0
                  </button>
                  <button
                    onClick={() => {
                      dispatch({
                        func: "DUPLICATE_COMPONENT",
                        compIndex: 1,
                      });
                    }}
                  >
                    Duplicate second component up
                  </button>
                </>
              );
            }}
          </InfoBoxContext.Consumer>
        </InfoBoxProvider>
      );

      expect(
        screen.getByText(/There is 1 component inside infobox/i)
      ).toBeTruthy();

      const changeButton = screen.getByRole("button", {
        name: /Add component in index 0/i,
      });
      await user.click(changeButton);
      const addButton = screen.getByRole("button", {
        name: /Add second omponent in index 0/i,
      });
      await user.click(addButton);
      const moveButton = screen.getByRole("button", {
        name: /Duplicate second component up/i,
      });
      await user.click(moveButton);
      // Text should get removed
      expect(screen.getByText(/Text,Image,Image,Text/i)).toBeTruthy();
    });
    test(`Test UPDATE_COMPONENT reducer option - test nested component update`, async () => {
      //for testing -> make change in one of nesteded component in components array

      const user = userEvent.setup();
      render(
        <InfoBoxProvider infoBoxState={mockState} setProp={setPropMock}>
          <InfoBoxContext.Consumer>
            {([state, dispatch]) => {
              const quill = new Quill(document.createElement("div"));
              quill.setContents(state.components[0].componentProps.body);
              return (
                <>
                  <span>{quill.getText()}</span>
                  <span></span>
                  <button
                    onClick={() => {
                      dispatch({
                        func: "UPDATE_COMPONENT",
                        compIndex: 0,
                        stateUpdate: {
                          body: { ops: [{ insert: "Hello dev\n" }] },
                        },
                      });
                    }}
                  >
                    Change Text component inside infoBox
                  </button>
                </>
              );
            }}
          </InfoBoxContext.Consumer>
        </InfoBoxProvider>
      );

      expect(screen.getByText(/Hi/i)).toBeTruthy();

      const changeButton = screen.getByRole("button", {
        name: /Change Text component inside infoBox/i,
      });
      await user.click(changeButton);
      // Text should get removed
      expect(screen.getByText(/Hello dev/i)).toBeTruthy();
    });
    test(`Test ADD_ID reducer option`, async () => {
      const user = userEvent.setup();
      render(
        <InfoBoxProvider infoBoxState={mockOldState} setProp={setPropMock}>
          <InfoBoxContext.Consumer>
            {([state, dispatch]) => {
              return (
                <>
                  <span>
                    {`The initial id is ${
                      state.id === undefined ? "undefined" : state.id
                    }`}
                  </span>
                  <button
                    onClick={() => {
                      dispatch({ func: "ADD_ID", id: uuidv4() });
                    }}
                  >
                    Add id
                  </button>
                </>
              );
            }}
          </InfoBoxContext.Consumer>
        </InfoBoxProvider>
      );
      expect(screen.getByText(/The initial id is undefined/i)).toBeTruthy();
      const changeButton = screen.getByRole("button", {
        name: /Add id/i,
      });
      await user.click(changeButton);
      expect(
        screen.queryByText(/The initial id is undefined/i)
      ).not.toBeInTheDocument();
    });
    test("should throw an error if not called within InfoBoxProvider", () => {
      const user = userEvent.setup();
      //render hook outside provider
      const { result } = renderHook(() => useInfoBoxContext());
      expect(result.error).toEqual(
        new Error("useInfoBoxContext must be called inside InfoBoxProvider")
      );
    });
  });
});
