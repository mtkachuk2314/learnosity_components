/**
 *  Infobox Unit Tests
 *
 *
 *  Test categories:
 *         ** Functionality --> ensure the component is functioning as expected
 *         ** Performance --> test the speed and efficiency of the component
 *         ** Security --> test the security of the component
 *         ** Accessibility --> test accessible for screen readers and keyboard navigation
 *
 */

import "@testing-library/jest-dom";
import userEvent from "@testing-library/user-event";

import { v4 as uuidv4 } from "uuid";

import { render, screen, fireEvent, act } from "../TestUtility/renderUtils";
import InfoBox from "../components/InfoBox/subcomponents/InfoBox";
import InfoBoxMain, {
  defaultProps as infoboxDefaultProps,
} from "../components/InfoBox/InfoBoxMain";
import { InfoBoxProvider } from "../components/InfoBox/InfoBoxContext";
import Header from "../components/InfoBox/subcomponents/Header";
import Label from "../components/InfoBox/subcomponents/Label";
import { FORMATS } from "../authoring/context/AuthoringExperienceFormatContext";

// import { logRoles } from "@testing-library/dom";

// Mocked uuid library
jest.mock("uuid", () => ({
  v4: () => "123456789",
}));

// should mock undo base on inverse array
const mockT = jest.fn((x) => {
  return x;
});

// Mocked context values
let mockState = {
  id: uuidv4(),
  infoBoxIcon: null,
  infoBoxLabel: "",
  infoBoxHeader: {
    heading: "",
    headingLevel: "",
  },
  body: null,
  infoBoxSettings: {
    header: true,
    label: true,
  },
  components: [],
};

let timeline = {
  mockInversePatches: [],
  current: mockState,
  mockForwardPatches: [],
};

// should mock undo base on inverse array
const undoMock = jest.fn(() => {
  if (timeline.mockInversePatches.length > 0) {
    const { mockInversePatches, current, mockForwardPatches } = timeline;
    const length = mockInversePatches.length;
    const lastItem = mockInversePatches[length - 1];
    const restOfArr = mockInversePatches.slice(0, length - 1);
    timeline = {
      mockInversePatches: restOfArr,
      current: lastItem,
      mockForwardPatches: [...mockForwardPatches, current],
    };
  }
});

// should mock redo base on inverse array
const redoMock = jest.fn(() => {
  if (timeline.mockForwardPatches.length > 0) {
    const { mockInversePatches, current, mockForwardPatches } = timeline;
    const length = mockForwardPatches.length;
    const lastItem = mockForwardPatches[length - 1];
    const restOfArr = mockForwardPatches.slice(0, length - 1);
    timeline = {
      mockInversePatches: [...mockInversePatches, current],
      current: lastItem,
      mockForwardPatches: restOfArr,
    };
  }
});

// should mock updating state in authoring - which can change layoutState base on inverse and forward patches arrays
const setPropMock = jest.fn((newState) => {
  const { layoutState } = newState;
  const { mockInversePatches, current, mockForwardPatches } = timeline;
  timeline = {
    mockInversePatches:
      JSON.stringify(current) === JSON.stringify(layoutState)
        ? [...mockInversePatches]
        : [...mockInversePatches, current],
    current: newState,
    // this seems to be the idiomatic approach for most applications
    mockForwardPatches: [...mockForwardPatches],
  };
});

const MockProvider = ({ children }) => {
  return (
    <InfoBoxProvider infoBoxState={mockState} setProp={setPropMock}>
      {children}
    </InfoBoxProvider>
  );
};

describe("Infobox Component", () => {
  describe("Functionality", () => {
    // <------------Debouncing - Undo/Redo behavior for Label and Header------------>
    describe("Undo/Redo", () => {
      beforeEach(() => {
        jest.useFakeTimers();
        timeline = {
          mockInversePatches: [],
          current: mockState,
          mockForwardPatches: [],
        };
      });

      afterEach(() => {
        jest.clearAllMocks();
        jest.clearAllTimers();
        jest.useRealTimers();
        timeline = {
          mockInversePatches: [],
          current: mockState,
          mockForwardPatches: [],
        };
      });

      // <------------Testing  - undo/redo with forward / reverse batching & race condition------------>
      test("Label and Header undo/redo with forward / reverse batching & race condition", () => {
        // Render the component
        render(
          <MockProvider infoBoxState={mockState}>
            <Label t={mockT} viewOnly={false} />
            <Header t={mockT} viewOnly={false} />
          </MockProvider>
        );

        // Label Component
        // get the input label using placeholder text
        let infoBoxLabelElement = screen.getByPlaceholderText(
          "Infobox Label Placeholder"
        );

        fireEvent.change(infoBoxLabelElement, { target: { value: "1" } });

        // setprop shoul not call before passing 500 milisec
        expect(setPropMock).not.toHaveBeenCalled();

        setTimeout(() => {}, 200);
        jest.advanceTimersByTime(200);

        fireEvent.change(infoBoxLabelElement, {
          target: { value: `${infoBoxLabelElement.value}:` },
        });
        fireEvent.change(infoBoxLabelElement, {
          target: { value: `${infoBoxLabelElement.value})` },
        });

        //need delay of a second
        setTimeout(() => {
          fireEvent.change(infoBoxLabelElement, {
            target: { value: `${infoBoxLabelElement.value}H` },
          });
          fireEvent.change(infoBoxLabelElement, {
            target: { value: `${infoBoxLabelElement.value}I` },
          });
        }, 1000);
        jest.advanceTimersByTime(1000);

        fireEvent.change(infoBoxLabelElement, {
          target: { value: `${infoBoxLabelElement.value}2` },
        });

        //need delay of a second
        setTimeout(() => {
          fireEvent.change(infoBoxLabelElement, {
            target: { value: `${infoBoxLabelElement.value}H` },
          });
          fireEvent.change(infoBoxLabelElement, {
            target: { value: `${infoBoxLabelElement.value}E` },
          });
          fireEvent.change(infoBoxLabelElement, {
            target: { value: `${infoBoxLabelElement.value}L` },
          });
          fireEvent.change(infoBoxLabelElement, {
            target: { value: `${infoBoxLabelElement.value}L` },
          });
          fireEvent.change(infoBoxLabelElement, {
            target: { value: `${infoBoxLabelElement.value}O` },
          });
        }, 1000);

        jest.advanceTimersByTime(1000);

        //need delay of a second
        setTimeout(() => {
          fireEvent.change(infoBoxLabelElement, {
            target: { value: `${infoBoxLabelElement.value}!` },
          });
        }, 1000);
        jest.advanceTimersByTime(1000);

        setTimeout(() => {}, 1000);
        jest.advanceTimersByTime(1000);

        // ----------------------------undo-----------------------------------//

        // Undo - Assertion - "1:)HI2HELLO"
        undoMock();
        fireEvent.change(infoBoxLabelElement, {
          target: { value: timeline.current.infoBoxState.infoBoxLabel },
        });
        expect(infoBoxLabelElement).toHaveValue("1:)HI2HELLO");

        // Undo - Assertion - "1:)HI2"
        undoMock();
        fireEvent.change(infoBoxLabelElement, {
          target: { value: timeline.current.infoBoxState.infoBoxLabel },
        });
        expect(infoBoxLabelElement).toHaveValue("1:)HI2");
        //

        // Undo - Assertion - "1:)HI2"
        undoMock();
        fireEvent.change(infoBoxLabelElement, {
          target: { value: timeline.current.infoBoxState.infoBoxLabel },
        });
        //assertion
        expect(infoBoxLabelElement).toHaveValue("1:)");
        expect(infoBoxLabelElement).not.toHaveValue("1:)H");

        // ----------------------------redo-----------------------------------//
        redoMock();
        fireEvent.change(infoBoxLabelElement, {
          target: { value: timeline.current.infoBoxState.infoBoxLabel },
        });
        //assertion
        expect(infoBoxLabelElement).toHaveValue("1:)HI2");

        redoMock();

        fireEvent.change(infoBoxLabelElement, {
          target: { value: timeline.current.infoBoxState.infoBoxLabel },
        });

        //assertion
        expect(infoBoxLabelElement).toHaveValue("1:)HI2HELLO");

        // ----------------------------undo/redo-----------------------------------//

        undoMock();

        fireEvent.change(infoBoxLabelElement, {
          target: { value: timeline.current.infoBoxState.infoBoxLabel },
        });

        //assertion
        expect(infoBoxLabelElement).toHaveValue("1:)HI2");

        redoMock();

        fireEvent.change(infoBoxLabelElement, {
          target: { value: timeline.current.infoBoxState.infoBoxLabel },
        });

        //assertion
        expect(infoBoxLabelElement).toHaveValue("1:)HI2HELLO");

        redoMock();

        fireEvent.change(infoBoxLabelElement, {
          target: { value: timeline.current.infoBoxState.infoBoxLabel },
        });

        //assertion
        expect(infoBoxLabelElement).toHaveValue("1:)HI2HELLO!");

        redoMock();

        // HEADER COMPONENT
        // get the input label using placeholder text
        let infoBoxHeaderElement = screen.getByPlaceholderText(
          "Infobox Header Placeholder"
        );

        fireEvent.change(infoBoxHeaderElement, { target: { value: "1" } });

        setTimeout(() => {}, 200);
        jest.advanceTimersByTime(200);

        fireEvent.change(infoBoxHeaderElement, {
          target: { value: `${infoBoxHeaderElement.value}:` },
        });
        fireEvent.change(infoBoxHeaderElement, {
          target: { value: `${infoBoxHeaderElement.value})` },
        });

        //need delay of a second
        setTimeout(() => {
          fireEvent.change(infoBoxHeaderElement, {
            target: { value: `${infoBoxHeaderElement.value}H` },
          });
          fireEvent.change(infoBoxHeaderElement, {
            target: { value: `${infoBoxHeaderElement.value}I` },
          });
        }, 1000);
        jest.advanceTimersByTime(1000);

        fireEvent.change(infoBoxHeaderElement, {
          target: { value: `${infoBoxHeaderElement.value}2` },
        });

        //need delay of a second
        setTimeout(() => {
          fireEvent.change(infoBoxHeaderElement, {
            target: { value: `${infoBoxHeaderElement.value}H` },
          });
          fireEvent.change(infoBoxHeaderElement, {
            target: { value: `${infoBoxHeaderElement.value}E` },
          });
          fireEvent.change(infoBoxHeaderElement, {
            target: { value: `${infoBoxHeaderElement.value}L` },
          });
          fireEvent.change(infoBoxHeaderElement, {
            target: { value: `${infoBoxHeaderElement.value}L` },
          });
          fireEvent.change(infoBoxHeaderElement, {
            target: { value: `${infoBoxHeaderElement.value}O` },
          });
        }, 1000);

        jest.advanceTimersByTime(1000);

        //need delay of a second
        setTimeout(() => {
          fireEvent.change(infoBoxHeaderElement, {
            target: { value: `${infoBoxHeaderElement.value}!` },
          });
        }, 1000);
        jest.advanceTimersByTime(1000);

        setTimeout(() => {}, 1000);
        jest.advanceTimersByTime(1000);

        // ----------------------------undo-----------------------------------//

        // Undo - Assertion - "1:)HI2HELLO"
        undoMock();
        fireEvent.change(infoBoxHeaderElement, {
          target: {
            value: timeline.current.infoBoxState.infoBoxHeader.heading,
          },
        });
        expect(infoBoxHeaderElement).toHaveValue("1:)HI2HELLO");

        // Undo - Assertion - "1:)HI2"
        undoMock();
        fireEvent.change(infoBoxHeaderElement, {
          target: {
            value: timeline.current.infoBoxState.infoBoxHeader.heading,
          },
        });
        expect(infoBoxHeaderElement).toHaveValue("1:)HI2");
        //

        // Undo - Assertion - "1:)HI2"
        undoMock();
        fireEvent.change(infoBoxHeaderElement, {
          target: {
            value: timeline.current.infoBoxState.infoBoxHeader.heading,
          },
        });
        //assertion
        expect(infoBoxHeaderElement).toHaveValue("1:)");
        expect(infoBoxHeaderElement).not.toHaveValue("1:)H");

        // ----------------------------redo-----------------------------------//
        redoMock();
        fireEvent.change(infoBoxHeaderElement, {
          target: {
            value: timeline.current.infoBoxState.infoBoxHeader.heading,
          },
        });
        //assertion
        expect(infoBoxHeaderElement).toHaveValue("1:)HI2");

        redoMock();

        fireEvent.change(infoBoxHeaderElement, {
          target: {
            value: timeline.current.infoBoxState.infoBoxHeader.heading,
          },
        });

        //assertion
        expect(infoBoxHeaderElement).toHaveValue("1:)HI2HELLO");

        // ----------------------------undo/redo-----------------------------------//

        undoMock();

        fireEvent.change(infoBoxHeaderElement, {
          target: {
            value: timeline.current.infoBoxState.infoBoxHeader.heading,
          },
        });

        //assertion
        expect(infoBoxHeaderElement).toHaveValue("1:)HI2");

        redoMock();

        fireEvent.change(infoBoxHeaderElement, {
          target: {
            value: timeline.current.infoBoxState.infoBoxHeader.heading,
          },
        });

        //assertion
        expect(infoBoxHeaderElement).toHaveValue("1:)HI2HELLO");

        redoMock();

        fireEvent.change(infoBoxHeaderElement, {
          target: {
            value: timeline.current.infoBoxState.infoBoxHeader.heading,
          },
        });

        //assertion
        expect(infoBoxHeaderElement).toHaveValue("1:)HI2HELLO!");

        redoMock();
      });
      // <------------Testing - state updates - debounced & none debounced 500ms ------------>
      test("Changing infobox label should group fast actions {500ms}, batch of updates", () => {
        render(
          <MockProvider infoBoxState={mockState}>
            <Label t={mockT} viewOnly={false} />
          </MockProvider>
        );
        // get the input label using placeholder text
        let infoBoxLabelElement = screen.getByPlaceholderText(
          "Infobox Label Placeholder"
        );

        fireEvent.change(infoBoxLabelElement, { target: { value: "1" } });

        // setprop shoul not call before passing 500 milisec
        expect(setPropMock).not.toHaveBeenCalled();

        setTimeout(() => {}, 200);
        act(() => {
          jest.advanceTimersByTime(200);
        });
        fireEvent.change(infoBoxLabelElement, {
          target: { value: `${infoBoxLabelElement.value}:` },
        });
        fireEvent.change(infoBoxLabelElement, {
          target: { value: `${infoBoxLabelElement.value})` },
        });

        expect(setPropMock).not.toHaveBeenCalled();

        //need delay of a second
        setTimeout(() => {
          fireEvent.change(infoBoxLabelElement, {
            target: { value: `${infoBoxLabelElement.value}H` },
          });
          fireEvent.change(infoBoxLabelElement, {
            target: { value: `${infoBoxLabelElement.value}I` },
          });
        }, 1000);
        act(() => {
          jest.advanceTimersByTime(1000);
        });
        expect(setPropMock).toHaveBeenCalledTimes(1);
        fireEvent.change(infoBoxLabelElement, {
          target: { value: `${infoBoxLabelElement.value}2` },
        });

        expect(setPropMock).toHaveBeenCalledTimes(1);

        //need delay of a second
        // so following sequence should consider as a group of chars
        setTimeout(() => {
          fireEvent.change(infoBoxLabelElement, {
            target: { value: `${infoBoxLabelElement.value}H` },
          });
          fireEvent.change(infoBoxLabelElement, {
            target: { value: `${infoBoxLabelElement.value}E` },
          });
          fireEvent.change(infoBoxLabelElement, {
            target: { value: `${infoBoxLabelElement.value}L` },
          });
          fireEvent.change(infoBoxLabelElement, {
            target: { value: `${infoBoxLabelElement.value}L` },
          });
          fireEvent.change(infoBoxLabelElement, {
            target: { value: `${infoBoxLabelElement.value}O` },
          });
        }, 1500);

        act(() => {
          jest.advanceTimersByTime(1500);
        });
        expect(setPropMock).toHaveBeenCalledTimes(2);

        //need delay of a second
        setTimeout(() => {
          fireEvent.change(infoBoxLabelElement, {
            target: { value: `${infoBoxLabelElement.value}!` },
          });
        }, 500);
        act(() => {
          jest.advanceTimersByTime(500);
        });
        expect(setPropMock).toHaveBeenCalledTimes(3);

        // make delay to have latest version in console.log
        setTimeout(() => {}, 500);
        act(() => {
          jest.advanceTimersByTime(500);
        });
      });

      test("Changing infobox header should group fast actions {500ms}, undo reverts last batch of updates / redo brings back last reverted batch", () => {
        render(
          <MockProvider infoBoxState={mockState}>
            <Header t={mockT} viewOnly={false} />
          </MockProvider>
        );
        // get the input label using placeholder text
        let infoBoxHeaderElement = screen.getByPlaceholderText(
          "Infobox Header Placeholder"
        );

        fireEvent.change(infoBoxHeaderElement, { target: { value: "1" } });

        // setprop shoul not call before passing 500 milisec
        expect(setPropMock).not.toHaveBeenCalled();

        setTimeout(() => {}, 200);
        act(() => {
          jest.advanceTimersByTime(200);
        });
        fireEvent.change(infoBoxHeaderElement, {
          target: { value: `${infoBoxHeaderElement.value}:` },
        });
        fireEvent.change(infoBoxHeaderElement, {
          target: { value: `${infoBoxHeaderElement.value})` },
        });

        expect(setPropMock).not.toHaveBeenCalled();

        //need delay of a second
        setTimeout(() => {
          fireEvent.change(infoBoxHeaderElement, {
            target: { value: `${infoBoxHeaderElement.value}H` },
          });
          fireEvent.change(infoBoxHeaderElement, {
            target: { value: `${infoBoxHeaderElement.value}I` },
          });
        }, 1000);
        act(() => {
          jest.advanceTimersByTime(1000);
        });
        expect(setPropMock).toHaveBeenCalledTimes(1);
        fireEvent.change(infoBoxHeaderElement, {
          target: { value: `${infoBoxHeaderElement.value}2` },
        });

        expect(setPropMock).toHaveBeenCalledTimes(1);

        //need delay of a second
        // so following sequence should consider as a group of chars
        setTimeout(() => {
          fireEvent.change(infoBoxHeaderElement, {
            target: { value: `${infoBoxHeaderElement.value}H` },
          });
          fireEvent.change(infoBoxHeaderElement, {
            target: { value: `${infoBoxHeaderElement.value}E` },
          });
          fireEvent.change(infoBoxHeaderElement, {
            target: { value: `${infoBoxHeaderElement.value}L` },
          });
          fireEvent.change(infoBoxHeaderElement, {
            target: { value: `${infoBoxHeaderElement.value}L` },
          });
          fireEvent.change(infoBoxHeaderElement, {
            target: { value: `${infoBoxHeaderElement.value}O` },
          });
        }, 1500);

        act(() => {
          jest.advanceTimersByTime(1500);
        });
        expect(setPropMock).toHaveBeenCalledTimes(2);

        //need delay of a second
        setTimeout(() => {
          fireEvent.change(infoBoxHeaderElement, {
            target: { value: `${infoBoxHeaderElement.value}!` },
          });
        }, 500);
        act(() => {
          jest.advanceTimersByTime(500);
        });
        expect(setPropMock).toHaveBeenCalledTimes(3);

        // make delay to have latest version in console.log
        setTimeout(() => {}, 500);
        act(() => {
          jest.advanceTimersByTime(500);
        });
      });
    });

    // <------------Testing default behavior------------>
    test("The component renders correctly with the given label and header placeholder", () => {
      render(
        <MockProvider>
          <InfoBox />
        </MockProvider>
      );

      const labelPlaceholder = screen.getByPlaceholderText(
        "Infobox Label Placeholder"
      );
      const headerPlaceHolder = screen.getByPlaceholderText(
        "Infobox Header Placeholder"
      );
      const iconPlaceholder = screen.getByTestId("icon");
      const infoboxDropZone = screen.getByTestId("infobox-drop-zone");

      expect(iconPlaceholder).toBeInTheDocument();
      expect(labelPlaceholder).toBeInTheDocument();
      expect(headerPlaceHolder).toBeInTheDocument();
      expect(infoboxDropZone).toBeInTheDocument();
    });

    test("The component renders correctly with the given placeholder", () => {
      render(
        <MockProvider>
          <InfoBox />
        </MockProvider>
      );

      const labelPlaceholder = screen.getByPlaceholderText(
        "Infobox Label Placeholder"
      );
      const headerPlaceHolder = screen.getByPlaceholderText(
        "Infobox Header Placeholder"
      );
      const iconPlaceholder = screen.getByTestId("icon");
      const infoboxDropZone = screen.getByTestId("infobox-drop-zone");

      expect(iconPlaceholder).toBeInTheDocument();
      expect(labelPlaceholder).toBeInTheDocument();
      expect(headerPlaceHolder).toBeInTheDocument();
      expect(infoboxDropZone).toBeInTheDocument();
    });

    test("The component renders correctly with the given placeholder - drag-drop zone placeholder", () => {
      //
    });

    test("Should allow the user to type text into text-based area inside the component - in InfoBox label", () => {
      //
    });

    test("Should allow the user to type text into text-based area inside the component - in InfoBox header", () => {
      //
    });

    test("The component renders correctly with default icon", () => {
      //
    });

    // <-- Onchange event for label and placeholder -->
    test("Onchange Event for Labels and Headers from User ", async () => {
      render(
        <MockProvider>
          <InfoBox />
        </MockProvider>
      );

      // Label Onchange Event
      const labelElement = screen.getByLabelText("Infobox Label Aria");

      fireEvent.change(labelElement, { target: { value: "This is Label" } });
      expect(labelElement.value).toBe("This is Label");

      // Header OnChange Event
      const headerElement = screen.getByLabelText("Infobox Header Aria");
      fireEvent.change(headerElement, {
        target: { value: "This is Header" },
      });
      expect(headerElement.value).toBe("This is Header");
    });
    // <------------Testing infobox toolbar functionality------------>

    test("The component is clickable and show toolbar when clicked on only icon, label and header", () => {
      //
    });

    test("The component hide toolbar when clicked outside", () => {
      //
    });

    test("The icon is changed with the correct type (definition, try it, ...)", () => {
      //
    });

    test("The icon is hide with no icon option", () => {
      //
    });

    test(`The label  is hide in unchecked "show Label" input`, () => {
      //
    });

    test(`The label is hide in unchecked "show Header" input`, () => {
      //
    });
  });

  describe("InfoboxMain", () => {
    test("It renders video nested component when digital format selected", () => {
      const componentProps = {
        infoBoxState: {
          id: "25ab223a-f0aa-4101-a774-a2a19b1b827d",
          infoBoxIcon: null,
          infoBoxLabel: "",
          infoBoxHeader: { heading: "", headingLevel: "" },
          body: null,
          infoBoxSettings: { header: true, label: true },
          components: [
            {
              componentName: "Video",
              componentProps: {
                videoState: {
                  id: "5abfd05b-8bf2-4705-9fcd-070859061c7b",
                  videoSource: null,
                  videoURL: null,
                  videoDescription: null,
                  videoCredit: null,
                  videoId: null,
                  videoTextSettings: { description: true, credit: true },
                },
              },
              formats: { digital: { shown: true }, print: { shown: false } },
            },
          ],
        },
      };

      const mockedSetProp = jest.fn();

      render(<InfoBoxMain setProp={mockedSetProp} {...componentProps} />, {
        wrapperProps: { selectedFormat: FORMATS.DIGITAL },
      });

      expect(
        screen.getByRole("region", { name: /video/i })
      ).toBeInTheDocument();
    });
    test("It does not render video nested component when print format selected", () => {
      const componentProps = {
        infoBoxState: {
          id: "25ab223a-f0aa-4101-a774-a2a19b1b827d",
          infoBoxIcon: null,
          infoBoxLabel: "",
          infoBoxHeader: { heading: "", headingLevel: "" },
          body: null,
          infoBoxSettings: { header: true, label: true },
          components: [
            {
              componentName: "Video",
              componentProps: {
                videoState: {
                  id: "5abfd05b-8bf2-4705-9fcd-070859061c7b",
                  videoSource: null,
                  videoURL: null,
                  videoDescription: null,
                  videoCredit: null,
                  videoId: null,
                  videoTextSettings: { description: true, credit: true },
                },
              },
              formats: { digital: { shown: true }, print: { shown: false } },
            },
          ],
        },
      };

      const mockedSetProp = jest.fn();

      render(<InfoBoxMain setProp={mockedSetProp} {...componentProps} />, {
        wrapperProps: { selectedFormat: FORMATS.PRINT },
      });

      expect(
        screen.queryByRole("region", { name: /video/i })
      ).not.toBeInTheDocument();
    });

    test("It renders image nested component when print format selected", () => {
      // TODO: Move to JSON files with sample data from real courses
      const componentProps = {
        infoBoxState: {
          id: "25ab223a-f0aa-4101-a774-a2a19b1b827d",
          infoBoxIcon: null,
          infoBoxLabel: "",
          infoBoxHeader: { heading: "", headingLevel: "" },
          body: null,
          infoBoxSettings: { header: true, label: true },
          components: [
            {
              componentName: "Image",
              componentProps: {
                imgSize: { width: 100, height: 100 },
                uploadedImg: null,
                alt: "",
                customAltTextOptionSelected: "Custom alt text",
                longDesc: "",
                credit: "",
                imageTextSettings: {
                  description: true,
                  credit: true,
                  fullWidth: false,
                },
                imgLink: null,
                caption: null,
              },
              formats: { digital: { shown: true }, print: { shown: true } },
            },
          ],
        },
      };

      const mockedSetProp = jest.fn();

      render(<InfoBoxMain setProp={mockedSetProp} {...componentProps} />, {
        wrapperProps: { selectedFormat: FORMATS.PRINT },
      });

      expect(
        screen.getByRole("region", { name: /image/i })
      ).toBeInTheDocument();
    });

    test("It renders image nested component when digital format selected", () => {
      const componentProps = {
        infoBoxState: {
          id: "25ab223a-f0aa-4101-a774-a2a19b1b827d",
          infoBoxIcon: null,
          infoBoxLabel: "",
          infoBoxHeader: { heading: "", headingLevel: "" },
          body: null,
          infoBoxSettings: { header: true, label: true },
          components: [
            {
              componentName: "Image",
              componentProps: {
                imgSize: { width: 100, height: 100 },
                uploadedImg: null,
                alt: "",
                customAltTextOptionSelected: "Custom alt text",
                longDesc: "",
                credit: "",
                imageTextSettings: {
                  description: true,
                  credit: true,
                  fullWidth: false,
                },
                imgLink: null,
                caption: null,
              },
              formats: { digital: { shown: true }, print: { shown: true } },
            },
          ],
        },
      };

      const mockedSetProp = jest.fn();

      render(<InfoBoxMain setProp={mockedSetProp} {...componentProps} />, {
        wrapperProps: { selectedFormat: FORMATS.DIGITAL },
      });

      expect(
        screen.getByRole("region", { name: /image/i })
      ).toBeInTheDocument();
    });
  });

  describe("Performance", () => {
    test("should handle large amounts of text without slowing down", () => {
      // Render the component
    });
  });

  describe.skip("Security", () => {
    test("should sanitize input to prevent XSS attacks", () => {
      const user = userEvent.setup();
    });
  });

  describe.skip("Accessibility", () => {
    test("The component is accessible for screen readers and keyboard navigation", async () => {
      const user = userEvent.setup();
    });
  });
});
