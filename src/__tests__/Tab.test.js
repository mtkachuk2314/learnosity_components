/**
 *  Tabs Unit Tests
 *
 *
 *  Test categories:
 *         ** Functionality --> ensure the component is functioning as expected
 *         ** Performance --> test the speed and efficiency of the component
 *         ** Security --> test the security of the component
 *         ** Accessibility --> test accessible for screen readers and keyboard navigation
 *
 */

import "@testing-library/jest-dom";
import { render, screen, fireEvent } from "../TestUtility/renderUtils";
import Tabs from "../components/Tabs/subcomponents/Tabs";
import { LayoutProvider } from "../components/Tabs/TabContext";

// import { logRoles } from "@testing-library/dom";

// Mocked uuid library
jest.mock("uuid", () => ({
  v4: () => "123456789",
}));

// Mocked context values
let mockState = [
  {
    id: 123456789,
    title: "",
    components: [],
    expanded: true,
  },
];

let timeline = {
  mockInversePatches: [],
  current: mockState,
  mockForwardPatches: [],
};

// should mock undo base on inverse array
const undoMock = jest.fn(() => {
  if (timeline.mockInversePatches.length > 0) {
    const { mockInversePatches, current, mockForwardPatches } = timeline;
    const length = mockInversePatches.length;
    const lastItem = mockInversePatches[length - 1];
    const restOfArr = mockInversePatches.slice(0, length - 1);
    timeline = {
      mockInversePatches: restOfArr,
      current: lastItem,
      mockForwardPatches: [...mockForwardPatches, current],
    };
  }
});

// should mock redo base on inverse array
const redoMock = jest.fn(() => {
  if (timeline.mockForwardPatches.length > 0) {
    const { mockInversePatches, current, mockForwardPatches } = timeline;
    const length = mockForwardPatches.length;
    const lastItem = mockForwardPatches[length - 1];
    const restOfArr = mockForwardPatches.slice(0, length - 1);
    timeline = {
      mockInversePatches: [...mockInversePatches, current],
      current: lastItem,
      mockForwardPatches: restOfArr,
    };
  }
});

// should mock updating state in authoring - which can change layoutState base on inverse and forward patches arrays
const setPropMock = jest.fn((newState) => {
  const { layoutState } = newState;
  const { mockInversePatches, current, mockForwardPatches } = timeline;
  timeline = {
    mockInversePatches:
      JSON.stringify(current) === JSON.stringify(layoutState)
        ? [...mockInversePatches]
        : [...mockInversePatches, current],
    current: layoutState,
    // this seems to be the idiomatic approach for most applications
    mockForwardPatches: [...mockForwardPatches],
  };
});

const MockProvider = ({ children, layoutState }) => {
  return (
    <LayoutProvider layoutState={layoutState} setProp={setPropMock}>
      {children}
    </LayoutProvider>
  );
};

describe("Tab Component", () => {
  describe("Functionality", () => {
    beforeEach(() => {
      jest.useFakeTimers();
      timeline = {
        mockInversePatches: [],
        current: mockState,
        mockForwardPatches: [],
      };
    });

    afterEach(() => {
      jest.clearAllMocks();
      jest.clearAllTimers();
      timeline = {
        mockInversePatches: [],
        current: mockState,
        mockForwardPatches: [],
      };
    });
    // <------------Testing updates - debounced & none debounced & undo/redo & race condition------------>
    test("Changing tab title should group fast actions and undo action removes a group of characters", () => {
      // Render the component
      render(
        <MockProvider layoutState={mockState}>
          <Tabs />
        </MockProvider>
      );

      // make pane active - otherwise it wont find title element
      const tabPaneElement = screen.getByTestId("tab-title-0");
      fireEvent.click(tabPaneElement);

      // Get the input element using a data-testid or other selector
      let tabTitleElement = screen.getByLabelText("tab title input");
      // Simulate user typing - check if setProp debounce properlly

      // grouping sequence : 1:) HI2 Hello  !

      fireEvent.change(tabTitleElement, { target: { value: "1" } });

      setTimeout(() => {}, 200);
      jest.advanceTimersByTime(200);

      fireEvent.change(tabTitleElement, {
        target: { value: `${tabTitleElement.value}:` },
      });
      fireEvent.change(tabTitleElement, {
        target: { value: `${tabTitleElement.value})` },
      });

      //need delay of a second
      setTimeout(() => {
        fireEvent.change(tabTitleElement, {
          target: { value: `${tabTitleElement.value}H` },
        });
        fireEvent.change(tabTitleElement, {
          target: { value: `${tabTitleElement.value}I` },
        });
      }, 1000);

      jest.advanceTimersByTime(1000);

      fireEvent.change(tabTitleElement, {
        target: { value: `${tabTitleElement.value}2` },
      });

      //need delay of a second
      // so following sequence should consider as a group of chars
      setTimeout(() => {
        fireEvent.change(tabTitleElement, {
          target: { value: `${tabTitleElement.value}H` },
        });
        fireEvent.change(tabTitleElement, {
          target: { value: `${tabTitleElement.value}E` },
        });
        fireEvent.change(tabTitleElement, {
          target: { value: `${tabTitleElement.value}L` },
        });
        fireEvent.change(tabTitleElement, {
          target: { value: `${tabTitleElement.value}L` },
        });
        fireEvent.change(tabTitleElement, {
          target: { value: `${tabTitleElement.value}O` },
        });
      }, 1000);

      jest.advanceTimersByTime(1000);

      //need delay of half second
      setTimeout(() => {
        fireEvent.change(tabTitleElement, {
          target: { value: `${tabTitleElement.value}!` },
        });
      }, 1000);
      jest.advanceTimersByTime(1000);

      setTimeout(() => {}, 1000);
      jest.advanceTimersByTime(1000);

      console.log(timeline.mockForwardPatches);
      console.log(timeline.mockInversePatches);
      console.log("----->", timeline.current);
      console.log(mockState);

      // ----------------------------undo-----------------------------------//
      // //undo act -> check debouncing and undo functionality
      // // - hit undo by user - call undo mock
      undoMock();

      fireEvent.change(tabTitleElement, {
        target: { value: timeline.current[0].title },
      });

      setTimeout(() => {}, 1000);
      jest.advanceTimersByTime(1000);

      // // //assertion
      expect(tabTitleElement).toHaveValue("1:)HI2HELLO");

      undoMock();

      fireEvent.change(tabTitleElement, {
        target: { value: timeline.current[0].title },
      });

      //assertion
      expect(tabTitleElement).toHaveValue("1:)HI2");

      undoMock();

      fireEvent.change(tabTitleElement, {
        target: { value: timeline.current[0].title },
      });

      //assertion
      expect(tabTitleElement).toHaveValue("1:)");
      expect(tabTitleElement).not.toHaveValue("1:)H");

      // ----------------------------redo-----------------------------------//
      redoMock();

      fireEvent.change(tabTitleElement, {
        target: { value: timeline.current[0].title },
      });
      //assertion
      expect(tabTitleElement).toHaveValue("1:)HI2");

      redoMock();

      fireEvent.change(tabTitleElement, {
        target: { value: timeline.current[0].title },
      });

      //assertion
      expect(tabTitleElement).toHaveValue("1:)HI2HELLO");

      // ----------------------------undo/redo-----------------------------------//

      undoMock();

      fireEvent.change(tabTitleElement, {
        target: { value: timeline.current[0].title },
      });

      //assertion
      expect(tabTitleElement).toHaveValue("1:)HI2");

      redoMock();

      fireEvent.change(tabTitleElement, {
        target: { value: timeline.current[0].title },
      });

      //assertion
      expect(tabTitleElement).toHaveValue("1:)HI2HELLO");

      redoMock();

      fireEvent.change(tabTitleElement, {
        target: { value: timeline.current[0].title },
      });

      //assertion
      expect(tabTitleElement).toHaveValue("1:)HI2HELLO!");

      redoMock();

      fireEvent.change(tabTitleElement, {
        target: { value: timeline.current[0].title },
      });

      //assertion
      expect(tabTitleElement).toHaveValue("1:)HI2HELLO!");
    });
    test("Changing tab title should group user input character and debounce state update", () => {
      // Render the component
      render(
        <MockProvider layoutState={mockState}>
          <Tabs />
        </MockProvider>
      );

      // make pane active - otherwise it wont find title element
      const tabPaneElement = screen.getByTestId("tab-title-0");
      fireEvent.click(tabPaneElement);

      // Get the input element using a data-testid or other selector
      let tabTitleElement = screen.getByLabelText("tab title input");
      // Simulate user typing - trigger onchange event handler which include debounce logic for each char

      // Simulate user typing - check if setProp debounce properlly

      // grouping sequence : 1:) HI2 Hello  !

      fireEvent.change(tabTitleElement, { target: { value: "1" } });

      // setprop shoul not call before passing 500 milisec
      expect(setPropMock).not.toHaveBeenCalled();

      setTimeout(() => {}, 200);
      jest.advanceTimersByTime(200);

      fireEvent.change(tabTitleElement, {
        target: { value: `${tabTitleElement.value}:` },
      });
      fireEvent.change(tabTitleElement, {
        target: { value: `${tabTitleElement.value})` },
      });

      expect(setPropMock).not.toHaveBeenCalled();

      //need delay of a second
      setTimeout(() => {
        fireEvent.change(tabTitleElement, {
          target: { value: `${tabTitleElement.value}H` },
        });
        fireEvent.change(tabTitleElement, {
          target: { value: `${tabTitleElement.value}I` },
        });
      }, 1000);

      jest.advanceTimersByTime(1000);
      expect(setPropMock).toHaveBeenCalledTimes(1);

      fireEvent.change(tabTitleElement, {
        target: { value: `${tabTitleElement.value}2` },
      });

      expect(setPropMock).toHaveBeenCalledTimes(1);

      //need delay of a second
      // so following sequence should consider as a group of chars
      setTimeout(() => {
        fireEvent.change(tabTitleElement, {
          target: { value: `${tabTitleElement.value}H` },
        });
        fireEvent.change(tabTitleElement, {
          target: { value: `${tabTitleElement.value}E` },
        });
        fireEvent.change(tabTitleElement, {
          target: { value: `${tabTitleElement.value}L` },
        });
        fireEvent.change(tabTitleElement, {
          target: { value: `${tabTitleElement.value}L` },
        });
        fireEvent.change(tabTitleElement, {
          target: { value: `${tabTitleElement.value}O` },
        });
      }, 1000);

      jest.advanceTimersByTime(1000);
      expect(setPropMock).toHaveBeenCalledTimes(2);

      //need delay of a second
      setTimeout(() => {
        fireEvent.change(tabTitleElement, {
          target: { value: `${tabTitleElement.value}!` },
        });
      }, 1000);
      jest.advanceTimersByTime(1000);
      expect(setPropMock).toHaveBeenCalledTimes(3);

      // make delay to have latest version in console.log
      setTimeout(() => {}, 1000);
      jest.advanceTimersByTime(1000);
    });
  });
});
