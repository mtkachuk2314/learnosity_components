import React, {
  createContext,
  useReducer,
  useEffect,
  useState,
  useMemo,
} from "react";
import produce from "immer";
import isEqual from "lodash/isEqual";

// state of video data stored in VideoContext
export const ImageContext = createContext();

// Reducers
export const imageConfig = (draft, action) => {
  switch (action.func) {
    case "UPDATE_STATE":
      return action.data;
    case "SET_IMAGE":
      draft.uploadedImg = action.src;
      return draft;
    case "SET_IMAGE_SIZE":
      draft.imgSize.height = action.imgHeight;
      draft.imgSize.width = action.imgWidth;
      return draft;
    case "CHANGE_DESCRIPTION":
      draft.longDesc = action.description;
      return draft;
    case "CHANGE_CREDIT":
      draft.credit = action.credit;
      return draft;
    case "CHANGE_TEXT_SETTINGS":
      draft.imageTextSettings = action.textSettings;
      return draft;
    case "CHANGE_ALT":
      draft.alt = action.alt;
      return draft;
    case "CHANGE_ALT_OPTION":
      draft.customAltTextOptionSelected = action.customAltTextOptionSelected;
      return draft;
    default:
      return draft;
  }
};

// video provider wraps the tab component to access reducer
export const ImageProvider = ({ children, setProp, imageState }) => {
  const [state, dispatch] = useReducer(produce(imageConfig), imageState);

  const diff = !isEqual(state, imageState);
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    if (!mounted) {
      dispatch({ func: "UPDATE_STATE", data: imageState });
      setMounted(true);
    }
    diff && mounted && setProp({ ...state });
  }, [state]);

  useEffect(() => {
    diff && mounted && dispatch({ func: "UPDATE_STATE", data: imageState });
  }, [imageState]);

  const imageValue = useMemo(() => [state, dispatch], [state]);
  return (
    <ImageContext.Provider value={imageValue}>{children}</ImageContext.Provider>
  );
};
