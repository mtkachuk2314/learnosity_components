import React, {
  useContext,
  useCallback,
  useMemo,
  useState,
  useRef,
  useEffect,
} from "react";
// Translation
import { useTranslation } from "react-i18next";
// Context
import {
  useSetFocused,
  useFocused,
  useSetDescriptionRef,
  useSetCreditRef,
} from "./TabContext";
import { ImageContext } from "../ImageContext";
// Components
import Text from "../../Text/Text";

const ImageDescription = ({
  toolbarRef,
  toolbar,
  imageTextSettings,
  isKebab,
  setIsKebab,
  toolbarContainerRef,
  showSelf,
  isActiveComponent,
}) => {
  // Translation
  const { t } = useTranslation();
  // State
  const [state, dispatch] = useContext(ImageContext);

  // WYSIWYG Editor
  const setFocused = useSetFocused();
  const focused = useFocused();
  const setDescriptionRef = useSetDescriptionRef();
  const setCreditRef = useSetCreditRef();

  // Description and Credit text
  const [creditText, setCreditText] = useState(null);
  const [descriptionText, setDescriptionText] = useState(null);

  // Description and Credit references
  const imgTextRef = useRef();
  const imgDescRef = useRef();
  const imgCredRef = useRef();

  // Description and Credit text update
  const updateDescription = useCallback((body) => {
    dispatch({ func: "CHANGE_DESCRIPTION", description: body.body });
  });

  const updateCredit = useCallback((body) => {
    dispatch({ func: "CHANGE_CREDIT", credit: body.body });
  });

  // Description and Credit portals
  const portalDescription = useMemo(() => {
    return {
      parentComponent: "image",
      parentSection: "description",
      placeholder: t("Type image description here"),
      toolbarReference: toolbar,
      shouldPortal: focused === "Description",
      isKebab,
      setIsKebab: (result) => setIsKebab(result),
      disabledButtons: [],
      setTextRef: (result) => setDescriptionRef(result),
      setTextContainer: (result) => setDescriptionText(result),
    };
  }, [toolbar, focused, isKebab]);

  const portalCredit = useMemo(() => {
    return {
      parentComponent: "image",
      parentSection: "credit",
      placeholder: t("Type credit here"),
      toolbarReference: toolbar,
      shouldPortal: focused === "Credit",
      isKebab,
      setIsKebab: (result) => setIsKebab(result),
      disabledButtons: [],
      setTextRef: (result) => setCreditRef(result),
      setTextContainer: (result) => setCreditText(result),
    };
  }, [toolbar, focused, isKebab]);

  // Description and Credit click handlers
  const handleDescriptionClick = useCallback((e) => {
    setFocused("Description");
  }, []);
  const handleCreditClick = useCallback((e) => {
    setFocused("Credit");
  }, []);

  useEffect(() => {
    if (focused === "Description" && imageTextSettings.description === false) {
      setFocused("Image");
    }
    if (focused === "Credit" && imageTextSettings.credit === false) {
      setFocused("Image");
    }
    dispatch({ func: "CHANGE_TEXT_SETTINGS", textSettings: imageTextSettings });
  }, [imageTextSettings]);

  return (
    <div
      className="description-credit-container"
      ref={imgTextRef}
      data-testid="image-text-container"
    >
      {state.imageTextSettings.description === true && (
        <div
          ref={imgDescRef}
          className="image-description"
          onClick={handleDescriptionClick}
          onFocus={handleDescriptionClick}
          onBlur={(e) => {
            const relatedTarget = e.relatedTarget || document.activeElement;
            if (descriptionText.contains(relatedTarget)) e.stopPropagation();
            if (!toolbarRef.current?.contains(relatedTarget)) setFocused(null);
          }}
        >
          {/* Description Text box */}
          <Text
            body={state.longDesc}
            setProp={updateDescription}
            portal={portalDescription}
            t={t}
            toolbarContainerRef={toolbarContainerRef}
            showSelf={showSelf}
            isActiveComponent={isActiveComponent}
          />
        </div>
      )}
      {state.imageTextSettings.credit === true && (
        <div
          ref={imgCredRef}
          className="image-credit"
          onClick={handleCreditClick}
          onFocus={handleCreditClick}
          onBlur={(e) => {
            const relatedTarget = e.relatedTarget || document.activeElement;
            if (creditText.contains(relatedTarget)) e.stopPropagation();
            if (!toolbarRef.current?.contains(relatedTarget)) setFocused(null);
          }}
        >
          {/* Credit Text box */}
          <Text
            body={state.credit}
            setProp={updateCredit}
            portal={portalCredit}
            t={t}
            toolbarContainerRef={toolbarContainerRef}
            showSelf={showSelf}
            isActiveComponent={isActiveComponent}
          />
        </div>
      )}
    </div>
  );
};

export default ImageDescription;
