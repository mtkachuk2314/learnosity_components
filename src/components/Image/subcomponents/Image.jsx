import React, { useState, useEffect, useRef, useMemo, useContext } from "react";
import { Box } from "@mui/material";
// Translation
import { useTranslation } from "react-i18next";

// Context
import { ImageContext } from "../ImageContext";
import { useSetFocused, useFocused } from "./TabContext";

// CK Finder
import TenantContext from "../../../Context/TenantContext";

// Components
import ImageFrame from "./ImageFrame";
import ImageDescription from "./ImageDescription";
import ToolBar from "./Toolbar";

// Stylesheet
import "../assets/styles.scss";
// portal
import PortalUtil from "../../../Utility/PortalUtil";

const Image = ({
  setActiveComponent,
  parentComponent,
  imageId,
  toolbarContainerRef,
  showSelf,
  isActiveComponent,
}) => {
  // Translation
  const { t } = useTranslation();

  // State
  const [state] = useContext(ImageContext);

  // Toolbar
  const [toolbar, setToolbar] = useState(null);
  const [isKebab, setIsKebab] = useState(false);
  const toolbarRef = useRef();

  // Focus
  const setFocused = useSetFocused();
  const focused = useFocused();
  const [imageFocused, setImageFocused] = useState(false);

  //   Refs
  const imageRef = useRef();

  // CKFinder / Image API
  const [imageSRC, setImageSRC] = useState(null);
  const { openAssetManager, mediaRootURL, viewOnlyComponents } =
    useContext(TenantContext);

  const viewOnly = !viewOnlyComponents;

  // Image Text Settings
  const [imageTextSettings, setImageTextSettings] = useState({
    description: true,
    credit: true,
    fullWidth: false,
  });

  // Aria Live
  const [ariaLive, setAriaLive] = useState("");
  const [ariaLive2, setAriaLive2] = useState("");

  // Handle Aria live region
  const handleAriaLive = (value) => {
    if (ariaLive === value) {
      setAriaLive("");
      setAriaLive2(value);
    } else {
      setAriaLive2("");
      setAriaLive(value);
    }
  };

  const imageCompFocused = () => {
    setActiveComponent(true);
  };

  useMemo(() => {
    if (state.imageTextSettings) {
      setImageTextSettings(state.imageTextSettings);
    }
  }, []);

  // Open CKFinder if ckFinderOpen is true / request data from CKFinder
  const CkFinderTrigger = () => {
    ckFinderShowUp();
    const myObj = async () => {
      const asset = await openAssetManager();
      if (!asset?.fileName) {
        // eslint-disable-next-line no-console
        console.error("CK finder does not have a name attribute");
        handleAriaLive("CK finder does not have a name attribute");
        CkFinderCloser();
        return;
      }

      // Also returns URL to S3 in "url"
      const { fileName, fileEnding } = asset;

      // If adding a file of a disallowed type, prevent and show error
      if (
        !["jpg", "jpeg", "png", "svg", "webp", "gif"].includes(
          fileEnding?.toLowerCase()
        )
      ) {
        console.error("File type is not supported");
        handleAriaLive("File type is not supported");
        CkFinderCloser();

        return;
      }
      setImageSRC(fileName);
      CkFinderCloser();
      setFocused("Image");
      imageRef.current.focus();
    };
    handleAriaLive("CKFinder Opened");
    myObj();
  };

  const CkFinderCloser = () => {
    if (focused === "Image") {
      handleAriaLive("CKFinder Closed");
    }
  };
  const ckFinderShowUp = () => {
    const modalElement = document.getElementById("ckf-modal");
    if (modalElement) modalElement.style.display = "";
  };

  useEffect(() => {
    return () => {
      const modalElement = document.getElementById("ckf-modal");
      if (modalElement) {
        modalElement.style.display = "none";
      }
    };
  }, []);

  return (
    <Box
      aria-label={t("Image")}
      data-testid="image-component-container"
      className="image-component-container"
      // tabIndex necessary to turn focus back to component after ckfinder
      tabIndex={0}
      role="region"
      ref={imageRef}
      onFocus={(e) => imageCompFocused(e)}
      onClick={(e) => imageCompFocused(e)}
      sx={{
        paddingTop: "20px",
        paddingBottom: "20px",
      }}
    >
      <span
        className="sr-only"
        role="status"
        aria-live="assertive"
        aria-relevant="all"
        aria-atomic="true"
      >
        {ariaLive}
      </span>
      <span
        className="sr-only"
        role="status"
        aria-live="assertive"
        aria-relevant="all"
        aria-atomic="true"
      >
        {ariaLive2}
      </span>

      <ImageFrame
        imageId={imageId}
        mediaRootURL={mediaRootURL}
        CkFinderTrigger={CkFinderTrigger}
        CkFinderCloser={CkFinderCloser}
        parentComponent={parentComponent}
        handleAriaLive={handleAriaLive}
        imageSRC={imageSRC}
        setImageSRC={setImageSRC}
        imageTextSettings={imageTextSettings}
        setImageFocused={setImageFocused}
        imageFocused={imageFocused}
        viewOnly={viewOnly}
      />
      {!viewOnly && (showSelf || isActiveComponent) ? (
        <PortalUtil portalRef={toolbarContainerRef}>
          <ToolBar
            ref={toolbarRef}
            handleAriaLive={handleAriaLive}
            setToolbar={setToolbar}
            parentComponent={parentComponent}
            setImageTextSettings={setImageTextSettings}
            CkFinderTrigger={CkFinderTrigger}
            imageId={imageId}
            setIsKebab={setIsKebab}
            isKebab={isKebab}
            isActiveComponent={isActiveComponent}
            showSelf={showSelf}
          />
        </PortalUtil>
      ) : null}

      <div className="image-text-container" data-testid="image-text-container">
        <ImageDescription
          toolbarRef={toolbarRef}
          toolbar={toolbar}
          imageTextSettings={imageTextSettings}
          isKebab={isKebab}
          setIsKebab={setIsKebab}
          imageFocused={imageFocused}
          toolbarContainerRef={toolbarContainerRef}
          showSelf={showSelf}
          isActiveComponent={isActiveComponent}
        />
      </div>
    </Box>
  );
};

export default Image;
