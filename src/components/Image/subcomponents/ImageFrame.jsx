import React, { useContext, useRef, useEffect, useState } from "react";
// Context
import { Button } from "@mui/material";
import { useTranslation } from "react-i18next";
import { useSetFocused, useFocused } from "./TabContext";
import { ImageContext } from "../ImageContext";
// MUI Imports
// assets
import ImageIcon from "../assets/ImageIcon.png";
// Translation

const ImageFrame = ({
  CkFinderTrigger,
  CkFinderCloser,
  handleAriaLive,
  imageSRC,
  setImageSRC,
  parentComponent,
  mediaRootURL,
  viewOnly,
}) => {
  // Translation
  const { t } = useTranslation();
  const [state, dispatch] = useContext(ImageContext);

  // Ref
  const imageRef = useRef();
  const imageLoadedRef = useRef();

  // Focus
  const setFocused = useSetFocused();

  // Prevent fetch on initial component mount
  const imgError = () => {
    handleAriaLive("Image could not be loaded");
    console.error("Image could not be loaded");
    setImageSRC(null);
  };

  const loadImage = (e) => {
    dispatch({
      func: "SET_IMAGE",
      src: imageSRC,
    });
    dispatch({
      func: "SET_IMAGE_SIZE",
      imgHeight: e.target.naturalHeight,
      imgWidth: e.target.naturalWidth,
    });

    setTimeout(CkFinderCloser, 1000);
    handleAriaLive("Image has been added");
    setImageSRC(null);
  };

  return (
    <div
      className="image-wrapper"
      data-testid="image-wrapper"
      ref={imageRef}
      tabIndex={0}
      onFocus={(e) => setFocused("Image")}
      onClick={(e) => setFocused("Image")}
      role="img"
      aria-label="image-wrapper-div"
    >
      {state.uploadedImg === null && imageSRC === null && (
        <div className="image-default-container no-image">
          <div className="icon-container">
            <img
              className="default-icon"
              src={ImageIcon}
              alt=""
              role="presentation"
            />
          </div>
          <Button
            aria-hidden={
              state.uploadedImg !== null || imageSRC ? "true" : "false"
            }
            tabIndex={state.uploadedImg !== null || !imageSRC ? 0 : -1}
            disableRipple
            disableFocusRipple
            className="select-image"
            disabled={viewOnly}
            onClick={() => {
              CkFinderTrigger();
            }}
          >
            {t("Select Image")}
          </Button>
        </div>
      )}
      {(state.uploadedImg !== null || imageSRC) && (
        <div
          className="image-default-container"
          style={{
            "--imageWidth":
              state.imageTextSettings.fullWidth === true ? "100%" : "auto",
            "--maxWidth":
              state.imageTextSettings.fullWidth === true ? "100%" : "78%",
          }}
        >
          {imageSRC ? (
            <img
              src={`${mediaRootURL}/${encodeURIComponent(imageSRC)}`}
              alt=""
              className="ckimage"
              onError={imgError}
              onLoad={(e) => loadImage(e)}
            />
          ) : (
            <img
              ref={imageLoadedRef}
              src={`${mediaRootURL}/${encodeURIComponent(state.uploadedImg)}`}
              alt={
                state.alt &&
                state.customAltTextOptionSelected === "Custom alt text"
                  ? state.alt
                  : ""
              }
              className="ckimage"
              onError={imgError}
            />
          )}
        </div>
      )}
    </div>
  );
};

export default ImageFrame;
