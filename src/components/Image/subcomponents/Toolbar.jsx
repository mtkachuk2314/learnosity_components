import React, {
  useState,
  useEffect,
  useRef,
  useContext,
  useCallback,
  forwardRef,
} from "react";
import { useTranslation } from "react-i18next";

// Context
import {
  Paper,
  AppBar,
  Toolbar,
  Button,
  IconButton,
  Popper,
  Grow,
  Tooltip,
  MenuList,
  ClickAwayListener,
  FormGroup,
  FormControl,
  FormControlLabel,
  Checkbox,
} from "@mui/material";
import { Done } from "@mui/icons-material";
import { ImageContext } from "../ImageContext";
import useDebounce from "../../../hooks/useDebounce";
import { useFocused } from "./TabContext";

// Components
// Styles
import "../assets/Toolbar.scss";
// Icons
import KebabSVG from "../assets/Kebab";
import icons from "../assets/icons";

const ToolBar = forwardRef(function ToolBar(
  {
    handleAriaLive,
    parentComponent,
    CkFinderTrigger,
    setToolbar,
    imageId,
    setImageTextSettings,
    setIsKebab,
    isKebab,
    isActiveComponent,
    showSelf,
  },
  ref
) {
  // Translation
  const { t } = useTranslation();

  // State Context
  const [state, dispatch] = useContext(ImageContext);
  const focused = useFocused();

  // Image State
  const [openAlt, setOpenAlt] = useState(false);
  const [openAltSettings, setOpenAltSettings] = useState(false);
  const [openDescriptionKebab, setOpenDescriptionKebab] = useState(false);
  const [openDescriptionKebabPortal, setOpenDescriptionKebabPortal] =
    useState(false);
  const [optionSelected, setOptionSelected] = useState("");
  const [inputAltText, setInputAltText] = useState(
    state.customAltTextOptionSelected === "Decorative image" ? "" : state.alt
  );

  // Refs
  const portalToolbarRef = useRef();

  const addImageRef = useRef(null);
  const altRef = useRef();
  const altSettingsRef = useRef();
  const altPopperRef = useRef();

  const selectImageRef = useRef(null);
  const DescriptionKebab = useRef(null);
  const DescriptionKebabPortal = useRef(null);
  const kebabSelectRefText = useRef(null);

  // On construction of component

  // for checking if state already has values
  useEffect(() => {
    if (
      state &&
      state.customAltTextOptionSelected === "Custom alt text" &&
      state.alt
    ) {
      setOptionSelected("Custom alt text");
    } else if (
      state &&
      state.customAltTextOptionSelected === "Decorative image"
    ) {
      setOptionSelected(state.customAltTextOptionSelected);
    }
  }, []);

  // debouncing typing in alt area
  const updateAltState = (altBody) => {
    dispatch({
      func: "CHANGE_ALT",
      alt: altBody,
    });
  };

  const handleDebounce = useDebounce(updateAltState);

  // Update Alt Function
  const updateAlt = useCallback(
    (body) => {
      setInputAltText(body);
      handleDebounce(body);
    },
    [handleDebounce]
  );

  // update alt after state change
  useEffect(() => {
    setInputAltText(state.alt);
  }, [state.alt]);

  // Update Alt Option Function
  const updateAltOption = (optionSelect) => {
    setOptionSelected(optionSelect);
    dispatch({
      func: "CHANGE_ALT_OPTION",
      customAltTextOptionSelected: optionSelect,
    });
  };

  // Toggle Add Image Button
  const handleToggleAddImage = () => {
    CkFinderTrigger();
    setOpenAlt(false);
    setOpenAltSettings(false);
    setOpenDescriptionKebab(false);
  };

  // Toggle Alt Text
  const handleToggleAlt = (e) => {
    e.stopPropagation();
    setOpenAlt(!openAlt);
    setOpenAltSettings(false);
    setOpenDescriptionKebab(false);
    if (openAlt === true) {
      handleAriaLive("Alt text menu closed");
    } else {
      handleAriaLive("Alt text menu opened");
    }
  };
  // Toggle Kebab Dropdown
  const handleToggleImageKebab = (e, isPortal) => {
    e.stopPropagation();
    if (isPortal) {
      setIsKebab(!isKebab);
      setOpenDescriptionKebabPortal(!openDescriptionKebabPortal);
    } else {
      setOpenDescriptionKebab(!openDescriptionKebab);
    }
    setOpenAlt(false);
    setOpenAltSettings(false);
    if (openDescriptionKebab === true || openDescriptionKebabPortal === true) {
      handleAriaLive("Image options menu closed");
    } else {
      handleAriaLive("Image options menu opened");
    }
  };
  // ? Image Text Settings
  const handleTextSettings = (e, source) => {
    e.stopPropagation();
    setImageTextSettings((imageTextSettings) => ({
      ...imageTextSettings,
      [source]: e.target.checked,
    }));
  };

  useEffect(() => {
    setOpenAlt(false);
    setOpenAltSettings(false);
    setOpenDescriptionKebab(false);
    setOpenDescriptionKebabPortal(false);
  }, [focused]);

  useEffect(() => {
    setToolbar(portalToolbarRef.current);
  }, []);

  return (
    <div
      ref={ref}
      className="ToolbarContainer"
      style={{
        "--active": isActiveComponent || showSelf ? "inline-flex" : "none",
        justifyContent: "center",
      }}
      onClick={(e) => e.stopPropagation()}
      onFocus={(e) => e.stopPropagation()}
      data-testid="image-toolbar"
    >
      <AppBar
        position="static"
        className="StyledAppbar"
        elevation={0}
        data-id={`image-${focused === "Image" ? "config" : focused}-toolbar`}
        style={{
          "--display": "flex",
          "--direction": "row",
          boxShadow:
            "0px 1px 3px 0px rgba(35, 35, 35, 0.1), 0px 2px 5px 0px rgba(161, 162, 163, 0.2), 3px 3px 4px 0px rgba(35, 35, 35, 0.15)",
          backgroundColor: "#fff",
          justifyContent: "center",
        }}
      >
        {!(focused === "Description" || focused === "Credit") &&
          state.uploadedImg !== null && (
            <Toolbar
              position="static"
              ref={selectImageRef}
              className="StyledToolbar"
              style={{
                "--width": "254px",
                "--grid-template-columns": "2fr 9px 1fr 9px 32px ",
                "--boxShadow":
                  "0px 0px 3px 0px rgba(35, 35, 35, 0.15),-1px 0px 5px 0px rgba(161, 162, 163, 0.2),3px 3px 4px 0px rgba(35, 35, 35, 0.15)",
              }}
            >
              {/* Change Image */}
              <Button
                ref={addImageRef}
                data-id="change-image-test"
                aria-label={`${t("change image")}`}
                disableRipple
                disableFocusRipple
                variant="contained"
                color="inherit"
                className="SelectButton"
                style={{
                  "--active": "#232323",
                  "--width": "100%",
                  "--height": "100%",
                  "--font-size": "16px",
                  "--grid-template-columns": "1fr",
                  "--hover-background-color": "transparent",
                }}
                onClick={handleToggleAddImage}
              >
                {`${t("Change Image")}`}
              </Button>
              {/* Divider */}
              <div className="StyledDivider" />
              {/* Alt Text */}
              <Button
                ref={altRef}
                data-id="alt-text-test"
                aria-label={`${t("Alt text")}`}
                variant="contained"
                disableRipple
                disableFocusRipple
                className="SelectButton"
                style={{
                  "--active": openAlt ? "#1565C0" : "#000",
                  "--width": "100%",
                  "--height": "100%",
                  "--font-size": "16px",
                  "--grid-template-columns": "1fr",
                  "--hover-background-color": "transparent",
                }}
                onClick={handleToggleAlt}
              >
                {`${t("Alt text")}`}
              </Button>
              {/* First dropdown for Alt, showing current selection */}
              <ClickAwayListener
                onClickAway={() => {
                  setOpenAlt(false);
                  setOpenAltSettings(false);
                }}
              >
                <Popper
                  ref={altPopperRef}
                  open={openAlt}
                  anchorEl={altRef.current}
                  placement="bottom-start"
                  transition
                  disablePortal
                  style={{ zIndex: 1098 }}
                  modifiers={[
                    {
                      name: "offset",
                      options: {
                        offset: [-5, 0],
                      },
                    },
                    {
                      name: "flip",
                      enabled: true,
                      options: {
                        altBoundary: true,
                        rootBoundary: "document",
                        padding: 8,
                      },
                    },
                    {
                      name: "preventOverflow",
                      enabled: false,
                      options: {
                        altAxis: true,
                        altBoundary: true,
                        tether: true,
                        rootBoundary: "document",
                        padding: 8,
                      },
                    },
                  ]}
                >
                  {({ TransitionProps }) => (
                    <Grow {...TransitionProps}>
                      <Paper
                        elevation={0}
                        className="StyledSelectPaper"
                        style={{
                          display: "flex",
                          flexDirection: "column",
                          justifyContent: "space-between",
                          height:
                            (!optionSelected &&
                              !state.customAltTextOptionSelected) ||
                            openAltSettings
                              ? "77px"
                              : (state.customAltTextOptionSelected ===
                                  "Custom alt text" ||
                                  optionSelected === "Custom alt text") &&
                                !openAltSettings
                              ? "139px"
                              : "153px",
                          "--width": "300px",
                          "--margin-top": "2px",
                          "--paperPadding": "14.5px 16px",
                        }}
                      >
                        <span className="px12GreySpan">{`${t(
                          "Alt text"
                        )}`}</span>
                        <IconButton
                          ref={altSettingsRef}
                          data-id="alt-settings-test"
                          aria-label={`${t("Alt text settings")}`}
                          disableRipple
                          color="inherit"
                          className="SelectButton"
                          style={{
                            "--active": "#000",
                            "--width": "100%",
                            "--height": "22px",
                            "--font-size": "16px",
                            "--grid-template-columns": "90% 10%",
                            "--hover-background-color": "transparent",
                            justifyItems: "start",
                          }}
                          onClick={(e) => {
                            e.stopPropagation(); // prevent the click from bubbling up to the parent
                            openAltSettings
                              ? setOpenAltSettings(false)
                              : setOpenAltSettings(true);
                          }}
                        >
                          {!state.customAltTextOptionSelected
                            ? t("Decorative image")
                            : t(state.customAltTextOptionSelected)}
                          {icons.chevron}
                        </IconButton>
                        <hr
                          style={{
                            border: "1px solid #1565C0",
                            margin: optionSelected ? "0px" : "1px",
                          }}
                        />
                        {state.customAltTextOptionSelected ===
                          "Decorative image" &&
                          !openAltSettings && (
                            <div className="decorative-alt-tip">
                              {t(
                                "Decorative images don’t add information to the content of a page."
                              )}
                            </div>
                          )}
                        {state.customAltTextOptionSelected ===
                          "Custom alt text" &&
                          !openAltSettings && (
                            <>
                              <input
                                data-id="alt-text-input-test"
                                aria-label={`${t("Alt text")}`}
                                maxLength="140"
                                style={{
                                  height: "29px",
                                  marginTop: "33.5px",
                                  borderBottom: "1px solid rgba(0, 0, 0, 0.42)",
                                }}
                                type="text"
                                placeholder={`${t("Alt text")} *`}
                                onClick={(e) => e.stopPropagation()}
                                onChange={(e) => {
                                  updateAlt(e.target.value);
                                }}
                                className="StyledInput"
                                value={inputAltText}
                              />
                              <span
                                className="px12GreySpan"
                                style={{
                                  marginTop: "4px",
                                  color: "rgba(0, 0, 0, 0.54)",
                                  lineHeight: "12px",
                                  letterSpacing: "0.4px",
                                }}
                              >
                                Maximum 140 characters.
                              </span>
                            </>
                          )}
                        {/* Second dropdown for Alt, selection */}
                        <Popper
                          open={openAltSettings}
                          anchorEl={altSettingsRef.current}
                          placement="bottom-start"
                          transition
                          disablePortal
                          modifiers={[
                            {
                              name: "offset",
                              options: {
                                offset: [0, 22.5],
                              },
                            },
                          ]}
                        >
                          {({ TransitionProps }) => (
                            <Grow {...TransitionProps}>
                              <Paper
                                elevation={0}
                                className="StyledSelectPaper"
                                style={{
                                  display: "flex",
                                  flexDirection: "column",
                                  justifyContent: "space-between",
                                  "--height": "88px",
                                  "--width": "268px",
                                  "--margin-left": "2px",
                                  "--margin-top": "2px",
                                  "--paperPadding": "14px 0px",
                                  boxShadow:
                                    "0px 1px 3px 0px rgba(35, 35, 35, 0.1), 0px 2px 5px 0px rgba(161, 162, 163, 0.2), 3px 3px 4px 0px rgba(35, 35, 35, 0.15)",
                                }}
                              >
                                <IconButton
                                  id={`alt-decorative-${imageId}`}
                                  aria-label={`${t("Select Custom alt text")}`}
                                  onClick={(e) => {
                                    e.stopPropagation(e);
                                    setOpenAltSettings(false);
                                    setOptionSelected("Custom alt text");
                                    updateAltOption("Custom alt text");
                                    altSettingsRef.current.focus();
                                  }}
                                  disabled={false}
                                  disableRipple
                                  color="inherit"
                                  className={`SelectButton ${
                                    state.customAltTextOptionSelected ===
                                    "Custom alt text"
                                      ? "alt-active-bg"
                                      : ""
                                  }`}
                                  style={{
                                    "--active":
                                      !true == true
                                        ? "rgba(21, 101, 192, 1)"
                                        : "#000",
                                    "--width": "100%",
                                    "--height": "28px",
                                    "--font-size": "16px",
                                    "--grid-template-columns": "4fr 1fr",
                                    "--hover-background-color": "transparent",
                                    justifyItems: "start",
                                  }}
                                >
                                  <div
                                    style={{
                                      display: "flex",
                                      flexDirection: "row",
                                    }}
                                  >
                                    <span
                                      style={{
                                        marginRight: "25px",
                                        visibility:
                                          state.customAltTextOptionSelected ===
                                          "Custom alt text"
                                            ? "visible"
                                            : "hidden",
                                        paddingLeft: "10px",
                                      }}
                                    >
                                      <Done onClick={() => {}} />
                                    </span>
                                    {t("Custom alt text")}
                                  </div>
                                </IconButton>
                                <IconButton
                                  id={`alt-decorative-${imageId}`}
                                  aria-label={t(
                                    "Select decorative or informative"
                                  )}
                                  onClick={(e) => {
                                    e.stopPropagation();
                                    setOptionSelected("Decorative image");
                                    setOpenAltSettings(false);
                                    updateAltOption("Decorative image");
                                    altSettingsRef.current.focus();
                                  }}
                                  disabled={false}
                                  disableRipple
                                  color="inherit"
                                  className={`SelectButton ${
                                    state.customAltTextOptionSelected ===
                                    "Decorative image"
                                      ? "alt-active-bg"
                                      : ""
                                  }`}
                                  style={{
                                    "--active":
                                      !true == true
                                        ? "rgba(21, 101, 192, 1)"
                                        : "#000",
                                    "--width": "100%",
                                    "--height": "28px",
                                    "--font-size": "16px",
                                    "--grid-template-columns": "4fr 1fr",
                                    "--hover-background-color": "transparent",
                                    "--svg": openAlt ? " " : "rotate(180deg)",
                                    justifyItems: "start",
                                  }}
                                >
                                  <div
                                    style={{
                                      display: "flex",
                                      flexDirection: "row",
                                    }}
                                  >
                                    <div
                                      style={{
                                        marginRight: "25px",
                                        visibility:
                                          state.customAltTextOptionSelected ===
                                          "Decorative image"
                                            ? "visible"
                                            : "hidden",
                                        paddingLeft: "10px",
                                      }}
                                    >
                                      <Done onClick={() => {}} />
                                    </div>
                                    <div>{t("Decorative image")}</div>
                                  </div>
                                </IconButton>
                              </Paper>
                            </Grow>
                          )}
                        </Popper>
                      </Paper>
                    </Grow>
                  )}
                </Popper>
              </ClickAwayListener>
              <div className="StyledDivider" />
              {/* Kebab */}
              <Tooltip
                aria-label={t("configure image component")}
                title={t("configure image component")}
                placement="top"
                arrow
              >
                <IconButton
                  ref={DescriptionKebab}
                  data-id="image-settings"
                  aria-controls={
                    openDescriptionKebab
                      ? t("configure image component")
                      : undefined
                  }
                  aria-expanded={openDescriptionKebab ? "true" : undefined}
                  variant="contained"
                  disableRipple
                  disableFocusRipple
                  onClick={(e) => handleToggleImageKebab(e)}
                  className="StyledIconButton"
                  style={{
                    "--active": openDescriptionKebab
                      ? "rgba(21, 101, 192, 1)"
                      : "#000",
                    "--background": openDescriptionKebab
                      ? "rgba(21, 101, 192, 0.12)"
                      : "#fff",
                  }}
                >
                  <KebabSVG />
                </IconButton>
              </Tooltip>
              <Popper
                open={openDescriptionKebab}
                anchorEl={DescriptionKebab.current}
                placement="bottom-start"
                transition
                disablePortal
                modifiers={[
                  {
                    name: "offset",
                    options: {
                      offset: [-5, 0],
                    },
                  },
                  {
                    name: "flip",
                    enabled: true,
                    options: {
                      altBoundary: true,
                      rootBoundary: "document",
                      padding: 8,
                    },
                  },
                  {
                    name: "preventOverflow",
                    enabled: false,
                    options: {
                      altAxis: true,
                      altBoundary: true,
                      tether: true,
                      rootBoundary: "document",
                      padding: 8,
                    },
                  },
                ]}
              >
                {({ TransitionProps }) => (
                  <Grow {...TransitionProps}>
                    <Paper
                      style={{
                        boxShadow:
                          "0px 1px 3px 0px rgba(35, 35, 35, 0.1), 0px 2px 5px 0px rgba(161, 162, 163, 0.2), 3px 3px 4px 0px rgba(35, 35, 35, 0.15)",
                      }}
                    >
                      <MenuList
                        data-id="image-description-settings-dropdown"
                        aria-labelledby="Image description settings"
                        className="StyledCheckboxMenu"
                        style={{
                          "--width": "204px",
                          "--height": "auto",
                          "--margin": "8px 0",
                        }}
                      >
                        <FormGroup
                          sx={{ gap: "14px", margin: "8px 16px !important" }}
                          tabIndex="-1"
                        >
                          <FormControl>
                            <FormControlLabel
                              className="StyledFormControlLabel"
                              control={
                                <Checkbox
                                  aria-checked={
                                    state.imageTextSettings.description
                                  }
                                  checked={state.imageTextSettings.description}
                                  onClick={(e) => {
                                    e.stopPropagation();
                                    handleTextSettings(e, "description");
                                  }}
                                  sx={{
                                    "&:hover": {
                                      bgcolor: "transparent",
                                      color: "rgba(21, 101, 192, 1)",
                                    },
                                    "&.Mui-checked": {
                                      bgcolor: "transparent",
                                      color: "rgba(21, 101, 192, 1)",
                                    },
                                  }}
                                />
                              }
                              label={t("Show description")}
                              size="small"
                            />
                          </FormControl>
                          <FormControl>
                            <FormControlLabel
                              className="StyledFormControlLabel"
                              control={
                                <Checkbox
                                  checked={state.imageTextSettings.credit}
                                  aria-checked={state.imageTextSettings.credit}
                                  onClick={(e) =>
                                    handleTextSettings(e, "credit")
                                  }
                                  sx={{
                                    "&:hover": {
                                      bgcolor: "transparent",
                                      color: "rgba(21, 101, 192, 1)",
                                    },
                                    "&.Mui-checked": {
                                      bgcolor: "transparent",
                                      color: "rgba(21, 101, 192, 1)",
                                    },
                                  }}
                                />
                              }
                              label={t("Show credit")}
                              size="small"
                            />
                          </FormControl>
                          <FormControl
                            disabled={
                              parentComponent === "accordion" ||
                              parentComponent === "tabs" ||
                              parentComponent === "multi-column" ||
                              parentComponent === "reveal" ||
                              parentComponent === "infobox"
                            }
                          >
                            <FormControlLabel
                              className="StyledFormControlLabel"
                              control={
                                <Checkbox
                                  checked={
                                    parentComponent !== "accordion" &&
                                    parentComponent !== "tabs" &&
                                    parentComponent !== "multi-column" &&
                                    parentComponent !== "reveal" &&
                                    state.imageTextSettings.fullWidth
                                  }
                                  aria-checked={
                                    parentComponent !== "accordion" &&
                                    parentComponent !== "tabs" &&
                                    parentComponent !== "multi-column" &&
                                    parentComponent !== "reveal" &&
                                    state.imageTextSettings.fullWidth
                                  }
                                  onClick={(e) => {
                                    e.stopPropagation();
                                    handleTextSettings(e, "fullWidth");
                                  }}
                                  sx={{
                                    "&:hover": {
                                      bgcolor: "transparent",
                                      color: "rgba(21, 101, 192, 1)",
                                    },
                                    "&.Mui-checked": {
                                      bgcolor: "transparent",
                                      color: "rgba(21, 101, 192, 1)",
                                    },
                                  }}
                                />
                              }
                              label={t("Full width")}
                              size="small"
                            />
                          </FormControl>
                        </FormGroup>
                      </MenuList>
                    </Paper>
                  </Grow>
                )}
              </Popper>
            </Toolbar>
          )}
        <div ref={portalToolbarRef} />
        {(focused === "Description" || focused === "Credit") && (
          <div
            ref={kebabSelectRefText}
            onBlur={(e) => {
              const relatedTarget = e.relatedTarget || document.activeElement;
              if (!kebabSelectRefText.current.contains(relatedTarget)) {
                setOpenDescriptionKebabPortal(false);
              }
            }}
            style={{
              display: "inline-flex",
              height: "40px",
              flexDirection: "row",
              justifyContent: "space-around",
              alignItems: "center",
              zIndex: "2000",
              marginRight: "0.1875rem",
            }}
          >
            <div className="StyledDivider" />

            {/* Kebab */}
            <Tooltip
              aria-label={t("configure image component")}
              title={t("configure image component")}
              placement="top"
              arrow
            >
              <IconButton
                ref={DescriptionKebabPortal}
                data-id="image-settings"
                aria-controls={
                  openDescriptionKebabPortal
                    ? t("configure image component")
                    : undefined
                }
                aria-expanded={openDescriptionKebabPortal ? "true" : undefined}
                variant="contained"
                disableRipple
                disableFocusRipple
                onClick={(e) => handleToggleImageKebab(e, "portal")}
                className="StyledIconButton"
                style={{
                  "--active": openDescriptionKebabPortal
                    ? "rgba(21, 101, 192, 1)"
                    : "#000",
                  "--background": openDescriptionKebabPortal
                    ? "rgba(21, 101, 192, 0.12)"
                    : "#fff",
                }}
              >
                <KebabSVG />
              </IconButton>
            </Tooltip>
            {/* drop down menu body */}
            <Popper
              open={openDescriptionKebabPortal}
              anchorEl={DescriptionKebabPortal.current}
              placement="bottom-start"
              transition
              disablePortal
              modifiers={[
                {
                  name: "flip",
                  enabled: true,
                  options: {
                    altBoundary: true,
                    rootBoundary: "document",
                    padding: 8,
                  },
                },
                {
                  name: "preventOverflow",
                  enabled: false,
                  options: {
                    altAxis: true,
                    altBoundary: true,
                    tether: true,
                    rootBoundary: "document",
                    padding: 8,
                  },
                },
              ]}
            >
              {({ TransitionProps }) => (
                <Grow {...TransitionProps}>
                  <Paper
                    style={{
                      boxShadow:
                        "0px 1px 3px 0px rgba(35, 35, 35, 0.1), 0px 2px 5px 0px rgba(161, 162, 163, 0.2), 3px 3px 4px 0px rgba(35, 35, 35, 0.15)",
                    }}
                  >
                    <MenuList
                      data-id="image-description-settings-dropdown"
                      aria-labelledby={t("configure image component")}
                      className="StyledCheckboxMenu"
                      style={{
                        "--width": "204px",
                        "--height": "auto",
                        "--margin": "8px 0",
                      }}
                    >
                      <FormGroup
                        sx={{ gap: "14px", margin: "8px 16px !important" }}
                        tabIndex="-1"
                      >
                        <FormControl>
                          <FormControlLabel
                            className="StyledFormControlLabel"
                            control={
                              <Checkbox
                                aria-checked={
                                  state.imageTextSettings.description
                                }
                                checked={state.imageTextSettings.description}
                                onClick={(e) => {
                                  e.stopPropagation();
                                  handleTextSettings(e, "description");
                                }}
                                sx={{
                                  "&:hover": {
                                    bgcolor: "transparent",
                                    color: "rgba(21, 101, 192, 1)",
                                  },
                                  "&.Mui-checked": {
                                    bgcolor: "transparent",
                                    color: "rgba(21, 101, 192, 1)",
                                  },
                                }}
                              />
                            }
                            label={t("Show description")}
                            size="small"
                          />
                        </FormControl>
                        <FormControl>
                          <FormControlLabel
                            className="StyledFormControlLabel"
                            control={
                              <Checkbox
                                checked={state.imageTextSettings.credit}
                                aria-checked={state.imageTextSettings.credit}
                                onClick={(e) => handleTextSettings(e, "credit")}
                                sx={{
                                  "&:hover": {
                                    bgcolor: "transparent",
                                    color: "rgba(21, 101, 192, 1)",
                                  },
                                  "&.Mui-checked": {
                                    bgcolor: "transparent",
                                    color: "rgba(21, 101, 192, 1)",
                                  },
                                }}
                              />
                            }
                            label={t("Show credit")}
                            size="small"
                          />
                        </FormControl>
                        <FormControl
                          disabled={
                            !!(
                              parentComponent === "accordion" ||
                              parentComponent === "tabs" ||
                              parentComponent === "multi-column" ||
                              parentComponent === "reveal" ||
                              parentComponent === "infobox"
                            )
                          }
                        >
                          <FormControlLabel
                            className="StyledFormControlLabel"
                            control={
                              <Checkbox
                                checked={
                                  parentComponent !== "accordion" &&
                                  parentComponent !== "tabs" &&
                                  parentComponent !== "multi-column" &&
                                  parentComponent !== "reveal" &&
                                  state.imageTextSettings.fullWidth
                                }
                                aria-checked={
                                  parentComponent !== "accordion" &&
                                  parentComponent !== "tabs" &&
                                  parentComponent !== "multi-column" &&
                                  parentComponent !== "reveal" &&
                                  state.imageTextSettings.fullWidth
                                }
                                onClick={(e) => {
                                  e.stopPropagation();
                                  handleTextSettings(e, "fullWidth");
                                }}
                                sx={{
                                  "&:hover": {
                                    bgcolor: "transparent",
                                    color: "rgba(21, 101, 192, 1)",
                                  },
                                  "&.Mui-checked": {
                                    bgcolor: "transparent",
                                    color: "rgba(21, 101, 192, 1)",
                                  },
                                }}
                              />
                            }
                            label={t("Full width")}
                            size="small"
                          />
                        </FormControl>
                      </FormGroup>
                    </MenuList>
                  </Paper>
                </Grow>
              )}
            </Popper>
          </div>
        )}
      </AppBar>
    </div>
  );
});

export default ToolBar;
