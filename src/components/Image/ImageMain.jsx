import React, { useMemo } from "react";
import { v4 as uuidv4 } from "uuid";
import { ImageProvider } from "./ImageContext";
import { TabProvider } from "./subcomponents/TabContext";
import Image from "./subcomponents/Image";
import { MainWrapper } from "../../theme/styledComponents/globalComponent";

export const defaultProps = {
  imgSize: {
    width: 100,
    height: 100,
  },
  uploadedImg: null,
  alt: "",
  customAltTextOptionSelected: "Custom alt text",
  longDesc: "",
  credit: "",
  imageTextSettings: {
    description: true,
    credit: true,
    fullWidth: false,
  },
  imgLink: null,
  caption: null,
};

const ImageMain = ({
  parentComponent = null,
  setProp = () => {},
  setActiveComponent = () => {},
  imgSize = {
    width: 100,
    height: 100,
  },
  uploadedImg = "",
  alt = "",
  customAltTextOptionSelected = "",
  longDesc = "",
  credit = "",
  imageTextSettings = {
    description: true,
    credit: true,
    fullWidth: false,
  },
  imgLink = null,
  caption = null,
  toolbarContainerRef,
  showSelf = null,
  isActiveComponent = null,
}) => {
  const imageId = useMemo(() => `image-${uuidv4()}`, []);
  return (
    <ImageProvider
      imageState={{
        imgSize,
        uploadedImg,
        imgLink,
        alt,
        longDesc,
        caption,
        credit,
        imageTextSettings,
        customAltTextOptionSelected,
      }}
      setProp={setProp}
    >
      <TabProvider>
        <MainWrapper>
          <Image
            parentComponent={parentComponent}
            setActiveComponent={setActiveComponent}
            imageId={imageId}
            toolbarContainerRef={toolbarContainerRef}
            showSelf={showSelf}
            isActiveComponent={isActiveComponent}
          />
        </MainWrapper>
      </TabProvider>
    </ImageProvider>
  );
};

export default ImageMain;
