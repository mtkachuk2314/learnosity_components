import InfoBox, {
  defaultProps as infoBoxDefaultProps,
} from "./InfoBox/InfoBoxMain";
// import ImageOld, { defaultProps as imageDefaultProps } from "./ImageOld/Image";
import ImageMain, {
  defaultProps as imageDefaultProps,
} from "./Image/ImageMain";
// import QuoteBox, {
//   defaultProps as quoteBoxDefaultProps,
// } from "./QuoteBox/QuoteBox";
import VideoMain, {
  defaultProps as videoDefaultProps,
} from "./Video/VideoMain";
import IFrameMain, {
  defaultProps as iFrameDefaultProps,
} from "./IFrame/IFrameMain";
import Text, { defaultProps as quillDefaultProps } from "./Text/Text";
import TabsMain, { defaultProps as testTabDefaultProps } from "./Tabs/TabsMain";
import TableMain, {
  defaultProps as tableDefaultProps,
} from "./Table/TableMain";
// Accordion
import AccordionMain, {
  defaultProps as testAccordionDefaultProps,
} from "./Accordion/AccordionMain";
// Reveal
import RevealMain, {
  defaultProps as testRevealDefaultprops,
} from "./Reveal/RevealMain";
// Header
import HeaderMain, {
  defaultProps as headerDefaultProps,
} from "./Header/HeaderMain";
// MultiColumn
import MultiColumnMain, {
  defaultProps as testMultiColumnDefaultProps,
} from "./MultiColumn/MultiColumnMain";
import MultipleChoice, {
  defaultProps as multipleChoiceDefaultProps,
} from "./MultipleChoice/MultipleChoiceMain";
import exposedVersion from "../../exposedStage";

console.log(
  `Component Library stage is ${exposedVersion.stage} and version of the app is ${exposedVersion.version}`
);

const categoryIcon = `${process.env.WEBPACK_PUBLIC_PATH}icons/menuIcons/categoryIcon`;
const componentIcon = `${process.env.WEBPACK_PUBLIC_PATH}icons/menuIcons/componentIcon`;

export const categories = {
  structure: {
    readableName: "Structure",
    icon: `${categoryIcon}/interactive.svg`,
  },
  text: {
    readableName: "Text",
    icon: `${categoryIcon}/textCategory.svg`,
  },
  media: {
    readableName: "Media",
    icon: `${categoryIcon}/media.svg`,
  },
  interactive: {
    readableName: "Interactive",
    icon: `${categoryIcon}/interactive.svg`,
  },
};

/*
Header: {
  // React component to be rendered
  Component: HeaderMain,
  // Human readable name, corresponds to localization key
  readableName: "Header",
  // Props to be rendered on component by Authoring Experience page when first added to a section
  defaultProps: headerDefaultProps,
  // Version number of this component
  version: "0.0.1",
  // Category in which to show the component in the Authoring Experience page's component menu, must be valid category
  category: categories.text,
  // Icon to represent component in component menu and when dragging component
  componentIcon: `${componentIcon}/heading.svg`,
  // Formats this component is compatible with, extensible with more formats in future; if not present the assumption is this is compatible in all formats
  compatibleFormats: {
      // Component is compatible with print outputs
      print: false,
      // Component is compatible with digital outputs
      digital: true,
  }
},
*/

const componentIndex = {
  Header: {
    Component: HeaderMain,
    readableName: "Header",
    defaultProps: headerDefaultProps,
    version: "0.0.1",
    category: categories.text,
    componentIcon: `${componentIcon}/heading.svg`,
    compatibleFormats: {
      print: true,
      digital: true,
    },
  },
  InfoBox: {
    Component: InfoBox,
    readableName: "InfoBox",
    defaultProps: infoBoxDefaultProps,
    version: "0.0.2",
    category: categories.text,
    componentIcon: `${componentIcon}/infobox.svg`,
    compatibleFormats: {
      print: true,
      digital: true,
    },
  },
  Text: {
    Component: Text,
    readableName: "Text",
    defaultProps: quillDefaultProps,
    version: "0.0.1",
    category: categories.text,
    componentIcon: `${componentIcon}/text.svg`,
    compatibleFormats: {
      print: true,
      digital: true,
    },
  },
  Image: {
    Component: ImageMain,
    readableName: "Image",
    defaultProps: imageDefaultProps,
    version: "0.0.1",
    category: categories.media,
    componentIcon: `${componentIcon}/image.svg`,
    compatibleFormats: {
      print: true,
      digital: true,
    },
  },
  // QuoteBox: {
  //   Component: QuoteBox,
  //   readableName: "Quote Box",
  //   defaultProps: quoteBoxDefaultProps,
  //   version: "0.0.1",
  //   category: categories.text,
  //   componentIcon: `${componentIcon}/text.svg`,
  // },
  IFrame: {
    Component: IFrameMain,
    readableName: "iFrame",
    defaultProps: iFrameDefaultProps,
    version: "0.0.1",
    category: categories.interactive,
    componentIcon: `${componentIcon}/iframe.svg`,
    compatibleFormats: {
      print: false,
      digital: true,
    },
  },
  Tab: {
    Component: TabsMain,
    readableName: "Tabs",
    defaultProps: testTabDefaultProps,
    version: "0.0.1",
    category: categories.interactive,
    componentIcon: `${componentIcon}/tabs.svg`,
    compatibleFormats: {
      print: false,
      digital: true,
    },
  },
  Accordion: {
    Component: AccordionMain,
    readableName: "Accordion",
    defaultProps: testAccordionDefaultProps,
    version: "0.0.1",
    category: categories.interactive,
    componentIcon: `${componentIcon}/accordion.svg`,
    compatibleFormats: {
      print: false,
      digital: true,
    },
  },
  Reveal: {
    Component: RevealMain,
    readableName: "Reveal",
    defaultProps: testRevealDefaultprops,
    version: "0.0.1",
    category: categories.interactive,
    componentIcon: `${componentIcon}/reveal.svg`,
    compatibleFormats: {
      print: false,
      digital: true,
    },
  },
  MultiColumn: {
    Component: MultiColumnMain,
    readableName: "Multi Column",
    defaultProps: testMultiColumnDefaultProps,
    version: "0.0.1",
    category: categories.structure,
    componentIcon: `${componentIcon}/twoColumn.svg`,
    compatibleFormats: {
      print: true,
      digital: true,
    },
  },
  Video: {
    Component: VideoMain,
    defaultProps: videoDefaultProps,
    readableName: "Video",
    version: "0.0.1",
    category: categories.media,
    componentIcon: `${componentIcon}/video.svg`,
    compatibleFormats: {
      print: false,
      digital: true,
    },
  },
  Table: {
    Component: TableMain,
    defaultProps: tableDefaultProps,
    readableName: "Table",
    version: "0.0.1",
    category: categories.interactive,
    componentIcon: `${componentIcon}/table.svg`,
    compatibleFormats: {
      print: true,
      digital: true,
    },
  },
  MultipleChoice: {
    Component: MultipleChoice,
    defaultProps: multipleChoiceDefaultProps,
    readableName: "Multiple Choice",
    version: "0.0.1",
    category: categories.interactive,
    componentIcon: `${componentIcon}/table.svg`,
    compatibleFormats: {
      print: false,
      digital: true,
    },
  },
};

export default componentIndex;
