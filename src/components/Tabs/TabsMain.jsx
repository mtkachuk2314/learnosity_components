import React from "react";
import PropTypes from "prop-types";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import { v4 as uuidv4 } from "uuid";
import { LayoutProvider } from "./TabContext";
import Tabs from "./subcomponents/Tabs";
import { MainWrapper } from "../../theme/styledComponents/globalComponent";
// tabs default props
export const defaultProps = {
  layoutState: [
    {
      id: uuidv4(),
      title: "",
      components: [],
      activeTab: true,
    },
    {
      id: uuidv4(),
      title: "",
      components: [],
      activeTab: false,
    },
  ],
};

const TabsMain = ({
  layoutState = [],
  setProp = () => {},
  setHoverMenu = () => {},
  toolbarContainerRef,
  // showSelf is not being passed to interactive component - keep it just for consistency
  showSelf = null,
  isActiveComponent = null,
  setActiveComponent = () => {},
}) => {
  return (
    <DndProvider backend={HTML5Backend}>
      <LayoutProvider layoutState={layoutState} setProp={setProp}>
        <MainWrapper>
          <Tabs
            toolbarContainerRef={toolbarContainerRef}
            isActiveComponent={isActiveComponent}
            setHoverMenu={setHoverMenu}
            setActiveComponent={setActiveComponent}
          />
        </MainWrapper>
      </LayoutProvider>
    </DndProvider>
  );
};

TabsMain.propTypes = {
  layoutState: PropTypes.array.isRequired,
  setProp: PropTypes.func.isRequired,
};

export default TabsMain;
