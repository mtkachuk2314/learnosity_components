import React, { useState, useContext, useRef, useEffect } from "react";
import styled from "@emotion/styled";
import { TabContext, LayoutContext } from "../TabContext";
import TenantContext from "../../../Context/TenantContext";

import Tab from "./Tab";
import DialogProvider from "../../../Utility/DialogProvider";

import ConfigBar from "./ConfigBar";
import TabTitle from "./TabTitle";
// portal
import PortalUtil from "../../../Utility/PortalUtil";

// Styled components for Tabs.js
const StyledTabContainer = styled("div")(() => ({
  boxSizing: "border-box",
  letterSpacing: "0.1px",
  lineHeight: "25px",
  color: "#636363",
}));

const StyledTabTitleWrapper = styled("div")(() => ({
  display: "flex",
  minHeight: "40px",
  maxHeight: "69px",
}));

// Styled component end.

const Tabs = ({
  setHoverMenu,
  toolbarContainerRef,
  isActiveComponent,
  setActiveComponent,
}) => {
  const [activeTab] = useContext(TabContext);
  const [state] = useContext(LayoutContext);
  const [toolbar, showToolbar] = useState(false);
  const [removeError, setRemoveError] = useState(false);
  const showToolbarRef = useRef(null);
  const toolbarRef = useRef(null);

  const authoringData = useContext(TenantContext);
  const viewOnly = !authoringData?.viewOnlyComponents;

  // TODO: If needed, move to useEffect
  // if (
  //   state.map((ele) => {
  //     if (ele.placeholder) {
  //       dispatch({
  //         func: "DELETE_PLACEHOLDER",
  //       });
  //     }
  //     return state;
  //   })
  // )
  return (
    <StyledTabContainer data-testid="tab-component">
      <DialogProvider>
        <StyledTabTitleWrapper ref={showToolbarRef} role="tablist">
          {!viewOnly && isActiveComponent ? (
            <PortalUtil portalRef={toolbarContainerRef}>
              <ConfigBar setRemoveError={setRemoveError} ref={toolbarRef} />
            </PortalUtil>
          ) : null}
          {state.map((_, tabIndex) => {
            return (
              <TabTitle
                key={`tab-title-${tabIndex}`}
                tabIndex={tabIndex}
                showToolbar={showToolbar}
                viewOnly={viewOnly}
              />
            );
          })}
        </StyledTabTitleWrapper>
        {state.map((_, tabIndex) => {
          return (
            <div key={`tab-${tabIndex}`}>
              {activeTab === tabIndex ? (
                <Tab
                  tabIndex={tabIndex}
                  removeError={removeError}
                  setRemoveError={setRemoveError}
                  setHoverMenu={setHoverMenu}
                  setActiveComponent={setActiveComponent}
                />
              ) : null}
            </div>
          );
        })}
      </DialogProvider>
    </StyledTabContainer>
  );
};

export default Tabs;
