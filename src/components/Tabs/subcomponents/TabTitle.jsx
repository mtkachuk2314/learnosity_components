import React, {
  useContext,
  useCallback,
  useRef,
  useState,
  useEffect,
} from "react";
import { TextareaAutosize } from "@material-ui/core";
import styled from "@emotion/styled";
// Localization
import { useTranslation } from "react-i18next";
// Component imports
import { TabContext, LayoutContext } from "../TabContext";
import useDebounce from "../../../hooks/useDebounce";

// Styled components for TabTitle.js
const StyledTitle = styled("div")(({ activeTab, tabIndexProp }) => ({
  backgroundColor: activeTab === tabIndexProp ? "#fff" : "#f5f5f5",
  borderStyle: "solid",
  borderColor: "#bdbdbd",
  borderWidth: activeTab === tabIndexProp ? "1px 1px 0 1px" : "1px",
  borderRadius: "10px 10px 0px 0px",
  width: "100%",
  padding: "8px 10px",
  color: activeTab === tabIndexProp ? "#232323" : "#636363",
  fontWeight: "500",
  textAlign: "center",
  "&:focus": {
    outline: "1px solid black",
    border: "1px solid black",
  },
  "&:focus-visible": {
    outline: "1px solid black",
    border: "1px solid black",
  },
}));

const StyledPlaceholder = styled("div")(({ activeTab, tabIndexProp }) => ({
  width: "100%",
  maxWidth: "484px",
  maxHeight: "50px",
  padding: "0",
  margin: "0",
  fontSize: "18px",
  wordWrap: "break-word",
  overflowX: "hidden",
  overflowY: "hidden",
  textOverflow: "ellipsis",
  display: "-webkit-box",
  WebkitBoxOrient: "vertical",
  WebkitLineClamp: activeTab === tabIndexProp ? "unset" : 2,
  lineHeight: "1.575rem",
}));

const StyledInput = styled(TextareaAutosize)(({ activeTab, tabIndexProp }) => ({
  fontFamily: '"Inter", sans-serif',
  border: "none",
  padding: "0",
  fontSize: "18px",
  fontWeight: 500,
  lineHeight: "1.575rem",
  width: "100%",
  minHeight: "25px",
  maxHeight: "50px",
  textAlign: "center",
  resize: "none",
  textOverflow: "ellipsis",
  display: "-webkit-box",
  WebkitBoxOrient: "vertical",
  "&::-webkit-scrollbar": {
    WebkitAppearance: "none",
    width: "7px",
  },
  "&::-webkit-scrollbar-thumb": {
    borderRadius: "4px",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    boxShadow: "0 0 1px rgba(255, 255, 255, 0.5)",
    WebkitBoxShadow: "0 0 1px rgba(255, 255, 255, 0.5)",
  },
  "&:disabled": {
    background: "white !important",
  },
  "&::placeholder": {
    color: activeTab === tabIndexProp && "rgba(35,35,35,1)",
  },
  "&:focus": {
    border: "none",
    outline: "none",
    "&:: placeholder": {
      color: "rgba(35, 35, 35, 0.12)",
    },
  },
}));
// Styled components end

const TabTitle = ({ tabIndex, showToolbar, viewOnly }) => {
  const [activeTab, setActiveTab] = useContext(TabContext);
  const [state, dispatch] = useContext(LayoutContext);

  const [titleValue, setTitleValue] = useState(state[tabIndex].title);

  const { t } = useTranslation();

  const updateTitleState = (title) => {
    dispatch({
      func: "CHANGE_TITLE",
      title,
      id: state[tabIndex].id,
    });
  };
  const handleDebounce = useDebounce(updateTitleState);

  const handleTitleChange = useCallback((e) => {
    const title = e.target.value;
    setTitleValue(title);
    handleDebounce(title);
  }, []);

  useEffect(() => {
    setTitleValue(state[tabIndex].title);
  }, [state, tabIndex]);

  const handleTitleBlur = (e) => {
    e.target.style.overflow = "hidden";
    e.target.scrollTo(0, 0);
  };

  const inputRef = useRef();

  const handleCursorFocus = (i) => {
    inputRef.current.setSelectionRange(
      state[i].title.length,
      state[i].title.length
    );
    inputRef.current.focus();
    inputRef.current.scrollTo(state[i].title.length, state[i].title.length);
  };

  return (
    <StyledTitle
      activeTab={activeTab}
      tabIndexProp={tabIndex}
      role="tab"
      key={`tab-title-${tabIndex}`}
      tabIndex={tabIndex}
      data-testid={`tab-title-${tabIndex}`}
      aria-label={state[tabIndex].title}
      onFocus={() => {
        showToolbar(true);
      }}
      onClick={() => {
        setActiveTab(tabIndex);
        showToolbar(true);
      }}
      onDragEnter={(e) => {
        setActiveTab(tabIndex);
      }}
      onKeyDown={(e) => {
        if (e.key === "Enter") {
          setActiveTab(tabIndex);
          showToolbar(true);
        }
      }}
    >
      {activeTab === tabIndex ? (
        <StyledInput
          activeTab={activeTab}
          tabIndexProp={tabIndex}
          placeholder={t("Untitled Tab")}
          aria-label={t("tab title input")}
          aria-multiline="true"
          role={activeTab === tabIndex ? "textbox" : "tab"}
          disabled={activeTab !== tabIndex || viewOnly}
          minRows="1"
          maxRows="2"
          maxLength="200"
          onChange={handleTitleChange}
          onFocus={() => handleCursorFocus(tabIndex)}
          data-id={state[tabIndex].id}
          value={titleValue.length > 0 ? titleValue : ""}
          onBlur={handleTitleBlur}
          ref={inputRef}
          onKeyDown={(e) => e.key === "Enter" && e.preventDefault()} // Prevent new line break
        />
      ) : (
        <StyledPlaceholder
          activeTab={activeTab}
          tabIndexProp={tabIndex}
          role="tab"
        >
          {state[tabIndex].title.length > 0
            ? state[tabIndex].title
            : t("Untitled Tab")}
        </StyledPlaceholder>
      )}
    </StyledTitle>
  );
};
export default TabTitle;
