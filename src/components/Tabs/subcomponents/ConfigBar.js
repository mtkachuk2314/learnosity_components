import React, { forwardRef, useContext, useState, useEffect } from "react";
import { v4 as uuidv4 } from "uuid";
import { useTranslation } from "react-i18next";
import { IconButton, Toolbar, AppBar, Tooltip } from "@mui/material";
import { ArrowBack, ArrowForward, Add, Remove } from "@mui/icons-material";
import { LayoutContext, TabContext } from "../TabContext";
import { DialogContext } from "../../../Utility/DialogProvider";
// SCSS Module/styles
import styles from "../styles/ConfigBar.module.scss";

const ConfigBar = forwardRef(function ConfigBar({ setRemoveError }, ref) {
  // ? State of tab component
  const [state, dispatch] = useContext(LayoutContext);

  // ? Active Tab
  const [activeTab, setActiveTab] = useContext(TabContext);

  // ? Dialog toggle for remove tab button
  const [showDialog, setShowDialog] = useState(false);
  const displayDialog = useContext(DialogContext);

  // Translation
  const { t } = useTranslation();

  // Function to move the tab to the left
  const moveTabLeft = () => {
    dispatch({
      func: "MOVE_TAB_LEFT",
      title: `Tab at position ${activeTab} is now at position ${activeTab - 1}`,
      tabIndex: activeTab,
      nextTab: activeTab - 1,
    });
    setActiveTab(activeTab - 1);
  };

  // Function to move the tab to the right
  const moveTabRight = () => {
    dispatch({
      func: "MOVE_TAB_RIGHT",
      title: `Tab at position ${activeTab} is now at position ${activeTab + 1}`,
      tabIndex: activeTab,
      nextTab: activeTab + 1,
    });
    setActiveTab(activeTab + 1);
  };

  // Function to remove a tab
  const removeTab = async () => {
    setRemoveError(true);
    // Determine the new active tab after removal
    let active;
    if (activeTab === state.length - 1) {
      // If the active tab is the last one, set the previous tab as active
      active = activeTab - 1;
    } else if (activeTab === 0) {
      // If the active tab is the first one, keep it as the active one after removal
      active = 0;
    } else {
      // Otherwise, keep the same tab active
      active = activeTab;
    }

    dispatch({
      func: "REMOVE_TAB",
      tabIndex: activeTab,
      nextTab: active,
    });
    setActiveTab(active);
  };

  // Function to add a new tab
  const addTab = () => {
    dispatch({
      func: "ADD_TAB",
      id: uuidv4(),
      activeTab,
    });
  };
  // Handle closing the dialog
  const handleClose = () => {
    setShowDialog(false);
  };

  // Handle confirming the action in the dialog
  const onConfirm = () => {
    setShowDialog(false);
    removeTab();
  };

  // Monitor showDialog state to display dialog when needed
  useEffect(() => {
    if (showDialog) {
      displayDialog({
        title: t("Delete Tab"),
        message: [
          t("Deleting Tab Confirm", {
            tab:
              state[activeTab].title.length > 0
                ? `"${state[activeTab].title}"`
                : `"${t("Untitled Tab")}"`,
            components: state[activeTab].components.length,
          }),
          <br key={1} />,
          <br key={2} />,
          `${t("You are able to undo this action")}`,
        ],
        onConfirm: () => {
          onConfirm();
        },
        onCancel: () => {
          handleClose();
        },
        confirmMessage: t("Dialog Delete"),
        cancelMessage: t("Dialog Cancel"),
      });
    }
  }, [showDialog]);

  return (
    <div className={styles.tabsConfigBarContainer}>
      <AppBar className={styles.tabsConfigAppBar} position="static">
        <Toolbar
          className={styles.tabsConfigToolBar}
          variant="dense"
          disableGutters
          test-id="tab-toolbar"
          ref={ref}
        >
          <Tooltip
            className={styles.tabsConfigTooltip}
            title={t("move tab left")}
            arrow
            placement="top"
          >
            <IconButton
              className={styles.tabsConfigIconButton}
              disableRipple
              color="inherit"
              disabled={activeTab === 0}
              onClick={() => moveTabLeft(state, activeTab)}
            >
              <ArrowBack />
            </IconButton>
          </Tooltip>
          <Tooltip
            className={styles.tabsConfigTooltip}
            title={t("move tab right")}
            arrow
            placement="top"
          >
            <IconButton
              className={styles.tabsConfigIconButton}
              disableRipple
              color="inherit"
              disabled={activeTab >= state.length - 1}
              onClick={() => moveTabRight(state, activeTab)}
            >
              <ArrowForward />
            </IconButton>
          </Tooltip>
          <Tooltip
            className={styles.tabsConfigTooltip}
            title={t("add tab")}
            arrow
            placement="top"
          >
            <IconButton
              className={styles.tabsConfigIconButton}
              disableRipple
              color="inherit"
              disabled={state.length >= 4}
              onClick={() => {
                addTab(state, activeTab);
              }}
            >
              <Add />
            </IconButton>
          </Tooltip>
          <Tooltip
            className={styles.tabsConfigTooltip}
            title={t("remove current tab")}
            arrow
            placement="top"
          >
            <IconButton
              className={styles.tabsConfigIconButton}
              disableRipple
              color="inherit"
              disabled={state.length <= 2}
              onClick={() => {
                state[activeTab].components.length > 0
                  ? setShowDialog(true)
                  : removeTab(state, activeTab);
              }}
            >
              <Remove />
            </IconButton>
          </Tooltip>
        </Toolbar>
      </AppBar>
    </div>
  );
});

export default ConfigBar;
