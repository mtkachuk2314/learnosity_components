import React, { forwardRef, useContext, useState, useEffect } from "react";
import { v4 as uuidv4 } from "uuid";
import { useTranslation } from "react-i18next";
import { IconButton, Toolbar, AppBar, Tooltip } from "@mui/material";
import { ArrowDownward, ArrowUpward, Add, Remove } from "@mui/icons-material";
import { LayoutContext } from "../../../Context/InteractivesContext";
import { DialogContext } from "../../../Utility/DialogProvider";
// SCSS module/styles
import styles from "../styles/ConfigBar.module.scss";

const ConfigBar = forwardRef(function ConfigBar(
  { paneIndex, setPaneIndex, setRemoveError },
  ref
) {
  const [state, dispatch] = useContext(LayoutContext);

  // ? Dialog toggle for remove tab button
  const [showDialog, setShowDialog] = useState(false);
  const displayDialog = useContext(DialogContext);

  // Translation
  const { t } = useTranslation();

  const [ariaLive, setAriaLive] = useState("");

  // Function to add a new tab
  const addTab = () => {
    dispatch({
      func: "ADD_LAYER",
      id: uuidv4(),
      title: "",
      expanded: true,
      paneIndex,
    });
  };

  // Function to move the accordion pane downwards
  const moveAccordionDown = () => {
    dispatch({
      func: "MOVE_PANE_DOWN",
      title: `Pane at position ${paneIndex} is now at position ${
        paneIndex + 1
      }`,
      paneIndex,
    });
    paneIndex === state.length - 1
      ? setPaneIndex(0)
      : setPaneIndex(paneIndex + 1);
  };

  // Function to move the accordion pane upwards
  const moveAccordionUp = () => {
    dispatch({
      func: "MOVE_PANE_UP",
      title: `Pane at position ${paneIndex} is now at position ${
        paneIndex - 1
      }`,
      paneIndex,
    });
    paneIndex === 0
      ? setPaneIndex(state.length - 1)
      : setPaneIndex(paneIndex - 1);
  };

  // Function to remove an existing tab
  const removeTab = async () => {
    setRemoveError(true);
    let active;
    paneIndex === state.length - 1 || 0 ? (active = 0) : (active = paneIndex);

    dispatch({
      func: "REMOVE_LAYER",
      paneIndex,
    });
    setPaneIndex(active);
  };

  // Handle closing the dialog
  const handleClose = () => {
    setShowDialog(false);
  };

  // Handle confirming the action in the dialog
  const onConfirm = () => {
    setShowDialog(false);
    removeTab();
  };

  // Monitor showDialog state to display dialog when needed
  useEffect(() => {
    if (showDialog) {
      displayDialog({
        title: t("Delete Pane"),
        message: [
          t("Deleting Pane Confirm", {
            pane:
              state[paneIndex].title.length > 0
                ? `"${state[paneIndex].title}"`
                : `"${t("Untitled Pane")}"`,
            components: state[paneIndex].components.length,
          }),
          <br key={1} />,
          <br key={2} />,
          `${t("You are able to undo this action")}`,
        ],
        onConfirm: () => {
          onConfirm();
        },
        onCancel: () => {
          handleClose();
        },
        confirmMessage: t("Dialog Delete"),
        cancelMessage: t("Dialog Cancel"),
      });
    }
  }, [showDialog]);

  return (
    <div className={styles.accordionConfigBarContainer} ref={ref}>
      <span
        className={styles.srOnly}
        role="status"
        aria-live="assertive"
        aria-relevant="all"
        aria-atomic="true"
      >
        {ariaLive}
      </span>
      <AppBar position="static" className={styles.accordionConfigAppBar}>
        <Toolbar
          variant="dense"
          disableGutters
          test-id="accordion-toolbar"
          className={styles.accordionConfigToolbar}
        >
          <Tooltip
            className={styles.accordionConfigTooltip}
            title={t("move pane up")}
            arrow
            placement="top"
          >
            <IconButton
              className={styles.accordionConfigIconButton}
              disableRipple
              color="inherit"
              onClick={() => {
                moveAccordionUp(state);
                setAriaLive(
                  `${
                    state[paneIndex].title.length !== 0
                      ? state[paneIndex].title
                      : "Untitled Pane"
                  } is moved up`
                );
              }}
              disabled={state.length == 1}
            >
              <ArrowUpward />
            </IconButton>
          </Tooltip>
          <Tooltip
            className={styles.accordionConfigTooltip}
            title={t("move pane down")}
            arrow
            placement="top"
          >
            <IconButton
              className={styles.accordionConfigIconButton}
              disableRipple
              color="inherit"
              onClick={() => {
                moveAccordionDown(state);
                setAriaLive(
                  `${
                    state[paneIndex].title.length !== 0
                      ? state[paneIndex].title
                      : "Untitled Pane"
                  } is moved down`
                );
              }}
              disabled={state.length == 1}
            >
              <ArrowDownward />
            </IconButton>
          </Tooltip>
          <Tooltip
            className={styles.accordionConfigTooltip}
            title={t("add pane")}
            arrow
            placement="top"
          >
            <IconButton
              className={styles.accordionConfigIconButton}
              disableRipple
              color="inherit"
              onClick={() => {
                addTab(state);
                setAriaLive(`new pane is added at ${paneIndex + 2} position`);
              }}
            >
              <Add />
            </IconButton>
          </Tooltip>
          <Tooltip
            className={styles.accordionConfigTooltip}
            title={t("remove current pane")}
            arrow
            placement="top"
          >
            <IconButton
              className={styles.accordionConfigIconButton}
              disableRipple
              color="inherit"
              disabled={state.length <= 1}
              onClick={() => {
                state[paneIndex].components.length > 0
                  ? setShowDialog(true)
                  : removeTab();
                setAriaLive(
                  `${
                    state[paneIndex].title.length !== 0
                      ? state[paneIndex].title
                      : "Untitled Pane"
                  } is deleted from ${paneIndex + 1} position`
                );
              }}
            >
              <Remove />
            </IconButton>
          </Tooltip>
        </Toolbar>
      </AppBar>
    </div>
  );
});

export default ConfigBar;
