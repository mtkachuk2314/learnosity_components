import React, { useContext, useCallback, useState, useEffect } from "react";
import { TextareaAutosize } from "@material-ui/core";
import styled from "@emotion/styled";
// Localization
import { useTranslation } from "react-i18next";
import { LayoutContext } from "../../../Context/InteractivesContext";
import useDebounce from "../../../hooks/useDebounce";

const StyledAccordionTitle = styled(TextareaAutosize)(({ isActive }) => ({
  fontFamily: '"Inter", sans-serif',
  backgroundColor: "rgba(21, 101, 192, 0)",
  border: "none",
  fontSize: "1.125rem",
  fontWeight: 500,
  width: "100%",
  minHeight: "1.5625rem",
  lineHeight: 1.5,
  resize: "none",
  textOverflow: "hidden",
  display: "-webkit-box",
  WebkitBoxOrient: "vertical",
  "&::-webkit-scrollbar": {
    WebkitAppearance: "none",
    width: "0.4375rem",
  },
  "&::-webkit-scrollbar-thumb": {
    borderRadius: "0.25rem",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    boxShadow: "0 0 0.0625rem rgba(255, 255, 255, 0.5)",
    WebkitBoxShadow: "0 0 0.0625rem rgba(255, 255, 255, 0.5)",
  },
  "&::placeholder": {
    color: isActive && "#232323",
    opacity: 1,
  },
  "&:focus": {
    border: "none",
    outline: "none",
    "&:: placeholder": {
      color: "#232323 ",
      opacity: 0.6,
    },
  },
  ":-ms-input-placeholder": {
    /* Internet Explorer 10-11 */ color: isActive && "#232323",
  },

  "::-ms-input-placeholder": {
    /* Microsoft Edge */ color: isActive && "#232323",
  },
}));

const StyledAccorPlaceholder = styled("div")(({ isActive }) => ({
  width: "100%",
  maxHeight: "3.125rem",
  fontSize: "1.125rem",
  wordWrap: "break-word",
  overflowX: "hidden",
  overflowY: "hidden",
  textOverflow: "ellipsis",
  wordBreak: "break-word",
  display: "-webkit-box",
  whiteSpace: "pre-wrap",
  WebkitBoxOrient: "vertical",
  WebkitLineClamp: isActive ? "unset" : 2,
}));

const AccordionTitle = ({ accordionIndex, isActive }) => {
  const [state, dispatch] = useContext(LayoutContext);
  const [titleValue, setTitleValue] = useState(state[accordionIndex].title);

  const { t } = useTranslation();

  const updateTitleState = (title) => {
    const trimCharLimit = title.substring(0, 200);
    dispatch({
      func: "CHANGE_TITLE",
      title: trimCharLimit,
      layerIndex: accordionIndex,
    });
  };
  const handleDebounce = useDebounce(updateTitleState);

  const handleTitleChange = useCallback(
    (e) => {
      const title = e.target.value;
      setTitleValue(title);
      handleDebounce(title);
    },
    [handleDebounce]
  );

  useEffect(() => {
    setTitleValue(state[accordionIndex].title);
  }, [state, accordionIndex]);

  return (
    <>
      {isActive !== null && (
        <StyledAccordionTitle
          placeholder={t("Untitled Pane")}
          aria-label={t("pane title input")}
          aria-multiline="true"
          minRows="1"
          maxRows="2"
          onChange={handleTitleChange}
          value={titleValue}
          onKeyDown={(e) => e.key === "Enter" && e.preventDefault()} // Prevent new line break
        />
      )}
      {isActive === null && (
        <StyledAccorPlaceholder accordionIndexProp={accordionIndex}>
          {state[accordionIndex].title || t("Untitled Pane")}
        </StyledAccorPlaceholder>
      )}
    </>
  );
};

export default AccordionTitle;
