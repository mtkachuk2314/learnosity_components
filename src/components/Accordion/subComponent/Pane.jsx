import React, { useContext, useEffect } from "react";
import { v4 as uuidv4 } from "uuid";
import { AccordionSummary } from "@mui/material";
import { ExpandMore } from "@mui/icons-material";
import styled from "@emotion/styled";
import AccordionTitle from "./AccordionTitle";
import {
  LayoutContext,
  ActivePaneContext,
} from "../../../Context/InteractivesContext";

// styles for accordion
const StyledPaneContainer = styled("div")({
  width: "100%",
});

const StyledAccordionSummary = styled(AccordionSummary)(
  ({ isActive, numOfPanes, expanded, accordionIndex }) => ({
    minHeight: "2.5rem",
    maxHeight: "4.125rem",
    opacity: "1 !important",
    width: "100%",
    padding: "0.5rem 0.5rem 0.5rem 0.625rem",
    fontSize: "1.125rem",
    color: "#232323",
    letterSpacing: "0.009375rem",
    fontcolor: "#232323",
    backgroundColor: isActive
      ? "rgba(21, 101, 192, 0.12) !important"
      : "#fff !important", //! important overrides the MUI grey background.
    borderWidth: isActive ? "0.0625rem 0 0.1875rem 0" : "0.0625rem 0.0625rem",
    borderBottomWidth: isActive
      ? "0.1875rem"
      : isActive && expanded
      ? "0.1875rem"
      : accordionIndex === numOfPanes - 1 || (expanded && !isActive)
      ? "0.06525rem"
      : "0",
    borderStyle: "solid",
    borderColor: isActive ? "#232323" : "#BDBDBD",
    borderRadius: isActive // accordion is active
      ? accordionIndex === 0 && numOfPanes > 1
        ? "0.625rem 0.625rem 0 0"
        : accordionIndex === numOfPanes - 1 && numOfPanes > 1 && !expanded // Accordion is active and last pane
        ? "0 0 0.625rem 0.625rem"
        : accordionIndex === numOfPanes - 1 && numOfPanes > 1 && expanded // Accordion is active, expanded and last pane
        ? "0 0 0 0"
        : accordionIndex !== numOfPanes - 1 || accordionIndex !== 0 // middle panes
        ? "none"
        : numOfPanes === 1 && !expanded // one pane, active not expanded
        ? "0.625rem"
        : numOfPanes === 1 && expanded // one pane, active and expanded
        ? "0.625rem 0.625rem 0 0"
        : "none"
      : expanded // accordion not active and accordion is expanded
      ? accordionIndex === 0 && numOfPanes > 1
        ? "0.625rem 0.625rem 0 0"
        : numOfPanes === 1
        ? "0.625rem 0.625rem 0 0"
        : "none"
      : accordionIndex === 0 && numOfPanes > 1 // Accordion is not active and Accordion is collapsed
      ? "0.625rem 0.625rem 0 0"
      : accordionIndex === numOfPanes - 1 && numOfPanes > 1 // Accordion is not active and last pane
      ? "0 0 0.625rem 0.625rem"
      : numOfPanes === 1
      ? "0.625rem 0.625rem 0.625rem 0.625rem"
      : "none",

    "&:focus, &:hover": {
      backgroundColor: "rgba(21, 101, 192, 0.12) !important",
    },
    ".MuiAccordionSummary-expandIconWrapper": {
      alignSelf: "start",
    },
  })
);

const StyledAccordionSummaryContents = styled("div")({
  width: "100%",
  display: "flex",
  justifyContent: "space-between",
  margin: "-0.75rem -0.125rem -0.75rem 0",
});
// styles end.

const Pane = ({
  accordionIndex,
  isActive,
  setIsActive,
  removeError,
  setRemoveError,
  viewOnly,
}) => {
  const [state, dispatch] = useContext(LayoutContext);
  const [, setActivePane] = useContext(ActivePaneContext);

  useEffect(() => {
    setRemoveError(false);
  }, [removeError]);

  return (
    <StyledPaneContainer key={`pane-${accordionIndex}`}>
      <StyledAccordionSummary
        id={`panel-${accordionIndex + 1}-add-components-${uuidv4()}`}
        data-testid={`pane-${accordionIndex}`}
        onClick={() => {
          setIsActive(accordionIndex);
        }}
        accordionIndex={accordionIndex}
        isActive={accordionIndex === isActive}
        numOfPanes={state.length}
        expanded={state[accordionIndex].expanded}
        disabled={viewOnly}
        expandIcon={
          <ExpandMore
            onClick={() => {
              dispatch({
                func: "TOGGLE_PANE",
                paneIndex: accordionIndex,
              });
              setActivePane({
                func: "TOGGLE_PANE",
                paneIndex: accordionIndex,
              });
            }}
            sx={{
              pointerEvents: "auto",
              color: "#000",
            }}
          />
        }
      >
        <StyledAccordionSummaryContents>
          <AccordionTitle
            key={`accordion-title-${accordionIndex}`}
            accordionIndex={accordionIndex}
            isActive={isActive}
          />
        </StyledAccordionSummaryContents>
      </StyledAccordionSummary>
    </StyledPaneContainer>
  );
};
export default Pane;
