import React, { useContext, useState, useRef } from "react";
import { Button, Accordion } from "@mui/material";
import styled from "@emotion/styled";
import { useTranslation } from "react-i18next";
import TenantContext from "../../../Context/TenantContext";
import { LayoutContext } from "../../../Context/InteractivesContext";
import AccordionItem from "./AccordionItem";
import Pane from "./Pane";
import ConfigBar from "./ConfigBar";
import DialogProvider from "../../../Utility/DialogProvider";

// portal
import PortalUtil from "../../../Utility/PortalUtil";

// styled components for Accordion styles
const StyledAccordionContainer = styled("div")({
  width: "100%",
});

const StyledAccordion = styled(Accordion)(() => ({
  backgroundColor: "#FFFFFF",
  "&::before": {
    backgroundColor: "transparent",
  },
}));

const StyledExpandCollapseButton = styled(Button)(({ disabled }) => ({
  fontWeight: "400",
  fontSize: "16px",
  color: disabled ? "#000000" : "#232323",
  letterSpacing: "0.15px",
  lineHeight: "24px",
  textTransform: "capitalize",
}));

const StyledButtonsDiv = styled("div")(() => ({
  display: "flex",
  justifyContent: "flex-end",
  gap: "16px",
}));

const StyledToolBar = styled("div")(() => ({
  backgroundColor: "#fff ",
}));
// Styled components end

const Accordions = ({
  setHoverMenu,
  toolbarContainerRef,
  isActiveComponent,
  setActiveComponent,
}) => {
  const [state, dispatch] = useContext(LayoutContext);
  const [isActive, setIsActive] = useState(null);

  const [removeError, setRemoveError] = useState(false);

  // click outside hook sets active pane to null when user clicks outside the accordion pane
  const paneRef = useRef();
  const toolbarRef = useRef(null);

  const authoringData = useContext(TenantContext);

  const viewOnly = !authoringData?.viewOnlyComponents;

  const { t } = useTranslation();

  // TODO: If needed, move to useEffect
  // if (
  //   state.map((ele) => {
  //     if (ele.placeholderTitle) {
  //       dispatch({
  //         func: "DELETE_PLACEHOLDER",
  //       });
  //     }
  //   })
  // )

  const onBlureAccordionItemsHandler = () => {
    !isActiveComponent && setIsActive(null);
  };

  return (
    <StyledAccordionContainer
      data-testid="accordion-component"
      ref={paneRef}
      onBlur={onBlureAccordionItemsHandler}
    >
      <DialogProvider>
        {!viewOnly && isActiveComponent && (isActive || isActive === 0) ? (
          <PortalUtil portalRef={toolbarContainerRef}>
            <StyledToolBar>
              <ConfigBar
                ref={toolbarRef}
                paneIndex={isActive}
                setPaneIndex={setIsActive}
                setRemoveError={setRemoveError}
              />
            </StyledToolBar>
          </PortalUtil>
        ) : null}
        {state.length > 1 && (
          <StyledButtonsDiv>
            <StyledExpandCollapseButton
              onClick={() => {
                dispatch({
                  func: "EXPAND_ALL_PANE",
                });
              }}
              disabled={state.every((s) => s.expanded === true)}
            >
              {t("Expand All")}
            </StyledExpandCollapseButton>
            <StyledExpandCollapseButton
              onClick={() => {
                dispatch({
                  func: "COLLAPSE_ALL_PANE",
                });
              }}
              disabled={state.every((s) => s.expanded === false)}
            >
              {t("Collapse All")}
            </StyledExpandCollapseButton>
          </StyledButtonsDiv>
        )}
        {state.map((accordion, accordionIndex) => {
          return (
            <StyledAccordion
              accordionIndex={accordionIndex}
              disableGutters
              expanded={accordion.expanded}
            >
              <Pane
                accordionIndex={accordionIndex}
                isActive={isActive}
                setIsActive={setIsActive}
                removeError={removeError}
                setRemoveError={setRemoveError}
                viewOnly={viewOnly}
              />
              <AccordionItem
                accordionIndex={accordionIndex}
                removeError={removeError}
                setRemoveError={setRemoveError}
                paneRef={paneRef}
                setHoverMenu={setHoverMenu}
                setActiveComponent={setActiveComponent}
              />
            </StyledAccordion>
          );
        })}
      </DialogProvider>
    </StyledAccordionContainer>
  );
};

export default Accordions;
