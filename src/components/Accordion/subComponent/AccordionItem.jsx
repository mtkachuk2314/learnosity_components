import React, { useState, useEffect, useContext } from "react";
import { useDrop } from "react-dnd";
import { AccordionDetails } from "@mui/material";
import styled from "@emotion/styled";
import {
  ActivePaneContext,
  LayoutContext,
} from "../../../Context/InteractivesContext";

// ? Components
import PlaceHolder from "../../../Utility/Placeholder";
import PlaceholderError from "../../../Utility/PlaceholderError";
import NestedComponentWrapper from "../../../Utility/NestedComponentWrapper";
import { MainInsideWrapper } from "../../../theme/styledComponents/globalComponent";

const StyledAccordionDetails = styled(AccordionDetails)(
  ({ isOver, showError, empty, accordionIndex, numOfPanes }) => ({
    backgroundColor:
      showError && empty
        ? "rgba(211, 47, 47, 0.04)"
        : isOver && empty
        ? "rgba(21, 101, 192, 0.04)"
        : "#ffffff",
    borderWidth: "1px",
    margin: "10px ,0px",
    padding: "10px",
    borderStyle: "solid",
    borderColor: "#BDBDBD",
    borderWidth: " 0 0.0625rem",
    borderBottomWidth: accordionIndex === numOfPanes - 1 ? "0.0625rem" : "0",
  })
);

const AccordionItem = ({
  accordionIndex,
  removeError,
  setRemoveError,
  setHoverMenu,
  setActiveComponent,
  paneRef,
}) => {
  const [state, dispatch] = useContext(LayoutContext);
  const [activeComp, setActiveComp] = useState(null);
  const [activeTab] = useContext(ActivePaneContext);
  const [isDragging, setIsDragging] = useState(false);
  const [droppedIndex, setDroppedIndex] = useState(null);
  const [inContainer, setInContainer] = useState(null);

  // List of accepted into tab componenets
  const acceptListComp = (item) => {
    return (
      ["Text", "Table", "Video", "Image", "Header"].indexOf(
        item.componentName
      ) >= 0
    );
  };

  // ? Error Message
  const [showError, setShowError] = useState();
  const [showDropError, setShowDropError] = useState();

  const [{ isOver, getItem }, drop] = useDrop(
    () => ({
      accept: [
        "Text",
        "Image",
        "Video",
        "Table",
        "InfoBox",
        "Header",
        "QuoteBox",
        "IFrame",
        "Accordion",
        "Tab",
        "section",
        "MultiColumn",
        "Reveal",
        "MultipleChoice",
      ],
      drop: async (item, monitor) => {
        if (
          !acceptListComp(item) &&
          state[accordionIndex].components.length !== 0
        )
          setShowDropError(true);
        if (item.within && state[accordionIndex].components.length !== 0)
          return;
        if (monitor.didDrop()) return;
        if (!monitor.isOver({ shallow: true })) return;
        if (acceptListComp(item)) {
          dispatch({
            func: "ADD_COMPONENT",
            tabIndex: accordionIndex,
            component: {
              componentName: item.componentName,
              componentProps: JSON.parse(item?.componentProps),
              formats: item.formats,
            },
          });
          item?.delete && item?.delete();
        }
      },
      collect: (monitor) => ({
        isOver: !!monitor.isOver(),
        getItem: monitor.getItem(),
      }),
    }),
    [state[accordionIndex].components]
  );

  // Adjust item name for better viewing
  const trimCap = (item) => {
    switch (item) {
      case "IFrame":
        return "iFrame";
      case "InfoBox":
        return "InfoBox";
      case "Tab":
        return "Tabs";
      case "NONLEARNING":
        return "Descriptive Container";
      case "LEARNING":
        return "Learning Container";
      default:
        return item.replace(/([A-Z])/g, " $1").trim();
    }
  };

  useEffect(() => {
    if (isOver && !acceptListComp(getItem)) {
      setShowError(trimCap(getItem.componentName || getItem.type));
    } else if (isOver) {
      setShowError();
      setShowDropError(false);
      setIsDragging(true);
    } else {
      setIsDragging(false);
    }
  }, [isOver]);

  useEffect(() => {
    setShowError();
    setRemoveError(false);
  }, [removeError]);

  return (
    <StyledAccordionDetails
      data-testid="accordion-dropzone"
      isOver={isOver}
      showError={showError}
      accordionIndex={accordionIndex}
      empty={state[accordionIndex].components.length == 0}
      numOfPanes={state.length}
      ref={drop}
      onDragLeave={() => setInContainer(false)}
      onDragOver={() => setInContainer(true)}
      role="document"
    >
      {state[accordionIndex].components.length !== 0 ? (
        state[accordionIndex].components.map((component, compIndex) => {
          if (compIndex !== 0)
            return (
              <NestedComponentWrapper
                key={`key-component-${compIndex}`}
                componentType="accordion"
                numOfComponent={state[accordionIndex].components.length}
                componentProps={component.componentProps}
                component={component}
                compIndex={compIndex}
                formats={component.formats}
                tabIndex={accordionIndex}
                inContainer={inContainer}
                setDroppedIndex={setDroppedIndex}
                setActiveComp={setActiveComp}
                activeComp={activeComp}
                draggingOver={isOver}
                setShowError={setShowError}
                setShowDropError={setShowDropError}
                setHoverMenu={setHoverMenu}
                setActiveComponent={setActiveComponent}
              />
            );
          return (
            <MainInsideWrapper key={`key-component-${compIndex}`}>
              <NestedComponentWrapper
                componentType="accordion"
                numOfComponent={state[accordionIndex].components.length}
                componentProps={component.componentProps}
                component={component}
                compIndex={compIndex}
                formats={component.formats}
                tabIndex={accordionIndex}
                inContainer={inContainer}
                setDroppedIndex={setDroppedIndex}
                setActiveComp={setActiveComp}
                activeComp={activeComp}
                draggingOver={isOver}
                setShowError={setShowError}
                setShowDropError={setShowDropError}
                setHoverMenu={setHoverMenu}
                setActiveComponent={setActiveComponent}
              />
            </MainInsideWrapper>
          );
        })
      ) : (
        <PlaceHolder isOver={isOver} showError={showError} />
      )}

      {showDropError && state[accordionIndex].components.length !== 0 && (
        <PlaceholderError errorType="addComponent" />
      )}
    </StyledAccordionDetails>
  );
};

export default AccordionItem;
