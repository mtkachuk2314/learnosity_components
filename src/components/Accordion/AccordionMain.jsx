import React from "react";
import PropTypes from "prop-types";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import { v4 as uuidv4 } from "uuid";
import { LayoutProvider } from "../../Context/InteractivesContext";
import Accordions from "./subComponent/Accordions";
import { MainWrapper } from "../../theme/styledComponents/globalComponent";

// Accordion default props
export const defaultProps = {
  layoutState: [
    {
      id: uuidv4(),
      title: "",
      components: [],
      expanded: true,
    },
  ],
};

const AccordionMain = ({
  layoutState = [],
  setProp = () => {},
  toolbarContainerRef,
  // showSelf is not being passed to interactive component - keep it just for consistency
  showSelf = null,
  isActiveComponent = null,
  setHoverMenu = () => {},
  setActiveComponent = () => {},
}) => {
  return (
    <DndProvider backend={HTML5Backend}>
      <LayoutProvider layoutState={layoutState} setProp={setProp}>
        <MainWrapper>
          <Accordions
            toolbarContainerRef={toolbarContainerRef}
            isActiveComponent={isActiveComponent}
            setHoverMenu={setHoverMenu}
            setActiveComponent={setActiveComponent}
          />
        </MainWrapper>
      </LayoutProvider>
    </DndProvider>
  );
};

AccordionMain.propTypes = {
  layoutState: PropTypes.array.isRequired,
  setProp: PropTypes.func.isRequired,
};

export default AccordionMain;
