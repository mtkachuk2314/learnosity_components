import React, { useState, useEffect, useContext, useRef } from "react";
import { v4 as uuidV4 } from "uuid";
import axios from "axios";
import { MultipleChoiceContext } from "../MultipleChoiceContext";
import useScript from "../../../Utility/useScript";
import TenantContext from "../../../Context/TenantContext";

const getSecuredRequest = async (refreshSession) => {
  try {
    const token = localStorage.getItem("accessToken");
    const headers = { authorizationToken: `Bearer ${token}` };
    const params = { mode: "item_list" };
    const { data } = await axios.get(
      process.env.REACT_APP_LEARNOSITY_ENDPOINT,
      { params, headers }
    );
    return data.response;
  } catch (error) {
    if (error.response && error.response.status === 401) {
      // eslint-disable-next-line no-console
      console.log(
        "Learnosity request failed with 401",
        {
          error,
        },
        "refreshing session..."
      );
      await refreshSession();
      return getSecuredRequest(true);
    }
    console.warn("error");
  }
};

const MultipleChoice = ({ componentID }) => {
  // Multiple Choice Context
  const [state, dispatch] = useContext(MultipleChoiceContext);
  // Component ID - Used to identify the container for the Learnosity component
  const [containerID] = useState(`activity-${componentID}`);
  // Response from Lamda API
  const [response, setResponse] = useState(null);
  // Learnosity script
  const learnosityScript = useScript(
    "https://authorapi.learnosity.com?v2022.3.LTS"
  );
  // Initialized Learnosity Assessment App - Used to render the Learnosity component
  const assessmentContainerRef = useRef(null);
  const [quizRendered, setQuizRendered] = useState(false);

  // Tenant Context - Authoring
  const authoringData = useContext(TenantContext);
  const { refreshSession } = authoringData;
  // View only mode - Observer
  const viewOnly = !authoringData?.viewOnlyComponents;

  // Call function that Gets secured request object Lamda at APIGW and component ID generation
  useEffect(() => {
    if (typeof refreshSession !== "function") return;
    if (quizRendered) return;
    if (response) return;
    const loadLearnosity = async () => {
      try {
        const lamdaResponse = await getSecuredRequest(false, refreshSession);
        setResponse(lamdaResponse);
      } catch (error) {
        throw new Error(error);
      }
    };
    loadLearnosity();
  }, [refreshSession, response]);

  // Call - Initialize Learnosity Author  when response, and script is set
  useEffect(() => {
    if (learnosityScript === "ready" && response && !quizRendered) {
      initializeLearnosityAuthor(state.activityID);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [learnosityScript, response, quizRendered]);

  // Initialization of Learnosity Author
  const initializeLearnosityAuthor = (reference) => {
    const assessmentApp = window.LearnosityAuthor.init(response, containerID, {
      readyListener() {
        setQuizRendered(true);
        reference !== null
          ? assessmentApp.navigate(`item/${reference}`)
          : assessmentApp.navigate(`items`);
        openItem();
        renderItem();
        renderItemList();
      },
      errorListener(error) {
        console.warn("Error", error);
      },
    });
    // When item is selected, save the item reference
    const openItem = () => {
      assessmentApp.on("open:item", (eventOpen) => {
        eventOpen.preventDefault();
        const itemID = eventOpen.data.item?.reference || uuidV4();
        if (itemID !== reference) {
          saveAssessment(itemID);
        } else {
          // If the item is already selected, navigate to the item (needed since preventDefault is used)
          assessmentApp.navigate(`item/${itemID}`);
        }
      });
    };
    // When item is rendered in viewOnly mode, disable all interactions
    const renderItem = () => {
      assessmentApp.on("render:item", () => {
        if (viewOnly) {
          const sideBarWrapper = document.querySelector(
            `#container-${containerID} .lrn-author-widget-sidebar-wrapper`
          );
          const bottomBarWrapper = document.querySelector(
            `#container-${containerID} .lrn-author-ui-add-extras`
          );

          if (sideBarWrapper && bottomBarWrapper) {
            sideBarWrapper.style.display = "none";
            bottomBarWrapper.style.display = "none";
          }
        }
        // add-button aria label

        const addButtonElement = document.querySelector(
          '[data-authorapi-selector="add-button"]'
        );
        if (addButtonElement) {
          addButtonElement.setAttribute("aria-label", "add button");
        }
        //hamburger Menu aria -abel
        const hamburgerMenuElement = document.querySelector(
          '[data-authorapi-selector="show-shared-passages-button"]'
        );
        if (hamburgerMenuElement) {
          hamburgerMenuElement.setAttribute("aria-label", "Hamburger Menu");
        }

        // fix for access assistant radio button in the multiple choice questons
        const optionElement = document.querySelectorAll(
          '[class="lrn-mcq-option"]'
        );
        optionElement.forEach((li, index) => {
          // Add the role attribute

          li.setAttribute("role", "radio");
          li?.children[0]?.setAttribute(
            "aria-label",
            `Select option ${index + 1}`
          );
          li?.children[0]?.removeAttribute("aria-labelledby");
        });
      });
    };
    // When item list is rendered, highlight the selected item
    const renderItemList = () => {
      assessmentApp.on("render:itemlist", () => {
        const allListItems = document.querySelectorAll(
          `#container-${containerID} ul.lrn-list-view li.lrn-list-view-item`
        );
        allListItems.forEach((li, index) => {
          const anchor = li.querySelector("a");
          if (anchor.textContent === reference) {
            allListItems[index].style.backgroundColor = "#c7e8a7";
          }
        });
      });
    };
  };

  // Save activity and item references
  const saveAssessment = (reference) => {
    dispatch({
      func: "SET_ACTIVITY",
      activityID: reference,
    });
  };

  // On state change, destroy and rebuild Learnosity node, set quizRendered to false
  useEffect(() => {
    const container = assessmentContainerRef.current;
    const createNode = () => {
      const childDiv = document.createElement("div");
      childDiv.id = containerID;
      container.appendChild(childDiv);
    };
    if (quizRendered) {
      container.removeChild(container.children[0]);
      createNode();
      setQuizRendered(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.activityID]);

  return (
    <div
      className="multiple-choice-container"
      style={{ pointerEvents: viewOnly && "none" }}
    >
      {containerID && (
        <div
          id={`container-${containerID}`}
          className="my-widget"
          ref={assessmentContainerRef}
        >
          <div id={containerID} />
        </div>
      )}
    </div>
  );
};

export default MultipleChoice;
