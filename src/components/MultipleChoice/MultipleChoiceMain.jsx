import React from "react";
import { MultipleChoiceProvider } from "./MultipleChoiceContext";
import MultipleChoice from "./subcomponents/MultipleChoice";

import "./assets/styles.scss";

export const defaultProps = {
  multipleChoiceState: {
    componentID: null,
    activityID: null, // Will change to itemID eventually
    itemIDS: null,
  },
};

const MultipleChoiceMain = ({
  multipleChoiceState = defaultProps,
  setProp = () => {},
  componentID = null,
}) => {
  if (!componentID) return <div style={{ height: "4rem" }} />;
  return (
    <MultipleChoiceProvider
      multipleChoiceState={multipleChoiceState}
      setProp={setProp}
    >
      <MultipleChoice componentID={componentID} />
    </MultipleChoiceProvider>
  );
};

export default MultipleChoiceMain;
