import React, {
  createContext,
  useReducer,
  useEffect,
  useState,
  useMemo,
} from "react";
// eslint-disable-next-line import/no-extraneous-dependencies
import produce from "immer";
import isEqual from "lodash/isEqual";

//  state of video data stored in VideoContext
export const MultipleChoiceContext = createContext();

// Reducers
export const MultipleChoiceConfig = (draft, action) => {
  switch (action.func) {
    case "UPDATE_STATE":
      return action.data;
    case "SET_ACTIVITY":
      draft.activityID = action.activityID;
      return draft;
    case "UPDATE_CONTAINER":
      draft.componentID = action.componentID;
      return draft;
    default:
      return draft;
  }
};

//  video provider wraps the tab component to access reducer
export const MultipleChoiceProvider = ({
  children,
  setProp,
  multipleChoiceState,
}) => {
  const [state, dispatch] = useReducer(
    produce(MultipleChoiceConfig),
    multipleChoiceState
  );

  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    dispatch({ func: "UPDATE_STATE", data: multipleChoiceState });
    setMounted(true);
  }, []);

  // Updates the parent if the local state changes (and has a difference from the passed in props)
  useEffect(() => {
    // This is being run on rerenders and wasn't a dependency of the useEffect, it's a good idea to include in the useEffects instead of in the parent component
    const diff = !isEqual(state, multipleChoiceState);
    if (diff && mounted) setProp({ multipleChoiceState: state });
    // This should include mounted, but that might introduce extra runs with side effects that you don't want so it's worth looking structurally
  }, [state]);

  // Updates the local state if the passed in props change compared to it
  useEffect(() => {
    // TODO: As a warning, this isn't actually comparing the objects.  Something like Lodash's deep compare would be better because the object having a different order of keys would trigger this
    const diff = !isEqual(state, multipleChoiceState);

    if (diff && mounted)
      dispatch({ func: "UPDATE_STATE", data: multipleChoiceState });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [multipleChoiceState]);

  return (
    <MultipleChoiceContext.Provider
      value={useMemo(() => [state, dispatch], [state])}
    >
      {children}
    </MultipleChoiceContext.Provider>
  );
};
