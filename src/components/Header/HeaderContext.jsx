import React, {
  createContext,
  useReducer,
  useEffect,
  useState,
  useContext,
  useMemo,
} from "react";
import produce from "immer";

// state of video data stored in VideoContext
export const HeaderContext = createContext();

// Reducers
export const headerConfig = (draft, action) => {
  switch (action.func) {
    case "UPDATE_STATE":
      return action.data;
    case "CHANGE_SIZE":
      draft.size = action.size;
      return draft;
    case "CHANGE_ALIGNMENT":
      draft.alignment = action.alignment;
      return draft;
    case "CHANGE_HEADING":
      draft.heading = action.heading;
      return draft;
    default:
      return draft;
  }
};

// video provider wraps the tab component to access reducer
export const HeaderProvider = ({ children, setProp, headerState }) => {
  const [state, dispatch] = useReducer(produce(headerConfig), headerState);

  const diff = JSON.stringify(state) !== JSON.stringify(headerState);
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    // set initial value of state at the first mount
    dispatch({ func: "UPDATE_STATE", data: headerState });
    setMounted(true);
  }, []);

  useEffect(() => {
    if (diff && mounted) {
      setProp({ headerState: state });
    }
  }, [state]);

  useEffect(() => {
    diff && mounted && dispatch({ func: "UPDATE_STATE", data: headerState });
  }, [headerState]);

  // usememo --> prevent passing a new reference to object or array as value in every time the provider component re-renders
  const headerValue = useMemo(() => [state, dispatch], [state]);
  return (
    <HeaderContext.Provider value={headerValue}>
      {children}
    </HeaderContext.Provider>
  );
};

// use custom hook to return context - check if it's being used inside provider
export const useHeaderContext = () => {
  const contextValue = useContext(HeaderContext);
  if (!contextValue)
    throw new Error("useHeaderContext must be called inside HeaderProvider");
  return contextValue;
};
