import React, {
  useState,
  useRef,
  useEffect,
  useContext,
  forwardRef,
} from "react";
import { useTranslation } from "react-i18next";
import "../../Text/styles/Toolbar.scss";
import {
  Paper,
  AppBar,
  Toolbar,
  MenuItem,
  ClickAwayListener,
  Button,
  IconButton,
  Popper,
  Grow,
  Tooltip,
  Card,
  MenuList,
  Box,
} from "@mui/material";
import { HeaderContext } from "../HeaderContext";

// MUI

// assets
import icons, { Chevron } from "../../Text/assets/icons";

// Header Size Options
const headerSizeDropdownOptions = [
  { value: "large", label: "Large" },
  { value: "medium", label: "Medium" },
  { value: "small", label: "Small" },
];

/**
 * Capitalizes the first letter of each word in a string.
 * @param {string} str - The input string.
 * @returns {string} - The new string with capitalized words.
 */
function capitalizeWords(str) {
  return str.replace(/(^|\s)\S/g, (match) => match.toUpperCase());
}
const HeaderToolbar = forwardRef(function HeaderToolbar(
  { activeTopMenu, setActiveTopMenu, headerContainerRef },
  ref
) {
  const [state, dispatch] = useContext(HeaderContext);
  // ? Header Size Dropdown Open/Close State
  const [openHeader, setHeaderOpen] = useState(false);
  // ? Header Size Dropdown Selection State
  const [selectedHeader, setSelectedHeader] = useState(null);

  //  ? Alignment Dropdown Selection State
  const [activeDropDownItem, setActiveDropDownItem] = useState();

  // Translation
  const { t } = useTranslation();

  // Refrence for Header Size Dropdown
  const HeaderDropDown = useRef(null);
  const alignRef = useRef(null);

  // Save Header Size Dropdown Selection
  const handleSizeSelect = (value) => {
    setSelectedHeader(value);
    dispatch({
      func: "CHANGE_SIZE",
      size: value,
    });
    headerContainerRef.current.focus();
  };
  // Handle Toolbar Alignment State Change
  useEffect(() => {
    const handleAlignmentChange = (activeDrDownItem) => {
      let currentAlignment;

      if (activeDrDownItem) {
        currentAlignment = activeDrDownItem;
      } else {
        currentAlignment = state.alignment;
        setActiveDropDownItem(state.alignment);
      }
      dispatch({
        func: "CHANGE_ALIGNMENT",
        alignment: currentAlignment,
      });
    };
    if (activeDropDownItem !== state.alignment) {
      handleAlignmentChange(activeDropDownItem);
    }
  }, [activeDropDownItem]);

  return (
    <Box
      ref={ref}
      onClick={(e) => e.stopPropagation()}
      onFocus={(e) => e.stopPropagation()}
      data-testid="header-toolbar"
      className="ToolbarContainer"
      sx={{
        outline: "none",
      }}
      role="button"
      aria-label={t("header-toolbar-label")}
    >
      <AppBar
        position="static"
        className="StyledAppbar"
        elevation={0}
        style={{
          "--display": "flex",
          "--direction": "row",
          "--width":
            state.size == "large"
              ? "134px"
              : state.size == "medium"
              ? "153px"
              : state.size == "small" && "134px",
        }}
      >
        <Toolbar
          position="static"
          className="StyledToolbar"
          style={{
            "--grid-template-columns": "3fr 9px 1fr",
            // main toolbar drop shadow
            "--boxShadow":
              "0px 0px 3px 0px rgba(35, 35, 35, 0.15),-1px 0px 5px 0px rgba(161, 162, 163, 0.2),3px 3px 4px 0px rgba(35, 35, 35, 0.15)",
          }}
        >
          <Button
            data-testid="header-toolbar-size"
            role="button"
            ref={HeaderDropDown}
            data-id="headerToolBar"
            aria-controls={openHeader ? "Header Select" : undefined}
            aria-expanded={openHeader ? "true" : undefined}
            variant="contained"
            disableRipple
            disableFocusRipple
            onClick={() => {
              setHeaderOpen(!openHeader);
              setActiveTopMenu(false);
            }}
            className="SelectButton"
            style={{
              "--active": openHeader ? "rgba(21, 101, 192, 1)" : "#000",
              "--svg": openHeader ? " " : "rotate(180deg)",
              "--width": "100%",
              "--height": "100%",
              "--font-size": "16px",
              "--padding": "0 4.5px",
              "--grid-template-columns": "1fr 3fr",
              "--hover-background-color": "transparent",
            }}
          >
            <Chevron />
            {t(`Header Size ${capitalizeWords(state.size)}`)}
            <Popper
              open={openHeader}
              anchorEl={HeaderDropDown.current}
              placement="bottom-start"
              transition
              disablePortal
            >
              {({ TransitionProps }) => (
                <Grow {...TransitionProps}>
                  <Paper
                    elevation={0}
                    className="StyledSelectPaper"
                    style={{
                      display: "grid",
                      gridTemplateColumns: "31px 31px 31px",
                      "--height": "7.0625rem",
                      "--width": "5.4375rem",
                      "--margin-left": "-0.1875rem",
                      "--margin-top": "0.125rem",
                      boxShadow:
                        "0px 1px 3px 0px rgba(35, 35, 35, 0.1), 0px 2px 5px 0px rgba(161, 162, 163, 0.2), 3px 3px 4px 0px rgba(35, 35, 35, 0.15)",
                    }}
                  >
                    <ClickAwayListener
                      onClickAway={() => setHeaderOpen(!openHeader)}
                    >
                      <MenuList
                        autoFocusItem={openHeader}
                        data-testid="header-select-dropdown"
                        aria-labelledby="Header Drop Down"
                        className="StyledMenu"
                        style={{
                          "--height": "113px",
                          "--gridTemplateRows": "31px 31px 31px",
                          "--width": "87px",
                          justifyItems: "normal",
                        }}
                      >
                        {headerSizeDropdownOptions.map((size) => {
                          const { value, label } = size;
                          const tValue = t(
                            `Header Size ${capitalizeWords(value)}`
                          );
                          const tLabel = t(
                            `Header Size ${capitalizeWords(label)}`
                          );

                          return (
                            <MenuItem
                              key={`${value} header`}
                              value={tValue}
                              selected={label === selectedHeader}
                              onClick={() => handleSizeSelect(value)}
                              data-testid={`${value} header`}
                              aria-labelledby={tLabel}
                              className="StyledMenuItem"
                              style={{
                                "--height": "31px",
                                "--fontSize":
                                  label === "Large"
                                    ? "1.375rem"
                                    : label === "Medium"
                                    ? "1rem"
                                    : "0.875rem",
                                "--fontWeight":
                                  label === "Large"
                                    ? "300"
                                    : label === "Medium"
                                    ? "300"
                                    : "500",
                                "--lineHeight":
                                  label === "Large"
                                    ? "116.7%"
                                    : label === "Medium"
                                    ? "120%"
                                    : "116.7%",
                                "--letterSpacing":
                                  label === "Large"
                                    ? "-1.5px"
                                    : label === "Medium"
                                    ? "-0.5px"
                                    : "normal",
                                "--width": "100%",
                              }}
                            >
                              {tLabel}
                            </MenuItem>
                          );
                        })}
                      </MenuList>
                    </ClickAwayListener>
                  </Paper>
                </Grow>
              )}
            </Popper>
          </Button>

          {/* <Divider orientation="vertical" variant="middle" flexItem /> */}
          <div className="StyledDivider" />

          <Tooltip
            aria-label={t("Header alignment")}
            title={t("Header alignment")}
            placement="top"
            arrow
            style={{ position: "relative" }}
          >
            <IconButton
              data-testid="header-toolbar-align"
              role="button"
              ref={alignRef}
              disableRipple
              color="inherit"
              onClick={() => {
                if (activeTopMenu) {
                  setActiveTopMenu(false);
                } else {
                  setActiveTopMenu(true);
                }
                setHeaderOpen(false);
              }}
              className="StyledIconButton"
              style={{
                "--active": activeTopMenu ? "rgba(21, 101, 192, 1)" : "#000",
                "--background": activeTopMenu
                  ? "rgba(21, 101, 192, 0.12)"
                  : "#fff",
              }}
              aria-label="alignment buttons dropdown"
              value={state.alignment}
              data-alignid="alignment-dropdown"
            >
              {{
                "left-align": icons.align,
                "center-align": icons.center,
                "right-align": icons.right,
              }[state.alignment] || icons.align}
            </IconButton>
            <AlignDropdownButton
              aria-label="alignment options"
              activeTopMenu={activeTopMenu}
              activeDropDownItem={activeDropDownItem}
              setActiveDropDownItem={setActiveDropDownItem}
              alignment={state.alignment}
              t={t}
            />
          </Tooltip>
        </Toolbar>
      </AppBar>
    </Box>
  );
});

export default HeaderToolbar;

const AlignDropdownButton = ({
  activeTopMenu,
  activeDropDownItem,
  setActiveDropDownItem,
  alignment,
  t,
}) => {
  return (
    <Card
      elevation={0}
      className="StyledCard"
      style={{
        "--card-display": activeTopMenu ? "flex" : "none",
        // toolbar submenu drop shadow
        "--box-shadow":
          "0px 1px 3px 0px rgba(35, 35, 35, 0.1), 0px 2px 5px 0px rgba(161, 162, 163, 0.2), 3px 3px 4px 0px rgba(35, 35, 35, 0.15)",
        "--left": "-0.3125rem",
        "--width": "7rem",
        top: "2.375rem",
      }}
    >
      <Tooltip
        aria-label={t("Header align left")}
        title={t("Header align left")}
        placement="top"
        arrow
      >
        <IconButton
          disableRipple
          role="button"
          value="left-align"
          color="inherit"
          aria-label={t("Header align left")}
          onClick={() => {
            setActiveDropDownItem("left-align");
          }}
          className="StyledIconButton"
          style={{
            "--active":
              alignment === "left-align" ? "rgba(21, 101, 192, 1)" : "#000",
            "--background":
              alignment === "left-align" ? "rgba(21, 101, 192, 0.12)" : "#fff",
          }}
        >
          {icons.align}
        </IconButton>
      </Tooltip>
      <Tooltip
        aria-label={t("Header center text")}
        title={t("Header center text")}
        placement="top"
        arrow
      >
        <IconButton
          disableRipple
          role="button"
          aria-label={t("Header center text")}
          title={t("Header center text")}
          value="center-align"
          onClick={() => {
            if (activeDropDownItem === "center-align") {
              setActiveDropDownItem("left-align");
            } else {
              setActiveDropDownItem("center-align");
            }
          }}
          className="StyledIconButton"
          style={{
            "--active":
              alignment === "center-align" ? "rgba(21, 101, 192, 1)" : "#000",
            "--background":
              alignment === "center-align"
                ? "rgba(21, 101, 192, 0.12)"
                : "#fff",
          }}
        >
          {icons.center}
        </IconButton>
      </Tooltip>
      <Tooltip
        aria-label={t("Header align right")}
        title={t("Header align right")}
        placement="top"
        arrow
      >
        <IconButton
          disableRipple
          role="button"
          aria-label={t("Header align right")}
          value="right-align"
          onClick={() => {
            if (activeDropDownItem === "right-align") {
              setActiveDropDownItem("left-align");
            } else {
              setActiveDropDownItem("right-align");
            }
          }}
          className="StyledIconButton"
          style={{
            "--active":
              alignment === "right-align" ? "rgba(21, 101, 192, 1)" : "#000",
            "--background":
              alignment === "right-align" ? "rgba(21, 101, 192, 0.12)" : "#fff",
          }}
        >
          {icons.right}
        </IconButton>
      </Tooltip>
    </Card>
  );
};
