import React, {
  useEffect,
  useState,
  useContext,
  useCallback,
  useRef,
  useMemo,
} from "react";
// MUI
import { useTranslation } from "react-i18next";
import { Paper, TextareaAutosize } from "@material-ui/core";
import Box from "@mui/material/Box";
// styles/emotion
import styled from "@emotion/styled";
// UUID
import { v4 as uuidv4 } from "uuid";
// Toolbar
import HeaderToolbar from "./HeaderToolbar";
// Context
import { useHeaderContext } from "../HeaderContext";
import TenantContext from "../../../Context/TenantContext";
import useDebounce from "../../../hooks/useDebounce";
// portal
import PortalUtil from "../../../Utility/PortalUtil";

// Styled components
// ? Styled Text Input
const StyledHeaderInput = styled(TextareaAutosize)(
  ({ headerlevel, alignment }) => {
    let fontSize;
    let lineHeight;
    let letterSpacing;
    if (headerlevel === "medium") {
      fontSize = "36px";
      lineHeight = "43.2px";
      letterSpacing = "-0.5px";
    } else if (headerlevel === "small") {
      fontSize = "34px";
      lineHeight = "39.68px";
      letterSpacing = "large";
    } else {
      fontSize = "42px";
      lineHeight = "49.01px";
      letterSpacing = "normal";
    }

    let textAlign;
    if (alignment === "center-align") {
      textAlign = "center";
    } else if (alignment === "right-align") {
      textAlign = "right";
    } else {
      textAlign = "left";
    }
    return {
      width: "100%",
      border: "none",
      fontFamily: `"Inter", sans-serif`,
      wordBreak: "break-word",
      whiteSpace: "pre-line",
      overflowWrap: "break-word",
      msWordBreak: "break-word",
      resize: "none",
      position: "relative",
      padding: "1.25rem 6.5rem",
      background: "#FFF",
      textAlign,
      fontSize,
      fontWeight: "500",
      lineHeight,
      letterSpacing,
      "&::placeholder": {
        color: "rgba(35, 35, 35, 1)",
      },
      "&:focus::placeholder": {
        color: "rgba(0, 0, 0, 0.12)",
      },
      "&:focus-visible": {
        outline: "none",
      },
      backgroundColor: "white",
    };
  }
);

const StyledHeaderLabel = styled("label")(() => ({
  position: "absolute",
  width: "1px",
  height: "1px",
  padding: "0",
  margin: "-1px",
  overflow: "hidden",
  clip: "rect(0, 0, 0, 0)",
  border: "0",
}));

const StyledConfigBar = styled("div")(({ viewOnly }) => ({
  display: viewOnly && "none",
}));

const Header = ({ toolbarContainerRef, showSelf, isActiveComponent }) => {
  // Header Component State
  const [state, dispatch] = useHeaderContext();
  const [headerValue, setHeaderValue] = useState(state.heading);

  // Translation
  const { t } = useTranslation();

  // Toolbar Active State
  // ? Alignment Dropdown Open/Close State
  const [activeTopMenu, setActiveTopMenu] = useState(false);

  // Refs
  const headerContainerRef = useRef(null);
  const toolbarRef = useRef(null);

  // Context
  const authoringData = useContext(TenantContext);

  const viewOnly = !authoringData?.viewOnlyComponents;

  const updateHeadingState = (heading) => {
    dispatch({
      func: "CHANGE_HEADING",
      heading,
    });
  };

  const handleDebounce = useDebounce(updateHeadingState);

  const handleHeadingChange = useCallback(
    (e) => {
      const heading = e.target.value;
      setHeaderValue(heading);
      handleDebounce(heading);
    },
    [handleDebounce]
  );

  useEffect(() => {
    setHeaderValue(state.heading);
  }, [state.heading]);

  const headerLabelId = useMemo(() => `header-label-${uuidv4()}`, []);

  return (
    <Box
      ref={headerContainerRef}
      sx={{ outline: "none" }}
      tabIndex={0}
      role="region"
      aria-label={t("header component")}
    >
      {showSelf || isActiveComponent ? (
        <PortalUtil portalRef={toolbarContainerRef}>
          <StyledConfigBar viewOnly={viewOnly}>
            <HeaderToolbar
              ref={toolbarRef}
              data-id="headerInput"
              state={state}
              dispatch={dispatch}
              activeTopMenu={activeTopMenu}
              setActiveTopMenu={setActiveTopMenu}
              headerContainerRef={headerContainerRef}
            />
          </StyledConfigBar>
        </PortalUtil>
      ) : null}
      <Paper elevation={0}>
        <StyledHeaderLabel htmlFor={headerLabelId} data-testid="header-label">
          {t("Header input field")}
        </StyledHeaderLabel>
        <StyledHeaderInput
          id={headerLabelId}
          data-id="headerInput"
          placeholder={t("Header placeholder")}
          aria-label={t("Header input field")}
          type="text"
          onChange={handleHeadingChange}
          onKeyDown={(e) => e.key === "Enter" && e.preventDefault()} // Prevent new line break
          value={headerValue}
          alignment={state.alignment}
          headerlevel={state.size}
          viewOnly={viewOnly}
          readOnly={viewOnly}
        />
      </Paper>
    </Box>
  );
};

export default Header;
