import React from "react";
import { HeaderProvider } from "./HeaderContext";
import Header from "./subcomponents/Header";
import { MainWrapper } from "../../theme/styledComponents/globalComponent";

export const defaultProps = {
  headerState: {
    size: "large",
    alignment: "left-align",
    heading: "",
  },
};

const HeaderMain = ({
  headerState = defaultProps,
  setProp = () => {},
  toolbarContainerRef,
  showSelf = null,
  isActiveComponent = null,
}) => {
  return (
    <HeaderProvider headerState={headerState} setProp={setProp}>
      <MainWrapper>
        <Header
          toolbarContainerRef={toolbarContainerRef}
          showSelf={showSelf}
          isActiveComponent={isActiveComponent}
        />
      </MainWrapper>
    </HeaderProvider>
  );
};

export default HeaderMain;
