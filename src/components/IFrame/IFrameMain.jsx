import React from "react";
import IFrame from "./subcomponents/IFrame";
import { IFrameProvider } from "./IFrameContext";
import { MainWrapper } from "../../theme/styledComponents/globalComponent";

export const defaultProps = {
  title: "",
  src: "",
  height: 500,
  width: 900,
};

const IFrameMain = ({
  title = "",
  src = "",
  height = 500,
  width = 900,
  setProp = () => {},
  showSelf = null,
  isActiveComponent = null,
  toolbarContainerRef,
}) => {
  return (
    <IFrameProvider
      title={title}
      src={src}
      height={height}
      width={width}
      setProp={setProp}
    >
      <MainWrapper>
        <IFrame
          showSelf={showSelf}
          isActiveComponent={isActiveComponent}
          toolbarContainerRef={toolbarContainerRef}
        />
      </MainWrapper>
    </IFrameProvider>
  );
};

export default IFrameMain;
