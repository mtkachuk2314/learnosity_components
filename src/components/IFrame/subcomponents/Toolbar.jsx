import React from "react";
// MUI imports
import { Tooltip } from "@mui/material";
// Styles
import styles from "../styles/Toolbar.module.scss";

const Toolbar = ({ t, createIFrame, isActiveComponent, showSelf }) => {
  const handleChangeIFrame = (e) => {
    createIFrame(e);
  };

  return (
    <Tooltip
      aria-label={t("change iFrame")}
      title={t("change iFrame")}
      placement="top"
      arrow
    >
      <button
        data-testid="iFrameToolbar"
        onClick={handleChangeIFrame}
        className={styles.iFrameToolbarContainer}
        style={{
          "--active": isActiveComponent || showSelf ? "block" : "none",
        }}
      >
        {t("Change iFrame")}
      </button>
    </Tooltip>
  );
};

export default Toolbar;
