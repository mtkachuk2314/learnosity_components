import React, { useContext } from "react";
// Other components/context
import { IFrameContext } from "../IFrameContext";
// Styles
import styles from "../styles/OpenInNewTab.module.scss";
// MUI Imports
import { Button } from "@mui/material";
import LaunchIcon from "@mui/icons-material/Launch";

const OpenInNewTab = ({ t }) => {
  const [state] = useContext(IFrameContext);

  return (
    <div
      data-testid="iFrame-open-in-new-tab"
      className={styles.openInNewTabLayoutContainer}
    >
      <div className={styles.openInNewTabContainer}>
        <Button
          variant="contained"
          className={styles.openInNewTabButton}
          href={state.src}
          target="_blank"
          endIcon={<LaunchIcon />}
          disableRipple={true}
          disableFocusRipple={true}
        >
          {t("Open in new tab")}
        </Button>
      </div>
    </div>
  );
};

export default OpenInNewTab;
