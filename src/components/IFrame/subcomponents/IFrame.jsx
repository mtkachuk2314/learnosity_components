import React, { useState, useEffect, useRef, useContext } from "react";
// Other components/context
import { Button } from "@mui/material";
import { v4 as uuid } from "uuid";
import { useTranslation } from "react-i18next";
import Modal from "./Modal";
import Toolbar from "./Toolbar";
import OpenInNewTab from "./OpenInNewTab";
import { IFrameContext } from "../IFrameContext";
import TenantContext from "../../../Context/TenantContext";
// Styles
import styles from "../styles/IFrame.module.scss";
// MUI Imports
// Other libraries
// Localization
// portal
import PortalUtil from "../../../Utility/PortalUtil";

const IFrame = ({ toolbarContainerRef, showSelf, isActiveComponent }) => {
  const [state] = useContext(IFrameContext);
  const [showModal, setShowModal] = useState(false);

  // Aria live
  const [ariaLive, setAriaLive] = useState("");

  const handleAriaLive = (action) => {
    if (action === "open") {
      setAriaLive(t("Add iFrame modal now open."));
    } else if (action === "close") {
      setAriaLive(t("Add iFrame modal now closed."));
    } else if (action === "add") {
      setAriaLive(t("iFrame successfully added to lesson."));
    } else if (action === "save") {
      setAriaLive(t("iFrame settings saved."));
    }
  };

  const [windowWidth, setWindowWidth] = useState(window.innerWidth);

  useEffect(() => {
    const handleWindowResize = () => {
      setWindowWidth(window.innerWidth);
    };

    window.addEventListener("resize", handleWindowResize);

    return () => {
      window.removeEventListener("resize", handleWindowResize);
    };
  }, [windowWidth]);

  const iFrameContainerRef = useRef(null);

  const { t } = useTranslation();

  const handleIFrameFocus = () => {
    setShowModal(false);
    handleAriaLive("close");
  };

  // useEffect(() => {
  //   window.focus();
  // }, []);

  const iFrameId = `iFrameComponent-${uuid()}`;

  window.addEventListener("blur", () => {
    setTimeout(() => {
      if (
        document.activeElement.tagName === "IFRAME" &&
        document.activeElement.id === iFrameId
      ) {
        handleIFrameFocus();
      }
      setShowModal(false);
      handleAriaLive("close");
    }, 0);
  });

  document.addEventListener("click", () => {
    setTimeout(() => {
      if (document.activeElement.tagName === "IFRAME") {
        setShowModal(false);
        handleAriaLive("close");
      }
    }, 0);
  });

  const createIFrame = (e) => {
    e.preventDefault();
    setShowModal(true);
    handleAriaLive("open");
  };

  useEffect(() => {
    state.src !== null && setShowModal(false);
  }, [state.src]);

  // Observer mode
  const authoringData = useContext(TenantContext);
  const viewOnly = !authoringData?.viewOnlyComponents;
  return (
    <>
      <span
        className={styles.srOnly}
        role="status"
        aria-live="assertive"
        aria-relevant="all"
        aria-atomic="true"
      >
        {ariaLive}
      </span>
      {showModal && (
        <Modal
          setShowModal={setShowModal}
          showModal={showModal}
          ariaLive={ariaLive}
          handleAriaLive={handleAriaLive}
          t={t}
        />
      )}
      <div
        data-testid="iFrameComponent"
        className={styles.iFrameContainer}
        ref={iFrameContainerRef}
        style={{
          "--pointer": showModal ? "none" : "auto",
        }}
      >
        {state.src && state.width < windowWidth ? (
          <div
            className={styles.iFrameDisplayContainer}
            aria-hidden={showModal ? "true" : "false"}
          >
            <iframe
              id={iFrameId}
              src={state.src}
              height={state.height}
              width={state.width}
              title={state.title}
              sandbox="allow-forms allow-modals allow-orientation-lock allow-pointer-lock allow-popups allow-popups-to-escape-sandbox allow-presentation allow-same-origin allow-scripts allow-top-navigation allow-top-navigation-by-user-activation"
            />
          </div>
        ) : state.title.length > 0 &&
          state.src &&
          state.width.length > 0 &&
          state.width > windowWidth &&
          state.height.length > 0 ? (
          <div onClick={handleIFrameFocus}>
            <OpenInNewTab t={t} />
          </div>
        ) : (
          <div className={styles.iFrameAddContainer}>
            <Button
              id="addIFrameButton"
              variant="contained"
              className={styles.iFrameAddButton}
              disableRipple
              disableFocusRipple
              onClick={createIFrame}
              disabled={showModal || viewOnly}
            >
              {t("Add iFrame")}
            </Button>
          </div>
        )}
        {!viewOnly && (showSelf || isActiveComponent) && state.src ? (
          <PortalUtil portalRef={toolbarContainerRef}>
            <div className={styles.toolbarWrapper}>
              <Toolbar
                t={t}
                createIFrame={createIFrame}
                showSelf={showSelf}
                isActiveComponent={isActiveComponent}
              />
            </div>
          </PortalUtil>
        ) : null}
      </div>
    </>
  );
};

export default IFrame;
