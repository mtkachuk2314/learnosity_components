import React, { useState, useEffect, useContext, useRef } from "react";
// Context/other components/custom hooks
// MUI Components
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { TextField } from "@mui/material";
// Other libraries
import { IFrameContext } from "../IFrameContext";
// Styles
import styles from "../styles/Modal.module.scss";

const theme = createTheme({
  components: {
    Input: {
      styleOverrides: {
        underline: {
          color: "#1565C0",
        },
      },
      "&:hover": {
        color: "#1565C0",
      },
    },
  },
});

const Modal = ({ setShowModal, showModal, ariaLive, handleAriaLive, t }) => {
  const [state, dispatch] = useContext(IFrameContext);

  const containerRef = useRef(null);

  useEffect(() => {
    containerRef.current.focus();
  }, []);

  const [iFrameWidth, setIFrameWidth] = useState(state.width);
  const [iFrameHeight, setIFrameHeight] = useState(state.height);
  const [iFrameTitle, setIFrameTitle] = useState(state.title);
  const [isTitleValid, setIsTitleValid] = useState(true);
  const [titleErrorMessage, setTitleErrorMessage] = useState("");
  const [urlToValidate, setUrlToValidate] = useState(state.src);
  const [isUrlValid, setIsUrlValid] = useState(true);
  const [urlErrorMessage, setUrlErrorMessage] = useState("");

  const handleSizeChange = (type, e) => {
    if (type === "width") {
      setIFrameWidth(e.target.value);
    } else if (type === "height") {
      setIFrameHeight(e.target.value);
    }
  };

  const handleTitleChange = (e) => {
    if (state.title.length >= 0) {
      setIsTitleValid(true);
      setIFrameTitle(e.target.value);
    }
  };

  const verifyTitleEntered = () => {
    if (iFrameTitle.length > 0) {
      setIsTitleValid(true);
    } else {
      setIsTitleValid(false);
      setTitleErrorMessage(t("Title attribute is required."));
    }
  };

  const verifyTitleAttribute = () => {
    if (iFrameTitle.length) {
      dispatch({ func: "SET_IFRAME_TITLE", title: iFrameTitle });
    } else {
      setIsTitleValid(false);
      setTitleErrorMessage(t("Title attribute is required."));
    }
  };

  const handleLinkChange = (e) => {
    if (urlToValidate.length >= 0) {
      setIsUrlValid(true);
      setUrlToValidate(e.target.value);
    }
  };

  const verifyUrlEntered = () => {
    const linkValidityRegex = new RegExp(
      "^(https:\\/\\/)?" + // validate protocol
        "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // validate domain name
        "((\\d{1,3}\\.){3}\\d{1,3}))" + // validate OR ip (v4) address
        "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // validate port and path
        "(\\?[;&a-z\\d%_.~+=-]*)?" + // validate query string
        "(\\#[-a-z\\d_]*)?$",
      "i"
    ); // validate fragment locator
    if (urlToValidate.length) {
      const isValid = linkValidityRegex.test(urlToValidate);
      if (isValid) {
        setIsUrlValid(true);
        setUrlErrorMessage(null);
        return true;
      }
      setIsUrlValid(false);
      setUrlErrorMessage(t("Invalid URL."));
      return false;
    }
    setIsUrlValid(false);
    setUrlErrorMessage(t("iFrame link is required."));
    return false;
  };

  const handleCloseModal = () => {
    if (state.title && state.src && state.width && state.height) {
      setShowModal(false);
    } else {
      setIFrameWidth(900);
      setIFrameHeight(500);
      dispatch({ func: "SET_IFRAME_TITLE", title: "" });
      setShowModal(false);
    }
    handleAriaLive("close");
  };

  const verifyIFrameSettings = async (e) => {
    e.preventDefault();
    verifyTitleAttribute();
    if (verifyUrlEntered(urlToValidate)) {
      setIsUrlValid(true);
      setUrlErrorMessage(null);
      setShowModal(false);
      dispatch({ func: "SET_IFRAME_SRC", src: urlToValidate });
    }
    // verifyLinkAttribute(urlToValidate);
    if (state.title && state.src) {
      dispatch({ func: "CHANGE_IFRAME_WIDTH", width: iFrameWidth });
      dispatch({ func: "CHANGE_IFRAME_HEIGHT", height: iFrameHeight });
      handleAriaLive("save");
    } else {
      handleAriaLive("add");
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <span
        className={styles.srOnly}
        role="status"
        aria-live="assertive"
        aria-relevant="all"
        aria-atomic="true"
      >
        {ariaLive}
      </span>
      <div
        className={styles.iFrameModalContainer}
        aria-labelledby="iFrameModalLabel"
        aria-modal="true"
        ref={containerRef}
        onKeyDown={(e) => {
          if (e.key === "Escape") {
            setShowModal(false);
          }
        }}
        onBlur={(e) => {
          const relatedTarget = e.relatedTarget || document.activeElement;
          if (!containerRef.current.contains(relatedTarget)) {
            setShowModal(false);
            handleAriaLive("close");
          }
        }}
        tabIndex={0}
        data-testid="iFrameModal"
      >
        <div className={styles.iFrameModalHeader}>
          <span id="iFrameModalLabel">{t("Add iFrame")}</span>
          <button
            id="closeModal"
            className={styles.closeModal}
            onClick={handleCloseModal}
            aria-label={t("Close iFrame modal")}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="14"
              height="14"
              viewBox="0 0 14 14"
              fill="none"
              alt=""
            >
              <path
                d="M1.2248 13.825L0.174805 12.775L5.9498 7.00005L0.174805 1.22505L1.2248 0.175049L6.9998 5.95005L12.7748 0.175049L13.8248 1.22505L8.0498 7.00005L13.8248 12.775L12.7748 13.825L6.9998 8.05005L1.2248 13.825Z"
                fill="#232323"
              />
            </svg>
          </button>
        </div>
        <form
          className={styles.modalMainContainer}
          noValidate
          onSubmit={verifyIFrameSettings}
        >
          <div className={styles.iFrameModalSizeContainer}>
            <label className={styles.labelFlexContainer}>
              {t("Width")}
              <div className={styles.inputFlexContainer}>
                <input
                  className={styles.sizeInput}
                  data-testid="widthInput"
                  type="number"
                  min={1}
                  max={960}
                  required
                  value={iFrameWidth}
                  onChange={(e) => handleSizeChange("width", e)}
                />
                <span>{t("px")}</span>
              </div>
            </label>
            <label className={styles.labelFlexContainer}>
              {t("Height")}
              <div className={styles.inputFlexContainer}>
                <input
                  className={styles.sizeInput}
                  data-testid="heightInput"
                  type="number"
                  min={1}
                  required
                  value={iFrameHeight}
                  onChange={(e) => handleSizeChange("height", e)}
                />
                <span>{t("px")}</span>
              </div>
            </label>
          </div>
          <div className={styles.modalDetailsContainer}>
            <TextField
              id="iFrameTitleInput"
              variant="standard"
              required
              label={t("Type Title attribute")}
              value={iFrameTitle}
              onChange={handleTitleChange}
              onBlur={verifyTitleEntered}
              error={!isTitleValid}
              helperText={
                !isTitleValid && titleErrorMessage
                  ? t(titleErrorMessage)
                  : t("Used for accessibility and responsiveness.")
              }
            />
            <TextField
              id="iFrameLinkInput"
              className={styles.linkInput}
              variant="standard"
              required
              type="url"
              label={t("Paste iFrame link")}
              value={urlToValidate}
              onChange={handleLinkChange}
              onBlur={verifyUrlEntered}
              error={!isUrlValid}
              helperText={
                !isUrlValid && urlErrorMessage
                  ? t(urlErrorMessage)
                  : t("iFrame link is required")
              }
            />
          </div>
          <button
            type="submit"
            id="iFrameAddButton"
            disabled={
              iFrameWidth > 960 ||
              !iFrameWidth ||
              iFrameWidth < 1 ||
              !iFrameHeight ||
              iFrameHeight < 1 ||
              !iFrameTitle ||
              !urlToValidate
            }
            className={styles.addButton}
          >
            {state.src && state.title && state.width && state.height
              ? t("Save")
              : t("Add")}
          </button>
        </form>
      </div>
    </ThemeProvider>
  );
};

export default Modal;
