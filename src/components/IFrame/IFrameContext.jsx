import React, {
  createContext,
  useReducer,
  useEffect,
  useState,
  useMemo,
} from "react";
import produce from "immer";

// state of iFrame data stored in IFrameContext
export const IFrameContext = createContext();

export const iFrameConfig = (draft, action) => {
  switch (action.func) {
    case "UPDATE_STATE":
      return action.data;
    case "SET_IFRAME_SRC":
      draft.src = action.src;
      return draft;
    case "SET_IFRAME_TITLE":
      draft.title = action.title;
      return draft;
    case "CHANGE_IFRAME_WIDTH":
      draft.width = action.width;
      return draft;
    case "CHANGE_IFRAME_HEIGHT":
      draft.height = action.height;
      return draft;
    default:
      return draft;
  }
};

// iFrame provider wraps the iFrame component to access reducer
export const IFrameProvider = ({
  children,
  setProp,
  title,
  src,
  height,
  width,
}) => {
  const [state, dispatch] = useReducer(produce(iFrameConfig), {
    title,
    src,
    height,
    width,
  });
  const diff =
    JSON.stringify(state) !== JSON.stringify({ title, src, height, width });
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    dispatch({ func: "UPDATE_STATE", data: { title, src, height, width } });
    setMounted(true);
  }, []);

  useEffect(() => {
    diff &&
      mounted &&
      setProp({
        title: state.title,
        src: state.src,
        height: state.height,
        width: state.width,
      });
  }, [state]);

  useEffect(() => {
    diff &&
      mounted &&
      dispatch({
        func: "UPDATE_STATE",
        data: {
          title,
          src,
          height,
          width,
        },
      });
  }, [title, src, height, width]);
  return (
    <IFrameContext.Provider value={[state, dispatch]}>
      {children}
    </IFrameContext.Provider>
  );
};
