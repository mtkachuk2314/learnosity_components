import React, { useState, useRef, useContext } from "react";
import { useTranslation } from "react-i18next";
import { AppBar, Card, Toolbar, IconButton, Tooltip } from "@mui/material";
import { MultiColumnContext } from "../MultiColumnContext";

// Components
// Styles
import styles from "../assets/Toolbar.module.scss";
// Icons
import icons from "../assets/icons";

const ToolBar = ({ isActiveComponent, showSelf, isDisabled }) => {
  // Active States
  const [columnLayoutDropdown, setColumnLayoutDropdown] = useState(false);
  // ? State of multicolumn component
  const [state, dispatch] = useContext(MultiColumnContext);
  // Refs
  const columnLayoutRef = useRef(null);
  const columnToolbarRef = useRef();

  // Translation
  const { t } = useTranslation();

  const handleAlignmentSelect = (alignment) => {
    setColumnLayoutDropdown(false);
    dispatch({
      func: "SET_VERTICAL_ALIGNMENT",
      verticalAlignment: alignment,
    });
  };

  return (
    <div
      className={styles.ToolbarContainer}
      style={{
        "--active": isActiveComponent || showSelf ? "block" : "none",
      }}
    >
      <AppBar
        ref={columnToolbarRef}
        position="static"
        className={styles.StyledAppbar}
        elevation={0}
        style={{
          "--display": "flex",
          "--direction": "row",
          "--gap": "10px",
          "--boxShadow": "none !important",
        }}
      >
        <Toolbar
          position="static"
          aria-label={t("Toolbar")}
          className={styles.StyledToolbar}
          style={{
            "--grid-template-columns": "1fr 9px 1fr 1fr 1fr",
            "--justify-items": "center",
            "--boxShadow":
              "0px 0px 3px 0px rgba(35, 35, 35, 0.15),-1px 0px 5px 0px rgba(161, 162, 163, 0.2),3px 3px 4px 0px rgba(35, 35, 35, 0.15)",
          }}
        >
          <Tooltip
            aria-label={t("column settings")}
            title={t(state.columnWidth)}
            placement="top"
            arrow
            style={{ position: "relative" }}
          >
            <IconButton
              ref={columnLayoutRef}
              variant="contained"
              role="button"
              disableRipple
              disableFocusRipple
              className={styles.StyledIconButton}
              style={{
                "--active": columnLayoutDropdown
                  ? "rgba(21, 101, 192, 1)"
                  : "#000",
                "--background": columnLayoutDropdown
                  ? "rgba(21, 101, 192, 0.12)"
                  : "#fff",
              }}
              onClick={(e) => {
                e.stopPropagation(); // prevent the click from bubbling up to the parent
                setColumnLayoutDropdown(!columnLayoutDropdown);
              }}
            >
              {icons[state.columnWidth]}
            </IconButton>
            <LayoutDropdownCard
              aria-label={t("alignment options")}
              columnLayoutDropdown={columnLayoutDropdown}
            />
          </Tooltip>
          <div className={styles.StyledDivider} />
          <Tooltip
            aria-label={t("column settings")}
            title={t("vertical align top")}
            placement="top"
            arrow
          >
            <IconButton
              variant="contained"
              role="button"
              disableRipple
              disableFocusRipple
              className={
                state.verticalAlignment === "top"
                  ? [styles.StyledIconButton, styles.active]
                  : styles.StyledIconButton
              }
              disabled={isDisabled}
              onClick={() => handleAlignmentSelect("top")}
            >
              {icons.vertical_top}
            </IconButton>
          </Tooltip>
          <Tooltip
            aria-label={t("column settings")}
            title={t("vertical align middle")}
            placement="top"
            arrow
          >
            <IconButton
              variant="contained"
              role="button"
              disableRipple
              disableFocusRipple
              className={
                state.verticalAlignment === "center"
                  ? [styles.StyledIconButton, styles.active]
                  : styles.StyledIconButton
              }
              disabled={isDisabled}
              onClick={() => handleAlignmentSelect("center")}
            >
              {icons.vertical_middle}
            </IconButton>
          </Tooltip>
          <Tooltip
            aria-label={t("column settings")}
            title={t("vertical align bottom")}
            placement="top"
            arrow
          >
            <IconButton
              variant="contained"
              role="button"
              disableRipple
              disableFocusRipple
              className={
                state.verticalAlignment === "bottom"
                  ? [styles.StyledIconButton, styles.active]
                  : styles.StyledIconButton
              }
              disabled={isDisabled}
              onClick={() => handleAlignmentSelect("bottom")}
            >
              {icons.vertical_bottom}
            </IconButton>
          </Tooltip>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default ToolBar;

const LayoutDropdownCard = ({ columnLayoutDropdown }) => {
  // State of multicolumn component
  const [state, dispatch] = useContext(MultiColumnContext);

  // Translation
  const { t } = useTranslation();

  return (
    <Card
      elevation={0}
      className={styles.StyledCard}
      style={{
        "--card-display": columnLayoutDropdown ? "flex" : "none",
        "--left": "-0.1875rem",
        "--width": "7rem",
        "--box-shadow":
          "0px 1px 3px 0px rgba(35, 35, 35, 0.1), 0px 2px 5px 0px rgba(161, 162, 163, 0.2), 3px 3px 4px 0px rgba(35, 35, 35, 0.15)",
        top: "2.375rem",
        justifyContent: "space-between",
        padding: "0.1875rem",
      }}
    >
      <Tooltip aria-label={t("50:50")} title={t("50:50")} placement="top" arrow>
        <IconButton
          disableRipple
          value="50:50"
          color="inherit"
          aria-label={t("50:50")}
          onClick={() => {
            dispatch({
              func: "SET_COLUMN_SIZE",
              columnWidth: "50:50",
            });
          }}
          className={styles.StyledIconButton}
          style={{
            "--active":
              state.columnWidth === "50:50" ? "rgba(21, 101, 192, 1)" : "#000",
            "--background":
              state.columnWidth == "50:50"
                ? "rgba(21, 101, 192, 0.12)"
                : "#fff",
          }}
        >
          {icons["50:50"]}
        </IconButton>
      </Tooltip>
      <Tooltip aria-label={t("30:70")} title={t("30:70")} placement="top" arrow>
        <IconButton
          disableRipple
          aria-label={t("30:70")}
          value="30:70"
          onClick={() => {
            if (state.columnWidth === "30:70") {
              dispatch({
                func: "SET_COLUMN_SIZE",
                columnWidth: "50:50",
              });
            } else {
              dispatch({
                func: "SET_COLUMN_SIZE",
                columnWidth: "30:70",
              });
            }
          }}
          className={styles.StyledIconButton}
          style={{
            "--active":
              state.columnWidth === "30:70" ? "rgba(21, 101, 192, 1)" : "#000",
            "--background":
              state.columnWidth == "30:70"
                ? "rgba(21, 101, 192, 0.12)"
                : "#fff",
          }}
        >
          {icons["30:70"]}
        </IconButton>
      </Tooltip>
      <Tooltip aria-label={t("70:30")} title={t("70:30")} placement="top" arrow>
        <IconButton
          disableRipple
          aria-label={t("70:30")}
          value="70:30"
          onClick={() => {
            if (state.columnWidth === "70:30") {
              dispatch({
                func: "SET_COLUMN_SIZE",
                columnWidth: "50:50",
              });
            } else {
              dispatch({
                func: "SET_COLUMN_SIZE",
                columnWidth: "70:30",
              });
            }
          }}
          className={styles.StyledIconButton}
          style={{
            "--active":
              state.columnWidth === "70:30" ? "rgba(21, 101, 192, 1)" : "#000",
            "--background":
              state.columnWidth == "70:30"
                ? "rgba(21, 101, 192, 0.12)"
                : "#fff",
          }}
        >
          {icons["70:30"]}
        </IconButton>
      </Tooltip>
    </Card>
  );
};
