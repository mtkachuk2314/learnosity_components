import React from "react";

//error style message
import ErrorOutlineIcon from "@mui/icons-material/ErrorOutline";

const PlaceholderError = ({ showError }) => {
  return (
    <>
      {showError && (
        <p className="placeholderError">
          <ErrorOutlineIcon sx={{ marginRight: "15px" }} />
          Error: only text and image components are supported.
        </p>
      )}
    </>
  );
};

export default PlaceholderError;