import React, { useState, useEffect, useContext } from "react";
import { useDrop } from "react-dnd";
import { MultiColumnContext } from "../MultiColumnContext";

// ? Components
import PlaceHolder from "../../../Utility/Placeholder";
import NestedComponentWrapper from "../../../Utility/NestedComponentWrapper";
import { MainInsideWrapperMultiColumn } from "../../../theme/styledComponents/globalComponent";
import ComponentWithFormat from "../../../authoring/components/ComponentWithFormat";

const MultiColumnItem = ({
  multiColumn,
  multiColumnIndex,
  removeError,
  setRemoveError,
  setShowDropError,
  setHoverMenu,
  setActiveComponent,
}) => {
  const { components } = multiColumn;
  const [activeComp, setActiveComp] = useState(null);
  const [state, dispatch] = useContext(MultiColumnContext);
  const [isDragging, setIsDragging] = useState(false);

  const [droppedIndex, setDroppedIndex] = useState(null);
  const [inContainer, setInContainer] = useState(null);

  // List of accepted componenets that can be placed in MultiColumn
  const acceptListComp = (item) => {
    return ["Text", "Image"].indexOf(item.componentName) >= 0;
  };

  // ? Error Message
  const [showError, setShowError] = useState();

  const [{ isOver, getItem }, drop] = useDrop(
    () => ({
      accept: [
        "Text",
        "Image",
        "Video",
        "Table",
        "InfoBox",
        "QuoteBox",
        "IFrame",
        "Accordion",
        "Tab",
        "section",
        "MultiColumn",
        "Header",
        "Reveal",
        "MultipleChoice",
      ],
      drop: async (item, monitor) => {
        if (!acceptListComp(item) && components.length !== 0)
          setShowDropError(true);
        if (item.within && components.length !== 0) return;
        if (monitor.didDrop()) return;
        if (!monitor.isOver({ shallow: true })) return;
        if (acceptListComp(item)) {
          dispatch({
            func: "ADD_COMPONENT",
            tabIndex: multiColumnIndex,
            component: {
              componentName: item.componentName,
              componentProps: JSON.parse(item?.componentProps),
              formats: item.formats,
            },
          });
          item?.delete && item?.delete();
        }
      },
      collect: (monitor) => ({
        isOver: !!monitor.isOver(),
        getItem: monitor.getItem(),
      }),
    }),
    [components]
  );

  // Adjust item name for better viewing
  const trimCap = (item) => {
    switch (item) {
      case "IFrame":
        return "iFrame";
      case "InfoBox":
        return "InfoBox";
      case "Tab":
        return "Tabs";
      case "NONLEARNING":
        return "Descriptive Container";
      case "LEARNING":
        return "Learning Container";
      default:
        return item.replace(/([A-Z])/g, " $1").trim();
    }
  };

  const classNameActiveDropDownItem = (activeDropDownItem) => {
    switch (activeDropDownItem) {
      case "30:70":
        return "thirtySeventy";
      case "70:30":
        return "seventyThirty";
      default:
        return "fiftyFifty";
    }
  };

  useEffect(() => {
    if (isOver && !acceptListComp(getItem)) {
      setShowError(trimCap(getItem.componentName || getItem.type));
    } else if (isOver) {
      setShowError();
      setShowDropError(false);
      setIsDragging(true);
    } else {
      setIsDragging(false);
    }
  }, [isOver]);

  useEffect(() => {
    setShowError();
    setRemoveError(false);
  }, [removeError]);

  useEffect(() => {
    setShowDropError(false);
  }, [components.length === 0]);

  return (
    <div
      className={classNameActiveDropDownItem(state.columnWidth)}
      data-testid="multi-column-dropzone"
      ref={drop}
      onDragLeave={() => setInContainer(false)}
      onDragOver={() => setInContainer(true)}
      role="document"
    >
      <MainInsideWrapperMultiColumn>
        {components.length !== 0 ? (
          components.map((component, compIndex) => {
            return (
              <ComponentWithFormat
                key={`key-component-${compIndex}`}
                formats={component.formats}
                componentName={component.componentName}
              >
                <NestedComponentWrapper
                  key={`key-component-${compIndex}`}
                  componentType="multi-column"
                  numOfComponent={components.length}
                  componentProps={component.componentProps}
                  component={component}
                  compIndex={compIndex}
                  formats={component.formats}
                  tabIndex={multiColumnIndex}
                  inContainer={inContainer}
                  setDroppedIndex={setDroppedIndex}
                  setActiveComp={setActiveComp}
                  activeComp={activeComp}
                  draggingOver={isOver}
                  multiColumn
                  setShowError={setShowError}
                  setShowDropError={setShowDropError}
                  setHoverMenu={setHoverMenu}
                  setActiveComponent={setActiveComponent}
                />
              </ComponentWithFormat>
            );
          })
        ) : (
          <PlaceHolder
            isOver={isOver}
            showError={showError}
            components={components}
            placeholderType="multiColumn"
          />
        )}
      </MainInsideWrapperMultiColumn>
    </div>
  );
};

export default MultiColumnItem;
