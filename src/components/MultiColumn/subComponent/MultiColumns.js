import React, { useContext, useEffect, useState } from "react";
// styles/emotion
import styled from "@emotion/styled";
import { MultiColumnContext } from "../MultiColumnContext";
import TenantContext from "../../../Context/TenantContext";
import MultiColumnItem from "./MultiColumnItem";

import ToolBar from "./Toolbar";

import "../assets/styles.scss";
// portal
import PortalUtil from "../../../Utility/PortalUtil";

const StyledConfigBar = styled("div")(({ viewOnly }) => ({
  display: viewOnly && "none",
}));

const MultiColumns = ({
  setShowDropError,
  setHoverMenu,
  isActiveComponent,
  showSelf,
  toolbarContainerRef,
  setActiveComponent,
}) => {
  const [state] = useContext(MultiColumnContext);
  const [isDisabled, setIsDisabled] = useState(false);
  const [removeError, setRemoveError] = useState(false);
  const [activeDropDownItem, setActiveDropDownItem] = useState("50:50");

  // Observer mode
  const authoringData = useContext(TenantContext);
  const viewOnly = !authoringData?.viewOnlyComponents;

  for (let i = 0; i < 1; i++) {
    useEffect(() => {
      if (state.layoutState[i].components.length > 0) {
        setIsDisabled(false);
      } else {
        setIsDisabled(true);
      }
    }, [state.layoutState[i].components.length]);
  }

  return (
    <div
      data-testid="multi-column-component"
      role="document"
      aria-label="Multi-column component"
      tabIndex={!viewOnly ? "0" : null}
      style={{
        outline: "none",
      }}
    >
      {!viewOnly && (isActiveComponent || showSelf) ? (
        <PortalUtil portalRef={toolbarContainerRef}>
          <StyledConfigBar viewOnly={viewOnly}>
            <ToolBar
              isActiveComponent={isActiveComponent}
              showSelf={showSelf}
              activeDropDownItem={activeDropDownItem}
              setActiveDropDownItem={setActiveDropDownItem}
              isDisabled={isDisabled}
            />
          </StyledConfigBar>
        </PortalUtil>
      ) : null}
      <div
        className="multiColumnContainer"
        style={{
          "--alignment":
            state.verticalAlignment === "top"
              ? "flex-start"
              : state.verticalAlignment === "center"
              ? "center"
              : state.verticalAlignment === "bottom"
              ? "flex-end"
              : null,
        }}
      >
        {state.layoutState.map((multiColumn, multiColumnIndex) => {
          return (
            <MultiColumnItem
              setShowDropError={setShowDropError}
              multiColumnIndex={multiColumnIndex}
              multiColumn={multiColumn}
              removeError={removeError}
              setRemoveError={setRemoveError}
              activeDropDownItem={activeDropDownItem}
              setHoverMenu={setHoverMenu}
              setActiveComponent={setActiveComponent}
            />
          );
        })}
      </div>
    </div>
  );
};

export default MultiColumns;
