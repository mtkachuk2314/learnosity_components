import React from "react";
import ReactQuill from "react-quill";

const icons = ReactQuill.Quill.import("ui/icons");

icons["50:50"] = (
  <svg
    width="39"
    height="34"
    viewBox="0 0 39 34"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M4 8C4 6.89543 4.89543 6 6 6H17V8H6H4ZM17 28H6C4.89543 28 4 27.1046 4 26H6H17V28ZM6 28C4.89543 28 4 27.1046 4 26V8C4 6.89543 4.89543 6 6 6V8V26V28ZM17 7V27V7Z"
      fill="black"
    />
    <path
      d="M22 6H33C34.1046 6 35 6.89543 35 8H33H22V6ZM35 26C35 27.1046 34.1046 28 33 28H22V26H33H35ZM22 27V7V27ZM33 6C34.1046 6 35 6.89543 35 8V26C35 27.1046 34.1046 28 33 28V26V8V6Z"
      fill="black"
    />
    <line
      x1="19.5"
      y1="5.5"
      x2="19.5"
      y2="28.5"
      stroke="black"
      stroke-linecap="round"
    />
  </svg>
);

icons["30:70"] = (
  <svg
    width="39"
    height="34"
    viewBox="0 0 39 34"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M4 8C4 6.89543 4.89543 6 6 6H12.2V8H6H4ZM12.2 28H6C4.89543 28 4 27.1046 4 26H6H12.2V28ZM6 28C4.89543 28 4 27.1046 4 26V8C4 6.89543 4.89543 6 6 6V8V26V28ZM12.2 7V27V7Z"
      fill="black"
      mask="url(#path-1-inside-1_6401_51350)"
    />
    <path
      d="M17 6H32.8C33.9046 6 34.8 6.89543 34.8 8H32.8H17V6ZM34.8 26C34.8 27.1046 33.9046 28 32.8 28H17V26H32.8H34.8ZM17 27V7V27ZM32.8 6C33.9046 6 34.8 6.89543 34.8 8V26C34.8 27.1046 33.9046 28 32.8 28V26V8V6Z"
      fill="black"
      mask="url(#path-3-inside-2_6401_51350)"
    />
    <line
      x1="14.5"
      y1="5.5"
      x2="14.5"
      y2="28.5"
      stroke="black"
      stroke-linecap="round"
    />
  </svg>
);
icons["70:30"] = (
  <svg
    width="39"
    height="35"
    viewBox="0 0 39 35"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M34.9999 27C34.9999 28.1046 34.1044 29 32.9999 29L27.9999 29L27.9999 27L32.9999 27L34.9999 27ZM27.9999 7L32.9999 7C34.1044 7 34.9999 7.89543 34.9999 9L32.9999 9L27.9999 9L27.9999 7ZM32.9999 7C34.1044 7 34.9999 7.89543 34.9999 9L34.9999 27C34.9999 28.1046 34.1044 29 32.9999 29L32.9999 27L32.9999 9L32.9999 7ZM27.9999 28L27.9999 8L27.9999 28Z"
      fill="black"
      mask="url(#path-1-inside-1_6371_52171)"
    />
    <path
      d="M23 29L6 29C4.89543 29 4 28.1046 4 27L6 27L23 27L23 29ZM4 9C4 7.89543 4.89543 7 6 7L23 7L23 9L6 9L4 9ZM23 8L23 28L23 8ZM6 29C4.89543 29 4 28.1046 4 27L4 9C4 7.89543 4.89543 7 6 7L6 9L6 27L6 29Z"
      fill="black"
      mask="url(#path-3-inside-2_6371_52171)"
    />
    <line
      x1="25.4999"
      y1="29.5"
      x2="25.4999"
      y2="5.5"
      stroke="black"
      stroke-linecap="round"
    />
  </svg>
);

icons["vertical_top"] = (
  <svg
    width="16"
    height="18"
    viewBox="0 0 16 18"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M7.24988 17.95V5.85L4.29988 8.8L3.24988 7.75L7.99988 3L12.7499 7.75L11.6999 8.8L8.74988 5.85V17.95H7.24988ZM-0.00012207 1.5V0H15.9999V1.5H-0.00012207Z"
      fill="#232323"
    />
  </svg>
);

icons["vertical_middle"] = (
  <svg
    width="16"
    height="20"
    viewBox="0 0 16 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M7.24988 20V14.95L5.09988 17.1L3.99988 16L7.99988 12L11.9999 16L10.8999 17.1L8.74988 14.95V20H7.24988ZM-0.00012207 10.75V9.25H15.9999V10.75H-0.00012207ZM7.99988 8L3.99988 4L5.09988 2.9L7.24988 5.05V0H8.74988V5.05L10.8999 2.9L11.9999 4L7.99988 8Z"
      fill="#232323"
    />
  </svg>
);

icons["vertical_bottom"] = (
  <svg
    width="16"
    height="18"
    viewBox="0 0 16 18"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M-0.00012207 18V16.5H15.9999V18H-0.00012207ZM7.99988 15L3.14988 10.15L4.22488 9.075L7.24988 12.1V0H8.74988V12.1L11.6749 9.175L12.7499 10.25L7.99988 15Z"
      fill="#232323"
    />
  </svg>
);

export default icons;
