import React, {
  createContext,
  useReducer,
  useEffect,
  useState,
  useMemo,
} from "react";
import produce from "immer";

// state of iFrame data stored in IFrameContext
export const MultiColumnContext = createContext();

export const multiColumnConfig = (draft, action) => {
  switch (action.func) {
    case "UPDATE_STATE":
      return action.data;
    case "DRAG_COMPONENT":
      // eslint-disable-next-line no-case-declarations
      const dragElement =
        draft.layoutState[action.tabIndex].components[action.dragIndex];
      draft.layoutState[action.tabIndex].components.splice(action.dragIndex, 1);
      draft.layoutState[action.tabIndex].components.splice(
        action.hoverIndex,
        0,
        dragElement
      );
      return draft;
    case "DRAG_ADD_NEW_COMPONENT":
      draft.layoutState[action.tabIndex].components.splice(
        action.hoverIndex,
        0,
        action.component
      );
      return draft;
    case "DUPLICATE_COMPONENT":
      draft.layoutState[action.tabIndex].components.splice(
        action.compIndex + 1,
        0,
        draft.layoutState[action.tabIndex].components[action.compIndex]
      );
      return draft;
    case "MOVE_COMPONENT_DOWN":
      // eslint-disable-next-line no-case-declarations
      const elementCR =
        draft.layoutState[action.tabIndex].components[action.compIndex];
      draft.layoutState[action.tabIndex].components.splice(action.compIndex, 1);
      draft.layoutState[action.tabIndex].components.splice(
        action.compIndex + 1,
        0,
        elementCR
      );
      return draft;
    case "MOVE_COMPONENT_UP":
      // eslint-disable-next-line no-case-declarations
      const elementCL =
        draft.layoutState[action.tabIndex].components[action.compIndex];
      draft.layoutState[action.tabIndex].components.splice(action.compIndex, 1);
      draft.layoutState[action.tabIndex].components.splice(
        action.compIndex - 1,
        0,
        elementCL
      );
      return draft;
    case "UPDATE_COMPONENT":
      draft.layoutState[action.tabIndex].components[
        action.compIndex
      ].componentProps = {
        ...action.stateUpdate,
      };
      return draft;
    case "DELETE_COMPONENT":
      draft.layoutState[action.tabIndex].components.splice(action.compIndex, 1);
      return draft;
    case "SET_COLUMN_SIZE":
      draft.columnWidth = action.columnWidth;
      return draft;
    case "SET_VERTICAL_ALIGNMENT":
      draft.verticalAlignment = action.verticalAlignment;
      return draft;
    case "ADD_COMPONENT":
      draft.layoutState[action.tabIndex].components.push({
        ...action.component,
      });
      return draft;
    default:
      return draft;
  }
};

// MultiColumn provider wraps the iFrame component to access reducer
export const MultiColumnProvider = ({
  children,
  setProp,
  columnWidth,
  verticalAlignment,
  layoutState,
}) => {
  const [state, dispatch] = useReducer(produce(multiColumnConfig), {
    columnWidth,
    verticalAlignment,
    layoutState,
  });
  const diff =
    JSON.stringify(state) !==
    JSON.stringify({ columnWidth, verticalAlignment, layoutState });
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    dispatch({
      func: "UPDATE_STATE",
      data: { columnWidth, verticalAlignment, layoutState },
    });
    setMounted(true);
  }, []);

  useEffect(() => {
    if (diff && mounted) {
      setProp({
        columnWidth: state.columnWidth,
        verticalAlignment: state.verticalAlignment,
        layoutState: state.layoutState,
      });
      dispatch({
        func: "SET_COLUMN_SIZE",
        columnWidth: state.columnWidth,
      });
      dispatch({
        func: "SET_VERTICAL_ALIGNMENT",
        verticalAlignment: state.verticalAlignment,
      });
    }
  }, [state]);

  useEffect(() => {
    diff &&
      mounted &&
      dispatch({
        func: "UPDATE_STATE",
        data: { columnWidth, verticalAlignment, layoutState },
      });
  }, [columnWidth, verticalAlignment, layoutState]);

  // usememo --> prevent passing a new reference to object or array as value in every time the provider component re-renders
  const multiColumnValue = useMemo(() => [state, dispatch], [state]);

  return (
    <MultiColumnContext.Provider value={multiColumnValue}>
      {children}
    </MultiColumnContext.Provider>
  );
};
