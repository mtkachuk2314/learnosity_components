import React, { useState } from "react";
import PropTypes from "prop-types";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import { v4 as uuidv4 } from "uuid";
import { MultiColumnProvider } from "./MultiColumnContext";
import MultiColumns from "./subComponent/MultiColumns";
import PlaceholderError from "./subComponent/PlaceholderError";
import { MainWrapper } from "../../theme/styledComponents/globalComponent";

// MultiColumn default props
export const defaultProps = {
  columnWidth: "50:50",
  verticalAlignment: "top",
  layoutState: [
    {
      id: uuidv4(),
      components: [],
    },
    {
      id: uuidv4(),
      components: [],
    },
  ],
};

const MultiColumnMain = ({
  columnWidth = "50:50",
  verticalAlignment = "top",
  layoutState = [],
  setProp = () => {},
  toolbarContainerRef,
  // showSelf is not being passed to interactive component - keep it just for consistency
  showSelf = null,
  isActiveComponent = null,
  setHoverMenu = () => {},
  setActiveComponent = () => {},
}) => {
  const [showDropError, setShowDropError] = useState(false);
  return (
    <DndProvider backend={HTML5Backend}>
      <MultiColumnProvider
        layoutState={layoutState}
        columnWidth={columnWidth}
        verticalAlignment={verticalAlignment}
        setProp={setProp}
      >
        <MainWrapper>
          <MultiColumns
            setShowDropError={setShowDropError}
            toolbarContainerRef={toolbarContainerRef}
            showSelf={showSelf}
            isActiveComponent={isActiveComponent}
            setHoverMenu={setHoverMenu}
            setActiveComponent={setActiveComponent}
          />
        </MainWrapper>
        <PlaceholderError showError={showDropError} />
      </MultiColumnProvider>
    </DndProvider>
  );
};

MultiColumnMain.propTypes = {
  columnWidth: PropTypes.string,
  verticalAlignment: PropTypes.string,
  layoutState: PropTypes.array.isRequired,
  setProp: PropTypes.func.isRequired,
};

export default MultiColumnMain;
