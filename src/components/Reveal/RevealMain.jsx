import React from "react";
import PropTypes from "prop-types";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import { v4 as uuidv4 } from "uuid";
import { LayoutProvider } from "../../Context/InteractivesContext";
import Reveal from "./subComponent/Reveal";
import { MainWrapper } from "../../theme/styledComponents/globalComponent";
// Reveal default props
export const defaultProps = {
  layoutState: [
    {
      id: uuidv4(),
      title: "",
      closedTitle: "",
      components: [],
      expanded: true,
    },
  ],
};

const RevealMain = ({
  layoutState = [],
  setProp = () => {},
  setHoverMenu = () => {},
  setActiveComponent = () => {},
}) => {
  return (
    <DndProvider backend={HTML5Backend}>
      <LayoutProvider layoutState={layoutState} setProp={setProp}>
        <MainWrapper>
          <Reveal
            setHoverMenu={setHoverMenu}
            setActiveComponent={setActiveComponent}
          />
        </MainWrapper>
      </LayoutProvider>
    </DndProvider>
  );
};

RevealMain.propTypes = {
  layoutState: PropTypes.arrayOf().isRequired,
  setProp: PropTypes.func.isRequired,
};

export default RevealMain;
