import React, { useContext, useEffect } from "react";
import PropTypes from "prop-types";
import { v4 as uuidv4 } from "uuid";
import { AccordionSummary } from "@mui/material";
import { ExpandMore } from "@mui/icons-material";
import TenantContext from "../../../Context/TenantContext";
import { LayoutContext } from "../../../Context/InteractivesContext";
import RevealTitle from "./RevealTitle";
import "../assets/RevealPane.scss";

const Pane = ({ revealIndex, removeError, setRemoveError }) => {
  const [state, dispatch] = useContext(LayoutContext);
  const authoringData = useContext(TenantContext);

  const viewOnly = !authoringData?.viewOnlyComponents;
  useEffect(() => {
    setRemoveError(false);
  }, [removeError, setRemoveError]);
  return (
    <div className="StyledPaneContainer">
      <AccordionSummary
        className={`reveal_summary reveal_summary_active ${
          state.at(revealIndex).expanded
            ? "border_radius_expand"
            : "border_radius"
        }`}
        // id attribute below creates an "aria-labelledby" and is REQUIRED for accessibilty.
        id={`panel-${revealIndex + 1}-add-components-${uuidv4()}`}
        aria-label={`Toggle panel ${revealIndex + 1}`}
        data-testid={`pane-${revealIndex}`}
        expanded={state.at(revealIndex).expanded}
        disabled={viewOnly}
        expandIcon={
          <ExpandMore
            onClick={() => {
              dispatch({
                func: "TOGGLE_PANE",
                paneIndex: revealIndex,
              });
            }}
            tabIndex={0}
            onKeyDown={(event) => {
              if (event.key === "Enter") {
                dispatch({
                  func: "TOGGLE_PANE",
                  paneIndex: revealIndex,
                });
              }
            }}
            sx={{
              pointerEvents: "auto",
              color: "#000",
            }}
          />
        }
      >
        <div className="reveal_summary_content">
          <RevealTitle
            key={`Reveal-title-${revealIndex}`}
            revealIndex={revealIndex}
            viewOnly={viewOnly}
          />
        </div>
      </AccordionSummary>
    </div>
  );
};

Pane.propTypes = {
  revealIndex: PropTypes.number.isRequired,
  reveal: PropTypes.shape({
    title: PropTypes.string,
    closedTitle: PropTypes.string,
    activeTitle: PropTypes.string,
    inActiveTitle: PropTypes.string,
  }).isRequired,
  removeError: PropTypes.bool.isRequired,
  setRemoveError: PropTypes.func.isRequired,
};

export default Pane;
