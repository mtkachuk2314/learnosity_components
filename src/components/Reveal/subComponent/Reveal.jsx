import React, { useContext, useState } from "react";
import PropTypes from "prop-types";
import { Accordion } from "@mui/material";
import { LayoutContext } from "../../../Context/InteractivesContext";
import "../assets/Reveal.scss";
import Pane from "./Pane";
import RevealItem from "./RevealItem";

// Styled components end

const Reveal = ({ expanded, setHoverMenu, setActiveComponent }) => {
  const [state] = useContext(LayoutContext);

  const [removeError, setRemoveError] = useState(false);

  // TODO: If needed, move to useEffect
  // if (
  //   state.map((ele) => {
  //     if (ele.activeTitle || ele.inActiveTitle) {
  //       dispatch({
  //         func: "DELETE_REVEAL_PLACEHOLDERS",
  //       });
  //     }
  //   })
  // )
  return (
    <div className="StyledRevealContainer" data-testid="reveal-component">
      <Accordion
        className={`StyledReveal ${
          expanded ? "borderRadius_expanded" : "borderRadius_notexpanded"
        }`}
        revealIndex={0}
        disableGutters
        expanded={state[0].expanded}
      >
        <Pane
          revealIndex={0}
          removeError={removeError}
          setRemoveError={setRemoveError}
          expanded={state[0].expanded}
        />
        <RevealItem
          revealIndex={0}
          removeError={removeError}
          setRemoveError={setRemoveError}
          setHoverMenu={setHoverMenu}
          setActiveComponent={setActiveComponent}
        />
      </Accordion>
    </div>
  );
};
Reveal.propTypes = {
  expanded: PropTypes.bool.isRequired,
};
export default Reveal;
