import React, { useState, useEffect, useContext } from "react";
import PropTypes from "prop-types";
import { useDrop } from "react-dnd";
import { AccordionDetails } from "@mui/material";
import styled from "@emotion/styled";
import { LayoutContext } from "../../../Context/InteractivesContext";

// ? Components
import NestedComponentWrapper from "../../../Utility/NestedComponentWrapper";
import { MainInsideWrapper } from "../../../theme/styledComponents/globalComponent";
import Placeholder from "../../../Utility/Placeholder";
import PlaceholderError from "../../../Utility/PlaceholderError";

const StyledRevealDetails = styled(AccordionDetails)(
  ({ isOver, showError, empty }) => ({
    backgroundColor:
      showError && empty
        ? "rgba(211, 47, 47, 0.04)"
        : isOver && empty
        ? "rgba(21, 101, 192, 0.04)"
        : "#ffffff",
    padding: "10px",
    borderStyle: "solid",
    borderColor: "#BDBDBD",
    borderWidth: "0 0.0625rem 0.0625rem 0.0625rem",
    minHeight: "8.125rem",
  })
);

const RevealItem = ({
  revealIndex,
  removeError,
  setRemoveError,
  setHoverMenu,
  setActiveComponent,
}) => {
  const [activeComp, setActiveComp] = useState(null);
  const [state, dispatch] = useContext(LayoutContext);
  const [, setIsDragging] = useState(false);

  const [, setDroppedIndex] = useState(null);
  const [inContainer, setInContainer] = useState(null);

  // List of accepted into tab componenets
  const acceptListComp = (item) => {
    return (
      ["Text", "Table", "Video", "Image", "Header"].indexOf(
        item.componentName
      ) >= 0
    );
  };

  // ? Error Message
  const [showError, setShowError] = useState();
  const [showDropError, setShowDropError] = useState();

  const [{ isOver, getItem }, drop] = useDrop(
    () => ({
      accept: [
        "Text",
        "Image",
        "Video",
        "Table",
        "InfoBox",
        "Header",
        "QuoteBox",
        "IFrame",
        "Accordion",
        "Tab",
        "section",
        "Reveal",
        "MultiColumn",
        "MultipleChoice",
      ],
      drop: async (item, monitor) => {
        if (
          !acceptListComp(item) &&
          state.at(revealIndex).components.length !== 0
        )
          setShowDropError(true);
        if (item.within && state.at(revealIndex).components.length !== 0)
          return;
        if (monitor.didDrop()) return;
        if (!monitor.isOver({ shallow: true })) return;
        if (acceptListComp(item)) {
          dispatch({
            func: "ADD_COMPONENT",
            tabIndex: revealIndex,
            component: {
              componentName: item.componentName,
              componentProps: JSON.parse(item?.componentProps),
              formats: item.formats,
            },
          });
          item?.delete && item?.delete();
        }
      },
      collect: (monitor) => ({
        isOver: !!monitor.isOver(),
        getItem: monitor.getItem(),
      }),
    }),
    [state.at(revealIndex).components]
  );

  // Adjust item name for better viewing
  const trimCap = (item) => {
    switch (item) {
      case "IFrame":
        return "iFrame";
      case "InfoBox":
        return "InfoBox";
      case "Tab":
        return "Tabs";
      case "NONLEARNING":
        return "Descriptive Container";
      case "LEARNING":
        return "Learning Container";
      default:
        return item.replace(/([A-Z])/g, " $1").trim();
    }
  };

  useEffect(() => {
    if (isOver && !acceptListComp(getItem)) {
      setShowError(trimCap(getItem.componentName || getItem.type));
    } else if (isOver) {
      setShowError();
      setShowDropError(false);
      setIsDragging(true);
    } else {
      setIsDragging(false);
    }
  }, [isOver]);

  useEffect(() => {
    setShowError();
    setRemoveError(false);
  }, [removeError, setRemoveError, setShowError]);

  return (
    <StyledRevealDetails
      data-testid="reveal-dropzone"
      isOver={isOver}
      showError={showError}
      empty={state.at(revealIndex).components.length === 0}
      ref={drop}
      onDragLeave={() => setInContainer(false)}
      onDragOver={() => setInContainer(true)}
      role="document"
    >
      {state.at(revealIndex).components.length !== 0 ? (
        state.at(revealIndex).components.map((component, compIndex) => {
          if (compIndex !== 0) {
            return (
              <NestedComponentWrapper
                key={`key-component-${compIndex}`}
                componentType="reveal"
                numOfComponent={state.at(revealIndex).components.length}
                componentProps={component.componentProps}
                component={component}
                compIndex={compIndex}
                formats={component.formats}
                tabIndex={revealIndex}
                inContainer={inContainer}
                setDroppedIndex={setDroppedIndex}
                setActiveComp={setActiveComp}
                activeComp={activeComp}
                draggingOver={isOver}
                setShowError={setShowError}
                setShowDropError={setShowDropError}
                setHoverMenu={setHoverMenu}
                setActiveComponent={setActiveComponent}
              />
            );
          }
          return (
            <MainInsideWrapper key={`key-component-${compIndex}`}>
              <NestedComponentWrapper
                componentType="reveal"
                numOfComponent={state.at(revealIndex).components.length}
                componentProps={component.componentProps}
                component={component}
                compIndex={compIndex}
                formats={component.formats}
                tabIndex={revealIndex}
                inContainer={inContainer}
                setDroppedIndex={setDroppedIndex}
                setActiveComp={setActiveComp}
                activeComp={activeComp}
                draggingOver={isOver}
                setShowError={setShowError}
                setShowDropError={setShowDropError}
                setHoverMenu={setHoverMenu}
                setActiveComponent={setActiveComponent}
              />
            </MainInsideWrapper>
          );
        })
      ) : (
        <Placeholder isOver={isOver} showError={showError} />
      )}
      {showDropError && state.at(revealIndex).components.length !== 0 && (
        <PlaceholderError errorType="addComponent" />
      )}
    </StyledRevealDetails>
  );
};
RevealItem.propTypes = {
  revealIndex: PropTypes.number.isRequired,
  removeError: PropTypes.bool.isRequired,
  setRemoveError: PropTypes.func.isRequired,
};

export default RevealItem;
