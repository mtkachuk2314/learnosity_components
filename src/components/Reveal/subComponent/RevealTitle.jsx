import React, { useContext, useCallback, useEffect, useReducer } from "react";
import PropTypes from "prop-types";
import { TextareaAutosize } from "@material-ui/core";
import produce from "immer";
// Localization
import { useTranslation } from "react-i18next";
import { LayoutContext } from "../../../Context/InteractivesContext";
import "../assets/RevealTitle.scss";
import useDebounce from "../../../hooks/useDebounce";

const titleReducer = (draft, action) => {
  switch (action.func) {
    case "CHANGE_TITLE":
      draft.title = action.value;
      return draft;
    case "CHANGE_CLOSED_TITLE":
      draft.closeTitle = action.value;
      return draft;
    default:
      return draft;
  }
};

const RevealTitle = ({ revealIndex, viewOnly }) => {
  const [state, dispatch] = useContext(LayoutContext);
  const [titleValue, dispatcher] = useReducer(produce(titleReducer), {
    title: state.at(revealIndex).title,
    closeTitle: state.at(revealIndex).closedTitle,
  });

  const { t } = useTranslation();

  const updateTitleState = (titleVal) => {
    if (state.at(revealIndex).expanded) {
      dispatch({
        func: "CHANGE_TITLE",
        title: titleVal,
        layerIndex: revealIndex,
      });
    } else {
      dispatch({
        func: "CHANGE_CLOSED_TITLE",
        closedTitle: titleVal,
        layerIndex: revealIndex,
      });
    }
  };

  const handleDebounce = useDebounce(updateTitleState);

  const handleTitleChange = useCallback(
    (e) => {
      const title = e.target.value;
      if (state.at(revealIndex).expanded)
        dispatcher({ func: "CHANGE_TITLE", value: title });
      else dispatcher({ func: "CHANGE_CLOSED_TITLE", value: title });
      handleDebounce(title);
    },
    [handleDebounce, state, revealIndex]
  );

  useEffect(() => {
    if (state.at(revealIndex).expanded)
      dispatcher({ func: "CHANGE_TITLE", value: state.at(revealIndex).title });
    else
      dispatcher({
        func: "CHANGE_CLOSED_TITLE",
        value: state.at(revealIndex).closedTitle,
      });
  }, [
    state.at(revealIndex).closedTitle,
    state.at(revealIndex).title,
    revealIndex,
  ]);

  return (
    <>
      {state.at(revealIndex).expanded && (
        // Expanded &
        <TextareaAutosize
          className={`styled-reveal-title ${
            viewOnly ? "view-only" : ""
          } active `}
          placeholder={t("Close Pane")}
          aria-label={t("Reveal open title input")}
          aria-multiline="true"
          minRows="1"
          maxRows="2"
          onChange={handleTitleChange}
          readOnly={viewOnly}
          viewOnly={viewOnly}
          value={titleValue.title}
          onKeyDown={(e) => e.key === "Enter" && e.preventDefault()} // Prevent new line break
        />
      )}
      {!state.at(revealIndex).expanded && (
        <TextareaAutosize
          className={`styled-reveal-title ${
            viewOnly ? "view-only" : ""
          } active`}
          placeholder={t("Open Pane")}
          aria-label={t("Reveal closed title input")}
          aria-multiline="true"
          minRows="1"
          maxRows="2"
          onChange={handleTitleChange}
          viewOnly={viewOnly}
          value={titleValue.closeTitle}
          onKeyDown={(e) => e.key === "Enter" && e.preventDefault()} // Prevent new line break
        />
      )}
    </>
  );
};
RevealTitle.propTypes = {
  revealIndex: PropTypes.number.isRequired,
  viewOnly: PropTypes.bool.isRequired,
};
export default RevealTitle;
