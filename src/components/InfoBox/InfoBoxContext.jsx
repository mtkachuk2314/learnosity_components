import React, {
  createContext,
  useReducer,
  useEffect,
  useState,
  useMemo,
  useContext,
} from "react";
import produce from "immer";

// state of infoBox data stored in InfoBoxContext
export const InfoBoxContext = createContext();

export const infoBoxConfig = (draft, action) => {
  // TODO: Put this inside the reducer that uses it
  const version1Infobox = {
    ...draft,
    components: [
      {
        componentName: "Text",
        id: action.id,
        componentProps: {
          body: action.bodyText,
        },
      },
    ],
  };

  // TODO: Put this inside the reducer that uses it
  let hideHeaderDraft;
  if (
    typeof draft === "undefined" ||
    typeof draft.infoBoxSettings === "undefined"
  ) {
    hideHeaderDraft = {
      ...draft,
      infoBoxSettings: {
        header: action.infoHeaderSetting,
        label: action.infoLabelSetting,
      },
    };
  } else {
    hideHeaderDraft = {
      ...draft,
      infoBoxSettings: {
        header: action.infoHeaderSetting,
        label: action.infoLabelSetting,
      },
    };
  }

  let hideLabelDraft;
  if (
    typeof draft === "undefined" ||
    typeof draft.infoBoxSettings === "undefined"
  ) {
    hideLabelDraft = {
      ...draft,
      infoBoxSettings: {
        header: action.infoHeaderSetting,
        label: action.infoLabelSetting,
      },
    };
  } else {
    hideLabelDraft = {
      ...draft,
      infoBoxSettings: {
        header: action.infoHeaderSetting,
        label: action.infoLabelSetting,
      },
    };
  }

  switch (action.func) {
    case "UPDATE_STATE":
      return action.data;
    case "VERSION2_INFOBOX":
      return version1Infobox;
    case "CHANGE_BODY":
      draft.body = action.body;
      return draft;
    case "CHANGE_LABEL":
      draft.infoBoxLabel = action.label;
      return draft;
    case "CHANGE_HEADER":
      draft.infoBoxHeader = { heading: action.header, headingLevel: "H3" };
      return draft;
    case "CHANGE_ICON":
      draft.infoBoxIcon = action.icon;
      return draft;
    case "CHANGE_INFOBOX_HEADER_SETTING":
      return hideHeaderDraft;
    case "CHANGE_INFOBOX_LABEL_SETTING":
      return hideLabelDraft;
    case "ADD_COMPONENT_DROPZONE":
      draft.components.push({
        ...action.component,
      });
      return draft;
    case "DRAG_ADD_NEW_COMPONENT":
      draft.components.splice(action.hoverIndex, 0, action.component);
      return draft;
    case "DRAG_COMPONENT":
      // eslint-disable-next-line no-case-declarations
      const sameZone = action.wrapperId === draft.id;
      if (sameZone) {
        const dragElement = draft.components[action.dragIndex];
        draft.components.splice(action.dragIndex, 1);
        draft.components.splice(action.hoverIndex, 0, dragElement);
      } else {
        action.hoverIndex =
          action.direction === "up" ? action.hoverIndex + 1 : action.hoverIndex;
        draft.components.splice(action.hoverIndex + 1, 0, action.component);
      }
      return draft;
    case "DELETE_COMPONENT":
      draft.components.splice(action.compIndex, 1);
      return draft;
    case "MOVE_COMPONENT_DOWN":
      // eslint-disable-next-line no-case-declarations
      const elementCR = draft.components[action.compIndex];
      draft.components.splice(action.compIndex, 1);
      draft.components.splice(action.compIndex + 1, 0, elementCR);
      return draft;
    case "MOVE_COMPONENT_UP":
      // eslint-disable-next-line no-case-declarations
      const elementCL = draft.components[action.compIndex];
      draft.components.splice(action.compIndex, 1);
      draft.components.splice(action.compIndex - 1, 0, elementCL);
      return draft;
    case "DUPLICATE_COMPONENT":
      draft.components.splice(
        action.compIndex + 1,
        0,
        draft.components[action.compIndex]
      );
      return draft;
    case "UPDATE_COMPONENT":
      draft.components[action.compIndex].componentProps = {
        ...action.stateUpdate,
      };
      return draft;
    case "ADD_ID":
      draft.id = action.id;
      break;
    default:
      return draft;
  }
};

// InfoBox provider wraps the tab component to access reducer
export const InfoBoxProvider = ({ children, setProp, infoBoxState }) => {
  const [state, dispatch] = useReducer(produce(infoBoxConfig), infoBoxState);

  const diff = JSON.stringify(state) !== JSON.stringify(infoBoxState);
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    dispatch({ func: "UPDATE_STATE", data: infoBoxState });
    setMounted(true);
  }, []);

  useEffect(() => {
    diff && mounted && setProp({ infoBoxState: state });
  }, [state]);

  useEffect(() => {
    diff && mounted && dispatch({ func: "UPDATE_STATE", data: infoBoxState });
  }, [infoBoxState]);

  const infoBoxValue = useMemo(() => [state, dispatch], [state]);

  return (
    <InfoBoxContext.Provider value={infoBoxValue}>
      {children}
    </InfoBoxContext.Provider>
  );
};

// use custom hook to return context - check if it's being used inside provider
export const useInfoBoxContext = () => {
  const contextValue = useContext(InfoBoxContext);
  if (!contextValue)
    throw new Error("useInfoBoxContext must be called inside InfoBoxProvider");
  return contextValue;
};
