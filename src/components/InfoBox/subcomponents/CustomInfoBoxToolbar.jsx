import React, { useState, useRef, useContext } from "react";

import {
  AppBar,
  Toolbar,
  Paper,
  Popper,
  Grow,
  MenuList,
  MenuItem,
  Button,
  IconButton,
  Tooltip,
  FormGroup,
  FormControl,
  FormControlLabel,
  Checkbox,
} from "@mui/material";
import ClickAwayListener from "@mui/material/ClickAwayListener";

import { iconDropdownOptions } from "../icons/infoBoxIcons";
import { Chevron } from "../../Text/assets/icons";

// TO DO:- Need to put into separate folder
import KebabSVG from "../../Video/assets/Kebab";

// Context
import { InfoBoxContext } from "../InfoBoxContext";

// CSS
import "../assets/styles_Toolbar.scss";

const CustomInfoBoxToolbar = ({ setSelectedIcon, selectedIcon, t }) => {
  // Context
  const [state, dispatch] = useContext(InfoBoxContext);

  // Refs
  const IconDropDown = useRef(null);
  const AppBarRef = useRef(null);
  const kebabselectRef = useRef(null);
  const DescriptionKebab = useRef(null);

  // IconBox
  const [openIcon, setIconOpen] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(null);
  const [openDescriptionKebab, setDescriptionKebabOpen] = useState(false);

  const handleListKeyDown = (event) => {
    if (event.key === "Tab") {
      event.preventDefault();
      setIconOpen(false);
    } else if (event.key === "Escape") {
      setIconOpen(false);
    }
  };

  // handle Close Icon
  const handleCloseIcon = (event) => {
    if (IconDropDown.current && IconDropDown.current.contains(event.target)) {
      return;
    }
    setIconOpen(false);
  };

  const handleIconMenuItemClick = (e, index) => {
    setSelectedIndex(index);
    setSelectedIcon(iconDropdownOptions[index].type);
  };

  const handleToggleInfoboxKebab = () => {
    // toggleCloseToolbar("Kebab");
    setDescriptionKebabOpen(!openDescriptionKebab);
  };

  return (
    <>
      {/* <span
                className="sr-only"
                role="status"
                aria-live="assertive"
                aria-relevant="all"
                aria-atomic="true"
            >
                {ariaLive}
            </span>
            <span
                className="sr-only"
                role="status"
                aria-live="assertive"
                aria-relevant="all"
                aria-atomic="true"
            >
                {ariaLive2}
            </span> */}
      <AppBar
        className="app-container"
        position="static"
        ref={AppBarRef}
        elevation={0}
        style={{
          "--boxShadow": "none !important",
        }}
      >
        <Toolbar
          position="static"
          className="toolbar-container"
          style={{
            "--boxShadow":
              "0px 0px 3px 0px rgba(35, 35, 35, 0.15),-1px 0px 5px 0px rgba(161, 162, 163, 0.2),3px 3px 4px 0px rgba(35, 35, 35, 0.15)",
            "--position": "static",
          }}
        >
          <Button
            ref={IconDropDown}
            aria-controls={openIcon ? t("Infobox Select Icon") : undefined}
            aria-expanded={openIcon ? "true" : undefined}
            variant="contained"
            disableRipple
            disableFocusRipple
            onClick={() => {
              setIconOpen(!openIcon);
            }}
            className="dropdown-button-select"
            style={{
              "--active": openIcon ? "rgba(21, 101, 192, 1)" : "#000",
              "--width": "100%",
              "--height": "100%",
              "--font-size": "16px",
              "--svg": openIcon ? " " : "rotate(180deg)",
              "--grid-template-columns": "1fr 10fr",
              "--hover-background-color": "transparent",
            }}
          >
            <Chevron />
            <span>
              {selectedIcon === null
                ? t("Infobox Select Icon")
                : t(`${selectedIcon}`)}
            </span>
            <Popper
              open={openIcon}
              anchorEl={IconDropDown.current}
              placement="bottom-start"
              transition
              disablePortal
            >
              {({ TransitionProps }) => (
                <Grow {...TransitionProps}>
                  <Paper
                    elevation={0}
                    className="paper-select-styled"
                    style={{
                      "--height": "196px",
                      "--margin-left": "-10px",
                      "--margin-top": "10px",
                    }}
                  >
                    <ClickAwayListener onClickAway={handleCloseIcon}>
                      <MenuList
                        autoFocusItem={openIcon}
                        data-testid="icon-select-dropdown"
                        aria-labelledby={t("Infobox Icon Drop Down")}
                        onKeyDown={handleListKeyDown}
                        className="styled-menu"
                        style={{
                          "--gridTemplateRows": "1fr 1fr 1fr 1fr 1fr",
                          "--padding": "8px 0px",
                          "--justifyItems": "start",
                          // "--width": "auto",
                          "--grid-template-columns": "24px auto",
                          "justify-content": "none",
                        }}
                      >
                        {iconDropdownOptions.map((infoBox, index) => {
                          return (
                            <MenuItem
                              key={infoBox.id}
                              value={infoBox.type}
                              selected={index === selectedIndex}
                              onClick={(e) => handleIconMenuItemClick(e, index)}
                              className="styled-menu-item"
                              data-testid={`${infoBox.type} icon`}
                              aria-labelledby={`${t(infoBox.type)} ${t(
                                "Icon"
                              )}`}
                              style={{
                                // "--width": "auto",
                                "--height": "36px",
                              }}
                            >
                              {t(infoBox.type)}
                            </MenuItem>
                          );
                        })}
                      </MenuList>
                    </ClickAwayListener>
                  </Paper>
                </Grow>
              )}
            </Popper>
          </Button>
          <ClickAwayListener onClickAway={() => setDescriptionKebabOpen(false)}>
            <div
              ref={kebabselectRef}
              style={{
                display: "grid",
                gridTemplateColumns: "9px 1fr",
                height: "40px",
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                zIndex: "1",
              }}
            >
              <div className="styled-divider" />
              <Tooltip
                aria-label={t("configure infobox description")}
                title={t("configure infobox description")}
                placement="top"
                arrow
              >
                <IconButton
                  ref={DescriptionKebab}
                  open={openDescriptionKebab}
                  data-infoboxId="infoboxSettings"
                  aria-controls={
                    openDescriptionKebab
                      ? t("Change Infobox settings")
                      : undefined
                  }
                  aria-expanded={openDescriptionKebab ? "true" : undefined}
                  variant="contained"
                  disableRipple
                  disableFocusRipple
                  onClick={handleToggleInfoboxKebab}
                  className="styled-icon-button"
                  style={{
                    "--active": openDescriptionKebab
                      ? "rgba(21, 101, 192, 1)"
                      : "#000",
                    "--background": openDescriptionKebab
                      ? "rgba(21, 101, 192, 0.12)"
                      : "#fff",
                  }}
                >
                  <KebabSVG />
                </IconButton>
              </Tooltip>
              <Popper
                open={openDescriptionKebab}
                anchorEl={DescriptionKebab.current}
                placement="bottom-start"
                transition
                disablePortal
              >
                {({ TransitionProps }) => (
                  <Grow {...TransitionProps}>
                    <Paper
                      style={{
                        boxShadow:
                          "0px 1px 3px 0px rgba(35, 35, 35, 0.1), 0px 2px 5px 0px rgba(161, 162, 163, 0.2), 3px 3px 4px 0px rgba(35, 35, 35, 0.15)",
                      }}
                    >
                      <MenuList
                        data-testid="infobox-header-settings-dropdown"
                        aria-labelledby="Infobox Header Settings"
                        className="StyledCheckboxMenu"
                        style={{
                          "--height": "auto",
                          "--margin": "8px 0",
                        }}
                      >
                        <FormGroup
                          sx={{ gap: "14px", margin: "8px 16px !important" }}
                        >
                          <FormControl>
                            <Tooltip
                              aria-label={t("Show Label").toLowerCase()}
                              title={t("Show Label").toLowerCase()}
                              placement="top"
                              arrow
                              PopperProps={{
                                modifiers: [
                                  {
                                    name: "offset",
                                    options: {
                                      offset: [0, -7],
                                    },
                                  },
                                ],
                              }}
                            >
                              <FormControlLabel
                                className="StyledFormControlLabel"
                                control={
                                  <Checkbox
                                    checked={
                                      state.infoBoxSettings?.label ||
                                      !state.infoBoxSettings
                                    }
                                    aria-checked={
                                      state.infoBoxSettings?.label ||
                                      !state.infoBoxSettings
                                    }
                                    onClick={(e) => {
                                      dispatch({
                                        func: "CHANGE_INFOBOX_LABEL_SETTING",
                                        infoHeaderSetting: state.infoBoxSettings
                                          ? state.infoBoxSettings?.header
                                          : true,
                                        infoLabelSetting: state.infoBoxSettings
                                          ? !state.infoBoxSettings?.label
                                          : false,
                                      });
                                    }}
                                    sx={{
                                      "&:hover": {
                                        bgcolor: "transparent",
                                        color: "rgba(21, 101, 192, 1)",
                                      },
                                      "&.Mui-checked": {
                                        bgcolor: "transparent",
                                        color: "rgba(21, 101, 192, 1)",
                                      },
                                    }}
                                  />
                                }
                                sx={{
                                  "&.StyledFormControlLabel": {
                                    color: "#232323 !important",
                                  },
                                }}
                                label={t("Show Label")}
                                size="small"
                              />
                            </Tooltip>
                          </FormControl>
                          <FormControl>
                            <Tooltip
                              aria-label={t("Show Header").toLowerCase()}
                              title={t("Show Header").toLowerCase()}
                              placement="top"
                              arrow
                              PopperProps={{
                                modifiers: [
                                  {
                                    name: "offset",
                                    options: {
                                      offset: [0, -7],
                                    },
                                  },
                                ],
                              }}
                            >
                              <FormControlLabel
                                className="StyledFormControlLabel"
                                control={
                                  <Checkbox
                                    checked={
                                      state.infoBoxSettings?.header ||
                                      !state.infoBoxSettings
                                    }
                                    aria-checked={
                                      state.infoBoxSettings?.header ||
                                      !state.infoBoxSettings
                                    }
                                    onClick={(e) => {
                                      dispatch({
                                        func: "CHANGE_INFOBOX_HEADER_SETTING",
                                        infoHeaderSetting: state.infoBoxSettings
                                          ? !state.infoBoxSettings?.header
                                          : false,
                                        infoLabelSetting: state.infoBoxSettings
                                          ? state.infoBoxSettings?.label
                                          : true,
                                      });
                                    }}
                                    sx={{
                                      "&:hover": {
                                        bgcolor: "transparent",
                                        color: "rgba(21, 101, 192, 1)",
                                      },
                                      "&.Mui-checked": {
                                        bgcolor: "transparent",
                                        color: "rgba(21, 101, 192, 1)",
                                      },
                                    }}
                                  />
                                }
                                sx={{
                                  "&.StyledFormControlLabel": {
                                    color: "#232323 !important",
                                  },
                                }}
                                label={t("Show Header")}
                                size="small"
                              />
                            </Tooltip>
                          </FormControl>
                        </FormGroup>
                      </MenuList>
                    </Paper>
                  </Grow>
                )}
              </Popper>
            </div>
          </ClickAwayListener>
        </Toolbar>
      </AppBar>
    </>
  );
};

export default CustomInfoBoxToolbar;
