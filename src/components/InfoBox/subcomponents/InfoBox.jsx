import React, { useState, useRef, useContext } from "react";
// MUI/@emotion imports
import { Paper, ClickAwayListener } from "@mui/material";
// Localization import
import { useTranslation } from "react-i18next";

// ?Provider
import InfoBoxDropZone from "./InfoBoxDropZone";
import { useInfoBoxContext } from "../InfoBoxContext";
import TenantContext from "../../../Context/TenantContext";
import CustomInfoBoxToolbar from "./CustomInfoBoxToolbar";
// Component imports

import Label from "./Label";
import Header from "./Header";
import Icon from "./Icon";

// Styles
import "../assets/styles_Infobox.scss";
// portal
import PortalUtil from "../../../Utility/PortalUtil";

// InfoBox component
const InfoBox = ({
  toolbarContainerRef,
  showSelf,
  isActiveComponent,
  setHoverMenu,
  setActiveComponent
}) => {
  // Localization
  const { t } = useTranslation();
  const [state, dispatch] = useInfoBoxContext();

  const [selectedIcon, setSelectedIcon] = useState(null);

  const [removeError, setRemoveError] = useState(false);

  const infoBoxRef = useRef();
  const toolbarRef = useRef();
  const authoringData = useContext(TenantContext);

  const viewOnly = !authoringData?.viewOnlyComponents;

  return (
    <ClickAwayListener
      onClickAway={() => {
        setRemoveError(false);
      }}
    >
      <Paper
        aria-label={t("InfoBox")}
        data-testid="infoBox-container"
        ref={infoBoxRef}
        className="infoBox-Paper"
        onBlur={(e) => {
          setRemoveError(false);
        }}
      >
        {(showSelf || isActiveComponent) && !viewOnly && (
          <PortalUtil portalRef={toolbarContainerRef}>
            <div className="infobox-toolbar-wrapper" ref={toolbarRef}>
              <CustomInfoBoxToolbar
                setSelectedIcon={setSelectedIcon}
                selectedIcon={selectedIcon}
                t={t}
              />
            </div>
          </PortalUtil>
        )}

        <div className="infoBox-content-wrapper">
          <div className="icon-container">
            <Icon
              setSelectedIcon={setSelectedIcon}
              selectedIcon={selectedIcon}
            />
          </div>
          <div className="header-label-dropzone-contaier">
            <div className="infoBox-textContainer">
              {state?.infoBoxSettings?.label !== false && (
                <Label t={t} viewOnly={viewOnly} />
              )}
              {state?.infoBoxSettings?.header !== false && (
                <Header t={t} viewOnly={viewOnly} />
              )}
            </div>
            <div>
              <div>
                <InfoBoxDropZone
                  removeError={removeError}
                  setRemoveError={setRemoveError}
                  setHoverMenu={setHoverMenu}
                  setActiveComponent={setActiveComponent}
                />
              </div>
            </div>
          </div>
        </div>
      </Paper>
    </ClickAwayListener>
  );
};

export default InfoBox;
