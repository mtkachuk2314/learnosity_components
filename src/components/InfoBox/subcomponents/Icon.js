import React, { useEffect, useContext, useMemo } from "react";

import { useTranslation } from "react-i18next";
import { useInfoBoxContext } from "../InfoBoxContext";
import { iconDropdownOptions, defaultIcon } from "../icons/infoBoxIcons";

const Icon = ({ setSelectedIcon, selectedIcon }) => {
  const [state, dispatch] = useInfoBoxContext();
  const { t } = useTranslation();

  const stateIcon = useMemo(() => state?.infoBoxIcon, [state?.infoBoxIcon]);

  useEffect(() => {
    !selectedIcon && setSelectedIcon(stateIcon);
  }, []);

  useEffect(() => {
    dispatch({ func: "CHANGE_ICON", icon: selectedIcon });
  }, [selectedIcon]);

  return (
    <div
      data-icon={stateIcon || ""}
      data-testid="icon"
      aria-label={t(`${selectedIcon}`)}
      className="infoBox-icon-container"
    >
      {stateIcon !== null
        ? iconDropdownOptions.find((icon) => icon.type === stateIcon)?.icon
        : defaultIcon}
    </div>
  );
};

export default Icon;
