import React, {
  useState,
  useCallback,
  useEffect,
  useContext,
  useMemo,
} from "react";

import { TextareaAutosize } from "@material-ui/core";
// UUID
import { v4 as uuidv4 } from "uuid";
import { InfoBoxContext } from "../InfoBoxContext";
import useDebounce from "../../../hooks/useDebounce";

const Header = ({ t, viewOnly, showSelf, isActiveComponent }) => {
  const [state, dispatch] = useContext(InfoBoxContext);
  const [headerValue, setHeaderValue] = useState(state.infoBoxHeader?.heading);

  const handleHeaderChange = (header) => {
    dispatch({
      func: "CHANGE_HEADER",
      header,
    });
  };

  const handleDebounce = useDebounce(handleHeaderChange);

  const handleHeadingChange = useCallback(
    (e) => {
      const header = e.target.value;
      setHeaderValue(header);
      handleDebounce(header);
    },
    [handleDebounce]
  );

  useEffect(() => {
    setHeaderValue(state.infoBoxHeader?.heading);
  }, [state.infoBoxHeader?.heading]);

  const infoboxHeaderLabelId = useMemo(
    () => `infobox-header-label-${uuidv4()}`,
    []
  );

  return (
    <>
      <label htmlFor={infoboxHeaderLabelId} className="srOnly">
        {t("Infobox Header Aria")}
      </label>
      <TextareaAutosize
        name="infoBoxHeader"
        id={infoboxHeaderLabelId}
        aria-label={t("Infobox Header Aria")}
        placeholder={t("Infobox Header Placeholder")}
        aria-multiline="true"
        value={headerValue}
        className="infoBox-header"
        onChange={handleHeadingChange}
        onKeyDown={(e) => e.key === "Enter" && e.preventDefault()} // Prevent new line break
        disabled={viewOnly}
      />
    </>
  );
};

export default Header;
