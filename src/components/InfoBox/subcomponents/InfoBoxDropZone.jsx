import React, { useContext, useState, useEffect, useRef } from "react";
import { useDrop } from "react-dnd";

import { v4 as uuidV4 } from "uuid";

import { InfoBoxContext } from "../InfoBoxContext";
import { MainInsideWrapperInfoBox } from "../../../theme/styledComponents/globalComponent";

// components
import Placeholder from "../../../Utility/Placeholder";
import PlaceholderError from "../../../Utility/PlaceholderError";
import NestedComponentWrapper from "../../../Utility/NestedComponentWrapper";
import ComponentWithFormat from "../../../authoring/components/ComponentWithFormat";

const InfoBoxDropZone = ({
  removeError,
  setRemoveError,
  setHoverMenu,
  setActiveComponent,
}) => {
  const [state, dispatch] = useContext(InfoBoxContext);
  const dropRef = useRef(null);
  const { components } = state;

  const isEmpty = components?.length === 0 || components === undefined;

  const [inContainer, setInContainer] = useState(null);
  const [, setIsDragging] = useState(false);
  const [droppedIndex, setDroppedIndex] = useState(null);
  const [activeComp, setActiveComp] = useState(null);
  // ? Error Message
  const [showError, setShowError] = useState();
  const [showDropError, setShowDropError] = useState();

  // List of accepted into tab componenets
  const acceptListComp = (item) => {
    return (
      ["Text", "Video", "Image", "Header"].indexOf(item.componentName) >= 0
    );
  };

  const [{ isOver, getItem }, drop] = useDrop(() => ({
    accept: [
      "Text",
      "Image",
      "Video",
      "Table",
      "InfoBox",
      "Header",
      "QuoteBox",
      "IFrame",
      "Accordion",
      "Tabs",
      "section",
      "MultiColumn",
      "Reveal",
      "Tab",
      "MultipleChoice",
    ],
    drop: async (item, monitor) => {
      if (!acceptListComp(item)) setShowDropError(true);
      if (item.within && components.length !== 0) return;
      if (monitor.didDrop()) return;
      if (acceptListComp(item)) {
        dispatch({
          func: "ADD_COMPONENT_DROPZONE",
          component: {
            componentName: item.componentName,
            componentProps: JSON.parse(item?.componentProps),
            formats: item.formats,
          },
        });
        item?.delete && item?.delete();
      }
    },

    collect: (monitor) => ({
      isOver: !!monitor.isOver(),
      getItem: monitor.getItem(),
    }),
  }));

  state.components == undefined &&
    dispatch({
      func: "VERSION2_INFOBOX",
      bodyText: state.body,
    });

  state.id == undefined &&
    dispatch({
      func: "ADD_ID",
      id: uuidV4(),
    });

  // Adding space between Cap except iFrame
  const trimCap = (item) => {
    switch (item) {
      case "IFrame":
        return "iFrame";
      case "InfoBox":
        return "InfoBox";
      case "Tab":
        return "Tabs";
      case "NONLEARNING":
        return "Descriptive Container";
      case "LEARNING":
        return "Learning Container";
      default:
        return item.replace(/([A-Z])/g, " $1").trim();
    }
  };

  useEffect(() => {
    if (isOver && !acceptListComp(getItem)) {
      setShowError(trimCap(getItem.componentName || getItem.type));
    } else if (isOver) {
      setShowError();
      setShowDropError(false);
      setIsDragging(true);
    } else {
      setIsDragging(false);
    }
  }, [isOver]);

  useEffect(() => {
    setShowError();
    setRemoveError(false);
  }, [removeError]);

  drop(dropRef);

  return (
    <div
      onDragLeave={() => setInContainer(false)}
      onDragOver={() => setInContainer(true)}
      ref={dropRef}
      data-testid="infobox-drop-zone"
      isOver={isOver}
      role="document"
      className="infobox-dropzone-body"
      style={{
        backgroundColor:
          showError && isEmpty
            ? "rgba(211, 47, 47, 0.04)"
            : isOver && isEmpty
            ? "rgba(21, 101, 192, 0.04)"
            : "#FFFFFF",
      }}
    >
      {isEmpty ? (
        <Placeholder
          isOver={isOver}
          showError={showError}
          placeholderType="infobox"
        />
      ) : (
        <MainInsideWrapperInfoBox>
          <div role="list" isOver={isOver}>
            {components.map((component, compIndex) => {
              return (
                <ComponentWithFormat
                  key={`key-component-${compIndex}`}
                  formats={component.formats}
                  componentName={component.componentName}
                >
                  <NestedComponentWrapper
                    componentType="infobox"
                    key={`key-component-${compIndex}`}
                    numOfComponent={components.length}
                    componentProps={component.componentProps}
                    formats={component.formats}
                    component={component}
                    compIndex={compIndex}
                    inContainer={inContainer}
                    draggingOver={isOver}
                    setDroppedIndex={setDroppedIndex}
                    droppedIndex={droppedIndex}
                    setActiveComp={setActiveComp}
                    activeComp={activeComp}
                    setShowError={setShowError}
                    setShowDropError={setShowDropError}
                    setIsDragging={setIsDragging}
                    setHoverMenu={setHoverMenu}
                    setActiveComponent={setActiveComponent}
                  />
                </ComponentWithFormat>
              );
            })}
            {showDropError && components.length !== 0 && (
              <PlaceholderError errorType="infoboxError" />
            )}
          </div>
        </MainInsideWrapperInfoBox>
      )}
    </div>
  );
};
export default InfoBoxDropZone;
