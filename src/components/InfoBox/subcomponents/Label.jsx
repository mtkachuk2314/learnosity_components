import React, {
  useState,
  useCallback,
  useContext,
  useEffect,
  useMemo,
} from "react";
// UUID
import { v4 as uuidv4 } from "uuid";
// ? Context
import { InfoBoxContext } from "../InfoBoxContext";
import useDebounce from "../../../hooks/useDebounce";
import { TextareaAutosize } from "@material-ui/core";

const Label = ({ t, viewOnly }) => {
  const [state, dispatch] = useContext(InfoBoxContext);
  const [labelValue, setLabelValue] = useState(state.infoBoxLabel);

  const updateLabelState = (label) => {
    dispatch({
      func: "CHANGE_LABEL",
      label,
    });
  };

  const handleDebounce = useDebounce(updateLabelState);

  const handleLabelChange = useCallback(
    (e) => {
      const label = e.target.value;
      setLabelValue(label);
      handleDebounce(label);
    },
    [handleDebounce]
  );

  useEffect(() => {
    setLabelValue(state.infoBoxLabel);
  }, [state.infoBoxLabel]);

  const infoboxLabelId = useMemo(() => `infobox-label-${uuidv4()}`, []);

  return (
    <>
      <label htmlFor={infoboxLabelId} className="srOnly">
        {t("Infobox Label Aria")}
      </label>
      <TextareaAutosize
        name="infoBoxLabel"
        type="text"
        id={infoboxLabelId}
        aria-label={t("Infobox Label Aria")}
        placeholder={t("Infobox Label Placeholder")}
        autoComplete="false"
        maxLength={50}
        value={labelValue}
        className="infoBox-label"
        onChange={handleLabelChange}
        disabled={viewOnly}
      />
    </>
  );
};

export default Label;
