import React from "react";
import { v4 as uuidv4 } from "uuid";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import { InfoBoxProvider } from "./InfoBoxContext";
import InfoBox from "./subcomponents/InfoBox";
import { MainWrapper } from "../../theme/styledComponents/globalComponent";

// Default props
export const defaultProps = {
  infoBoxState: {
    id: uuidv4(),
    infoBoxIcon: null,
    infoBoxLabel: "",
    infoBoxHeader: {
      heading: "",
      headingLevel: "",
    },
    body: null,
    infoBoxSettings: {
      header: true,
      label: true,
    },
    components: [],
  },
};

const InfoBoxMain = ({
  infoBoxState = defaultProps,
  setProp = () => {},
  toolbarContainerRef,
  showSelf = null,
  isActiveComponent = null,
  setHoverMenu = () => {},
  setActiveComponent = () => {},
}) => {
  return (
    <DndProvider backend={HTML5Backend}>
      <InfoBoxProvider infoBoxState={infoBoxState} setProp={setProp}>
        <MainWrapper>
          <InfoBox
            toolbarContainerRef={toolbarContainerRef}
            showSelf={showSelf}
            isActiveComponent={isActiveComponent}
            setHoverMenu={setHoverMenu}
            setActiveComponent={setActiveComponent}
          />
        </MainWrapper>
      </InfoBoxProvider>
    </DndProvider>
  );
};

export default InfoBoxMain;
