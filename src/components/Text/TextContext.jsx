import React, {
  createContext,
  useReducer,
  useEffect,
  useState,
  useContext,
  useMemo,
} from "react";
import { isEqualWith, isEqual } from "lodash";

// state of video data stored in VideoContext
export const TextContext = createContext();

// Reducers
const textReducers = (prevState, action) => {
  switch (action.func) {
    case "UPDATE_STATE":
      return { ...action.data };
    default:
      return { ...prevState };
  }
};

// video provider wraps the tab component to access reducer
const TextProvider = ({ children, setProp, textState }) => {
  const [state, dispatch] = useReducer(textReducers, textState);

  const [mounted, setMounted] = useState(false);

  // Checking for any empty data /// null, "", {}, { ops: [{ insert: "" }] }
  // Check is required due to change in default props used in Text, Image & Video Components
  const isEmptyOrOps = (value) => {
    return (
      value === null ||
      value === "" ||
      Object.keys(value).length === 0 ||
      isEqual(value, { ops: [{ insert: "" }] })
    );
  };

  useEffect(() => {
    if (!mounted) {
      dispatch({
        func: "UPDATE_STATE",
        data: textState === null ? { ops: [{ insert: "" }] } : textState,
      });
      setMounted(true);
    }
    // ------------- Diff logic to prevent empty updates -------------
    // text  state is Delta object - different diff logic
    // convert state comming from authoring to delta
    // comparing the object other than id check
    const diff = !isEqualWith(textState, state, (objValue, othValue, key) => {
      if (key === "id") return true;
      return undefined;
    });

    // If both textState & state are any of the possible empty states don't save to db and return
    if (isEmptyOrOps(textState) && isEmptyOrOps(state)) {
      return; // Return early if both are "empty"
    }
    if (diff && mounted) {
      setProp({ body: state });
    }
  }, [state]);

  useEffect(() => {
    // ------------- Diff logic to prevent empty updates -------------
    // text  state is Delta object - different diff logic
    // convert state comming from authoring to delta
    const diff = !isEqualWith(textState, state, (objValue, othValue, key) => {
      if (key === "id") return true;
      return undefined;
    });
    // This diff needs to filter out IDs on attributes from caring if there's a differene, but still keep them on data
    diff && mounted && dispatch({ func: "UPDATE_STATE", data: textState });
  }, [textState]);

  // usememo --> prevent passing a new reference to object or array as value in every time the provider component re-renders
  const TextValue = useMemo(() => [state, dispatch], [state]);
  return (
    <TextContext.Provider value={TextValue}>{children}</TextContext.Provider>
  );
};

export default TextProvider;

// use custom hook to return context - check if it's being used inside provider
export const useTextContext = () => {
  const contextValue = useContext(TextContext);
  if (!contextValue)
    throw new Error("useTextContext must be called inside TextProvider");
  return contextValue;
};
