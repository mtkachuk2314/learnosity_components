/**
 * Handle events for formula elements.
 * @param {string} componentId - The ID for the component.
 * @param {string} operation - The operation to perform (add/remove).
 */

export const FormulaEvents = (componentId, operation = "add") => {
  // Obtain the main container for the formulas using the provided ID.
  const textContainer = document.getElementById(`toolbar-${componentId}`);

  // If the container doesn't exist, exit the function early.
  if (!textContainer) return;

  // Find the placeholder for the MathPix formula within the container.
  const mathPixPlaceHolder = textContainer.querySelector(
    `#mathpix-placeholder-${componentId}`
  );

  /**
   * Helper function to set multiple attributes for a DOM element.
   *
   * @param {HTMLElement} elem - The DOM element to set attributes for.
   * @param {Object} attrs - An object containing attribute key-value pairs.
   */
  const setAttributes = (elem, attrs) => {
    for (const key in attrs) {
      if (Object.prototype.hasOwnProperty.call(attrs, key)) {
        elem?.setAttribute(key, attrs[key]);
      }
    }
  };

  // Iterate over each formula element within the container.
  [...textContainer.querySelectorAll(".ql-mathpix")].forEach((item) => {
    // Define an event handler for when a formula element is clicked.
    const setPlaceholderInfo = (e) => {
      // Check if the clicked element matches the current formula element.
      if (
        e.target.closest(".ql-mathpix").getAttribute("data-id") ===
        item.getAttribute("data-id")
      ) {
        // Set attributes for the MathPix placeholder based on the clicked element.
        setAttributes(mathPixPlaceHolder, {
          "data-value": item.getAttribute("data-value"),
          "data-id": item.getAttribute("data-id"),
          "data-clientX": item.offsetLeft,
          "data-clientY": item.offsetTop + 25,
          "c-listener": "true",
        });

        // Trigger a click event on the placeholder.
        mathPixPlaceHolder?.click();
      }
    };

    // If the operation is "remove", remove the click event listener from the formula element.
    if (operation === "remove") {
      item.removeEventListener("click", setPlaceholderInfo);
      return;
    }

    // If the formula element doesn't already have a listener and the operation is "add", add the click event listener.
    if (item.getAttribute("c-listener") !== "true" && operation === "add") {
      item.addEventListener("click", setPlaceholderInfo);
    }

    // Set the cursor style to "default" when hovering over the formula element.
    item.addEventListener("mouseover", () => {
      // eslint-disable-next-line no-param-reassign
      item.style.cursor = "default";
    });
  });
};

export const linkClickEvent = (componentId, index, text, link) => {
  const linkPlaceHolder = document.getElementById(
    `link-placeholder-${componentId}`
  );
  if (linkPlaceHolder) {
    linkPlaceHolder.setAttribute("data-text", text);
    linkPlaceHolder.setAttribute("data-index", index);
    linkPlaceHolder.setAttribute("data-link", link);
    link && linkPlaceHolder.click();
  }
};
