import React, { useState, useEffect, useRef, useContext, useMemo } from "react";
import { useTranslation } from "react-i18next";
import styled from "@emotion/styled";
import { v4 as uuidv4 } from "uuid";
import DefaultText from "./subcomponent/DefaultText";
import EditorComponent from "./subcomponent/EditorComponent";
import { Provider } from "./Provider";
import TenantContext from "../../Context/TenantContext";
import TextProvider from "./TextContext";
import "./styles/Text.scss";
import CustomToolBar from "./subcomponent/CustomToolBar";
// portal
import PortalUtil from "../../Utility/PortalUtil";
import PopupDialogs from "./dialogs/PopupDialogs";

import ReactQuillContainer from "../../theme/styledComponents/quillEditor";

export const defaultProps = { body: { ops: [{ insert: "" }] } };

const StyledConfigBar = styled("div")(({ portal }) => {
  let display =
    portal?.parentComponent && portal?.shouldPortal ? "flex" : "none";

  display = !portal?.parentComponent ? "flex" : display;

  const configBarStyles = {
    display,
  };
  return configBarStyles;
});

const Text = ({
  body = defaultProps.body,
  setProp = () => {},
  setActiveComponent = () => {},
  selectedIcon = null,
  componentType = null,
  setSelectedIcon = () => {},
  setTextRef = () => {},
  portal = null,
  toolbarContainerRef,
  showSelf = null,
  isActiveComponent = null,
}) => {
  const { t } = useTranslation();
  const [showEditor, setShowEditor] = useState(false);

  const containerRef = useRef(null);
  const toolbarRef = useRef(null);

  // generate a unique id for toolbar and keep it from changing with useMemo
  const componentId = useMemo(() => `unique-id-${uuidv4()}`, []);

  // state for Align & List toolbar selection
  const [activeDropDownAlignItem, setActiveDropDownAlignItem] = useState("");
  const [activeDropDownListItem, setActiveDropDownListItem] = useState("");

  // Observer mode
  const authoringData = useContext(TenantContext);
  const viewOnly = !authoringData?.viewOnlyComponents;

  useEffect(() => {
    if (portal) {
      portal?.setTextContainer(containerRef.current);
    }
  }, [portal]);

  // const onBlureHandler = () => {
  //   // quill instance
  //   const quill = focusRef?.current?.getEditor();
  //   const editorContent = quill.getContents();
  //   setShowEditor(false);
  // };

  return (
    <TextProvider textState={body} setProp={setProp}>
      <Provider>
        <ReactQuillContainer
          isVideo={portal?.parentComponent === "video"}
          isImage={portal?.parentComponent === "image"}
          isInfoBox={componentType === "infobox"}
          parentSection={portal?.parentSection}
          ref={containerRef}
          data-testid={
            portal
              ? `${portal?.parentComponent}-${portal?.parentSection}-text-container`
              : "text-container"
          }
        >
          {!viewOnly && (showSelf || isActiveComponent) ? (
            <>
              <PopupDialogs closeToolBar={!(showSelf || isActiveComponent)} />
              <PortalUtil portalRef={toolbarContainerRef}>
                <StyledConfigBar portal={portal}>
                  <CustomToolBar
                    ref={toolbarRef}
                    componentId={componentId}
                    activeDropDownListItem={activeDropDownListItem}
                    setActiveDropDownListItem={setActiveDropDownListItem}
                    activeDropDownAlignItem={activeDropDownAlignItem}
                    setActiveDropDownAlignItem={setActiveDropDownAlignItem}
                    selectedIcon={selectedIcon}
                    setSelectedIcon={setSelectedIcon}
                    portal={portal}
                    t={t}
                  />
                </StyledConfigBar>
              </PortalUtil>
            </>
          ) : null}
          {(!showEditor && body === null) ||
          (!showEditor && !body.ops) ||
          (!showEditor &&
            body.ops[0].insert === "" &&
            !portal?.shouldPortal) ? (
            <div
              onClick={() => {
                if (!viewOnly) {
                  setShowEditor(true);
                  setActiveComponent(true);
                  setTextRef(true);
                }
              }}
              onFocus={() => {
                if (!viewOnly) {
                  setShowEditor(true);
                  setActiveComponent(true);
                  setTextRef(true);
                }
              }}
              className={
                portal?.parentComponent === "video" ||
                portal?.parentComponent === "image"
                  ? ""
                  : "mainContainer"
              }
              data-testid="text-component"
              role="text"
              tabIndex={0}
            >
              <DefaultText portal={portal} />
            </div>
          ) : (
            <EditorComponent
              toolbarRef={toolbarRef}
              setShowEditor={setShowEditor}
              showEditor={showEditor}
              selectedIcon={selectedIcon}
              setSelectedIcon={setSelectedIcon}
              portal={portal}
              toolbarContainerRef={toolbarContainerRef}
              showSelf={showSelf}
              isActiveComponent={isActiveComponent}
              componentId={componentId}
              setActiveDropDownAlignItem={setActiveDropDownAlignItem}
              setActiveDropDownListItem={setActiveDropDownListItem}
              t={t}
            />
          )}
        </ReactQuillContainer>
      </Provider>

      {/* </ThemeProvider> */}
    </TextProvider>
  );
};

export default React.memo(Text);
