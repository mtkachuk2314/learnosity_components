import Quill from "quill";
const Delta = Quill.import("delta");

export const matchMsWordList = (node, delta) => {
  // Clone the operations
  let ops = delta.ops.map((op) => Object.assign({}, op));

  for (let i = 0; i < ops.length; i++) {
    if (ops[i].attributes.list) {
      ops[i].attributes.align = "start"
    }
    if (ops[i].insert === ".\n\n") {
      delete ops[i].attributes.list
    }
  }
  return new Delta(ops);
};

export const maybeMatchMsWordList = (node, delta) => {
  if (!delta?.ops[0]?.insert?.image) {
    if (
      delta?.ops[0]?.insert?.trimLeft()[0] === "·" ||
      delta?.ops[0]?.insert
        ?.trimLeft()
        ?.substring(0, 3)
        ?.match(/^(\d+\.\s)/)
    ) {
      return matchMsWordList(node, delta);
    }
  }

  return delta;
};
