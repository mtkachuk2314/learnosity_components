export const subtract = (
<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M7.30659 11C6.48138 11 6 11.382 6 12.0112C6 12.6404 6.51576 13 7.34097 13H16.659C17.4842 13 18 12.6404 18 12.0112C18 11.382 17.5186 11 16.6934 11H7.30659Z" fill="#232323"/>
</svg>
);
