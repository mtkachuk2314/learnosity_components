export const setZsign = (
<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M19 19V17.5818H10.6L19 6.41823V5H5.99556V6.41823H13.4415L5 17.5818V19H19ZM17.237 6.41823L8.77481 17.5818H6.7837L15.2459 6.41823H17.237Z" fill="#232323"/>
</svg>
);
