export const spacebar = (
<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M5 15C4.44772 15 4 14.5523 4 14V10.75C4 10.3358 4.33579 10 4.75 10C5.16421 10 5.5 10.3358 5.5 10.75V13.25H18.5V10.75C18.5 10.3358 18.8358 10 19.25 10C19.6642 10 20 10.3358 20 10.75V14C20 14.5523 19.5523 15 19 15H18.5H5.5H5Z" fill="#232323"/>
</svg>
);
