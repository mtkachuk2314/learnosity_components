import React, { useState, useRef, useEffect, forwardRef } from "react";

import Portal from "@mui/material/Portal";
import { useTranslation } from "react-i18next";
import { AppBar, Toolbar, IconButton, Tooltip } from "@mui/material";
import {
  useShowMath,
  useBoldRef,
  useLinkRef,
  useShowLink,
  useSetShowLink,
  useQuill,
  useSetLinkRange,
  useIsLink,
  useFormat,
} from "../Provider";

// ? Text Toolbar imports
import BoldDropdownButton from "./popupToolBar/BoldDropdownButton";
import ListDropdownButton from "./popupToolBar/ListDropdownButton";
import AlignDropdownButton from "./popupToolBar/AlignDropdownButton";
import MathDropdownButton from "./popupToolBar/MathDropdownButton";

import icons from "../assets/icons";

// import "react-quill/dist/quill.snow.css";
import "../styles/Toolbar.scss";

const CustomToolBar = forwardRef(function CustomToolBar(props, ref) {
  return (
    <div ref={ref}>
      {props?.portal?.shouldPortal && props?.portal?.toolbarReference ? (
        <Portal container={props?.portal?.toolbarReference}>
          <ToolBar {...props} />
        </Portal>
      ) : (
        <ToolBar {...props} />
      )}
    </div>
  );
});

const ToolBar = ({
  componentId,
  activeDropDownAlignItem,
  setActiveDropDownAlignItem,
  setSelectedIcon,
  portal,
  t,
}) => {
  const showMath = useShowMath();
  const showLink = useShowLink();
  const setShowLink = useSetShowLink();
  const quill = useQuill();
  const linkRef = useLinkRef();
  const setLinkRange = useSetLinkRange();
  const isLink = useIsLink();
  const format = useFormat();

  const [boldVisibility, setBoldVisibility] = useState(false);
  const [listVisibility, setListVisibility] = useState(false);
  const [alignVisibility, setAlignVisibility] = useState(false);
  const [mathVisibility, setMathVisibility] = useState(false);

  // Aria Live
  const [ariaLive, setAriaLive] = useState("");
  const [ariaLive2, setAriaLive2] = useState("");

  // IconBox
  const [openIcon, setIconOpen] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(null);
  const [openDescriptionKebab, setDescriptionKebabOpen] = useState(false);
  const [activeDropDownItem, setActiveDropDownItem] = useState("");
  const [activeTopMenu, setActiveTopMenu] = useState("");
  const [visibleAlignIcon, setVisibleAlignIcon] = useState(icons.align);

  const AppBarRef = useRef(null);
  // focus to the list and align. Bold Ref is found in EditorComponent.js
  const boldRef = useBoldRef();
  const listRef = useRef(null);
  const alignRef = useRef(null);
  const mathRef = useRef(null);
  // ? IconBox refs
  const IconDropDown = useRef(null);

  const handleCloseIcon = (event) => {
    if (IconDropDown.current && IconDropDown.current.contains(event.target)) {
      return;
    }
    setIconOpen(false);
  };

  const handleIconMenuItemClick = (e, index) => {
    setSelectedIndex(index);
    setSelectedIcon(iconDropdownOptions[index].type);
  };

  // ? Main Toolbar
  function handleListKeyDown(event) {
    if (event.key === "Tab") {
      event.preventDefault();
      setIconOpen(false);
    } else if (event.key === "Escape") {
      setIconOpen(false);
    }
  }

  const onKeyDropDown = (e, currentRef) => {
    if (e.key === "Escape") {
      currentRef.current.focus();
      setMathVisibility(false);
      setAlignVisibility(false);
      setListVisibility(false);
      setBoldVisibility(false);
      setActiveTopMenu(false);
    }
  };
  useEffect(() => {
    if (activeTopMenu === "link" && !isLink) {
      const selection = quill.getSelection();

      if (selection?.length > 0) {
        setLinkRange(selection);
        setShowLink(true);
      }
    }
  }, [activeTopMenu]);

  useEffect(() => {
    if (activeTopMenu === "link" && !showLink) {
      setActiveDropDownItem("");
      setActiveTopMenu("");
    }
    if (activeTopMenu === "" && (showLink || isLink)) {
      setActiveTopMenu("link");
    }
  }, [showLink, isLink]);

  useEffect(() => {
    if (!showMath) {
      setActiveDropDownItem("");
    }
    if (showMath && activeDropDownItem == "equationKeyboard") {
      setActiveDropDownItem("equationKeyboard");
    }
    if (showMath && activeDropDownItem == "imageConversion") {
      setActiveDropDownItem("imageConversion");
    }
  }, [showMath]);

  // Hndle Aria live region
  const handleAriaLive = (value) => {
    if (ariaLive === value) {
      setAriaLive("");
      setAriaLive2(value);
    } else {
      setAriaLive2("");
      setAriaLive(value);
    }
  };

  useEffect(() => {
    if (portal?.parentComponent === "image") {
      if (portal) {
        setActiveTopMenu("");
        setActiveTopMenu("");
        setActiveDropDownItem("");
        setBoldVisibility(false);
        setListVisibility(false);
        setAlignVisibility(false);
      }
    }
  }, [portal]);

  return (
    <div className="ToolbarContainer">
      <span
        className="sr-only"
        role="status"
        aria-live="assertive"
        aria-relevant="all"
        aria-atomic="true"
      >
        {ariaLive}
      </span>
      <span
        className="sr-only"
        role="status"
        aria-live="assertive"
        aria-relevant="all"
        aria-atomic="true"
      >
        {ariaLive2}
      </span>
      <AppBar
        className="StyledAppbar"
        position="static"
        ref={AppBarRef}
        elevation={0}
        style={{
          "--width": portal?.parentComponent === "video" ? "196px" : "auto",
          "--display": "flex",
          "--direction": "row",
          "--gap": "none",
        }}
      >
        {/* <div> */}
        <Toolbar
          id={componentId}
          data-testid="text-toolbar"
          position="static"
          className="StyledToolbar"
          style={{
            "--width":
              portal?.parentComponent === "video"
                ? "12.25rem"
                : portal?.parentComponent === "image"
                ? portal?.parentSection === "credit"
                  ? "auto"
                  : "9.5625rem"
                : "11.5rem",
            "--grid-template-columns": `${
              portal?.parentComponent === "video"
                ? "1fr 1fr 1fr 8px 1fr 8px 1fr"
                : portal?.parentComponent === "image" &&
                  portal?.parentSection === "description"
                ? "1fr 1fr 1fr 8px 1fr"
                : portal?.parentComponent === "image" &&
                  portal?.parentSection === "credit"
                ? "1fr"
                : "1fr 1fr 1fr 1fr 8px 1fr"
            }`,
            "--boxShadow":
              portal?.parentComponent === "image"
                ? "none"
                : "0px 0px 3px 0px rgba(35, 35, 35, 0.15),-1px 0px 5px 0px rgba(161, 162, 163, 0.2),3px 3px 4px 0px rgba(35, 35, 35, 0.15)",
            "--justify-items": "center",
            "--position": "center",
          }}
        >
          {/* Bold */}
          {(portal?.parentComponent !== "image" ||
            portal?.parentSection !== "credit") && (
            <Tooltip
              title={t("font styles")}
              describeChild
              placement="top"
              arrow
              style={{ position: "relative" }}
            >
              <IconButton
                ref={boldRef}
                className="StyledIconButton"
                style={{
                  "--active":
                    activeTopMenu === "bold" ? "rgba(21, 101, 192, 1)" : "#000",
                  "--background":
                    activeTopMenu === "bold"
                      ? "rgba(21, 101, 192, 0.12)"
                      : "#fff",
                }}
                disabled={portal?.disabledButtons?.includes("bold")}
                disableRipple
                color="inherit"
                onClick={() => {
                  setBoldVisibility((boldVisibility) => !boldVisibility);
                  setAlignVisibility(false);
                  setListVisibility(false);
                  setMathVisibility(false);
                  if (activeTopMenu === "bold") {
                    setActiveTopMenu("");
                  } else {
                    setActiveTopMenu("bold");
                  }
                  setActiveDropDownItem("");
                  boldVisibility === false
                    ? handleAriaLive(
                        t(
                          "Text formatting dropdown selected, 6 available options"
                        )
                      )
                    : handleAriaLive(t("Text formatting dropdown closed"));
                }}
                onKeyDown={(e) => {
                  onKeyDropDown(e, boldRef);
                }}
                id={`bold-${componentId}`}
                data-testid="formatting-dropdown"
                aria-label={t("formatting dropdown")}
              >
                {icons.customBold}
              </IconButton>
              <BoldDropdownButton
                t={t}
                show={boldVisibility}
                onKeyDropDown={(e) => {
                  onKeyDropDown(e, boldRef);
                }}
                isVideo={portal?.parentComponent === "video"}
              />
            </Tooltip>
          )}
          {portal?.parentComponent !== "video" &&
            portal?.parentComponent !== "image" && (
              <Tooltip
                title={t("equation")}
                describeChild
                placement="top"
                arrow
                style={{ position: "relative" }}
              >
                <IconButton
                  ref={mathRef}
                  className="StyledIconButton"
                  style={{
                    "--active":
                      activeTopMenu === "math"
                        ? "rgba(21, 101, 192, 1)"
                        : "#000",
                    "--background":
                      activeTopMenu === "math"
                        ? "rgba(21, 101, 192, 0.12)"
                        : "#fff",
                  }}
                  aria-label={t("math equation dropdown")}
                  disableRipple
                  color="inherit"
                  onClick={() => {
                    setAlignVisibility(false);
                    setBoldVisibility(false);
                    setListVisibility(false);
                    setMathVisibility(!mathVisibility);

                    if (activeTopMenu === "math") {
                      setActiveTopMenu("");
                    } else {
                      setActiveTopMenu("math");
                    }
                    mathVisibility === false
                      ? handleAriaLive(
                          t(
                            "Math equation dropdown selected, 3 available options"
                          )
                        )
                      : handleAriaLive(t("Math equation dropdown closed"));
                  }}
                >
                  {icons.formula}
                </IconButton>
                <MathDropdownButton
                  t={t}
                  show={mathVisibility}
                  aria-label={t("math buttons options")}
                  setActiveDropDownItem={setActiveDropDownItem}
                  activeDropDownItem={activeDropDownItem}
                  onKeyDropDown={(e) => {
                    onKeyDropDown(e, mathRef);
                  }}
                />
              </Tooltip>
            )}

          {/* alignment dropdown */}
          {(portal?.parentComponent !== "image" ||
            portal?.parentSection !== "credit") && (
            <Tooltip
              title={t("alignment")}
              describeChild
              placement="top"
              arrow
              style={{ position: "relative" }}
            >
              <IconButton
                ref={alignRef}
                disabled={portal?.disabledButtons?.includes("align")}
                disableRipple
                color="inherit"
                onClick={() => {
                  setAlignVisibility(!alignVisibility);
                  setBoldVisibility(false);
                  setListVisibility(false);
                  setMathVisibility(false);
                  if (activeTopMenu === "align") {
                    setActiveTopMenu("");
                  } else {
                    setActiveTopMenu("align");
                  }
                  setActiveDropDownItem("");
                  alignVisibility === false
                    ? handleAriaLive(
                        t(
                          "Alignment formatting dropdown selected, 3 available options"
                        )
                      )
                    : handleAriaLive(t("Alignment formatting dropdown closed"));
                }}
                className="StyledIconButton"
                style={{
                  "--active":
                    activeTopMenu === "align"
                      ? "rgba(21, 101, 192, 1)"
                      : "#000",
                  "--background":
                    activeTopMenu === "align"
                      ? "rgba(21, 101, 192, 0.12)"
                      : "#fff",
                }}
                aria-label={t("alignment dropdown")}
                value={visibleAlignIcon}
                data-alignid="alignment-dropdown"
                onKeyDown={(e) => {
                  onKeyDropDown(e, alignRef);
                }}
              >
                {visibleAlignIcon}
              </IconButton>
              <AlignDropdownButton
                t={t}
                show={alignVisibility}
                isVideo={portal?.parentComponent === "video"}
                isImage={portal?.parentComponent === "image"}
                aria-label={t("alignment options")}
                activeDropDownItem={activeDropDownAlignItem}
                setActiveDropDownItem={setActiveDropDownAlignItem}
                setVisibleAlignIcon={setVisibleAlignIcon}
                onKeyDropDown={(e) => {
                  onKeyDropDown(e, alignRef);
                }}
              />
            </Tooltip>
          )}
          {/* bullets drowdown starts */}
          {(portal?.parentComponent !== "image" ||
            portal?.parentSection !== "credit") && (
            <Tooltip
              title={t("add list")}
              describeChild
              placement="top"
              arrow
              style={{ position: "relative" }}
            >
              <IconButton
                ref={listRef}
                disabled={portal?.disabledButtons?.includes("list")}
                disableRipple
                color="inherit"
                onClick={() => {
                  setListVisibility(!listVisibility);
                  setAlignVisibility(false);
                  setBoldVisibility(false);
                  setMathVisibility(false);
                  if (activeTopMenu === "lists") {
                    setActiveTopMenu("");
                  } else {
                    setActiveTopMenu("lists");
                  }
                  alignVisibility === false
                    ? handleAriaLive(
                        t(
                          "List formatting dropdown selected, 2 available options"
                        )
                      )
                    : handleAriaLive(t("List formatting dropdown closed"));
                }}
                className="StyledIconButton"
                style={{
                  "--active":
                    activeTopMenu === "lists"
                      ? "rgba(21, 101, 192, 1)"
                      : "#000",
                  "--background":
                    activeTopMenu === "lists"
                      ? "rgba(21, 101, 192, 0.12)"
                      : "#fff",
                }}
                value="bullet"
                aria-label={t("list options")}
                onKeyDown={(e) => {
                  onKeyDropDown(e, listRef);
                }}
              >
                {icons.bullet}
              </IconButton>
              <ListDropdownButton
                t={t}
                show={listVisibility}
                isVideo={portal?.parentComponent === "video"}
                isImage={portal?.parentComponent === "image"}
                aria-label={t("list buttons dropdown")}
                onKeyDropDown={(e) => {
                  onKeyDropDown(e, listRef);
                }}
              />
            </Tooltip>
          )}
          {(portal?.parentComponent !== "image" ||
            portal?.parentSection !== "credit") && (
            <div className="StyledDivider" />
          )}
          {/* link btn and divider */}
          <Tooltip title={t("link")} describeChild placement="top" arrow>
            <IconButton
              ref={linkRef}
              disabled={portal?.disabledButtons?.includes("link")}
              disableRipple
              color="inherit"
              aria-label={t("add hyperlink")}
              className="StyledIconButton"
              style={{
                "--active":
                  activeTopMenu === "link" ? "rgba(21, 101, 192, 1)" : "#000",
                "--background":
                  activeTopMenu === "link"
                    ? "rgba(21, 101, 192, 0.12)"
                    : "#fff",
              }}
              onClick={() => {
                setAlignVisibility(false);
                setBoldVisibility(false);
                setListVisibility(false);
                setMathVisibility(false);

                if (format?.link && activeTopMenu === "link") {
                  const selection = quill.getSelection();
                  quill.formatText(selection, "link", false);
                  setActiveTopMenu("");
                  return;
                }

                setActiveTopMenu(activeTopMenu === "link" ? "" : "link");
              }}
              sx={{ paddingLeft: "12px", paddingRight: "12px" }}
            >
              {icons.link}
            </IconButton>
          </Tooltip>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default React.memo(CustomToolBar);
