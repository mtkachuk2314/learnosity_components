import React, { useState, useEffect } from "react";
import "react-quill/dist/quill.snow.css";
import { Card } from "@mui/material";
import { Tooltip } from "@material-ui/core";

import { useQuill, useFormat } from "../../Provider";

import icons from "../../assets/icons";
import "../../styles/Toolbar.scss";

const ListDropdownButton = ({ t, show, onKeyDropDown, isVideo, isImage }) => {
  const quill = useQuill();
  const format = useFormat();

  const [activeDropDownList, setActiveDropDownList] = useState("");

  useEffect(() => {
    format?.list
      ? setActiveDropDownList(format.list)
      : setActiveDropDownList("");
  }, [format]);
  return (
    <Card
      show={show}
      isVideo={isVideo}
      onKeyDown={onKeyDropDown}
      className="StyledCard"
      style={{
        "--card-display": show ? "flex" : "none",
        "--left": "-0.3125rem",
        "--width": "4.875rem",
        "--box-shadow":
          "0px 1px 3px 0px rgba(35, 35, 35, 0.1), 0px 2px 5px 0px rgba(161, 162, 163, 0.2), 3px 3px 4px 0px rgba(35, 35, 35, 0.15)",
        top: "2.375rem",
      }}
    >
      <Tooltip
        aria-label={t("bullets")}
        title={t("bullets")}
        placement="top"
        arrow
      >
        <button
          aria-label={t("bullet list")}
          className="StyledIconButton"
          style={{
            "--active":
              activeDropDownList === "bullet"
                ? "rgba(21, 101, 192, 1)"
                : "#000",
            "--background":
              activeDropDownList == "bullet"
                ? "rgba(21, 101, 192, 0.12)"
                : "#fff",
          }}
          value="bullet"
          onClick={() => {
            if (activeDropDownList === "bullet") {
              setActiveDropDownList("");
              quill.format("list", false);
            } else {
              setActiveDropDownList("bullet");
              quill.format("list", "bullet");
            }
          }}
        >
          {icons.bullet}
        </button>
      </Tooltip>

      <Tooltip
        aria-label={t("numbering")}
        title={t("numbering")}
        placement="top"
        arrow
      >
        <button
          aria-label={t("numbered list")}
          className="StyledIconButton"
          style={{
            "--active":
              activeDropDownList === "ordered"
                ? "rgba(21, 101, 192, 1)"
                : "#000",
            "--background":
              activeDropDownList == "ordered"
                ? "rgba(21, 101, 192, 0.12)"
                : "#fff",
          }}
          value="ordered"
          onClick={() => {
            if (activeDropDownList === "ordered") {
              setActiveDropDownList("");
              quill.format("list", false);
            } else {
              setActiveDropDownList("ordered");
              quill.format("list", "ordered");
            }
          }}
        >
          {icons.ordered}
        </button>
      </Tooltip>
    </Card>
  );
};

export default ListDropdownButton;
