import React, { useEffect } from "react";
import "react-quill/dist/quill.snow.css";
import { Card } from "@mui/material";
import { Tooltip } from "@material-ui/core";
import "../../styles/Toolbar.scss";

import { useQuill, useFormat } from "../../Provider";

import icons from "../../assets/icons";

const AlignDropdownButton = ({
  t,
  isVideo,
  isImage,
  show,
  activeDropDownItem,
  setActiveDropDownItem,
  setVisibleAlignIcon,
  onKeyDropDown,
}) => {
  const quill = useQuill();
  const format = useFormat();
  useEffect(() => {
    if (format?.align) {
      setActiveDropDownItem(format.align);
      setVisibleAlignIcon(icons[format.align]);
    } else {
      setActiveDropDownItem("left");
      setVisibleAlignIcon(icons.align);
    }
  }, [format]);

  return (
    <Card
      show={show}
      isVideo={isVideo}
      className="StyledCard"
      style={{
        "--card-display": show ? "flex" : "none",
        "--left": "-0.3125rem",
        "--width": "7rem",
        "--box-shadow":
          "0px 1px 3px 0px rgba(35, 35, 35, 0.1), 0px 2px 5px 0px rgba(161, 162, 163, 0.2), 3px 3px 4px 0px rgba(35, 35, 35, 0.15)",
        top: "2.375rem",
      }}
      onKeyDown={onKeyDropDown}
    >
      <Tooltip
        aria-label={t("align left")}
        title={t("align left")}
        placement="top"
        arrow
      >
        <button
          aria-label={t("align left")}
          onClick={() => {
            setActiveDropDownItem("left");
            setVisibleAlignIcon(icons.align);
            quill.format("align", false);
          }}
          className="StyledIconButton"
          style={{
            "--active":
              activeDropDownItem === "left" ? "rgba(21, 101, 192, 1)" : "#000",
            "--background":
              activeDropDownItem == "left"
                ? "rgba(21, 101, 192, 0.12)"
                : "#fff",
          }}
          value=""
        >
          {icons.align}
        </button>
      </Tooltip>
      <Tooltip
        aria-label={t("align center")}
        title={t("align center")}
        placement="top"
        arrow
      >
        <button
          aria-label={t("align center")}
          className="StyledIconButton"
          style={{
            "--active":
              activeDropDownItem === "center"
                ? "rgba(21, 101, 192, 1)"
                : "#000",
            "--background":
              activeDropDownItem == "center"
                ? "rgba(21, 101, 192, 0.12)"
                : "#fff",
          }}
          value="center"
          onClick={() => {
            if (activeDropDownItem === "center") {
              setActiveDropDownItem("left");
              setVisibleAlignIcon(icons.align);
              quill.format("align", false);
            } else {
              setActiveDropDownItem("center");
              setVisibleAlignIcon(icons.center);
              quill.format("align", "center");
            }
          }}
        >
          {icons.center}
        </button>
      </Tooltip>
      <Tooltip
        aria-label={t("align right")}
        title={t("align right")}
        placement="top"
        arrow
      >
        <button
          aria-label={t("align right")}
          className="StyledIconButton"
          style={{
            "--active":
              activeDropDownItem === "right" ? "rgba(21, 101, 192, 1)" : "#000",
            "--background":
              activeDropDownItem == "right"
                ? "rgba(21, 101, 192, 0.12)"
                : "#fff",
          }}
          value="right"
          onClick={() => {
            if (activeDropDownItem === "right") {
              setActiveDropDownItem("left");
              setVisibleAlignIcon(icons.align);
              quill.format("align", false);
            } else {
              setActiveDropDownItem("right");
              setVisibleAlignIcon(icons.right);
              quill.format("align", "right");
            }
          }}
        >
          {icons.right}
        </button>
      </Tooltip>
    </Card>
  );
};

export default AlignDropdownButton;
