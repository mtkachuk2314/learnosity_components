import React, { useState, useEffect } from "react";
import { Card } from "@mui/material";
import { Tooltip } from "@material-ui/core";
import "../../styles/Toolbar.scss";

import { MathKeyboard, MathDraw, MathImageUpload } from "../../assets/icons";

import {
  useSetShowMath,
  useShowMath,
  useSetMathpixOption,
  useMathpixOption,
} from "../../Provider";

const MathDropdownButton = ({
  t,
  show,
  setActiveDropDownItem,
  activeDropDownItem,
}) => {
  const setShowMath = useSetShowMath();
  const showMath = useShowMath();
  const mathpixOption = useMathpixOption();
  const setMathpixOption = useSetMathpixOption();
  const [ariaLive, setAriaLive] = useState("");
  const [ariaLive2, setAriaLive2] = useState("");
  // Aria Live Handler
  const handleAriaLive = (value) => {
    if (ariaLive === value) {
      setAriaLive("");
      setAriaLive2(value);
    } else {
      setAriaLive2("");
      setAriaLive(value);
    }
  };

  useEffect(() => {
    setMathpixOption(activeDropDownItem);
  }, [activeDropDownItem]);

  return (
    <Card
      show={show}
      className="StyledCard"
      style={{
        "--card-display": show ? "flex" : "none",
        "--left": "-0.3125rem",
        "--width": "7rem",
        "--box-shadow":
          "0px 1px 3px 0px rgba(35, 35, 35, 0.1), 0px 2px 5px 0px rgba(161, 162, 163, 0.2), 3px 3px 4px 0px rgba(35, 35, 35, 0.15)",
        top: "2.375rem",
      }}
      // onKeyDown={onKeyDropDown}
    >
      {/* Aria Live */}
      <span
        className="sr-only"
        role="status"
        aria-live="assertive"
        aria-relevant="all"
        aria-atomic="true"
      >
        {ariaLive}
      </span>
      <span
        className="sr-only"
        role="status"
        aria-live="assertive"
        aria-relevant="all"
        aria-atomic="true"
      >
        {ariaLive2}
      </span>
      <Tooltip
        aria-label={t("draw equation")}
        title={t("draw equation")}
        placement="top"
        arrow
      >
        <button
          aria-label={t("draw equation")}
          className="StyledIconButton"
          style={{
            "--active":
              mathpixOption === "mathDraw" ? "rgba(21, 101, 192, 1)" : "#000",
            "--background":
              mathpixOption == "mathDraw" ? "rgba(21, 101, 192, 0.12)" : "#fff",
          }}
          onClick={() => {
            if (activeDropDownItem === "mathDraw" && showMath) {
              setActiveDropDownItem("");
              setShowMath(false);
            } else {
              setActiveDropDownItem("mathDraw");
              setShowMath(true);
            }
            handleAriaLive(t("mathDraw keyboard is now open."));
          }}
          value="mathDraw"
        >
          {MathDraw}
        </button>
      </Tooltip>
      <Tooltip
        aria-label={t("image conversion")}
        title={t("image conversion")}
        placement="top"
        arrow
      >
        <button
          aria-label={t("image conversion")}
          className="StyledIconButton"
          style={{
            "--active":
              mathpixOption === "imageConversion"
                ? "rgba(21, 101, 192, 1)"
                : "#000",
            "--background":
              mathpixOption == "imageConversion"
                ? "rgba(21, 101, 192, 0.12)"
                : "#fff",
          }}
          onClick={() => {
            if (activeDropDownItem === "imageConversion" && showMath) {
              setActiveDropDownItem("");
              setShowMath(false);
            } else {
              setActiveDropDownItem("imageConversion");
              setShowMath(true);
            }
            handleAriaLive(t("image conversion keyboard is now open."));
          }}
          value="imageConversion"
        >
          {MathImageUpload}
        </button>
      </Tooltip>
      <Tooltip
        aria-label={t("equation keyboard")}
        title={t("equation keyboard")}
        placement="top"
        arrow
      >
        <button
          aria-label={t("equation keyboard")}
          className="StyledIconButton"
          style={{
            "--active":
              mathpixOption === "equationKeyboard"
                ? "rgba(21, 101, 192, 1)"
                : "#000",
            "--background":
              mathpixOption == "equationKeyboard"
                ? "rgba(21, 101, 192, 0.12)"
                : "#fff",
          }}
          onClick={() => {
            if (activeDropDownItem === "equationKeyboard" && showMath) {
              setActiveDropDownItem("");
              setShowMath(false);
            } else {
              setActiveDropDownItem("equationKeyboard");
              setShowMath(true);
            }
            handleAriaLive(t("Math equation keyboard is now open."));
          }}
          value="equationKeyboard"
        >
          {MathKeyboard}
        </button>
      </Tooltip>
    </Card>
  );
};

export default MathDropdownButton;
