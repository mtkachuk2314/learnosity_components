import React, { useState, useEffect } from "react";
import "react-quill/dist/quill.snow.css";
import { Card } from "@mui/material";
import { Tooltip } from "@material-ui/core";
import icons from "../../assets/icons";
import "../../styles/Toolbar.scss";

import { useQuill, useFormat } from "../../Provider";

const BoldDropdownButton = ({ t, show, onKeyDropDown, isVideo }) => {
  const quill = useQuill();
  const format = useFormat();

  const [active, setActive] = useState({
    bold: false,
    italic: false,
    underline: false,
    strike: false,
    sub: false,
    super: false,
  });

  useEffect(() => {
    if (format) {
      const formatList = [];

      Object.keys(format).forEach((key) => {
        key === "script"
          ? formatList.push(format.script)
          : formatList.push(key);
      });

      Object.keys(active).forEach((key) =>
        setActive((prev) => ({ ...prev, [key]: formatList.includes(key) }))
      );
    }
  }, [format]);

  const handleFormat = (type) => {
    /* Creating a copy of the active object. */
    let temp = { ...active };

    /* Checking if the type is either sub or super. */
    const isScript = ["sub", "super"].includes(type);

    /* Checking if the type is either sub or super. */
    if (isScript) {
      /* Setting the sub and super to true or false depending on the type. */
      temp.sub = type === "sub";
      temp.super = type === "super";

      /* Setting the script to false. */
      quill.format("script", false);
    }

    /* Checking if the type is active. */
    const isActive = active[type];

    /* Creating a copy of the active object and then setting the type to the opposite of what it was. */
    temp = { ...temp, [type]: !isActive };

    /* Checking if the type is either sub or super. If it is, it is setting the script to false. */
    isScript && quill.format("script", isActive ? false : type);

    /* Formatting the text. */
    quill.format(type, !isActive);

    /* Setting the state of the active object. */
    setActive(temp);
  };

  return (
    <Card
      show={show}
      isVideo={isVideo}
      className="StyledCard"
      data-testid="bold-dropdown"
      style={{
        "--card-display": show ? "flex" : "none",
        "--left": "-0.3125rem",
        "--width": "13.375rem",
        "--box-shadow":
          "0px 1px 3px 0px rgba(35, 35, 35, 0.1), 0px 2px 5px 0px rgba(161, 162, 163, 0.2), 3px 3px 4px 0px rgba(35, 35, 35, 0.15)",
        top: "2.375rem",
      }}
      onKeyDown={onKeyDropDown}
    >
      <Tooltip aria-label={t("bold")} title={t("bold")} placement="top" arrow>
        <button
          aria-label={t("bold")}
          data-testid="bold-toolbar"
          className="StyledIconButton bold"
          style={{
            "--active": active.bold ? "rgba(21, 101, 192, 1)" : "#000",
            "--background": active.bold ? "rgba(21, 101, 192, 0.12)" : "#fff",
          }}
          onClick={(e) => handleFormat("bold")}
        >
          {icons.customBold}
        </button>
      </Tooltip>
      <Tooltip
        aria-label={t("italic")}
        title={t("italic")}
        placement="top"
        arrow
      >
        <button
          aria-label={t("italic")}
          className="StyledIconButton"
          style={{
            "--active": active.italic ? "rgba(21, 101, 192, 1)" : "#000",
            "--background": active.italic ? "rgba(21, 101, 192, 0.12)" : "#fff",
          }}
          onClick={(e) => handleFormat("italic")}
        >
          {icons.italic}
        </button>
      </Tooltip>
      <Tooltip
        aria-label={t("underline")}
        title={t("underline")}
        placement="top"
        arrow
      >
        <button
          aria-label={t("underline")}
          className="StyledIconButton"
          style={{
            "--active": active.underline ? "rgba(21, 101, 192, 1)" : "#000",
            "--background": active.underline
              ? "rgba(21, 101, 192, 0.12)"
              : "#fff",
          }}
          onClick={(e) => handleFormat("underline")}
        >
          {icons.underline}
        </button>
      </Tooltip>
      <Tooltip
        aria-label={t("strikethrough")}
        title={t("strikethrough")}
        placement="top"
        arrow
      >
        <button
          aria-label={t("strikethrough")}
          className="StyledIconButton"
          style={{
            "--active": active.strike ? "rgba(21, 101, 192, 1)" : "#000",
            "--background": active.strike ? "rgba(21, 101, 192, 0.12)" : "#fff",
          }}
          onClick={(e) => handleFormat("strike")}
        >
          {icons.strike}
        </button>
      </Tooltip>
      <Tooltip
        aria-label={t("subscript")}
        title={t("subscript")}
        placement="top"
        arrow
      >
        <button
          aria-label={t("subscript")}
          className="StyledIconButton"
          style={{
            "--active": active.sub ? "rgba(21, 101, 192, 1)" : "#000",
            "--background": active.sub ? "rgba(21, 101, 192, 0.12)" : "#fff",
          }}
          onClick={(e) => handleFormat("sub")}
          value="sub"
        >
          {icons.script}
        </button>
      </Tooltip>
      <Tooltip
        aria-label={t("superscript")}
        title={t("superscript")}
        placement="top"
        arrow
      >
        <button
          aria-label={t("superscript")}
          className="StyledIconButton"
          style={{
            "--active": active.super ? "rgba(21, 101, 192, 1)" : "#000",
            "--background": active.super ? "rgba(21, 101, 192, 0.12)" : "#fff",
          }}
          onClick={(e) => handleFormat("super")}
          value="super"
        >
          {icons.super}
        </button>
      </Tooltip>
    </Card>
  );
};

export default BoldDropdownButton;
