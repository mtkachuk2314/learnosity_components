import React from "react";
import MathpixEqation from "./MathpixOptions/MathpixEquation";
import MathpixDraw from "./MathpixOptions/MathpixDraw/MathpixDraw";
import ImageConversion from "./MathpixOptions/ImageConversion/ImageConversion";

const MathPopup = ({ mathOption }) => {
  return (
    <>
      {mathOption === "equationKeyboard" && <MathpixEqation />}
      {mathOption === "mathDraw" && <MathpixDraw />}
      {mathOption === "imageConversion" && <ImageConversion />}
    </>
  );
};

export default MathPopup;
