import React, { useEffect } from "react";
import { useCanvas } from "./CanvasContext";
import { UndoButton, RedoButton, CanvasPlaceholder, ClearCanvasButton } from "./Utils";
import "../../canvas.scss"

const CanvasInternal = () => {
    const { canvasRef, prepareCanvas, placeholderText } = useCanvas();

    useEffect(() => {
        prepareCanvas();
    }, []);

    return (
        <div className="canvas-draw-container">
            <canvas ref={canvasRef} className="canvas-draw" aria-label="mathpix-draw-canvas"
                style={{
                    border: !placeholderText ? "2px solid #1565C0" : "none"
                }} />
            <CanvasPlaceholder />
            <div className="canvas-buttons-container">
                <UndoButton />
                <RedoButton />
                <ClearCanvasButton />
            </div>
        </div>
    );
};

export default CanvasInternal