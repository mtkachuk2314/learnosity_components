import React, { useRef, useEffect, useState } from "react";
import CloseIcon from "@mui/icons-material/Close";
// import { useDrag, useDragDropManager } from "react-dnd";
// import { getEmptyImage } from "react-dnd-html5-backend";

import {
  useUniqueId,
  useSetShowMath,
  useSetEditFormula,
} from "../../../../../Provider";
import CanvasInternal from "./canvas/CanvasInternal";
import { CanvasProvider } from "./canvas/CanvasContext";
import { LatexRenderer, CopyToClipboardButton } from "./canvas/Utils";
import { useOnClickOutside } from "../../../../../../../hooks/useOnClickOutside";
import "../canvas.scss";

const MathpixDraw = () => {
  const containerRef = useRef(null);
  const uniqueId = useUniqueId();
  const setMathShow = useSetShowMath();
  const setEditFormula = useSetEditFormula();
  const [coords, setCoords] = useState(null);

  const closeMath = () => {
    setEditFormula({ value: null, id: null });
    setMathShow(false);
  };

  useOnClickOutside(containerRef, closeMath);

  // const [{ isDragging }, drag, preview] = useDrag({
  //   type: "math",
  //   collect: (monitor) => ({
  //     isDragging: monitor.isDragging(),
  //     offset: monitor.getSourceClientOffset(),
  //   }),
  // });

  // const dragDropManager = useDragDropManager();
  // const monitor = dragDropManager.getMonitor();

  // useEffect(
  //   () =>
  //     monitor.subscribeToOffsetChange(() => {
  //       const offset = monitor.getClientOffset();
  //       offset && setCoords(offset);
  //     }),
  //   [monitor]
  // );

  // useEffect(() => {
  //   preview(getEmptyImage(), { captureDraggingState: true });
  // }, [])

  return (
    <div
      style={{
        position: "fixed",
        width: "592px",
        top: coords ? `${coords.y}px` : "165px",
        left: coords ? `${coords.x}px` : "50%",
        transform: "translateX(-50%)",
        zIndex: 2000,
        backgroundColor: "#fff",
        display: "flex",
        flexDirection: "column",
        borderTop: "4px solid #1565C0",
        boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.1)",
        borderRadius: "4px",
      }}
      ref={containerRef}
      tabIndex={0}
      aria-label="virtual keyboard"
      id={`mathpopup-${uniqueId}`}
      role="math"
      onKeyDown={(e) => {
        if (e.key === "Escape") {
          closeMath();
        }
      }}
    >
      <div className="mathpix-draw-header">
        <span>Convert Drawing into an equation</span>
        <button
          aria-label="close math popup"
          autoFocus
          style={{
            border: "none",
            backgroundColor: "transparent",
            cursor: "pointer",
            padding: "0px 21.18px 16px 0px",
          }}
          onClick={closeMath}
          onKeyDown={(e) => {
            if (e.key === "Enter") {
              closeMath();
            }
          }}
        >
          <CloseIcon alt="close modal" />
        </button>
      </div>
      <div
        style={{
          margin: "16px 16px 0px 16px",
        }}
      >
        <CanvasProvider>
          <LatexRenderer />
          <CanvasInternal />
          <div className="insert-button-container">
            <CopyToClipboardButton />
          </div>
        </CanvasProvider>
      </div>
    </div>
  );
};

export default MathpixDraw;
