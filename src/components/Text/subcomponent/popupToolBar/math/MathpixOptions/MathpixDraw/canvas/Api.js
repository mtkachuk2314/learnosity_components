import axios from "axios";

const getToken = () => localStorage.getItem("accessToken");

const getConfigMathpix = async (authoringData, exitLoop) => {
  try {
    const response = await axios.get(
      process.env.REACT_APP_MATHPIX_TOKEN_ENDPOINT,
      { headers: { authorizationToken: `Bearer ${getToken()}` } }
    );
    return response;
  } catch (error) {
    if (exitLoop) throw error;
    await authoringData.refreshSession();
    return getConfigMathpix(authoringData, true);
  }
};

const getConfigWithToken = (tokenContext) => {
  const config = {
    headers: {
      app_token: tokenContext.app_token,
      strokes_session_id: tokenContext.strokes_session_id,
      "Content-Type": "application/json",
    },
  };
  return config;
};

export const getStrokesToken = async (authoringData) => {
  const configMathpix = await getConfigMathpix(authoringData, false);
  if (configMathpix.status === 200) {
    return {
      app_token: configMathpix.data.app_token,
      strokes_session_id: configMathpix.data.strokes_session_id,
      app_token_expires_at: configMathpix.data.app_token_expires_at,
    };
  }
  return null;
};

export const getLatex = async (tokenContext, strokes) => {
  const config = getConfigWithToken(tokenContext);
  var X = [];
  strokes.map((stroke) => {
    X.push(stroke.points.map((point) => point.x));
  });
  var Y = [];
  strokes.map((stroke) => {
    Y.push(stroke.points.map((point) => point.y));
  });

  return axios.post(
    "https://api.mathpix.com/v3/strokes",
    {
      strokes: {
        strokes: {
          x: X,
          y: Y,
        },
      },
    },
    config
  );
};
