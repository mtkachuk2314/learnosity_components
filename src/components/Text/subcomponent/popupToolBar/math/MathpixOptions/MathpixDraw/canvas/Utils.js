import React from "react";
import { MathpixLoader, MathpixMarkdown } from "mathpix-markdown-it";
import { IconButton } from "@mui/material";
import Tooltip from "@mui/material/Tooltip";
import { useCanvas } from "./CanvasContext";
import { useQuill, useSetShowMath } from "../../../../../../Provider";
import {
  CanvasUndo,
  CanvasRedo,
  CanvasDelete,
  CanvasDraw,
} from "../../../../../../assets/icons";
import "../../canvas.scss";

export const ClearCanvasButton = () => {
  const { clearCanvas, strokes } = useCanvas();
  const handleClick = () => {
    clearCanvas(false);
  };
  return (
    <Tooltip title="Clear Drawing">
      <IconButton
        disableRipple
        onClick={handleClick}
        disabled={strokes.length === 0}
        color="primary"
        alt="clear canvas"
      >
        {CanvasDelete}
      </IconButton>
    </Tooltip>
  );
};

export const LatexRenderer = () => {
  const { latex } = useCanvas();
  return (
    <div className="latex-renderer-container">
      <MathpixLoader notScrolling={true}>
        <MathpixMarkdown text={latex.code} />
      </MathpixLoader>
    </div>
  );
};

export const UndoButton = () => {
  const { undoHistory, undo, strokes } = useCanvas();
  const handleClick = () => {
    undo();
  };

  return (
    <Tooltip title="Undo">
      <IconButton
        disableRipple
        onClick={handleClick}
        disabled={strokes.length === 0}
        color="primary"
        alt="undo canvas"
      >
        {CanvasUndo}
      </IconButton>
    </Tooltip>
  );
};

export const RedoButton = () => {
  const { redoHistory, redo } = useCanvas();

  const handleClick = () => {
    redo();
  };

  return (
    <Tooltip title="Redo">
      <IconButton
        disableRipple
        onClick={handleClick}
        disabled={redoHistory.length === 0}
        color="primary"
        alt="redo canvas"
      >
        {CanvasRedo}
      </IconButton>
    </Tooltip>
  );
};

export const CanvasPlaceholder = () => {
  const { placeholderText } = useCanvas();
  return (
    <div
      className="CanvasPlaceholder"
      style={!placeholderText ? { display: "none" } : { display: "flex" }}
    >
      {CanvasDraw}
      <span>Start drawing here</span>
    </div>
  );
};

export const CopyToClipboardButton = () => {
  const { latex, setUndoHistory, clearCanvas, strokes } = useCanvas();
  const setMathShow = useSetShowMath();
  const textLength = latex.code.substring(4, latex.code.length - 2);

  const quill = useQuill();

  const splitMultiLineEquation = (latexInput) => {
    const blocks = []
    if (latexInput.includes("\n")) {
      const multiLineEquationLatex = latexInput.slice('\\begin{array}{l}'.length, -'\\end{array}'.length).trim().replace("\n", "")
      const equations = multiLineEquationLatex.split('\\\\').reverse()
      for (const equation of equations) {
        blocks.push({
          type: "newLine",
          content: "\n"
        })
        blocks.push({
          type: "block",
          content: equation.replace(/\\\\/g, '').trim()
        });
      }

      // Now Convert Stirg into multiple equation
    } else {
      blocks.push({
        type: "inline",
        content: latexInput.replace(/\\\\/g, '').trim()
      });
    }

    return blocks
  }

  const insertQuill = (input) => {
    const range = quill.getSelection(true);

    const multiLineEquation = splitMultiLineEquation(input)
    const format = quill.getFormat();
    // checking if the list has ordered or bullet
    if (!format) {
      quill.removeFormat(
        range?.index, range?.length
      );
    } else if (
      format?.list !== "ordered" &&
      format?.list !== "bullet" &&
      format.align !== "center" &&
      format.align !== "right"
    ) {
      quill.removeFormat(
        range?.index, range?.length
      );
    }

    for (const block of multiLineEquation) {
      if (block.type === 'block' || block.type === 'inline') {
        quill.insertEmbed(range.index, "mathpix", block.content);
        quill.setSelection(range.index + range.length + 1);
      } else {
        quill.insertText(range.index, block.content);
        quill.setSelection(range.index + block.content.length);
      }
    }
  };
  const handleClick = () => {
    const latexCode = latex.code.substring(2, latex.code.length - 2);
    insertQuill(latexCode);
    setUndoHistory([]);
    clearCanvas(false);
    setMathShow(false);
  };
  return (
    // <>
    //   <div className="Insert-Button">
    //     <button
    //       onClick={handleClick}
    //       color="primary"
    //       disabled={strokes.length === 0}
    //       className="insert-button"
    //       alt="insert draw equation"
    //     >
    //       {"Insert"}
    //     </button>
    //   </div>
    // </>
    <>
      {textLength === "ext{}" && (
        <div className="Insert-Button">
          <span
            disabled
            style={{
              color: 'rgba(0,0,0,0.38)',
              fontWeight: '500',
              fontSize: '14px',
              lineHeight: '24px',
              letterSpacing: '0.4px'
            }}
            className="insert-disable-button"
          >
            {"Insert"}
          </span>
        </div>
      )}
      {textLength != "ext{}" && (
        <div className="Insert-Button">
          <button
            onClick={handleClick}
            color="primary"
            className="insert-button"
            alt="insert draw equation"
          >
            {"Insert"}
          </button>
        </div>
      )}
    </>
  );
};
