import React, { useRef, useState, useEffect } from "react";

import { InlineMath } from "react-katex";

import Tab from "@mui/material/Tab";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";

import CloseIcon from "@mui/icons-material/Close";
import {
  useQuill,
  useSetShowMath,
  useUniqueId,
  useEditFormula,
  useSetEditFormula,
} from "../../../../Provider";

import { useOnClickOutside } from "../../../../../../hooks/useOnClickOutside";
import PlaceholderError from "../../../../../../Utility/PlaceholderError";

import MathLiveEditor from "../MathLiveEditor";
import "../../../../styles/MathEquation.scss";

import Basic from "../keys/Basic";
import Operator from "../keys/Operator";
import Greek from "../keys/Greek";
import Sets from "../keys/Sets";
import Trig from "../keys/Trig";
import { TopSideKeys, BottomLeftKeys } from "../keys/SideKeys";

const MathpixEqation = () => {
  const quill = useQuill();
  const setMathShow = useSetShowMath();
  const uniqueId = useUniqueId();
  const editFormula = useEditFormula();
  const setEditFormula = useSetEditFormula();

  const isEdit = Boolean(editFormula.value);

  const [value, setValue] = useState("1");
  const [formula, setFormula] = useState(null);
  const [ariaLive, setAriaLive] = useState("");
  const [ariaLive2, setAriaLive2] = useState("");
  const [coords, setCoords] = useState(null);
  const [showError, setShowError] = useState(false);

  const mathfield = useRef(null);
  const containerRef = useRef(null);

  const tabs = [
    { label: "Basic", render: Basic },
    { label: "Operator", render: Operator },
    { label: "Greek", render: Greek },
    { label: "Sets", render: Sets },
    { label: "Trig", render: Trig },
  ];

  const closeMath = () => {
    setEditFormula({ value: null, id: null });
    setMathShow(false);
  };

  useOnClickOutside(containerRef, closeMath);

  const handleChange = (e, newValue) => {
    setValue(newValue);
  };

  // const [{ isDragging }, drag, preview] = useDrag({
  //     type: "math",
  //     collect: (monitor) => ({
  //         isDragging: monitor.isDragging(),
  //         offset: monitor.getSourceClientOffset(),
  //     }),
  // });

  // const dragDropManager = useDragDropManager();
  // const monitor = dragDropManager.getMonitor();

  // useEffect(
  //     () =>
  //         monitor.subscribeToOffsetChange(() => {
  //             const offset = monitor.getClientOffset();
  //             offset && setCoords(offset);
  //         }),
  //     [monitor]
  // );

  const handleClick = () => {
    const range = quill.getSelection(true);
    const equationToInsert = mathfield.current.getValue("latext-expanded");
    if (equationToInsert.includes("placeholder{}")) {
      setShowError(true);
    } else {
      const format = quill.getFormat();
      // checking if the list has ordered or bullet
      if (!format || format?.align === "center" || format?.align === "right") {
        quill.removeFormat(
          range.index,
          isEdit ? range.length + 1 : range.length
        );
        if (
          format &&
          (format?.align === "center" || format?.align === "right")
        ) {
          quill.format("align", format.align);
          if (format?.list === "ordered" || format?.list === "bullet") {
            quill.format("list", format.list);
          }
        }
      } else if (
        format?.list !== "ordered" &&
        format?.list !== "bullet" &&
        format.align !== "center" &&
        format.align !== "right"
      ) {
        quill.removeFormat(
          range.index,
          isEdit ? range.length + 1 : range.length
        );
      }
      setShowError(false);
      quill.insertEmbed(range.index, "mathpix", equationToInsert);
      quill.setSelection(range.index + range.length + 1);
      closeMath();
    }
  };

  // Aria Live Handler
  const handleAriaLive = (ariaValue) => {
    if (ariaLive === ariaValue) {
      setAriaLive("");
      setAriaLive2(ariaValue);
    } else {
      setAriaLive2("");
      setAriaLive(ariaValue);
    }
  };

  useEffect(() => {
    containerRef.current?.focus();
    document.body.style.setProperty("--selection-background-color", "#A1CAF1");
    document.body.style.setProperty("--placeholder-color", "#000000");
    document.body.style.setProperty("--selection-color", "#000000");
    mathfield.current.setValue("");

    isEdit && mathfield.current.setValue(editFormula.value);

    // preview(getEmptyImage(), { captureDraggingState: true });

    mathfield.current.setOptions({
      mathModeSpace: "\\:",
      plonkSound: false,
      keypressSound: false,
    });
    mathfield.current.focus();
  }, []);

  // Manual Keystroke for keyboard
  const KeyStroke = (event) => {
    if (
      (event.keyCode >= 48 && event.keyCode <= 57) ||
      (event.keyCode >= 65 && event.keyCode <= 90) ||
      (event.keyCode >= 96 && event.keyCode <= 105)
    ) {
      mathfield.current.insert(event.key);
    } else if (
      event.key == "Backspace" ||
      event.key == "Delete" ||
      event.code === 8
    ) {
      mathfield.current.executeCommand("deleteBackward");
    } else if (event.key === ".") {
      mathfield.current.insert(".");
    } else if (event.key === "+") {
      mathfield.current.insert("+");
    } else if (event.key === "-") {
      mathfield.current.insert("-");
    } else if (event.key === "*") {
      mathfield.current.insert("\\times");
    } else if (event.key === "/") {
      mathfield.current.insert("\\div");
    } else if (event.key === "%") {
      mathfield.current.insert("%");
    } else if (event.key === "^") {
      mathfield.current.insert("^");
    } else if (event.key === "(") {
      mathfield.current.insert("(");
    } else if (event.key === ")") {
      mathfield.current.insert(")");
    } else if (event.key === "=") {
      mathfield.current.insert("=");
    }
  };
  return (
    // Equation Keyboard Container
    <div
      style={{
        position: "fixed",
        width: "37.5rem",
        height: "34.8125rem",
        top: coords ? `${coords.y}px` : "165px",
        left: coords ? `${coords.x}px` : "50%",
        transform: "translateX(-50%)",
        zIndex: 2000,
        backgroundColor: "#fff",
        borderTop: "4px solid #1565C0",
        boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.1)",
        borderRadius: "4px",
      }}
      ref={containerRef}
      tabIndex={0}
      autoFocus
      aria-label="virtual keyboard"
      id={`mathpopup-${uniqueId}`}
      role="math"
      className="MathEquationKeyboard"
      onKeyDown={(e) => {
        if (e.key === "Escape") {
          closeMath();
        }
      }}
    >
      {/* Aria Live */}
      <span
        className="sr-only"
        role="status"
        aria-live="assertive"
        aria-relevant="all"
        aria-atomic="true"
      >
        {ariaLive}
      </span>
      <span
        className="sr-only"
        role="status"
        aria-live="assertive"
        aria-relevant="all"
        aria-atomic="true"
      >
        {ariaLive2}
      </span>
      {/* Top of keyboard - Label Text and Close Modal Button */}
      <div
        style={{
          height: "3rem",
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          borderBottom: "0.0625rem solid rgba(0, 0, 0, 0.12)",
        }}
      >
        <span
          style={{
            marginLeft: "1rem",
            color: "rgba(35, 35, 35, 1)",
            fontSize: "0.875rem",
            fontFamily: "Inter",
            fontWeight: 500,
            lineHeight: "157%",
            letterSpacing: "0.00625rem",
          }}
        >
          Write equation using keyboard
        </span>
        <button
          style={{
            width: "1.5rem",
            height: "1.5rem",
            marginRight: "1rem",
            border: "none",
            color: "rgba(35, 35, 35, 1)",
            backgroundColor: "transparent",
            cursor: "pointer",
          }}
          type="button"
          aria-label="close math popup"
          onClick={closeMath}
          onKeyDown={(e) => {
            if (e.key === "Enter") {
              closeMath();
            }
          }}
          tabIndex={0}
        >
          <CloseIcon />
        </button>
      </div>
      {/* Math Live Editor - Input Field */}
      <div style={{ padding: "1rem 1rem 1.25rem 1rem" }}>
        <MathLiveEditor
          style={{
            width: "calc(35.5rem, - 1rem)", // removed 1rem padding - 0.5 each side
            height: "4.0625rem",
            padding: "0 0.5rem",
            borderRadius: "4px",
            border: "0.5px solid rgba(224, 224, 224, 1)",
            zIndex: 2010,
            color: "rgba(35, 35, 35, 1)",
          }}
          ref={mathfield}
          onEditorInput={(inputValue) => setFormula(inputValue)}
        />
      </div>
      {/* Input Controls ,  Entire Control Panel Beneath Input Editor */}
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          gap: "1rem",
          height: "23.375rem",
          padding: "0 1rem 1.81rem 1rem",
        }}
      >
        {/* Advanced Functions - Left Side of Control Panel */}
        <div
          style={{
            height: "25.375rem",
            width: "25.375rem",
          }}
        >
          <TabContext value={value}>
            {/* Tabs - List of possible Advanced Function */}
            <TabList
              onChange={handleChange}
              style={{
                height: "2rem",
                minHeight: "2rem",
                alignItems: "center",
              }}
            >
              {tabs.map((tab, index) => (
                <Tab
                  aria-label={tab.label}
                  key={tab}
                  label={tab.label}
                  value={`${index + 1}`}
                  style={{
                    minWidth: "5.075rem",
                    minHeight: "2rem",
                    padding: "0rem 0.5rem",
                    textTransform: "none",
                    fontFamily: "Inter",
                    fontSize: "0.875rem",
                    fontWeight: "500",
                    lineHeight: "0.875rem",
                    letterSpacing: "0.025rem",
                    color:
                      value === index + 1
                        ? "rgba(21, 101, 192, 1)"
                        : "rgba(99, 99, 99, 1)",
                  }}
                  onKeyDown={(e) => {
                    KeyStroke(e);
                  }}
                  disableRipple
                  disableFocusRipple
                />
              ))}
            </TabList>
            {tabs.map((tab, index) => (
              <TabPanel
                key={tab}
                value={`${index + 1}`}
                style={{
                  border: "0.5px solid #E0E0E0",
                  height: "21.375rem",
                  borderRadius: "0px 0px 4px 4px",
                  padding: "1rem 1rem 0 1rem",
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    flexWrap: "wrap",
                    gap: "5.5px",
                  }}
                >
                  {/* Advanced Function */}
                  {tab.render.map((btn, index) => (
                    <button
                      key={index}
                      aria-label={
                        btn.aria ? `${btn.aria} sign` : `${btn.text} sign`
                      }
                      className="MathButton"
                      style={{
                        "--width": btn?.width ? btn.width : "32px",
                        "--background": "#eeeeee",
                        "--hoverBackground": "#e0e0e0",
                        "--height": "32px",
                      }}
                      onClick={(e) => {
                        e.preventDefault();
                        mathfield.current.insert(
                          btn?.insert ? btn.insert : btn.text
                        );
                        handleAriaLive(btn.aria);
                        // setAriaLive(`${btn.aria}`);
                        // mathfield.current.executeCommand("moveAfterParent");
                        // mathfield.current.focus();
                      }}
                      onKeyDown={(e) => {
                        KeyStroke(e);
                      }}
                    >
                      {btn.svg ? btn.svg : <InlineMath math={btn.text} />}
                    </button>
                  ))}
                </div>
              </TabPanel>
            ))}
          </TabContext>
        </div>
        {/* Basic Functions and Numbers - Right Side */}
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            height: "23.375rem",
            marginTop: "2rem",
          }}
        >
          {/* Top Side Container */}
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              flexWrap: "wrap",
              justifyContent: "center",
              gap: "0.313rem",
            }}
          >
            {TopSideKeys.map((key, index) => (
              <button
                aria-label={key.aria ? `${key.aria} sign` : `${key.text} sign`}
                key={key.text || key.aria}
                type="button"
                className="MathButton"
                style={{
                  "--width": key?.width ? key.width : "32px",
                  "--background": key?.color ? key.color : "#EEEEEE",
                  "--hoverBackground": key?.hover ? key?.hover : "#E0E0E0",
                  "--height": "32px",
                }}
                onClick={(e) => {
                  e.preventDefault();
                  key?.insert
                    ? mathfield.current.insert(key.insert)
                    : mathfield.current.executeCommand(key.command);
                  handleAriaLive(key.aria);
                  // key?.insert &&
                  // mathfield.current.executeCommand("moveAfterParent");
                  // mathfield.current.focus();
                }}
                onKeyDown={(e) => {
                  KeyStroke(e);
                }}
              >
                {key?.svg ? key?.svg : key.text}
              </button>
            ))}
          </div>
          {/* Arrow controls, delete / space and calculate */}
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              marginTop: "1rem",
            }}
          >
            <div
              style={{
                height: "68px",
                width: "106px",
                display: "flex",
                flexDirection: "row",
                flexWrap: "wrap",
                gap: "5px",
              }}
            >
              {/* Arrow Keys and 2 undefined keys */}
              {BottomLeftKeys.map((key, index) => (
                <button
                  aria-label={
                    key.aria ? `${key.aria} sign` : `${key.text} sign`
                  }
                  className="MathButton"
                  style={{
                    "--width": key?.width ? key.width : "32px",
                    "--background": key?.color ? key.color : "#EEEEEE",
                    "--hoverBackground": key?.hover ? key?.hover : "#E0E0E0",
                    "--height": "32px",
                  }}
                  onClick={(e) => {
                    e.preventDefault();
                    key?.insert
                      ? mathfield.current.insert(key.insert)
                      : mathfield.current.executeCommand(key.command);
                    handleAriaLive(key.aria);
                    // key?.insert &&
                    // mathfield.current.executeCommand("moveAfterParent");
                    mathfield.current.focus();
                  }}
                  onKeyDown={(e) => {
                    KeyStroke(e);
                  }}
                >
                  {key?.svg ? key?.svg : key.text}
                </button>
              ))}
            </div>
            {/* All Clear Button */}
            <div>
              <button
                aria-label="All clear"
                className="MathButton"
                style={{
                  "--width": "32px",
                  "--height": "68px",
                  "--margin": "0 0 0 5px",
                  "--background": "rgba(21, 101, 192, 0.12)",
                  "--hoverBackground": "rgba(21, 101, 192, 0.3)",
                }}
                onClick={(e) => {
                  e.preventDefault();
                  mathfield.current.setValue("");
                  setAriaLive("All clear");
                  // mathfield.current.focus();
                }}
                onKeyDown={(e) => {
                  KeyStroke(e);
                }}
              >
                CA
              </button>
            </div>
          </div>
          {/* Insert Button */}
          <div
            style={{
              width: "100%",
              marginTop: "2.25rem",
              display: "flex",
              flexDirection: "row",
              justifyContent: "flex-end",
            }}
          >
            <button
              aria-label={isEdit ? "Update" : "Insert"}
              type="button"
              style={{
                width: "3.563rem",
                height: "2.25rem",
                display: "flex",
                padding: "0.375rem 0.5rem",
                background: "none",
                border: "none",
                fontFamily: "Inter",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: "14px",
                justifyContent: "center",
                alignItems: "center",
                color: formula ? "#1565C0" : "rgba(0, 0, 0, 0.38)",
                cursor: formula ? "pointer" : "default",
              }}
              onClick={(e) => handleClick(e)}
              disabled={!formula}
            >
              {isEdit ? "Update" : "Insert"}
            </button>
          </div>
        </div>
      </div>
      {showError && <PlaceholderError errorType="equationKeyboard" />}
    </div>
  );
};

export default MathpixEqation;
