import React, { useState, useRef, useContext } from "react";
import { MathpixMarkdown, MathpixLoader } from "mathpix-markdown-it";
import CloseIcon from "@mui/icons-material/Close";

import { getStrokesToken } from "../MathpixDraw/canvas/Api";
import TenantContext from "../../../../../../../Context/TenantContext";

import Cropper from "react-cropper";
import "cropperjs/dist/cropper.css";

import {
  useQuill,
  useSetShowMath,
  useSetEditFormula,
} from "../../../../../Provider";

import { useOnClickOutside } from "../../../../../../../hooks/useOnClickOutside";

import "../canvas.scss";
import PlaceholderError from "../../../../../../../Utility/PlaceholderError";

const outMath = {
  include_svg: true,
  include_smiles: true,
  include_asciimath: true,
  include_latex: true,
  include_mathml: true,
  include_mathml_word: true,
};

const onClickInput = {
  position: "absolute",
  width: "9%",
  textIndent: "-999em",
  outline: "none",
  opacity: 0,
  cursor: "pointer",
  zIndex: "1000",
};

const dragAndDropInput = {
  opacity: "0",
  position: "absolute",
  top: "0",
  left: "0",
  width: "100%",
  height: "100%",
  cursor: "default",
};

const imageContainer = {
  border: "1px solid #E0E0E0",
  borderRadius: "10px",
  width: "560px",
  height: "360px",
  margin: "0px 16px 22px 16px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
};

const imgStyle = {
  height: "30%",
  objectFit: "contain",
  width: "219px",
  height: "123px",
};

const placeholderHeading = {
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  height: "32px",
  fontWeight: "400",
  fontSize: "24px",
  color: "#636363",
  marginBottom: "5px",
};
const placeholderMiddleDiv = {
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  gap: "5px",
  boxSizing: "border-box",
};
const placeholderMiddleLeft = {
  fontSize: "14px",
  color: "#636363",
  height: "22px",
};
const placeholderMiddleRight = {
  fontSize: "14px",
  color: "#1565C0",
  height: "22px",
  cursor: "pointer",
};
const placeholderfileTypes = {
  fontSize: "14px",
  color: "#9E9E9E",
  margin: 0,
  height: "22px",
  padding: 0,
  textAlign: "center",
};

const buttonsDivContainer = {
  display: "flex",
  gap: "26px",
  flexDirection: "row",
  justifyContent: "end",
  marginBottom: "22px",
  marginRight: "26px",
};

const cropperContainer = {
  width: "560px",
  height: "360px",
  borderRadius: "10px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  background: "#B3B3B3",
};

const cropImageContainer = {
  height: "123px",
  width: "219px",
  background: "#B3B3B3",
};

const ImageConversion = () => {
  const wrapperRef = useRef(null);
  const containerRef = useRef(null);
  const setMathShow = useSetShowMath();
  const setEditFormula = useSetEditFormula();
  const focusRef = useRef(null);
  const fileInputRef = useRef(null);
  const fileInputUploadRef = useRef(null);

  const [image, setImage] = useState(null);
  const [cropImage, setCropImage] = useState(null);
  const [math, setMath] = useState("");
  const [data, setData] = useState(null);
  const [crop, setCrop] = useState(false);
  const [showError, setShowError] = useState(false);
  const [showConvertError, setShowConvertError] = useState(false);
  const [disableInsert, setDisableInsert] = useState(false);
  const [disableConvert, setDisableConvert] = useState(false);
  const authoringData = useContext(TenantContext);
  const [inputMode, setinputMode] = useState(true);

  const [cropper, setCropper] = useState();
  const getBase64 = (file) => {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      setImage(reader.result);
      setCropImage(reader.result);
    };
  };

  const closeMath = () => {
    setEditFormula({ value: null, id: null });
    setMathShow(false);
  };

  useOnClickOutside(containerRef, closeMath);
  const onFileDrop = (e) => {
    e.preventDefault();
    const newFile = e.target.files[0] || e.dataTransfer.files[0];
    const validExts = new Array(".jpg", ".png", ".jpeg");
    let fileExt = newFile.name;
    fileExt = fileExt.substring(fileExt.lastIndexOf("."));
    if (validExts.indexOf(fileExt.toString().toLowerCase()) < 0) {
      setShowConvertError(false);
      setShowError(true);
      focusRef.current.focus();
    } else {
      setShowError(false);
      getBase64(newFile);
      focusRef.current.focus();
    }
  };

  const fetchToken = async (authoringData) => {
    return await getStrokesToken(authoringData);
  };

  const convertToMathPix = async (e, authoringData) => {
    e.preventDefault();
    setDisableConvert(true);
    const tokenContext = await fetchToken(authoringData);
    const response = await fetch("https://api.mathpix.com/v3/text", {
      method: "POST",
      headers: {
        app_token: tokenContext.app_token,
        app_token_expires_at: tokenContext.app_token_expires_at,
        "content-type": "application/json",
      },
      body: JSON.stringify({
        src: image,
        formats: ["text", "data", "html", "latex_styled"],
        include_smiles: true,
        data_options: {
          include_asciimath: true,
          include_latex: true,
        },
      }),
    });
    setDisableConvert(false);
    const result = await response.json();
    if (result.text === undefined) {
      setShowConvertError(true);
      setImage(null);
    } else {
      setShowConvertError(false);
      setData(result);
      setMath(result.text);
    }
  };

  const getCropData = () => {
    if (typeof cropper !== "undefined") {
      setImage(cropper.getCroppedCanvas().toDataURL());
      setCrop(false);
    }
  };

  const quill = useQuill();

  const splitLaTeX = (latex) => {
    const blocks = [];
    const blockPattern = /\\\[(.*?)\\\]/gs;
    const inlinePattern = /\\\((.*?)\\\)/gs;

    const blockMatches = latex.split(blockPattern);
    for (let i = 0; i < blockMatches.length; i++) {
      if (i % 2 == 0) {
        // Inline LaTeX
        let inlineMatches = blockMatches[i].split(inlinePattern);
        for (let j = 0; j < inlineMatches.length; j++) {
          if (j % 2 == 0) {
            blocks.push({
              type: "normal",
              content: `${inlineMatches[j].trim()}`,
            });
          } else {
            const trimmedInlineContent = inlineMatches[j].trim()
            if (trimmedInlineContent.startsWith('\\begin{array}')) {
              let inlineArrayContent = trimmedInlineContent.slice('\\begin{array}{l}'.length, -'\\end{array}'.length).trim();

              const inlineBlockEquations = inlineArrayContent.split("\\\\")

              for (let inlineBlockEquation of inlineBlockEquations) {
                blocks.push({
                  type: "newLine",
                  content: "\n"
                })
                blocks.push({
                  type: "block",
                  content: inlineBlockEquation.replace(/\\\\/g, '').trim()
                })
              }
            } else if (trimmedInlineContent.startsWith('\\begin{aligned}')) {
              let inlineAlignedContent = trimmedInlineContent.slice('\\begin{aligned}'.length, -'\\end{aligned}'.length).trim();

              const inlineBlockEquationsAligned = inlineAlignedContent.replace(/ & /g, '').split("\\\\");

              for (let inlineBlockEquation of inlineBlockEquationsAligned) {
                blocks.push({
                  type: "newLine",
                  content: "\n"
                })
                blocks.push({
                  type: "block",
                  content: inlineBlockEquation.replace(/\\\\/g, '').trim()
                })
              }
            }
            else {
              blocks.push({
                type: "inline",
                content: `${inlineMatches[j].trim()}`,
              });
            }

          }
        }
      } else {
        const trimmedContent = blockMatches[i].trim();
        if (trimmedContent.startsWith('\\begin{array}') && trimmedContent.endsWith('\\end{array}')) {
          let arrayContent = trimmedContent.slice('\\begin{array}{l}'.length, -'\\end{array}'.length).trim();
          const equations = arrayContent.split('\\\\');
          for (const equation of equations) {
            blocks.push({
              type: "newLine",
              content: "\n"
            })
            blocks.push({
              type: "block",
              content: equation.replace(/\\\\/g, '').trim()
            });
          }
        } else {
          blocks.push({
            type: "newLine",
            content: "\n"
          })
          blocks.push({
            type: "block",
            content: trimmedContent.replace(/\\\\/g, '')
          });
          blocks.push({
            type: "newLine",
            content: "\n"
          })
        }
      }
    }

    return blocks;
  }

  const insertQuill = (input) => {
    const blocks = splitLaTeX(input);
    for (const block of blocks) {
      const range = quill.getSelection(true);
      const format = quill.getFormat();
      // checking if the list has ordered or bullet
      if (!format) {
        quill.removeFormat(
          range?.index, range?.length
        );
      } else if (
        format?.list !== "ordered" &&
        format?.list !== "bullet" &&
        format.align !== "center" &&
        format.align !== "right"
      ) {
        quill.removeFormat(
          range?.index, range?.length
        );
      }
      if (block.type === 'block' || block.type === 'inline') {
        let md;
        if (block.type === 'block') {
          md = `\\[${block.content}\\]`;
        } else {
          md = `${block.content}`;
        }
        quill.insertEmbed(range.index, "mathpix", md);
      quill.setSelection(range.index + range.length + 1);
      } else {
        quill.insertText(range.index, block.content);
        quill.setSelection(range.index + block.content.length);
      }
    }
  };

  const insertToEditor = () => {
    setDisableInsert(true);
    insertQuill(data.text);
    setMathShow(false);
  };

  const cancelCrop = () => {
    setCrop(false);
  };

  const handleCropFromResult = () => {
    setCrop(true);
    setMath("");
  };

  const handlePaste = (e) => {
    const items = e.clipboardData && e.clipboardData.items;

    if (items) {
      for (let i = 0; i < items.length; i++) {
        if (items[i].type.indexOf("image") !== -1) {
          const blob = items[i].getAsFile();
          getBase64(blob);
        }
      }
    }
  };

  return (
    <div
      style={{
        position: "fixed",
        maxWidth: "592px",
        top: "165px",
        left: "50%",
        transform: "translateX(-50%)",
        zIndex: 2000,
        backgroundColor: "#fff",
        display: "flex",
        flexDirection: "column",
        borderTop: "4px solid #1565C0",
        boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.1)",
        borderRadius: "4px",
        height: !image ? "35vh" : "50vh",
        maxHeight: !image ? "285px" : "497px",
        overflowY: "auto",
      }}
      ref={containerRef}
      onKeyDown={(e) => {
        if (e.key === "Escape") {
          closeMath();
        }
      }}
      className="mathpix-upload-container"
    >
      <div
        className="mathpix-draw-header"
        onKeyDown={(e) => {
          if (e.key === "Escape") {
            closeMath();
          }
        }}
      >
        <span>Convert Image into an equation</span>
        <button
          aria-label="close math popup"
          onClick={closeMath}
          autoFocus
          onKeyDown={(e) => {
            if (e.key === "Enter") {
              closeMath();
            }
          }}
          style={{
            border: "none",
            backgroundColor: "transparent",
            cursor: "pointer",
            padding: "0px 21.18px 16px 0px",
          }}
          tabIndex={0}
          ref={focusRef}
        >
          <CloseIcon />
        </button>
      </div>
      {image && !math && (
        <>
          <div style={imageContainer}>
            {!crop ? (
              <img src={image} alt="" style={imgStyle} />
            ) : (
              <div style={cropperContainer}>
                <Cropper
                  zoomTo={0}
                  initialAspectRatio={1}
                  preview=".img-preview"
                  src={cropImage}
                  viewMode={1}
                  minCropBoxHeight={10}
                  minCropBoxWidth={10}
                  background={false}
                  responsive={true}
                  autoCropArea={0}
                  checkOrientation={false}
                  onInitialized={(instance) => {
                    setCropper(instance);
                  }}
                  guides={true}
                  style={cropImageContainer}
                  className="crop-image-container"
                />
              </div>
            )}
          </div>
          {!crop ? (
            <div>
              <div style={buttonsDivContainer}>
                <label
                  style={{ color: disableConvert ? "#9E9E9E" : "#1565C0" }}
                  className="change-image btnStyles"
                  tabIndex={0}
                  onKeyDown={(e) => {
                    if (e.key === "Enter") {
                      fileInputRef.current.click();
                    }
                  }}
                  aria-label="Change Image"
                >
                  <input
                    type="file"
                    value=""
                    onChange={onFileDrop}
                    onDrop={onFileDrop}
                    onInput={onFileDrop}
                    disabled={disableConvert}
                    ref={fileInputRef}
                    accept="image/jpg, image/jpeg, image/png"
                  />
                  Change Image
                </label>

                <button
                  style={{ color: disableConvert ? "#9E9E9E" : "#1565C0" }}
                  className="btnStyles"
                  onClick={() => {
                    setCrop(true);
                    setDisableConvert(false);
                  }}
                  disabled={disableConvert}
                  aria-label="Crop Image"
                >
                  Crop
                </button>
                <button
                  style={{ color: disableConvert ? "#9E9E9E" : "#1565C0" }}
                  className="btnStyles"
                  onClick={(e) => convertToMathPix(e, authoringData)}
                  disabled={disableConvert}
                  aria-label="Convert Image into Equation"
                >
                  Convert
                </button>
              </div>
            </div>
          ) : (
            <>
              <div style={buttonsDivContainer}>
                <button
                  className="btnStyles cropBtns"
                  onClick={cancelCrop}
                  aria-label="Cancel"
                >
                  Cancel
                </button>
                <button
                  className="btnStyles cropBtns"
                  onClick={getCropData}
                  aria-label="apply"
                >
                  Apply
                </button>
              </div>
            </>
          )}
        </>
      )}
      {!image && !math && (
        <>
          <div
            style={{
              position: "relative",
              width: "560px",
              height: "200px",
              border: "3px dashed",
              borderColor: showError ? "#D32F2F" : "#1565C0",
              borderRadius: "15px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: showError
                ? "#FBEAEA"
                : inputMode
                  ? "white"
                  : "rgba(21, 101, 192, 0.04)",
              margin: "0px 16px 16px 16px",
            }}
            ref={wrapperRef}
            className="drop-file-input"
          >
            <div className="drop-file-input__label">
              <span style={placeholderHeading}>Add an image here</span>
              <div style={placeholderMiddleDiv}>
                <p style={placeholderMiddleLeft}>
                  Copy and paste files, or Drag and drop files, or
                </p>
                <p style={placeholderMiddleRight}>
                  <input
                    type="file"
                    value=""
                    ref={fileInputUploadRef}
                    onChange={onFileDrop}
                    onDrop={onFileDrop}
                    onInput={onFileDrop}
                    style={onClickInput}
                    accept="image/jpg, image/jpeg, image/png"
                  />
                  Upload
                </p>
              </div>
              <p style={placeholderfileTypes}>
                Accepted file types: jpeg, png, jpg
              </p>
            </div>
            <input
              type={inputMode ? "file" : "text"}
              value=""
              onDrop={onFileDrop}
              style={dragAndDropInput}
              onClick={() => {
                setinputMode(false);
              }}
              onBlur={() => setinputMode(true)}
              accept="image/jpg, image/jpeg, image/png"
              onPaste={handlePaste}
            />
          </div>
          {showError && <PlaceholderError errorType="mathImageUpload" />}
          {showConvertError && (
            <PlaceholderError errorType="convertMathImage" />
          )}
        </>
      )}
      {image && math && (
        <>
          <div className="converted-image-main-container">
            <div className="original-image-container">
              <span>Original</span>
              <div className="image-div original-div-result">
                <img src={image} alt="" className="original-image" />
              </div>
            </div>
            <div className="result-image-container">
              <span>Result</span>
              <div className="image-div image-div-result">
                <MathpixLoader notScrolling={true}>
                  <MathpixMarkdown
                    text={math}
                    outMath={outMath}
                    htmlTags="true"
                    miles={{
                      ringVisualization: "circle",
                      ringAromaticVisualization: "dashed",
                    }}
                  />
                </MathpixLoader>
              </div>
            </div>
          </div>
          <div className="accuracy-rate-container">
            <span className="accuracy-info">Accuracy Rate</span>
            <span className="acucuracy-info">
              {(data.confidence_rate * 100).toFixed(2)}%
            </span>
          </div>
          <div className="response-formula-container">
            <span className="response-formula-title">Response Formula</span>
            <div className="response-formula-info-container">
              {data?.latex_styled ? data.latex_styled : data.text}
            </div>
          </div>

          <div className="crop-insert-button-container">
            <button
              style={{ color: disableInsert ? "#9E9E9E" : "#1565C0" }}
              className="btnStyles"
              onClick={handleCropFromResult}
              disabled={disableInsert}
              aria-label="Crop Image"
            >
              Crop
            </button>
            <button
              style={{ color: disableInsert ? "#9E9E9E" : "#1565C0" }}
              className="btnStyles"
              onClick={insertToEditor}
              disabled={disableInsert}
              aria-label="Insert Equation"
            >
              Insert
            </button>
          </div>
        </>
      )}
    </div>
  );
};

export default ImageConversion;
