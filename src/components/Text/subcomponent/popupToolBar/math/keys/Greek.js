import * as Icons from "../../../../assets/Greek/index";

const Greek = [
  {
    text: "\\alpha",
    svg: Icons.alphasign,
    aria: "alpha",
  },
  {
    text: "\\beta",
    svg: Icons.betasign,
    aria: "beta",
  },
  {
    text: "\\gamma",
    svg: Icons.gammasign,
    aria: "gamma",
  },
  {
    text: "\\delta",
    svg: Icons.deltasign,
    aria: "delta",
  },
  {
    text: "\\epsilon",
    svg: Icons.epsilonsign,
    aria: "epsilon",
  },
  {
    text: "\\varepsilon",
    svg: Icons.varepsilonsign,
    aria: "varepsilon",
  },
  {
    text: "\\zeta",
    svg: Icons.zetasign,
    aria: "zeta",
  },
  {
    text: "\\eta",
    svg: Icons.etasign,
    aria: "eta",
  },
  {
    text: "\\theta",
    svg: Icons.thetasign,
    aria: "theta",
  },
  {
    text: "\\vartheta",
    svg: Icons.varthetasign,
    aria: "vartheta",
  },
  {
    text: "\\iota",
    svg: Icons.iotasign,
    aria: "iota",
  },
  {
    text: "\\kappa",
    svg: Icons.kappasign,
    aria: "kappa",
  },
  {
    text: "\\lambda",
    svg: Icons.lambdasign,
    aria: "lambda",
  },
  {
    text: "\\mu",
    svg: Icons.musign,
    aria: "mu",
  },
  {
    text: "\\nu",
    svg: Icons.nusign,
    aria: "nu",
  },
  {
    text: "\\xi",
    svg: Icons.xisign,
    aria: "xi",
  },
  {
    text: "\\omicron",
    svg: Icons.omicronsign,
    aria: "omicron",
  },
  {
    text: "\\pi",
    svg: Icons.pisign,
    aria: "pi",
  },
  {
    text: "\\rho",
    svg: Icons.rhosign,
    aria: "rho",
  },
  {
    text: "\\sigma",
    svg: Icons.sigmalower,
    aria: "sigma",
  },
  {
    text: "\\tau",
    svg: Icons.tausign,
    aria: "tau",
  },
  {
    text: "\\upsilon",
    svg: Icons.upsilonsign,
    aria: "upsilon",
  },
  {
    text: "\\phi",
    svg: Icons.greekicon,
    aria: "phi",
  },
  {
    text: "\\varphi",
    svg: Icons.varphisign,
    aria: "varphi",
  },
  {
    text: "\\chi",
    svg: Icons.chisign,
    aria: "chi",
  },
  {
    text: "\\psi",
    svg: Icons.psisign,
    aria: "psi",
  },
  {
    text: "\\omega",
    svg: Icons.omegasign,
    aria: "omega",
  },
];

export default Greek;
