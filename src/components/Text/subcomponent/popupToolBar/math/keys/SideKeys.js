import {
  PlaceHolder,
  ArrowUp,
  ArrowDown,
  ArrowLeft,
  ArrowRight,
} from "../MathIcons";
import * as Icons from "../../../../assets/numerickeypad/index";
import {
  equalsign,
  divide,
  multiply,
  subtract,
  add,
  dot,
} from "../../../../assets/Basic/index";

export const TopSideKeys = [
  {
    insert: "\\left(",
    text: "left",
    aria: "left parenthesis",
    svg: Icons.bracketequalleft,
    color: "rgba(21, 101, 192, 0.12)",
    hover: "rgba(21, 101, 192, 0.3)",
  },
  {
    insert: "\\right)",
    text: "right",
    aria: "right parenthesis",
    svg: Icons.bracketequalright,
    color: "rgba(21, 101, 192, 0.12)",
    hover: "rgba(21, 101, 192, 0.3)",
  },
  {
    insert: "=",
    svg: equalsign,
    text: "=",
    aria: "equals",
    color: "rgba(21, 101, 192, 0.12)",
    hover: "rgba(21, 101, 192, 0.3)",
    width: "70px",
  },
  {
    text: "7",
    aria: "seven",
    insert: "7",
  },
  {
    text: "8",
    aria: "eight",
    insert: "8",
  },
  {
    text: "9",
    aria: "nine",
    insert: "9",
  },
  {
    insert: "\\div",
    aria: "divide",
    svg: divide,
    color: "rgba(21, 101, 192, 0.12)",
    hover: "rgba(21, 101, 192, 0.3)",
  },
  {
    text: "4",
    aria: "four",
    insert: "4",
  },
  {
    text: "5",
    aria: "five",
    insert: "5",
  },
  {
    text: "6",
    aria: "six",
    insert: "6",
  },
  {
    insert: "\\times",
    text: "times",
    aria: "multiply",
    svg: multiply,
    color: "rgba(21, 101, 192, 0.12)",
    hover: "rgba(21, 101, 192, 0.3)",
  },
  {
    text: "1",
    aria: "one",
    insert: "1",
  },
  {
    text: "2",
    aria: "two",
    insert: "2",
  },
  {
    text: "3",
    aria: "three",
    insert: "3",
  },
  {
    insert: "-",
    text: "minus",
    aria: "minus",
    svg: subtract,
    color: "rgba(21, 101, 192, 0.12)",
    hover: "rgba(21, 101, 192, 0.3)",
  },
  {
    text: "0",
    insert: "0",
    aria: "zero",
  },
  {
    text: "00",
    insert: "00",
    aria: "double zero",
  },
  {
    text: ".",
    svg: dot,
    insert: ".",
    aria: "decimal",
  },
  {
    insert: "+",
    text: "plus",
    aria: "plus",
    svg: add,
    color: "rgba(21, 101, 192, 0.12)",
    hover: "rgba(21, 101, 192, 0.3)",
  },
];

export const BottomLeftKeys = [
  {
    command: "deleteBackward",
    text: "delete backward",
    aria: "delete backward",
    svg: Icons.backspace,
  },
  {
    svg: Icons.positionequalup,
    text: "up",
    aria: "up",
    command: "moveUp",
    color: "rgba(224, 224, 224, 1)",
  },
  {
    command: "deleteForward",
    text: "delete forward",
    aria: "delete forward",
    svg: Icons.spacebar,
    insert: "\\:"
  },
  {
    svg: Icons.positionequalleft,
    text: "left",
    aria: "left",
    command: "moveToPreviousChar",
    color: "rgba(224, 224, 224, 1)",
  },
  {
    svg: Icons.positionequaldown,
    text: "down",
    aria: "down",
    command: "moveDown",
    color: "rgba(224, 224, 224, 1)",
  },
  {
    svg: Icons.positionequalright,
    text: "right",
    aria: "right",
    command: "moveToNextChar",
    color: "rgba(224, 224, 224, 1)",
  },
];
