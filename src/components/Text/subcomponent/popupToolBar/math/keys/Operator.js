import * as Icons from "../../../../assets/Operator/index";

const Operator = [
  {
    text: "\\sum_{\\square}^{\\square}",
    svg: Icons.sigma,
    insert: "\\sum_{#0}^{#0}",
  },
  {
    text: "\\prod_{\\square}^{\\square}",
    svg: Icons.operatoricon,
    insert: "\\prod_{#0}^{#0}",
  },
  {
    text: "\\int_{\\square}^{\\square}",
    svg: Icons.integral,
    insert: "\\int_{#0}^{#0}",
  },
  {
    text: "\\iint_{\\square}^{\\square}",
    svg: Icons.doubleintegral,
    insert: "\\iint_{#0}^{#0}",
  },
  {
    text: "\\iiint_{\\square}^{\\square}",
    svg: Icons.tripleintegral,
    insert: "\\iiint_{#0}^{#0}",
  },
  {
    text: "\\oint_{\\square}^{\\square}",
    svg: Icons.contourintegral,
    insert: "\\oint_{#0}^{#0}",
  },
  {
    text: "\\oiint_{\\square}^{\\square}",
    svg: Icons.surfaceintegral,
    insert: "\\oiint_{#0}^{#0}",
  },
  {
    text: "\\oiiint_{\\square}^{\\square}",
    svg: Icons.volumeintegral,
    insert: "\\oiiint_{#0}^{#0}",
  },
  {
    text: "\\bigcup_{\\square}^{\\square}",
    svg: Icons.naryunion,
    insert: "\\bigcup_{#0}^{#0}",
  },
  {
    text: "\\bigcap_{\\square}^{\\square}",
    svg: Icons.naryintersection,
    insert: "\\bigcap_{#0}^{#0}",
  },
  {
    text: "\\bigwedge_{\\square}^{\\square}",
    svg: Icons.narylogicaland,
    insert: "\\bigwedge_{#0}^{#0}",
  },
  {
    text: "\\bigvee_{\\square}^{\\square}",
    svg: Icons.narylogicalor,
    insert: "\\bigvee_{#0}^{#0}",
  },
  {
    text: "\\bigotimes_{\\square}^{\\square}",
    svg: Icons.narycircledtimesoperator,
    insert: "\\bigotimes_{#0}^{#0}",
  },
  {
    text: "\\bigoplus_{\\square}^{\\square}",
    svg: Icons.narycircledplusoperator,
    insert: "\\bigoplus_{#0}^{#0}",
  },
  {
    text: "\\bigodot_{\\square}^{\\square}",
    svg: Icons.narycircleddotoperator,
    insert: "\\bigodot_{#0}^{#0}",
  },
  {
    text: "\\biguplus_{\\square}^{\\square}",
    svg: Icons.naryunionoperatorwithplus,
    insert: "\\biguplus_{#0}^{#0}",
  },
];

export default Operator;
