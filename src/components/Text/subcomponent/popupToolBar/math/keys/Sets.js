import * as Icons from "../../../../assets/Sets/index";

const Sets = [
  {
    text: "\\emptyset",
    svg: Icons.emptysetsign,
    aria: "empty set",
  },
  {
    text: "\\varnothing",
    svg: Icons.varnothingsign,
    aria: "var nothing",
  },
  {
    text: "\\N",
    svg: Icons.setNsign,
    aria: "set N",
  },
  {
    text: "\\R",
    svg: Icons.setRsign,
    aria: "set R",
  },
  {
    text: "\\Z",
    svg: Icons.setZsign,
    aria: "set Z",
  },
  {
    text: "\\subset",
    svg: Icons.subsetsign,
    insert: "\\subset",
    aria: "subset",
  },
  {
    text: "\\supset",
    svg: Icons.supersetsign,
    insert: "\\supset",
    aria: "superset",
  },
  {
    text: "\\subseteq",
    svg: Icons.subsetorequaltosign,
    insert: "\\subseteq",
    aria: "subset or equal to",
  },
  {
    text: "\\supseteq",
    svg: Icons.supersetorequaltosign,
    insert: "\\supseteq",
    aria: "superset or equal to",
  },
  {
    text: "\\because",
    svg: Icons.becausesign,
    insert: "\\because",
    aria: "because",
  },
  {
    text: "\\therefore",
    svg: Icons.thereforesign,
    insert: "\\therefore",
    aria: "therefore",
  },
  {
    text: "\\exists",
    svg: Icons.existssign,
    insert: "\\exists",
    aria: "exists",
  },
  {
    text: "\\nexists",
    svg: Icons.notexistssign,
    insert: "\\nexists",
    aria: "not exists",
  },
  {
    text: "\\forall",
    svg: Icons.forallsign,
    insert: "\\forall",
    aria: "for all",
  },
];

export default Sets;
