import * as Icons from "../../../../assets/Basic/index";

const Basic = [
  {
    text: "+",
    svg: Icons.add,
    insert: "+",
    aria: "plus",
  },
  {
    text: "-",
    svg: Icons.subtract,
    insert: "-",
    aria: "minus",
  },
  {
    text: "\\cdot",
    svg: Icons.dot,
    insert: "\\cdot",
    aria: "dot",
  },
  {
    text: "\\times",
    svg: Icons.multiply,
    insert: "\\times",
    aria: "times",
  },
  {
    text: "\\div",
    svg: Icons.divide,
    insert: "\\div",
    aria: "divided",
  },
  {
    text: "\\frac{\\square}{\\square}",
    svg: Icons.fraction,
    insert: "\\frac{#0}{#1}",
    aria: "fraction",
  },
  {
    text: "=",
    svg: Icons.equalsign,
    insert: "=",
    aria: "equals",
  },
  {
    text: "\\neq",
    svg: Icons.neg,
    insert: "\\neq",
    aria: "not equal",
  },
  {
    text: "\\pi",
    svg: Icons.pi,
    insert: "\\pi",
    aria: "pi",
  },
  {
    text: "e",
    svg: Icons.esign,
    insert: "e",
    aria: "e",
  },
  {
    text: "\\sqrt[\\square]{\\square}",
    svg: Icons.nthrootsign,
    insert: "\\sqrt[#0]{#0}",
    aria: "Nth root",
  },
  {
    text: "{x}^{\\square}",
    svg: Icons.exponentsign,
    insert: "{x}^{#1}",
    aria: "exponent",
  },
  {
    text: "{\\square}^2",
    svg: Icons.squaresign,
    insert: "#0^2",
    aria: "square",
  },
  {
    text: "\\square^{\\square}",
    svg: Icons.iconsuperscript,
    insert: "{#0}^{#0}",
    aria: "superscript",
  },
  {
    text: "\\square_{\\square}",
    svg: Icons.iconsubscript,
    insert: "{#0}_{#0}",
    aria: "subscript",
  },
  {
    text: "\\sqrt{\\square}",
    svg: Icons.squareroot,
    insert: "\\sqrt{#0}",
    aria: "square root",
  },
  {
    text: "\\log",
    svg: Icons.logsign,
    insert: "\\log",
    aria: "log",
  },
  {
    text: "\\log_{\\square}",
    svg: Icons.logbasesign2,
    insert: "\\log_{#0}",
    aria: "log base",
  },
  {
    text: "\\ln",
    svg: Icons.naturallogarithm,
    insert: "\\ln",
    aria: "natural logarithm",
  },
  {
    text: "\\amalg",
    svg: Icons.amalgamation,
    insert: "\\amalg",
    aria: "amalgamation",
  },
  {
    text: "\\And",
    svg: Icons.AndSymbol,
    insert: "\\And",
    aria: "and",
  },
  {
    text: "\\left[\\begin{smallmatrix} \\square \\\\ \\square \\end{smallmatrix}\\right]",
    svg: Icons.brackets1,
    insert:
      "\\left[\\begin{smallmatrix} {#0} \\\\ {#1} \\end{smallmatrix}\\right]",
    aria: "matrix",
  },
  {
    text: "\\lbrace\\begin{smallmatrix} \\square \\\\ \\square \\end{smallmatrix}\\rbrace",
    svg: Icons.brackets2,
    insert:
      "\\lbrace\\begin{smallmatrix} {#0} \\\\ {#1} \\end{smallmatrix}\\rbrace",
    aria: "set",
  },
  {
    text: "\\binom{\\square}{\\square}",
    svg: Icons.brackets3,
    insert: "\\binom{#0}{#1}",
    aria: "binomial",
  },
  {
    text: "<",
    svg: Icons.lessthan,
    insert: "<",
    aria: "less than",
  },
  {
    text: ">",
    svg: Icons.greaterthan,
    insert: ">",
    aria: "greater than",
  },
  {
    text: "\\leq",
    svg: Icons.leq,
    insert: "\\leq",
    aria: "less than or equal",
  },
  {
    text: "\\geq",
    svg: Icons.geq,
    insert: "\\geq",
    aria: "greater than or equal",
  },
  {
    text: "\\equiv",
    svg: Icons.equiv,
    insert: "\\equiv",
    aria: "equivalent",
  },
  {
    text: "\\approx",
    svg: Icons.approx,
    insert: "\\approx",
    aria: "approximately equal",
  },
  {
    text: "\\in",
    svg: Icons.memberof,
    insert: "\\in",
    aria: "element of",
  },
  {
    text: "\\notin",
    svg: Icons.notin,
    insert: "\\notin",
    aria: "not element of",
  },
];

export default Basic;
