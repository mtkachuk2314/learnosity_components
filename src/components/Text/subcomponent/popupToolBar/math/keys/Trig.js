import * as Icons from "../../../../assets/trig/index";

const Trig = [
  {
    text: "\\sin",
    svg: Icons.sin,
    aria: "sine",
    width: "3.5625rem",
    height: "2rem",
  },
  {
    text: "\\cos",
    svg: Icons.cos,
    aria: "cosine",
    width: "3.5625rem",
    height: "2rem",
  },
  {
    text: "\\tan",
    svg: Icons.tan,
    aria: "tangent",
    width: "3.5625rem",
    height: "2rem",
  },
  {
    text: "\\sec",
    svg: Icons.sec,
    aria: "secant",
    width: "3.5625rem",
    height: "2rem",
  },
  {
    text: "\\csc",
    svg: Icons.csc,
    aria: "cosecant",
    width: "3.5625rem",
    height: "2rem",
  },
  {
    text: "\\cot",
    svg: Icons.cot,
    aria: "cotangent",
    width: "3.5625rem",
    height: "2rem",
  },
  {
    text: "\\sinh",
    svg: Icons.sinh,
    aria: "hyperbolic sine",
    width: "3.5625rem",
    height: "2rem",
  },
  {
    text: "\\cosh",
    svg: Icons.cosh,
    aria: "hyperbolic cosine",
    width: "3.5625rem",
    height: "2rem",
  },
  {
    text: "\\tanh",
    svg: Icons.tanh,
    aria: "hyperbolic tangent",
    width: "3.5625rem",
    height: "2rem",
  },
  {
    text: "\\coth",
    svg: Icons.coth,
    aria: "hyperbolic cotangent",
    width: "3.5625rem",
    height: "2rem",
  },
  {
    text: "\\arcsin",
    svg: Icons.arcsin,
    aria: "arcsine",
    width: "3.5625rem",
    height: "2rem",
  },
  {
    text: "\\arccos",
    svg: Icons.arccos,
    aria: "arccosine",
    width: "3.5625rem",
    height: "2rem",
  },
  {
    text: "\\arctan",
    svg: Icons.arctan,
    aria: "arctangent",
    width: "3.5625rem",
    height: "2rem",
  },
];

export default Trig;
