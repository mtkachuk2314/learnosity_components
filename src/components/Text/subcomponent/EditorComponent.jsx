import React, {
  useEffect,
  useRef,
  useState,
  useMemo,
  useContext,
  forwardRef,
} from "react";
import ReactQuill, { Quill } from "react-quill";
import "react-quill/dist/quill.snow.css";
import "quill-paste-smart";

import TenantContext from "../../../Context/TenantContext";
import { useTextContext } from "../TextContext";

import "../styles/EditorComponent.scss";
import { checkTextForUrl } from "../utils/HandleLinks";
import CheckHighlights from "../utils/CheckHighlights";
import { FormulaEvents, linkClickEvent } from "../utils/FormulaEvents";

import CustomMathBlot from "../blots/CustomMathBlot";
import useDebounce from "../../../hooks/useDebounce";

import { MathEditDialog } from "../dialogs/PopupDialogs";

import {
  useSetQuill,
  useSetUniqueId,
  useShowMath,
  useKeepEditor,
  useBoldRef,
  useLinkRef,
  useShowLink,
  useSetShowLink,
  useIsLink,
  useSetIsLink,
  useSetFormat,
} from "../Provider";

import { matchMsWordList, maybeMatchMsWordList } from "../matchers/pasteLists";
import { matchSpan } from "../matchers/pasteSpan";
import { matchText } from "../matchers/pasteText";

import "katex/dist/katex.css";

const Delta = Quill.import("delta");

Quill.register("formats/mathpix", CustomMathBlot);
const EditorComponent = forwardRef(function EditorComponent(
  {
    toolbarRef,
    setShowEditor,
    showEditor,
    portal,
    componentId,
    setActiveDropDownAlignItem,
    setActiveDropDownListItem,
    t,
  },
  ref
) {
  // context hooks
  const boldRef = useBoldRef();
  const linkRef = useLinkRef();
  const setQuill = useSetQuill();
  const showMath = useShowMath();
  const showLink = useShowLink();
  const setShowLink = useSetShowLink();
  const keepEditor = useKeepEditor();
  const setUniqueId = useSetUniqueId();
  const isLink = useIsLink();
  const setIsLink = useSetIsLink();
  const setFormat = useSetFormat();

  const authoringData = useContext(TenantContext);
  const viewOnly = !authoringData?.viewOnlyComponents;

  // state to hide toolbar if clicked outside text component
  const [editorIsFocus, setEditorIsFocus] = useState(false);

  // add focus to editor
  const focusRef = useRef(null);

  // text context
  const [state, dispatch] = useTextContext();
  const [textContent, setTextContent] = useState(state);

  const updateBodyState = (bodyValue) => {
    dispatch({ func: "UPDATE_STATE", data: bodyValue });
  };
  const handleDebounce = useDebounce(updateBodyState);

  useEffect(() => {
    if (showLink || showMath || keepEditor) return;
    // quill instance
    const quill = focusRef?.current?.getEditor();
    // adding aria label in image and video description, credit for NVDA scenario
    if (quill) {
      quill.container.children[0].setAttribute("aria-label", ariaLabel());
    }

    // setEditorIsFocus(false);
    showLink && setShowLink(false);
  }, [showMath, keepEditor, showLink]);

  useEffect(() => {
    // set quill instance
    setQuill(focusRef?.current?.getEditor());
    // adding aria label in image and video description, credit for NVDA scenario
    if (focusRef?.current?.getEditor()) {
      const quillInstance = focusRef.current.getEditor();
      quillInstance.container.children[0].setAttribute(
        "aria-label",
        ariaLabel()
      );
    }
    // set unique id instance
    setUniqueId(componentId);
    // check for formulas
    FormulaEvents(componentId);
    // on render editor is focused
    showEditor && focusRef.current.focus();
    // on render toolbar appears
    showEditor && setEditorIsFocus(true);
    // on mount pass back focusRef
  }, []);

  useEffect(() => {
    if (state) {
      // quill instance
      // adding aria label in image and video description, credit for NVDA scenario
      const quill = focusRef?.current?.getEditor();
      if (quill) {
        quill.container.children[0].setAttribute("aria-label", ariaLabel());
      }
      // convert body to delta
      const deltaBody = new Delta(state);
      // get currentContents
      const currentContents = quill.getContents();
      // check if currentContents is equal to deltaBody
      const diff = currentContents.diff(deltaBody);
      // if not equal set quill to body
      // check if deltas first insert is empty and attributes is align
      const alignment =
        deltaBody.ops[0]?.attributes?.align && deltaBody?.ops[0]?.insert === "";
      // check if deltas first insert is empty and attributes is list
      const list =
        deltaBody.ops[0]?.attributes?.list && deltaBody?.ops[0]?.insert === "";
      // if difference and not alignment or list set quill to body
      if (diff.ops.length > 0 && !alignment && !list) {
        setTextContent(state);
        quill.setContents(state, "silent");
      }
    }
  }, [state]);

  // Set formating - align & list  @ current focus
  const formatSelection = (range, quillRef) => {
    if (quillRef !== null && editorIsFocus) {
      const quill = quillRef.getEditor();
      if (quill.hasFocus()) {
        const currentFormat = quill.getFormat();
        setFormat(currentFormat);
        const nexFormat = quill.getFormat(range.index, range.length + 1);
        currentFormat?.list
          ? setActiveDropDownListItem(currentFormat.list)
          : setActiveDropDownListItem("");
        currentFormat?.align
          ? setActiveDropDownAlignItem(currentFormat.align)
          : setActiveDropDownAlignItem("left");

        if ((currentFormat?.link || nexFormat?.link) && range.length < 1) {
          const [blot, offset] = quill.getLeaf(range.index);
          const index = quill.getIndex(blot);
          const delta = quill.getContents(index);
          let opIndex = 1;
          if (Object.keys(currentFormat)?.length !== 0) {
            if (
              currentFormat.align == "center" ||
              currentFormat.align == "right"
            ) {
              opIndex = 1;
            } else {
              opIndex = 0;
            }
          } else {
            opIndex = 1;
          }
          const { link } = delta?.ops[opIndex]?.attributes;
          const text = delta?.ops[opIndex]?.insert;

          text.length !== offset
            ? linkClickEvent(componentId, index, text, link)
            : showLink && setShowLink(false);
        } else {
          showLink && setShowLink(false);
        }

        if (currentFormat?.link && range.length > 0) {
          setIsLink(true);
        } else {
          isLink && setIsLink(false);
        }
      }
    }
  };

  // set the data when the editor content changes
  const handleDataChange = (content, delta, source, editor) => {
    const editorContent = editor.getContents();

    // quill instance
    const quill = focusRef?.current?.getEditor();

    // check for links
    let checkLinks = null;
    const { ops } = delta;
    const lastOp = ops[ops.length - 1];
    if (
      lastOp.insert &&
      typeof lastOp.insert === "string" &&
      lastOp.insert.match(/\s/)
    ) {
      checkLinks = checkTextForUrl(quill);
    }

    // add eventListeners to editor
    // AddLinkEvents(`toolbar-${componentId}`);

    // check for selection with highlights
    const noHighlights = CheckHighlights(editorContent);

    // check for formulas
    FormulaEvents(componentId);

    // edit ops on paste
    const onPaste =
      editorContent.ops[0].insert === "\n" && editorContent.ops.length === 1;
    onPaste && (editorContent.ops[0].insert = "");

    checkLinks && quill.updateContents(checkLinks);
    const formatchanged =
      delta.ops.filter(
        (op) => op.attributes && op.retain >= 0 && !op.attributes.id
      ).length > 0;
    // update setProp with new editorContent
    if (noHighlights && source !== "silent" && !checkLinks) {
      setTextContent(editorContent);
      !formatchanged
        ? handleDebounce(editorContent)
        : updateBodyState(editorContent);
    }
  };

  const onKeyDropDown = (e) => {
    if (!e.shiftKey && e.key === "Tab") {
      if (portal?.parentSection === "description") {
        if (portal?.parentComponent !== "image") boldRef?.current.focus();
      }
    } else if (e.shiftKey && e.key === "Tab") {
      if (
        portal?.parentSection === "description" ||
        portal?.parentSection === "credit"
      ) {
        if (portal?.parentComponent === "image") e.preventDefault();
        else if (portal?.parentComponent !== "image") linkRef?.current.focus();
      }
    }
  };

  // customization settings for toolbar
  const formats = [
    "bold",
    "italic",
    "underline",
    "script",
    "strike",
    "formula",
    "align",
    "list",
    "bullet",
    "indent",
    "link",
    "background",
    "mathpix",
  ];

  // Backspace Handler

  const backspaceHandler = (range, context) => {
    if (range.length === 0 && range.index !== 0) {
      focusRef?.current
        .getEditor()
        .deleteText(range.index - 1, 1, Quill.sources.USER);
    } else {
      focusRef?.current.getEditor().deleteText(range, Quill.sources.USER);
    }
  };

  const modules = useMemo(
    () => ({
      toolbar: false,
      keyboard: {
        bindings: {
          tab: false,
          // list: {
          //   key: "backspace",
          //   context: { format: ["list"] },
          //   handler(range, context) {
          //     backspaceHandler(range, context);
          //   },
          // },
          enter:
            portal?.parentSection === "credit"
              ? {
                  key: 13,
                  handler: () => {},
                }
              : null,
        },
      },
      clipboard: {
        matchVisual: false,
        allowed: {
          tags: [
            "a",
            "b",
            "strong",
            "u",
            "s",
            "i",
            "p",
            "br",
            "ul",
            "ol",
            "li",
            "span",
            "em",
            "sub",
            "sup",
          ],
          attributes: ["href", "rel", "target", "class"],
        },
        matchers: [
          ["p.MsoListParagraphCxSpFirst", matchMsWordList],
          ["p.MsoListParagraphCxSpMiddle", matchMsWordList],
          ["p.MsoListParagraphCxSpLast", matchMsWordList],
          ["p.MsoListParagraph", matchMsWordList],
          ["p.msolistparagraph", matchMsWordList],
          ["p.MsoNormal", maybeMatchMsWordList],
          [Node.TEXT_NODE, matchText],
          ["SPAN", matchSpan],
          ["UL", matchMsWordList],
          // ["DIV.OL", matchMsWordList],
          // ["DIV.UL", matchMsWordList]
        ],
      },
    }),
    []
  );

  const placeholder = () => {
    if (
      portal?.parentComponent === "video" ||
      portal?.parentComponent === "image"
    )
      return portal?.placeholder;
    return `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
          enim ad minim veniam, quis nostrud exercitation ullamco laboris
          nisi ut aliquip ex ea commodo consequat.`;
  };

  const ariaLabel = () => {
    switch (`${portal?.parentComponent}-${portal?.parentSection}`) {
      case "video-description":
        return t("Video Description");
      case "video-credit":
        return t("Video credit");
      case "image-description":
        return t("Image description");
      case "image-credit":
        return t("Image credit");
      default:
        return "";
    }
  };

  return (
    <div
      onClick={() => {
        setEditorIsFocus(true);
      }}
      onFocus={(e) => {
        setEditorIsFocus(true);
      }}
      onBlur={(e) => {
        const quill = focusRef?.current?.getEditor();
        const editorContent = quill.getContents();
        if (
          editorContent.ops[0].insert === "\n" &&
          !toolbarRef.current?.contains(e.relatedTarget)
        )
          setShowEditor(false);
      }}
      className="text-editor"
      id={`toolbar-${componentId}`}
      data-testid="text-editor-component"
    >
      <MathEditDialog />
      <ReactQuill
        ref={focusRef}
        data-testid="quill-editor-component"
        modules={modules}
        scrollingContainer="html"
        formats={formats}
        readOnly={viewOnly}
        theme="snow"
        placeholder={placeholder()}
        className="quillEditor"
        onChange={handleDataChange}
        defaultValue={textContent}
        onChangeSelection={(range, source, editor) => {
          formatSelection(range, focusRef.current);
        }}
        onFocus={() => {
          // Add onClick handler to each equation inside the component
          if (componentId) FormulaEvents(componentId, "add");
        }}
        // Remove onClick handler from each equation inside the component
        onBlur={() => {
          if (componentId) FormulaEvents(componentId, "remove");
        }}
        // onKeyDown={(e) => {
        //   onKeyDropDown(e);
        // }}
      />
    </div>
  );
});

export default React.memo(EditorComponent);
