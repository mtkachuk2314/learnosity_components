import Quill from "quill";
import { MathpixMarkdownModel } from "mathpix-markdown-it";
import showdown from "showdown";
import sha1 from "sha1";

const Embed = Quill.import("blots/embed");

const options = {
  display: "inline-block",
  alignMathBlock: "left",
  outMath: {
    include_asciimath: true,
    include_smiles: true,
    include_mathml: true,
    include_latex: true,
    include_svg: true,
    include_tsv: true,
    include_table_html: true,
  },
};

class CustomMathBlot extends Embed {
  static blotName = "mathpix";

  static tagName = "SPAN";

  static className = "ql-mathpix";

  static create(value) {
    const node = super.create(value);
    if (typeof value === "string") {
      let md;
      if (value.includes("<smiles>") || value.includes("\\[")) {
        md = value;
        node.appendChild(
          this.equationToSVG(MathpixMarkdownModel.render(md, options))
        );
      } else {
        md = `\\(${value}\\)`;
        node.appendChild(
          this.equationToSVG(MathpixMarkdownModel.render(md, options))
        );
      }
      node.contentEditable = "true";
      node.setAttribute("data-id", sha1(value));
      node.setAttribute("data-value", value);
    }
    node.innerHTML += "&nbsp";
    return node;
  }

  static value(domNode) {
    return domNode.getAttribute("data-value");
  }

  static equationToSVG(text) {
    const MathpixEquation = document.createElement("span");

    const converter = new showdown.Converter();
    const convertedHTML = converter.makeHtml(text);
    // Use a temporary div to facilitate conversion from string to DOM elements
    const tempDiv = document.createElement("div");
    tempDiv.innerHTML = convertedHTML;

    // Use querySelector on the temporary div to get the svg element
    const svgElement = tempDiv.querySelector("svg");

    // Initialize a new XMLSerializer
    const serializer = new XMLSerializer();

    // Check if the svgElement exists
    if (svgElement) {
      const svgString = serializer.serializeToString(svgElement);
      MathpixEquation.innerHTML = svgString;
    } else {
      console.log("No SVG element found");
    }

    return MathpixEquation;
  }

  static formats(domNode) {
    return { id: domNode.getAttribute("data-id") };
  }

  format(name, value) {
    if (name !== this.statics.blotName || !value) {
      super.format(name, value);
    } else {
      // @ts-expect-error
      this.domNode.setAttribute("data-id", sha1(value));
    }
  }

  html() {
    const { mathpix } = this.value();
    return `<span>${mathpix}</span>`;
  }
}

export default CustomMathBlot;
