import React, {
  createContext,
  useReducer,
  useEffect,
  useState,
  useMemo,
} from "react";
import produce from "immer";
// state of video data stored in VideoContext
export const VideoContext = createContext();

export const videoConfig = (draft, action) => {
  switch (action.func) {
    case "UPDATE_STATE":
      return action.data;
    case "SET_VIDEO_ID":
      draft.videoId = action.videoId;
      return draft;
    case "SET_VIDEO_SOURCE":
      draft.videoSource = action.videoSource;
      return draft;
    case "SET_VIDEO_URL":
      draft.videoURL = action.videoURL;
      return draft;
    case "CHANGE_DESCRIPTION":
      draft.videoDescription = action.description;
      return draft;
    case "CHANGE_CREDIT":
      draft.videoCredit = action.credit;
      return draft;
    case "CHANGE_TEXT_SETTINGS":
      draft.videoTextSettings = action.textSettings;
      return draft;
    default:
      return draft;
  }
};

// video provider wraps the tab component to access reducer
export const VideoProvider = ({ children, setProp, videoState }) => {
  const [state, dispatch] = useReducer(produce(videoConfig), videoState);
  const diff = JSON.stringify(state) !== JSON.stringify(videoState);
  const [mounted, setMounted] = useState(false);
  useEffect(() => {
    dispatch({ func: "UPDATE_STATE", data: videoState });
    setMounted(true);
  }, []);

  useEffect(() => {
    diff && mounted && setProp({ videoState: state });
  }, [state]);

  useEffect(() => {
    diff && mounted && dispatch({ func: "UPDATE_STATE", data: videoState });
  }, [videoState]);

  const videoValue = useMemo(() => [state, dispatch], [state]);

  return (
    <VideoContext.Provider value={videoValue}>{children}</VideoContext.Provider>
  );
};
