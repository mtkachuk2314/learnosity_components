import React, { useState, useRef, useMemo, useContext } from "react";
import styled from "@emotion/styled";
import Quill from "quill";
// Localization import
import { useTranslation } from "react-i18next";

import TenantContext from "../../../Context/TenantContext";

// Internal Imports
import VideoDescriptionCredit from "./VideoDescriptionCredit";
import Player from "./Player";
import Checkmark from "../assets/Checkmark";
import Toolbar from "./Toolbar";

// ?Provider
import { VideoContext } from "../VideoContext";
import { useSetFocused } from "./TabContext";
// portal
import PortalUtil from "../../../Utility/PortalUtil";

// styled components for Accordion styles

const StyledVideoContainer = styled("div")({
  width: "78%",
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  margin: "0 auto",
});

const StyledVideoDescriptionContainer = styled("div")({
  width: "100%",
  display: "flex",
  justifyContent: "space-between",
  flexwrap: "wrap",
  gap: "1.875rem",
  marginTop: "0.938rem",
});

const DescriptionCreditContainer = styled("div")({
  width: "calc(80.8% - 6.75rem)",
  display: "flex",
  flexGrow: "2",
  flexDirection: "column",
  gap: "10px",
});

const TranscriptButtonContainer = styled("button")(
  ({ videoData, videoSource }) => ({
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    gap: "0.35rem",
    height: "2rem",
    width: "6.75rem !important",
    padding:
      !videoData || videoSource === "youTube"
        ? "0.438rem 0.781rem"
        : "0.375rem 0.625rem 0.375rem 0.375rem",
    backgroundColor:
      videoData && videoSource !== "youTube"
        ? "rgba(46, 125, 50, 1)"
        : "rgba(0, 0, 0, 0.08)",
    color:
      videoData && videoSource !== "youTube"
        ? "rgba(255, 255, 255, 1)"
        : "rgba(0, 0, 0, 0.38)",
    border: "none",
    borderRadius: "16px",
    fontWeight: "400",
    fontSize: "0.813",
    lineHeight: "1.125rem",
    letterSpacing: "0.01rem",
    textAlign: "center",
    whiteSpace: "nowrap",
  })
);

const StyledConfigBar = styled("div")(() => ({
  backgroundColor: "transparent",
}));

// Video component
const Video = ({
  toolbarContainerRef,
  showSelf,
  isActiveComponent,
  setActiveComponent,
}) => {
  // Localization
  const { t } = useTranslation();
  // State
  const [state, dispatch] = useContext(VideoContext);
  const authoringData = useContext(TenantContext);
  // Observer mode
  const viewOnly = !authoringData?.viewOnlyComponents;
  const { brightcovePolicyKey } = authoringData;

  // Ref
  const videoRef = useRef();

  // Focus
  const [videoAreaFocused, setVideoAreaFocused] = useState(false);
  const isVideo = useMemo(() => true, []);

  // Toolbar
  const [mainToolbar, setMainToolbar] = useState(null);
  const [toolbar, setToolbar] = useState(null);

  const headers = {
    "BCOV-Policy": brightcovePolicyKey,
  };
  const Delta = Quill.import("delta");

  async function fetchVideoDescription(videoId, videoSource, videoURL) {
    // due to us not disabling the "Add from YouTube" button when first developing the Brightcove half of the functionality, videoSource could have been set to be "brightcove" or "youtube" for Brightcove videos.  Going forward, YouTube videoSource will be "youTube" - hence the odd logic in this if statement:
    if (videoSource === "brightcove" || videoSource === "youtube") {
      try {
        const result = await fetch(videoURL, { headers });
        const json = await result.json();

        if (videoId !== null && videoSource !== null) {
          const currentDescription = state.videoDescription
            ? state.videoDescription.ops
            : null;

          const descriptionDelta = new Delta([
            ...(currentDescription || []),
            {
              insert: `\n${json?.long_description}\n`,
            },
          ]);

          dispatch({
            func: "CHANGE_DESCRIPTION",
            description: descriptionDelta,
          });
        }
      } catch (error) {
        throw new Error(error);
      }
    }
  }

  const setVideoData = ({ videoId, videoSource, videoURL }) => {
    dispatch({
      func: "SET_VIDEO_ID",
      videoId,
    });
    dispatch({
      func: "SET_VIDEO_SOURCE",
      videoSource,
    });
    dispatch({
      func: "SET_VIDEO_URL",
      videoURL,
    });
    fetchVideoDescription(videoId, videoSource, videoURL);
  };

  const setVideoSetting = (videoTextSettings) => {
    dispatch({ func: "CHANGE_TEXT_SETTINGS", textSettings: videoTextSettings });
  };

  return (
    <div
      aria-label={t("Video component")}
      data-testid="video-container"
      className="infobox-videoContainer"
      style={{
        width: "100%",
        paddingTop: "46px",
        paddingBottom: "46px",
      }}
      ref={videoRef}
      tabIndex={0}
      role="region"
    >
      {!viewOnly && (showSelf || isActiveComponent) ? (
        <PortalUtil portalRef={toolbarContainerRef}>
          <StyledConfigBar>
            <Toolbar
              t={t}
              disconnect={!(showSelf || isActiveComponent)}
              setToolbar={setToolbar}
              setMainToolbar={setMainToolbar}
              isVideo={isVideo}
              setVideoSetting={setVideoSetting}
              setVideoData={setVideoData}
              showSelf={showSelf}
              isActiveComponent={isActiveComponent}
            />
          </StyledConfigBar>
        </PortalUtil>
      ) : null}
      <Player />
      <StyledVideoContainer className="video-infobox">
        <StyledVideoDescriptionContainer>
          <DescriptionCreditContainer>
            <div>
              <VideoDescriptionCredit
                t={t}
                disconnect={!(showSelf || isActiveComponent)}
                toolbar={toolbar}
                videoAreaFocused={videoAreaFocused}
                toolbarContainerRef={toolbarContainerRef}
                setVideoAreaFocused={setVideoAreaFocused}
                showSelf={showSelf}
                isActiveComponent={isActiveComponent}
                setActiveComponent={setActiveComponent}
              />
            </div>
          </DescriptionCreditContainer>
          <TranscriptButtonContainer
            data-testid="transcript"
            videoData={!!state.videoId}
            videoSource={state?.videoSource}
          >
            {state.videoId !== null && state.videoSource !== "youTube" ? (
              <Checkmark />
            ) : null}
            <span>
              {state.videoId && state.videoSource !== "youTube"
                ? t("Transcript")
                : t("No Transcript")}
            </span>
          </TranscriptButtonContainer>
        </StyledVideoDescriptionContainer>
      </StyledVideoContainer>
    </div>
  );
};

export default Video;
