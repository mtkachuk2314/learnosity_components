import React, { useContext } from "react";
import styled from "@emotion/styled";
import ReactPlayerLoader from "@brightcove/react-player-loader";
import YouTube from "react-youtube";
import TenantContext from "../../../Context/TenantContext";
import { VideoContext } from "../VideoContext";
import TriangleIcon from "../assets/Triangle.png";

// WYSIWYG Editor

const PlayerContainer = styled("div")({
  width: "78%",
  margin: "0 auto",
  "& .brightcove-react-player-loader": {
    width: "100%",
    height: "26.71875rem !important",
  },
  "& .video-js": {
    width: "100%",
    height: "100%",
  },
});

const StyledVideoContainer = styled("div")({
  width: "100%",
  height: "100%",
  maxWidth: "60.5rem",
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
});

const StyledVideoDefaultContainer = styled("div")({
  width: "100%",
  height: "26.71875rem !important",
  backgroundColor: "#EEEEEE",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
});

const StyledCircleContainer = styled("div")({
  width: "200px",
  height: "200px",
  outline: "5px solid #E0E0E0",
  borderRadius: "50%",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
});

const StyledTriangleImage = styled("img")({
  paddingLeft: "20px",
});

const Player = () => {
  const [state] = useContext(VideoContext);

  const BRIGHTCOVE_API = "https://edge.api.brightcove.com/playback/v1/accounts";

  const authoringData = useContext(TenantContext);
  const { brightcovePlayer, brightcoveAccountId } = authoringData;

  const opts = {
    display: "block",
    height: "427px",
    width: "100%",
    playerVars: {
      // https://developers.google.com/youtube/player_parameters
      autoplay: 0,
    },
  };

  const onReady = (e) => {
    return e.target === YouTube.PlayerState.UNSTARTED;
  };

  // TODO: Look into Access Assistant issue with "kind=captions"

  return (
    <PlayerContainer className="player-infobox">
      <StyledVideoContainer>
        {state.videoId == null ? (
          <StyledVideoDefaultContainer data-testid="video">
            <StyledCircleContainer>
              <StyledTriangleImage src={TriangleIcon} alt="Play Img" />
            </StyledCircleContainer>
          </StyledVideoDefaultContainer>
        ) : state.videoId !== null &&
          (state.videoSource === "brightcove" ||
            state.videoSource === "youtube") ? (
          <ReactPlayerLoader
            videoId={state.videoId}
            BRIGHTCOVE_API={BRIGHTCOVE_API}
            accountId={brightcoveAccountId}
            playerId={brightcovePlayer}
            data-testid="brightcove-video-player"
          />
        ) : (
          state.videoId !== null &&
          state.videoSource === "youTube" && (
            <YouTube
              style={{
                width: "100%",
                height: "26.71875rem !important",
              }}
              videoId={state.videoId}
              opts={opts}
              onReady={onReady}
              data-testid="youtube-video-player"
            />
          )
        )}
      </StyledVideoContainer>
    </PlayerContainer>
  );
};

export default Player;
