import React, { useState, useEffect, useRef, useContext } from "react";
// ? Video imports
import {
  ClickAwayListener,
  FormGroup,
  FormControl,
  FormControlLabel,
  Checkbox,
  Popper,
  Grow,
  Paper,
  AppBar,
  Toolbar,
  MenuItem,
  MenuList,
  IconButton,
  Button,
  Tooltip,
} from "@mui/material/";
import ErrorOutlineIcon from "@mui/icons-material/ErrorOutline";
import TenantContext from "../../../Context/TenantContext";
import "../../Text/styles/Toolbar.scss";
import "../assets/Toolbar.scss";
import { VideoContext } from "../VideoContext";

import BrightcoveSVG from "../assets/Brightcove";
import YoutubeSVG from "../assets/Youtube";
import KebabSVG from "../assets/Kebab";

import { useFocused, useSetFocused } from "./TabContext";
import icons from "../assets/icons";

const ToolBar = ({
  disconnect,
  setMainToolbar,
  setToolbar,
  setVideoData,
  setVideoSetting,
  showSelf,
  isActiveComponent,
  t,
}) => {
  // State Context
  const [state, dispatch] = useContext(VideoContext);

  // Video
  const [openVideo, setVideoOpen] = useState(false);
  const [openTranscript, setTranscriptOpen] = useState(false);
  const [openDescriptionKebab, setDescriptionKebabOpen] = useState(false);
  const [selectBrightcove, setSelectBrightcove] = useState(false);
  const [selectYoutube, setSelectYoutube] = useState(false);
  const [videoEdit, setVideoEdit] = useState(false);
  const [invalidVideoInput, setInvalidVideoInput] = useState(false);
  const [videoInput, setVideoInput] = useState("");

  const authoringData = useContext(TenantContext);
  const { brightcovePolicyKey, brightcoveAccountId } = authoringData;
  const focused = useFocused();
  const setFocused = useSetFocused();

  // Refs
  const portalToolbarRef = useRef(null);

  const AddVideo = useRef(null);
  const TranscriptVideo = useRef(null);
  const DescriptionKebab = useRef(null);
  const inputId = useRef(null);
  const inputError = useRef(null);
  const selectRef = useRef(null);
  const kebabselectRef = useRef(null);

  // Video fetch header
  const headers = {
    "BCOV-Policy": brightcovePolicyKey,
  };
  const BRIGHTCOVE_API = "https://edge.api.brightcove.com/playback/v1/accounts";

  // Video Close Toolbar
  const toggleCloseToolbar = (source) => {
    setInvalidVideoInput(false);
    setVideoEdit(false);
    if (
      source.includes("Kebab") ||
      source.includes("Transcript") ||
      source.includes("API")
    ) {
      setVideoOpen(false);
    }
    if (source.includes("Video")) {
      setSelectYoutube(false);
      setSelectBrightcove(false);
      setDescriptionKebabOpen(false);
    }
  };

  // ? Video Dropdown Handler
  const handleToggleVideo = (e) => {
    toggleCloseToolbar("Video");
    e.target.contains(AddVideo.current) && setVideoOpen(!openVideo);
  };

  const getFile = async (url, responseEdited, regex, json) => {
    const result = await fetch(url)
      .then((res) => res.text())
      .then((response) => {
        if (response) {
          responseEdited = response.replace(
            "X-TIMESTAMP-MAP=LOCAL:00:00:00.000,MPEGTS:0",
            ""
          );
          responseEdited = responseEdited.replace(regex, "");
          responseEdited = responseEdited.replace("WEBVTT", "");
          responseEdited = responseEdited.replace(
            "00:00.000 --> 01:00:00.000",
            ""
          );
          responseEdited = responseEdited.replace(
            /^(?:[\t ]*(?:\r?\n|\r))+/,
            ""
          );
          responseEdited = responseEdited.replaceAll("<br />", "");
          responseEdited = responseEdited.replaceAll("&quot;", '"');
          responseEdited = responseEdited.replaceAll("&apos;", "'");
        }
        const texts = [responseEdited]; // text content
        const element = document.createElement("a"); // anchor link
        const file = new Blob(texts, { type: "text/plain" }); // file object
        element.href = URL.createObjectURL(file);
        element.download = `${json.name}.txt`;
        document.body.appendChild(element); // simulate link click
        element.click(); // Required for this to work in FireFox
      })
      .catch((err) => console.log(err));
    return result;
  };

  const apiCall = async () => {
    const result = await fetch(state.videoURL, { headers });
    const json = await result.json();
    const responseEdited = "";
    const regex = /\d\d:\d\d\.\d\d\d\s+-->\s+\d\d:\d\d\.\d\d\d.*\n/gi;
    const chosenTrack = json.text_tracks[1].src;
    const colonLocation = chosenTrack.indexOf(":");
    const url = chosenTrack.substr(colonLocation + 1);
    getFile(url, responseEdited, regex, json);
  };

  // Download transcript button
  const handleClickTranscript = (e) => {
    setVideoOpen(false);
    toggleCloseToolbar("Transcript");
    e.target.contains(TranscriptVideo.current) && setTranscriptOpen(!openVideo);

    apiCall();
  };
  // Toggle Kebab Dropdown
  const handleToggleVideoKebab = () => {
    toggleCloseToolbar("Kebab");
    setDescriptionKebabOpen(!openDescriptionKebab);
  };

  const setVideoSource = (e, source) => {
    e.stopPropagation();
    dispatch({
      func: "SET_VIDEO_SOURCE",
      videoSource: source,
    });
    if (source === "brightcove" || source === "youtube") {
      setSelectBrightcove(true);
    } else if (source === "youTube") {
      setSelectYoutube(true);
    }
  };

  // ? Video API Select Toolbar (Brightcove, Youtube) and Video Add/Edit / Delete
  const handleVideoAPI = (e, source, action) => {
    const inputValue = inputId?.current?.value;
    const linkValidityRegex = new RegExp(
      "^(https:\\/\\/)?" + // validate protocol
        "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // validate domain name
        "((\\d{1,3}\\.){3}\\d{1,3}))" + // validate OR ip (v4) address
        "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // validate port and path
        "(\\?[;&a-z\\d%_.~+=-]*)?" + // validate query string
        "(\\#[-a-z\\d_]*)?$",
      "i"
    ); // validate fragment locator
    const isValid = linkValidityRegex.test(inputValue);
    const regExp =
      /^https?\:\/\/(?:www\.youtube(?:\-nocookie)?\.com\/|m\.youtube\.com\/|youtube\.com\/)?(?:ytscreeningroom\?vi?=|youtu\.be\/|vi?\/|user\/.+\/u\/\w{1,2}\/|embed\/|watch\?(?:.*\&)?vi?=|\&vi?=|\?(?:.*\&)?vi?=)([^#\&\?\n\/<>"']*)/i;
    const match = inputValue?.match(regExp);
    if (action === "AddVideo") {
      if (state.videoSource === "brightcove" && inputValue.length === 13) {
        setVideoData({
          videoSource: source,
          videoId: inputValue,
          videoURL: `${BRIGHTCOVE_API}/${brightcoveAccountId}/videos/${inputValue}`,
        });
        toggleCloseToolbar("API");
        setVideoEdit(false);
        setInvalidVideoInput(false);
      } else if (state.videoSource === "youTube" && match) {
        if (match[1] && match[1].length === 11) {
          setVideoData({
            videoSource: source,
            videoId: match[1],
            videoURL: inputValue,
          });
          toggleCloseToolbar("API");
          setVideoEdit(false);
          setInvalidVideoInput(false);
        } else {
          setInvalidVideoInput(true);
        }
      } else if (state.videoSource === "youTube" && isValid && !match) {
        setVideoData({
          videoSource: source,
          videoId: inputValue,
          videoURL: inputValue,
        });
        setInvalidVideoInput(false);
      } else if (inputValue.length === 0) {
        handleVideoAPI(
          e,
          selectBrightcove ? "brightcove" : "youTube",
          "RemoveVideo"
        );
      } else {
        setInvalidVideoInput(true);
      }
    } else if (action === "RemoveVideo") {
      setVideoData({
        videoSource: null,
        videoId: null,
        videoURL: null,
      });
      toggleCloseToolbar("API");
      setVideoEdit(false);
    } else if (action === "EditVideo") {
      setVideoEdit(true);
    }
  };

  // ? Video Text Settings
  const handleTextSettings = (e, source) => {
    source === "description" && focused === "Description" && setFocused(null);
    source === "credit" && focused === "Credit" && setFocused(null);

    e.stopPropagation();
    setVideoSetting({
      ...state.videoTextSettings,
      [source]: e.target.checked,
    });
  };

  // Source state
  useEffect(() => {
    if (state.videoId) {
      state.videoSource === "brightcove" && setSelectBrightcove(true);
      state.videoSource === "youTube" && setSelectYoutube(true);
    }
  });

  useEffect(() => {
    setToolbar(portalToolbarRef.current);
  }, []);

  return (
    <div
      ref={setMainToolbar}
      className="ToolbarContainer"
      style={{
        "--active": !disconnect ? "block" : "none",
      }}
      onClick={(e) => e.stopPropagation()}
      onFocus={(e) => e.stopPropagation()}
      data-testid="video-toolbar"
    >
      <AppBar
        position="static"
        className="StyledAppbar"
        elevation={0}
        style={{
          "--display": "flex",
          "--direction": "row",
          "--gap": "0.625rem",
          "--boxShadow": "none !important",
        }}
      >
        {/* Add Video Drop Down */}
        <ClickAwayListener onClickAway={() => toggleCloseToolbar(["Kebab"])}>
          <Toolbar
            position="static"
            ref={selectRef}
            tabIndex={0}
            className="StyledToolbar"
            style={{
              outline: "none",
              "--width": state.videoId ? "19.375rem" : "12.5rem",
              "--grid-template-columns": state.videoId
                ? "7.3125rem 0.5625rem 10.5625rem"
                : "5.5rem 0.5625rem 5.5rem",
              "--boxShadow":
                "0rem 0rem 0.1875rem 0rem rgba(35, 35, 35, 0.15),-0.0625rem 0rem 0.3125rem 0rem rgba(161, 162, 163, 0.2),0.1875rem 0.1875rem 0.25rem 0rem rgba(35, 35, 35, 0.15)",
            }}
            role="menubar"
          >
            <Tooltip
              title={state.videoId ? t("change video") : t("add video")}
              describeChild
              placement="top"
              arrow
              PopperProps={{
                modifiers: [
                  {
                    name: "offset",
                    options: {
                      offset: [0, -7],
                    },
                  },
                ],
              }}
            >
              <Button
                ref={AddVideo}
                data-addVideoid="AddVideo"
                aria-label={state.videoId ? t("Change Video") : t("Add Video")}
                aria-expanded={openVideo ? "true" : undefined}
                disableRipple
                disableFocusRipple
                variant="contained"
                className="SelectButton_video"
                style={{
                  "--active": openVideo ? "rgba(21, 101, 192, 1)" : "#000",
                  "--width": "100%",
                  "--height": "100%",
                  "--font-size": "1rem",
                  "--grid-template-columns": "1fr",
                  "--hover-background-color": "transparent",
                }}
                onClick={handleToggleVideo}
              >
                {state.videoId ? t("Change Video") : t("Add Video")}
              </Button>
            </Tooltip>
            {/* Select Brightspace OR Youtube Dropdown */}
            {!selectYoutube && !selectBrightcove && openVideo && (
              <Popper
                open={openVideo}
                anchorEl={AddVideo.current}
                placement="bottom-start"
                transition
                disablePortal
                modifiers={[
                  {
                    name: "offset",
                    options: {
                      offset: state.videoId ? [-55, 0] : [-5, 0],
                    },
                  },
                ]}
              >
                {({ TransitionProps }) => (
                  <Grow {...TransitionProps}>
                    <Paper
                      elevation={0}
                      className="StyledSelectPaper"
                      style={{
                        "--height": "5.5rem",
                        "--width": "16rem",
                        "--margin-left": "0.125rem",
                        "--margin-top": "0.125rem",
                      }}
                    >
                      <MenuList
                        data-testid="video-select-dropdown"
                        className="StyledMenu"
                        style={{
                          "--gridTemplateRows": "1fr 1fr",
                          "--padding": "0.5rem 0rem",
                        }}
                      >
                        <Tooltip
                          title={t("add brightcove video")}
                          describeChild
                          placement="top"
                          arrow
                          PopperProps={{
                            modifiers: [
                              {
                                name: "offset",
                                options: {
                                  offset: [0, -7],
                                },
                              },
                            ],
                          }}
                        >
                          <MenuItem
                            onClick={(e) => {
                              setVideoSource(e, "brightcove");
                            }}
                            data-testid="brightcove-select-button"
                            aria-label={t("add brightcove video")}
                            className="StyledMenuItem"
                            style={{
                              width: "16rem",
                              height: "2.25rem",
                              "--letterSpacing": "0.009375rem",
                            }}
                            disableRipple
                            disableFocusRipple
                          >
                            <BrightcoveSVG />
                            <span style={{ marginLeft: "2rem" }}>
                              {t("Add from Brightcove")}
                            </span>
                          </MenuItem>
                        </Tooltip>
                        <Tooltip
                          title={t("add youtube video")}
                          describeChild
                          placement="top"
                          arrow
                          style={{ pointerEvents: "none" }}
                          PopperProps={{
                            modifiers: [
                              {
                                name: "offset",
                                options: {
                                  offset: [0, -7],
                                },
                              },
                            ],
                          }}
                        >
                          <MenuItem
                            onClick={(e) => {
                              setVideoSource(e, "youTube");
                            }}
                            data-testid="youtube-select-button"
                            aria-label={t("add youtube video")}
                            disableRipple
                            disableFocusRipple
                            className="StyledMenuItem"
                            style={{
                              width: "16rem",
                              height: "2.25rem",
                              "--letterSpacing": "0.009375rem",
                            }}
                          >
                            <YoutubeSVG />
                            <span style={{ marginLeft: "2rem" }}>
                              {t("Add from YouTube")}
                            </span>
                          </MenuItem>
                        </Tooltip>
                      </MenuList>
                    </Paper>
                  </Grow>
                )}
              </Popper>
            )}
            {/* Add , Edit , Delete ID Dropdown */}
            {(selectBrightcove || selectYoutube) && openVideo && (
              <Popper
                open={openVideo}
                anchorEl={AddVideo.current}
                placement="bottom-start"
                transition
                disablePortal
                modifiers={[
                  {
                    name: "offset",
                    options: {
                      offset: state.videoId ? [-5, 0] : [-5, 0],
                    },
                  },
                ]}
              >
                {({ TransitionProps }) => (
                  <Grow {...TransitionProps}>
                    <Paper
                      elevation={0}
                      className="StyledSelectPaper"
                      disableFocusRipple
                      disablePortal
                      disableRipple
                      style={{
                        "--height": "2.5rem",
                        "--width": "16rem",
                        "--margin-left": "0.125rem",
                        "--margin-top": "0.125rem",
                      }}
                    >
                      <MenuList
                        className="StyledMenu"
                        style={{
                          "--gridTemplateColumns": state.videoId
                            ? "1fr auto"
                            : "1fr auto",
                          "--width": "16rem",
                          height: "100%",
                          "--padding": state.videoId
                            ? "0rem 0.4375rem 0 0.625rem"
                            : "0rem 0.3125rem 0 0.625rem",
                        }}
                      >
                        {/* Add Video */}
                        {!!(state.videoId && !videoEdit) &&
                        state.videoSource === "youTube" &&
                        state.videoURL.length > 0 ? (
                          <a
                            href={state.videoURL}
                            target="_blank"
                            className="validYouTubeURL"
                            rel="noreferrer"
                          >
                            {state.videoURL}
                          </a>
                        ) : (
                          <input
                            ref={inputId}
                            data-testid={
                              selectBrightcove
                                ? "brightcove-input-field"
                                : "youtube-input-field"
                            }
                            aria-labelledby={`${
                              selectBrightcove ? "brightcove" : "youtube"
                            } input field`}
                            type="text"
                            placeholder={
                              state.videoSource === "brightcove" ||
                              state.videoSource === "youtube"
                                ? t("Paste unique identifier")
                                : state.videoSource === "youTube"
                                ? t("Paste video link")
                                : ""
                            }
                            defaultValue={
                              (state.videoSource === "brightcove" ||
                                state.videoSource === "youtube") &&
                              selectBrightcove
                                ? state.videoId
                                : state.videoSource === "youTube" &&
                                  selectYoutube
                                ? state.videoURL
                                : ""
                            }
                            disabled={!!(state.videoId && !videoEdit)}
                            onClick={(e) => e.stopPropagation()}
                            onChange={() => {
                              setInvalidVideoInput(false);
                              setVideoInput(inputId.current.value);
                            }}
                            className="StyledInput"
                            style={{
                              "--color":
                                invalidVideoInput && "rgba(211, 47, 47, 1)",
                            }}
                          />
                        )}
                        {!state.videoId || videoEdit ? (
                          <Tooltip
                            title={t("add video id")}
                            describeChild
                            placement="top"
                            arrow
                            PopperProps={{
                              modifiers: [
                                {
                                  name: "offset",
                                  options: {
                                    offset: [0, -7],
                                  },
                                },
                              ],
                            }}
                          >
                            <Button
                              type="submit"
                              data-testid={`${state.videoSource}-submit-button`}
                              aria-label={t(
                                `${state.videoSource} video submit button`
                              )}
                              onClick={(e) => {
                                selectRef.current.focus();
                                handleVideoAPI(
                                  e,
                                  selectBrightcove ? "brightcove" : "youTube",
                                  "AddVideo"
                                );
                              }}
                              className="SelectButton_video"
                              style={{
                                "--width": "2.4375rem",
                                "--min-width": "2.4375rem !important",
                                "--height": "2rem",
                                "--padding": "0.25rem 0.3125rem",
                                "--font-size": "0.875rem",
                                "--grid-template-columns": "1fr",
                                "--hover-background-color":
                                  "rgba(21, 101, 192, 0.04)",
                                "--disabled": "rgba(0, 0, 0, 0.38)",
                              }}
                              disabled={videoInput.length === 0}
                              disableRipple
                              disableFocusRipple
                            >
                              {t("Add")}
                            </Button>
                          </Tooltip>
                        ) : (
                          // Edit / Delete Video
                          <div
                            className="StyledToolbar"
                            style={{
                              "--width": "4rem",
                              "--boxShadow": "none",
                              "--borderLeft": "none",
                              "--grid-template-columns": "1fr 1fr",
                              gap: "0.25rem",
                            }}
                          >
                            <Tooltip
                              title={
                                state.videoSource === "brightcove" ||
                                state.videoSource === "youtube"
                                  ? t("edit video id")
                                  : state.videoSource === "youTube"
                                  ? t("edit video link")
                                  : null
                              }
                              describeChild
                              placement="top"
                              arrow
                              PopperProps={{
                                modifiers: [
                                  {
                                    name: "offset",
                                    options: {
                                      offset: [0, -7],
                                    },
                                  },
                                ],
                              }}
                            >
                              <IconButton
                                aria-label={
                                  state.videoSource === "brightcove" ||
                                  state.videoSource === "youtube"
                                    ? t("edit video id")
                                    : state.videoSource === "youTube"
                                    ? t("edit video link")
                                    : null
                                }
                                className="StyledIconButton"
                                onClick={(e) =>
                                  handleVideoAPI(
                                    e,
                                    selectBrightcove ? "brightcove" : "youTube",
                                    "EditVideo"
                                  )
                                }
                              >
                                {icons.pencil}
                              </IconButton>
                            </Tooltip>
                            <Tooltip
                              title={
                                state.videoSource === "brightcove" ||
                                state.videoSource === "youtube"
                                  ? t("delete video id")
                                  : state.videoSource === "youTube"
                                  ? t("delete video link")
                                  : null
                              }
                              describeChild
                              placement="top"
                              arrow
                              PopperProps={{
                                modifiers: [
                                  {
                                    name: "offset",
                                    options: {
                                      offset: [0, -7],
                                    },
                                  },
                                ],
                              }}
                            >
                              <IconButton
                                aria-label={
                                  state.videoSource === "brightcove" ||
                                  state.videoSource === "youtube"
                                    ? t("delete video id")
                                    : state.videoSource === "youTube"
                                    ? t("delete video link")
                                    : null
                                }
                                className="StyledIconButton"
                                onClick={(e) =>
                                  handleVideoAPI(
                                    e,
                                    selectBrightcove ? "brightcove" : "youTube",
                                    "RemoveVideo"
                                  )
                                }
                              >
                                {icons.trashcan}
                              </IconButton>
                            </Tooltip>
                          </div>
                        )}
                        {/* </Button> */}
                      </MenuList>
                    </Paper>
                  </Grow>
                )}
              </Popper>
            )}
            {/* Invalid Id Error */}
            {invalidVideoInput && (
              <Popper
                open={invalidVideoInput}
                anchorEl={inputError.current}
                placement="bottom-start"
                transition
                disablePortal
                sx={{ pointerEvents: "none" }}
              >
                {({ TransitionProps }) => (
                  <Grow {...TransitionProps}>
                    <Paper
                      sx={{
                        backgroundColor: "rgb(251, 234, 234) !important",
                        marginTop: "5.25rem",
                        marginLeft: "0.25rem",
                        width: "16rem",
                        height: "1.875rem",
                        cursorEvents: "none",
                      }}
                    >
                      <div
                        data-testid="input-invalid-error"
                        aria-labelledby="input-invalid-error"
                        style={{
                          display: "flex",
                          justifyContent: "flex-start",
                          alignItems: "center",
                          height: "100%",
                        }}
                      >
                        {/* Input Error */}
                        <ErrorOutlineIcon
                          color="error"
                          fontSize="small"
                          sx={{
                            margin: "0.364375rem 0.739375rem",
                          }}
                        />
                        <span
                          style={{
                            fontSize: "0.75rem",
                            fontWeight: "400",
                            lineHeight: "1.25rem",
                            letterSpacing: "0.025rem",
                          }}
                        >
                          {t("Invalid URL")}
                        </span>
                      </div>
                    </Paper>
                  </Grow>
                )}
              </Popper>
            )}
            <div className="StyledDivider" />
            {/* Download Transcript Button */}
            <Tooltip
              title={t("Download Transcript")}
              describeChild
              placement="top"
              arrow
              PopperProps={{
                modifiers: [
                  {
                    name: "offset",
                    options: {
                      offset: [0, -7],
                    },
                  },
                ],
              }}
            >
              <Button
                data-testid="download-transcript"
                ref={TranscriptVideo}
                aria-expanded={openTranscript ? "true" : undefined}
                variant="contained"
                disableRipple
                disableFocusRipple
                className="SelectButton_video"
                style={{
                  "--width": "100%",
                  "--height": "100%",
                  "--font-size": "1rem",
                  "--grid-template-columns": "1fr",
                  "--hover-background-color": "transparent",
                  "--disabled": "rgba(0, 0, 0, 0.38)",
                }}
                onClick={handleClickTranscript}
                // TODO: Update this logic when we discover more re: YouTube transcripts
                disabled={!state.videoId || state.videoSource === "youTube"}
              >
                {state.videoId ? t("Download Transcript") : t("Transcript")}
              </Button>
            </Tooltip>
          </Toolbar>
        </ClickAwayListener>

        <div
          ref={portalToolbarRef}
          style={{ position: "static", width: "12.25rem" }}
        >
          {/* formatting dropdown */}
          {(showSelf || isActiveComponent) &&
            focused !== "Credit" &&
            focused !== "Description" && (
              <Toolbar
                data-testid="video-formatting-toolbar"
                position="static"
                className="StyledToolbar"
                style={{
                  "--width": "12.25rem !important",
                  "--boxShadow": "0rem 0rem 0.625rem rgba(0, 0, 0, 0.1)",
                  "--borderLeft": "none",
                  "--grid-template-columns":
                    "1fr 1fr 1fr 0.5rem 1fr 0.5rem 1fr",
                  "--justify-items": "center",
                }}
              >
                <IconButton
                  disableRipple
                  disabled
                  className="StyledIconButton bold"
                  aria-label={t("formatting dropdown button")}
                >
                  {icons.customBold}
                </IconButton>
                {/* alignment dropdown */}
                <IconButton
                  disableRipple
                  disabled
                  className="StyledIconButton"
                  aria-label={t("alignment dropdown button")}
                >
                  {icons.align}
                </IconButton>

                {/* bullets drowdown starts */}

                <IconButton
                  disableRipple
                  disabled
                  className="StyledIconButton list"
                  aria-label={t("list dropdown button")}
                >
                  {icons.bullet}
                </IconButton>

                {/* link btn and divider */}
                <div className="StyledDivider" />

                <IconButton
                  disableRipple
                  disabled
                  className="StyledIconButton link"
                  aria-label={t("link button")}
                >
                  {icons.link}
                </IconButton>
              </Toolbar>
            )}
        </div>
        {/* {/* Video Kebab */}
        <ClickAwayListener onClickAway={() => toggleCloseToolbar(["Video"])}>
          <div
            ref={kebabselectRef}
            style={{
              position: "absolute",
              display: "grid",
              gridTemplateColumns: "0.5625rem 1fr",
              height: "2.5rem",
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
              left: state.videoId ? "29.4375rem" : "22.5rem",
              zIndex: "1",
            }}
          >
            <div className="StyledDivider" />
            <Tooltip
              title={t("configure video description")}
              describeChild
              placement="top"
              arrow
            >
              <IconButton
                ref={DescriptionKebab}
                open={openDescriptionKebab}
                data-videoid="videoSettings"
                aria-label={t("Change video settings")}
                aria-expanded={openDescriptionKebab ? "true" : undefined}
                variant="contained"
                disableRipple
                disableFocusRipple
                onClick={handleToggleVideoKebab}
                className="StyledIconButton"
                style={{
                  "--active": openDescriptionKebab
                    ? "rgba(21, 101, 192, 1)"
                    : "#000",
                  "--background": openDescriptionKebab
                    ? "rgba(21, 101, 192, 0.12)"
                    : "#fff",
                }}
              >
                <KebabSVG />
              </IconButton>
            </Tooltip>
            <Popper
              open={openDescriptionKebab}
              anchorEl={DescriptionKebab.current}
              placement="bottom-start"
              transition
              disablePortal
            >
              {({ TransitionProps }) => (
                <Grow {...TransitionProps}>
                  <Paper
                    style={{
                      boxShadow:
                        "0rem 0.0625rem 0.1875rem 0rem rgba(35, 35, 35, 0.1), 0rem 0.125rem 0.3125rem 0rem rgba(161, 162, 163, 0.2), 0.1875rem 0.1875rem 0.25rem 0rem rgba(35, 35, 35, 0.15)",
                    }}
                  >
                    <MenuList
                      data-testid="video-description-settings-dropdown"
                      className="StyledCheckboxMenu"
                      style={{
                        "--height": "auto",
                        "--margin": "0.5rem 0",
                      }}
                    >
                      <FormGroup
                        sx={{
                          gap: "0.875rem",
                          margin: "0.5rem 1rem !important",

                          "&:focus-visible": {
                            outline: "none",
                          },
                        }}
                        tabIndex="-1"
                      >
                        <FormControl>
                          <Tooltip
                            title={t("show description")}
                            describeChild
                            placement="top"
                            arrow
                            PopperProps={{
                              modifiers: [
                                {
                                  name: "offset",
                                  options: {
                                    offset: [0, -7],
                                  },
                                },
                              ],
                            }}
                          >
                            <FormControlLabel
                              className="StyledFormControlLabel"
                              control={
                                <Checkbox
                                  checked={state.videoTextSettings.description}
                                  onClick={(e) => {
                                    e.stopPropagation();
                                    handleTextSettings(e, "description");
                                  }}
                                  sx={{
                                    "&:hover": {
                                      bgcolor: "transparent",
                                      color: "rgba(21, 101, 192, 1)",
                                    },
                                    "&.Mui-checked": {
                                      bgcolor: "transparent",
                                      color: "rgba(21, 101, 192, 1)",
                                    },
                                  }}
                                />
                              }
                              label={t("Show Description")}
                              size="small"
                            />
                          </Tooltip>
                        </FormControl>
                        <FormControl>
                          <Tooltip
                            title={t("show credit")}
                            describeChild
                            placement="top"
                            arrow
                            PopperProps={{
                              modifiers: [
                                {
                                  name: "offset",
                                  options: {
                                    offset: [0, -7],
                                  },
                                },
                              ],
                            }}
                          >
                            <FormControlLabel
                              className="StyledFormControlLabel"
                              control={
                                <Checkbox
                                  checked={state.videoTextSettings.credit}
                                  onClick={(e) => {
                                    e.stopPropagation();
                                    handleTextSettings(e, "credit");
                                  }}
                                  sx={{
                                    "&:hover": {
                                      bgcolor: "transparent",
                                      color: "rgba(21, 101, 192, 1)",
                                    },
                                    "&.Mui-checked": {
                                      bgcolor: "transparent",
                                      color: "rgba(21, 101, 192, 1)",
                                    },
                                  }}
                                />
                              }
                              label={t("Show Credit")}
                              size="small"
                            />
                          </Tooltip>
                        </FormControl>
                      </FormGroup>
                    </MenuList>
                  </Paper>
                </Grow>
              )}
            </Popper>
          </div>
        </ClickAwayListener>
      </AppBar>
    </div>
  );
};

export default React.memo(ToolBar);
