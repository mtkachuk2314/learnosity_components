import React, {
  useContext,
  useCallback,
  useMemo,
  useState,
  useRef,
  useEffect,
  Fragment,
} from "react";
import styled from "@emotion/styled";
// Context
import {
  useSetFocused,
  useSetDescriptionRef,
  useSetCreditRef,
  useFocused,
} from "./TabContext";
import { VideoContext } from "../VideoContext";
// Components
import Text from "../../Text/Text";

const TextContainer = styled("div")({
  position: "relative",
  minHeight: "20px",
  width: "100%",
});

const VideoDescriptionCredit = ({
  t,
  toolbar,
  setVideoAreaFocused,
  toolbarContainerRef,
  showSelf,
  isActiveComponent,
  setActiveComponent,
}) => {
  // State
  const [state, dispatch] = useContext(VideoContext);

  // WYSIWYG Editor
  const setFocused = useSetFocused();
  const focused = useFocused();
  const setDescriptionRef = useSetDescriptionRef();
  const setCreditRef = useSetCreditRef();

  // Description and Credit text
  const [creditText, setCreditText] = useState(null);
  const [descriptionText, setDescriptionText] = useState(null);

  // Description and Credit references
  const descRef = useRef();
  const credRef = useRef();

  // Description and Credit text update
  const updateDescription = useCallback((body) => {
    dispatch({ func: "CHANGE_DESCRIPTION", description: body.body });
  });

  const updateCredit = useCallback((body) => {
    dispatch({ func: "CHANGE_CREDIT", credit: body.body });
  });

  // Description and Credit portals
  const portalDescription = useMemo(() => {
    return {
      parentComponent: "video",
      parentSection: "description",
      placeholder: t("Video Description"),
      toolbarReference: toolbar,
      shouldPortal: focused === "Description",
      isKebab: null,
      setIsKebab: (result) => null,
      disabledButtons: [],
      setParentFocused: (result) => setVideoAreaFocused(result),
      setTextRef: (result) => setDescriptionRef(result),
      setTextContainer: (result) => setDescriptionText(result),
    };
  }, [toolbar, focused]);

  const portalCredit = useMemo(() => {
    return {
      parentComponent: "video",
      parentSection: "credit",
      placeholder: t("Credit"),
      toolbarReference: toolbar,
      shouldPortal: focused === "Credit",
      isKebab: null,
      setIsKebab: (result) => null,

      disabledButtons: ["bold", "list"],
      setParentFocused: (result) => setVideoAreaFocused(result),
      setTextRef: (result) => setCreditRef(result),
      setTextContainer: (result) => setCreditText(result),
    };
  }, [toolbar, focused]);

  // Description and Credit click handlers
  const handleDescriptionClick = useCallback(() => {
    setActiveComponent(true);
    setFocused("Description");
  }, []);
  const handleCreditClick = useCallback(() => {
    setActiveComponent(true);
    setFocused("Credit");
  }, []);

  useEffect(() => {
    !(isActiveComponent || showSelf) && setFocused(null);
  }, [isActiveComponent, showSelf]);

  return (
    <>
      {state.videoTextSettings.description === true && (
        <TextContainer
          ref={descRef}
          className="videoDescription"
          onClick={handleDescriptionClick}
          onFocus={handleDescriptionClick}
          onBlur={(e) => {
            const relatedTarget = e.relatedTarget || document.activeElement;
            if (descriptionText.contains(relatedTarget)) e.stopPropagation();
          }}
        >
          {/* Description Text box */}
          <Text
            body={state.videoDescription}
            setProp={updateDescription}
            portal={portalDescription}
            t={t}
            toolbarContainerRef={toolbarContainerRef}
            showSelf={showSelf}
            isActiveComponent={isActiveComponent}
          />
        </TextContainer>
      )}
      {state.videoTextSettings.credit === true && (
        <div
          ref={credRef}
          onClick={handleCreditClick}
          onFocus={handleCreditClick}
          onBlur={(e) => {
            const relatedTarget = e.relatedTarget || document.activeElement;
            if (creditText.contains(relatedTarget)) e.stopPropagation();
          }}
        >
          {/* Credit Text box */}
          <Text
            body={state.videoCredit}
            setProp={updateCredit}
            portal={portalCredit}
            t={t}
            toolbarContainerRef={toolbarContainerRef}
            showSelf={showSelf}
            isActiveComponent={isActiveComponent}
          />
        </div>
      )}
    </>
  );
};

export default VideoDescriptionCredit;
