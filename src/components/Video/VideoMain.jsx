import React from "react";
import { v4 as uuidv4 } from "uuid";
import { TabProvider } from "./subcomponents/TabContext";
import { VideoProvider } from "./VideoContext";
import Video from "./subcomponents/Video";
import { MainWrapper } from "../../theme/styledComponents/globalComponent";

export const defaultProps = {
  videoState: {
    id: uuidv4(),
    videoSource: null,
    videoURL: null,
    videoDescription: null,
    videoCredit: null,
    videoId: null,
    videoTextSettings: {
      description: true,
      credit: true,
    },
  },
};

const VideoMain = ({
  videoState = {},
  setProp = () => {},
  toolbarContainerRef,
  showSelf = null,
  isActiveComponent = null,
  setActiveComponent = () => {},
}) => {
  return (
    <VideoProvider videoState={videoState} setProp={setProp}>
      <TabProvider>
        <MainWrapper>
          <Video
            toolbarContainerRef={toolbarContainerRef}
            showSelf={showSelf}
            isActiveComponent={isActiveComponent}
            setActiveComponent={setActiveComponent}
          />
        </MainWrapper>
      </TabProvider>
    </VideoProvider>
  );
};

export default VideoMain;
