import React from "react";
import { useDragLayer } from "react-dnd";
import styled from "@emotion/styled";

const StyledPreview = styled("table")(
  ({ x, y, tablePreviewHeight, tablePreviewWidth }) => ({
    position: "fixed",
    left: 0,
    top: 0,
    transform: `translate(${x}px, ${y}px)`,
    height: `${tablePreviewHeight}px`,
    width: `${tablePreviewWidth}px`,
    pointerEvents: "none",
  })
);

const ColumnPreview = ({ tableRef, theadRef }) => {
  const { currentOffset } = useDragLayer((monitor) => ({
    currentOffset: monitor.getSourceClientOffset(),
  }));

  const { x, y } = currentOffset || {};

  return (
    <StyledPreview
      x={x}
      y={y}
      tablePreviewHeight={tableRef.current && tableRef.current.offsetHeight}
      tablePreviewWidth={
        theadRef.current && theadRef.current.lastChild.lastChild.offsetWidth
      }
      data-testid="table-drop-locator"
    />
  );
};

export default ColumnPreview;
