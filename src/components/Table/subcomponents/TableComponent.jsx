import React, { useState, useContext, useRef, useEffect } from "react";

// react-table imports
import {
  flexRender,
  getCoreRowModel,
  useReactTable,
} from "@tanstack/react-table";
import DragIndicatorIcon from "@mui/icons-material/DragIndicator";

import { TextareaAutosize } from "@material-ui/core";
// react-dnd imports
import { DndProvider, useDrag, useDrop } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";

import styled from "@emotion/styled";
import TenantContext from "../../../Context/TenantContext";
import useDebounce from "../../../hooks/useDebounce";

import Toolbar from "./Toolbar";
import ColumnPreview from "./ColumnPreview";

// Styled import
import { LayoutContext } from "../TableContext";
import "../styles/TableComponent.scss";

// Toolbar
import PortalUtil from "../../../Utility/PortalUtil";
// Styled components
const StyledTable = styled("table")(({ showStripes, headerType, tableId }) => ({
  "tr:nth-of-type(odd):not(:first-of-type)": {
    backgroundColor: headerType === "top-header" && showStripes && "#F5F5F5",
  },
  "tr:nth-of-type(even)": {
    backgroundColor: headerType === "side-header" && showStripes && "#F5F5F5",
  },
}));

const StyledTd = styled("td")(
  ({ selectHighlight, titleType, verticalAlignment, viewOnly }) => ({
    border: selectHighlight ? "1px double #1565C0" : "1px solid #232323",
    backgroundColor: titleType
      ? selectHighlight
        ? "#e2e6ea"
        : "#EEEEEE"
      : selectHighlight
      ? "rgba(21, 101, 192, 0.08)"
      : "",
    verticalAlign:
      verticalAlignment === "top-align"
        ? "top"
        : verticalAlignment === "middle-align"
        ? "middle"
        : verticalAlignment === "bottom-align"
        ? "bottom"
        : "middle",
  })
);

const StyledInput = styled(TextareaAutosize)(
  ({ type, horizontalAlignment, verticalAlignment, viewOnly }) => {
    let backgroundColor = "";
    let padding = "20px 10px";
    let fontSize = "16px";
    let fontWeight = "400";
    let textAlign = "left";
    let height = "50px";

    if (type === "title") {
      fontSize = "18px";
      fontWeight = "500";
      textAlign = "center";
      height = "74px";
      if (verticalAlignment === "top-align") {
        padding = "15px 10px 35px 10px";
      } else if (verticalAlignment === "middle-align") {
        padding = "16px 10px";
      } else if (verticalAlignment === "bottom-align") {
        padding = "35px 10px 15px 10px";
      }
    } else if (type === "cell") {
      if (verticalAlignment === "top-align") {
        padding = "10px 10px 30px 10px";
      } else if (verticalAlignment === "middle-align") {
        padding = "15px 15px";
      } else if (verticalAlignment === "bottom-align") {
        padding = "30px 10px 10px 10px";
      }
    }

    if (viewOnly && type === "title") {
      backgroundColor = "#f5f5f5 !important";
    }

    if (horizontalAlignment === "left-align") {
      textAlign = "left";
    } else if (horizontalAlignment === "right-align") {
      textAlign = "right";
    } else if (
      horizontalAlignment === "center-align" ||
      (type === "title" && horizontalAlignment === "center-align")
    ) {
      textAlign = "center";
    }

    return {
      backgroundColor,
      padding,
      fontSize,
      fontWeight,
      textAlign,
      height,
      textOverflow: type === "title" ? "ellipsis" : "initial",
    };
  }
);

const StyledConfigBar = styled("div")({
  backgroundColor: "transparent",
});

// Filter SelectSection string to return aria-label
const ariaSection = (selection) => {
  let readOut;
  if (selection?.charAt(0) === "c") {
    readOut = `column ${selection?.replace(/[^0-9]/g, "")}`;
  } else {
    readOut = `row ${+selection?.replace(/[^0-9]/g, "") + 1}`;
  }

  return readOut;
};

const reorderColumn = (draggedColumnId, targetColumnId, columnOrder) => {
  columnOrder.splice(
    columnOrder.indexOf(targetColumnId),
    0,
    columnOrder.splice(columnOrder.indexOf(draggedColumnId), 1)[0]
  );
  return [...columnOrder];
};

const DraggableColumnHeader = ({
  t,
  tableRef,
  theadRef,
  index,
  header,
  selectSection,
  setSelectSection,
  toolbarRef,
  viewOnly,
}) => {
  const [state, dispatch] = useContext(LayoutContext);
  const { column } = header;
  const columnRef = useRef();

  const [, dropRef] = useDrop({
    accept: "column",
    drop: (item) => {
      const dragIndex = state.headers.findIndex(
        (column) => column.id === item.id
      );

      const hoverIndex = state.headers.findIndex(
        (column) => column.id === header.id
      );
      if (dragIndex === hoverIndex) {
        return;
      }

      dispatch({
        func: "DRAG_COLUMN",
        dragId: dragIndex,
        hoverId: hoverIndex,
      });
    },
  });

  const [{ isDragging }, dragRef, preview] = useDrag({
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
    item: () => column,
    type: "column",
  });

  dragRef(
    dropRef(
      state.headerType === "side-header" && index === 0 ? null : columnRef
    )
  );

  return (
    <th
      ref={columnRef}
      colSpan={header.colSpan}
      style={{
        opacity: isDragging ? 0.5 : 1,
        backgroundColor: "#DEE7F5",
        ...(column.id === "column1" &&
          state.hideSideHeader && { display: "none" }),
      }}
      data-testid={`column${index}`}
    >
      <div
        ref={preview}
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
        }}
      >
        {header.isPlaceholder
          ? null
          : flexRender(header.column.columnDef.header, header.getContext())}
        {viewOnly ? (
          <DragIndicatorIcon />
        ) : (
          <button
            type="button"
            onFocus={(e) => {
              setSelectSection(header.id);
            }}
            onBlur={(e) => {
              const relatedTarget = e.relatedTarget || document.activeElement;
              if (!toolbarRef?.contains(relatedTarget)) {
                setSelectSection(null);
              }
            }}
            aria-label={`${ariaSection(selectSection)} drag icon`}
            className="drag-indicator-icon-btn"
          >
            <DragIndicatorIcon />
          </button>
        )}
        <ColumnPreview tableRef={tableRef} theadRef={theadRef} />
      </div>
    </th>
  );
};

const RenderTextArea = ({
  row,
  col,
  type,
  setSelectedCell,
  viewOnly,
  toolbarRef,
  t,
}) => {
  const [state, dispatch] = useContext(LayoutContext);
  const [cellState, setCellState] = useState(
    state.data[row][Object.keys(state.data[row])[col]].value
  );
  const updatCellState = (cell) => {
    dispatch({
      func: "UPDATE_CELL",
      row,
      col,
      value: cell,
    });
  };
  const handleDebounce = useDebounce(updatCellState);
  const onTextChange = (e) => {
    const cell = e.target.value;
    setCellState(cell);
    handleDebounce(cell);
  };

  useEffect(() => {
    if (state.data[row][Object.keys(state.data[row])[col]].value !== cellState)
      setCellState(state.data[row][Object.keys(state.data[row])[col]].value);
  }, [state]);

  const setCell = (e) => {
    setSelectedCell({ row, col });
  };

  let horizontalAlignment = "";
  const cellData = state.data[row][`column${col + 1}`];

  if (cellData.horizontalAlignment !== undefined) {
    horizontalAlignment = cellData.horizontalAlignment;
  } else if (type === "title") {
    horizontalAlignment = "center-align";
  } else {
    horizontalAlignment = "left-align";
  }

  return (
    <StyledInput
      value={cellState || ""}
      aria-label={type === "title" ? `Header input` : "Table cell input"}
      data-testid={`row${row + 1}-${col + 1}`}
      className="styled-input"
      placeholder={
        type === "title"
          ? `${t("Placeholder Title")}${
              state.headerType === "top-header" ? col + 1 : row + 1
            }`
          : "Lorem ipsum"
      }
      data-row={row}
      data-col={col}
      type={type}
      horizontalAlignment={horizontalAlignment}
      verticalAlignment={
        state.data[row][`column${col + 1}`].verticalAlignment !== undefined
          ? state.data[row][`column${col + 1}`].verticalAlignment
          : "middle-align"
      }
      onChange={onTextChange}
      onFocus={setCell}
      onClick={setCell}
      onBlur={(e) => {
        const relatedTarget = e.relatedTarget || document.activeElement;
        if (!toolbarRef?.contains(relatedTarget)) {
          setSelectedCell(null);
        }
      }}
      readOnly={viewOnly}
      viewOnly={viewOnly}
    />
  );
};

const DraggableRow = ({
  t,
  row,
  reorderRow,
  setSelectSection,
  setSelectedCell,
  selectSection,
  toolbarRef,
  handleAriaLive,
  index,
  viewOnly,
}) => {
  const [state] = useContext(LayoutContext);
  const rowRef = useRef(null);

  const [, dropRef] = useDrop({
    accept: "row",
    drop: (draggedRow) => {
      reorderRow(draggedRow.index, row.index);
      setSelectSection(row.id);
    },
  });

  const [{ isDragging }, dragRef] = useDrag({
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
    item: () => row,
    type: "row",
  });

  dragRef(
    dropRef(state.headerType === "top-header" && index === 0 ? null : rowRef)
  );

  return (
    <tr
      ref={rowRef}
      style={{
        opacity: isDragging ? 0.5 : 1,
      }}
      data-testid={`row-${index}`}
    >
      <td
        style={{
          ...(row.original.column1.type === "title" &&
            state.hideTopHeader && { display: "none" }),
          backgroundColor: "#DEE7F5",
        }}
      >
        <span className="draggable-row-span">
          {!viewOnly ? (
            <button
              type="button"
              style={{
                ...(row.original.column1.type === "title" &&
                  state.headerType === "top-header" && {
                    cursor: "auto",
                  }),
              }}
              onFocus={(e) => {
                setSelectSection(row.id);
              }}
              onBlur={(e) => {
                const relatedTarget = e.relatedTarget || document.activeElement;
                if (!toolbarRef?.contains(relatedTarget)) {
                  setSelectSection(null);
                  setSelectedCell(null);
                }
              }}
              aria-label={`${ariaSection(selectSection)} drag icon`}
              className="drag-indicator-icon-btn"
            >
              <DragIndicatorIcon />
            </button>
          ) : (
            <DragIndicatorIcon />
          )}
        </span>
      </td>
      {row.getVisibleCells().map((cell) => {
        const { type } = cell.row.original[cell.column.id];
        return (
          <StyledTd
            selectHighlight={
              selectSection === cell.column.id || selectSection === row.id
            }
            titleType={type === "title"}
            className="styled-td"
            key={cell.id}
            data-testid={`row${cell.row.index + 1}-${cell.column.id}`}
            style={{
              ...(type === "title" &&
                (state.hideSideHeader || state.hideTopHeader) && {
                  display: "none",
                }),
            }}
            verticalAlignment={
              cell.row.original[cell.column.id].verticalAlignment
            }
            // align="center"
            onFocus={(e) => {
              // Column title at index
              const columnTitle =
                state.data[0][
                  `column${parseInt(cell.column.id.replace("column", ""), 10)}`
                ].value === ""
                  ? `Title${parseInt(cell.column.id.replace("column", ""), 10)}`
                  : state.data[0][
                      `column${parseInt(
                        cell.column.id.replace("column", ""),
                        10
                      )}`
                    ].value;

              // row Title at index
              const rowTitle =
                state.data[cell.row.index].column1.value === ""
                  ? `Title ${+cell.row.index + 1}`
                  : state.data[cell.row.index].column1.value;

              // cell at index with blank value
              let mycell = "";
              const cellValue = cell.row.original[cell.column.id].value;
              if (cellValue === "" && type === "cell") {
                mycell = "empty cell";
              } else if (
                cellValue === "" &&
                type === "title" &&
                state.headerType === "side-header"
              ) {
                mycell = rowTitle;
              } else if (
                cellValue === "" &&
                type === "title" &&
                state.headerType === "top-header"
              ) {
                mycell = columnTitle;
              } else {
                mycell = cellValue;
              }

              let cellAria = "";
              const rowNumber = cell.row.index + 1;
              const columnNumber = parseInt(
                cell.column.id.replace("column", ""),
                10
              );
              if (type === "title" && state.headerType === "side-header") {
                cellAria = `Row ${rowNumber} header, ${mycell}`;
              } else if (
                type === "title" &&
                state.headerType === "top-header"
              ) {
                cellAria = `Column ${columnNumber} header, ${mycell}`;
              } else if (
                type === "cell" &&
                state.headerType === "side-header"
              ) {
                cellAria = `Row ${rowNumber} ${rowTitle}, column ${columnNumber}, ${mycell}`;
              } else if (type === "cell" && state.headerType === "top-header") {
                cellAria = `Column ${columnNumber} ${columnTitle}, row ${rowNumber}, ${mycell}`;
              } else {
                cellAria = `${rowTitle} row ${mycell}`;
              }

              handleAriaLive(cellAria);
            }}
            onKeyDown={(e) => {
              if (e.key === "Enter" && type === "title") {
                e.preventDefault();
              }
            }}
            viewOnly={viewOnly}
          >
            <RenderTextArea
              row={cell.row.index}
              col={parseInt(cell.column.id.replace("column", ""), 10) - 1}
              type={type}
              setSelectedCell={setSelectedCell}
              viewOnly={viewOnly}
              toolbarRef={toolbarRef}
              t={t}
            />
          </StyledTd>
        );
      })}
    </tr>
  );
};

const TableComponent = ({
  tableId,
  t,
  showSelf,
  isActiveComponent,
  toolbarContainerRef,
}) => {
  const [state, dispatch] = useContext(LayoutContext);
  const [selectSection, setSelectSection] = useState(null);
  const [selectedCell, setSelectedCell] = useState(null);
  const [toolbarRef, setToolbarRef] = useState(null);
  const tableRef = useRef();
  const theadRef = useRef();

  const authoringData = useContext(TenantContext);

  const viewOnly = !authoringData?.viewOnlyComponents;

  // Aria Live
  const [ariaLive, setAriaLive] = useState("");
  const [ariaLive2, setAriaLive2] = useState("");

  // Handle Aria live region
  const handleAriaLive = (value) => {
    if (ariaLive === value) {
      setAriaLive("");
      setAriaLive2(value);
    } else {
      setAriaLive2("");
      setAriaLive(value);
    }
  };

  const [columnOrder, setColumnOrder] = useState(
    state.headers.map((column) => column.id)
  );

  const reorderRow = (draggedRowIndex, targetRowIndex) => {
    dispatch({
      func: "UPDATE_ROW",
      draggedRowIndex,
      targetRowIndex,
    });
  };

  const table = useReactTable(
    {
      data: state.data,
      columns: state.headers,
      state: {
        columnOrder,
      },
      onColumnOrderChange: setColumnOrder,
      getCoreRowModel: getCoreRowModel(),
      getRowId: (row) => row.name,
      debugTable: true,
      debugHeaders: true,
      debugColumns: true,
    },
    [state, columnOrder]
  );

  useEffect(() => {
    setColumnOrder(state.headers.map((column) => column.id));
  }, [state]);

  return (
    <DndProvider backend={HTML5Backend}>
      <span
        className="sr-only"
        role="status"
        aria-live="assertive"
        aria-relevant="all"
        aria-atomic="true"
      >
        {ariaLive}
      </span>
      <span
        className="sr-only"
        role="status"
        aria-live="assertive"
        aria-relevant="all"
        aria-atomic="true"
      >
        {ariaLive2}
      </span>
      <StyledTable
        role="presentation"
        className="style-table"
        ref={tableRef}
        showStripes={state.showStripes}
        headerType={state.headerType}
      >
        <span className="sr-only" tabIndex={0} role="status">
          {`Table with ${state.data.length} rows and ${state.headers.length} columns focused`}
        </span>
        {!viewOnly && (showSelf || isActiveComponent) && (
          <PortalUtil portalRef={toolbarContainerRef}>
            <StyledConfigBar>
              <Toolbar
                t={t}
                setSelectSection={setSelectSection}
                selectSection={selectSection}
                setSelectedCell={setSelectedCell}
                selectedCell={selectedCell}
                setToolbarRef={setToolbarRef}
                toolbarRef={toolbarRef}
                tableId={tableId}
                handleAriaLive={handleAriaLive}
                isActiveComponent={isActiveComponent}
                showSelf={showSelf}
              />
            </StyledConfigBar>
          </PortalUtil>
        )}
        <thead>
          {table.getHeaderGroups().map((headerGroup) => (
            <tr key={headerGroup.id}>
              <th style={{ width: "30px" }} aria-label="" />
              {headerGroup.headers.map((header, index) => (
                <DraggableColumnHeader
                  t={t}
                  key={header.id}
                  tableRef={tableRef}
                  theadRef={theadRef}
                  index={index}
                  toolbarRef={toolbarRef}
                  len={table.length}
                  header={header}
                  table={table}
                  selectSection={selectSection}
                  setSelectSection={setSelectSection}
                  viewOnly={viewOnly}
                />
              ))}
            </tr>
          ))}
        </thead>
        <tbody aria-hidden="true">
          {table.getRowModel().rows.map((row, index) => (
            <DraggableRow
              t={t}
              key={row.id}
              row={row}
              reorderRow={reorderRow}
              setSelectSection={setSelectSection}
              selectSection={selectSection}
              setSelectedCell={setSelectedCell}
              selectedCell={selectedCell}
              toolbarRef={toolbarRef}
              handleAriaLive={handleAriaLive}
              index={index}
              viewOnly={viewOnly}
            />
          ))}
        </tbody>
      </StyledTable>
    </DndProvider>
  );
};

export const defaultProps = {};

export default TableComponent;
