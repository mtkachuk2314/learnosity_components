import React, { useState, useContext } from "react";
import styled from "@emotion/styled";
import { useTranslation } from "react-i18next";
import { LayoutContext } from "../TableContext";

import Modal from "./Modal";
import TableComponent from "./TableComponent";
import TenantContext from "../../../Context/TenantContext";

const ButtonContainer = styled("div")(() => ({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  width: "100%",
  minHeight: "152px",
  background: "#FFFFFF",
}));

const StyledButton = styled("button")(({ viewOnly }) => ({
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  padding: "0px",
  width: "151px",
  height: "42px",
  background: "#1565C0",
  borderRadius: "4px",
  fontFamily: '"Inter", sans-serif',
  fontSize: "0.9375rem",
  fontWeight: "500",
  lineHeight: "1.625rem",
  color: "#FFFFFF",
  border: "none",
  outline: "none",
  cursor: !viewOnly && "pointer",

  "&:hover": {
    cursor: "pointer",
  },

  "&:focus": {
    outline: "2px solid rgba(0, 0, 0, 1)",
  },
}));

const Table = ({
  tableId,
  isActiveComponent,
  toolbarContainerRef,
  showSelf,
}) => {
  const [showModal, setShowModal] = useState(false);
  const [state] = useContext(LayoutContext);
  const { t } = useTranslation();

  const authoringData = useContext(TenantContext);

  const viewOnly = !authoringData?.viewOnlyComponents;

  const createTable = (e) => {
    setShowModal(true);
  };

  return (
    <div data-testid="table-component">
      {state.data.length !== 0 ? (
        <TableComponent
          tableId={tableId}
          t={t}
          isActiveComponent={isActiveComponent}
          toolbarContainerRef={toolbarContainerRef}
          showSelf={showSelf}
        />
      ) : (
        <>
          {showModal && <Modal setShowModal={setShowModal} t={t} />}
          <ButtonContainer data-testid="create-table-container">
            <StyledButton
              onClick={createTable}
              data-testid="create-a-table-button"
              disabled={viewOnly}
              viewOnly={viewOnly}
            >
              {t("Create a table")}
            </StyledButton>
          </ButtonContainer>
        </>
      )}
    </div>
  );
};

export default Table;
