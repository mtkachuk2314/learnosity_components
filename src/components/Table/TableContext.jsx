import React, {
  createContext,
  useReducer,
  useEffect,
  useMemo,
  useRef,
} from "react";
import produce from "immer";

// state of tabs data stored in LayoutContext
export const LayoutContext = createContext();

export const layoutConfig = (draft, action) => {
  switch (action.func) {
    case "UPDATE_STATE":
      return action.data;
    case "SET_STATE":
      draft.headers = action.headers;
      draft.data = action.data;
      draft.headerType = action.headerType;
      draft.hideTopHeader = action.hideTopHeader;
      draft.hideSideHeader = action.hideSideHeader;
      draft.showStripes = action.showStripes;
      return draft;
    case "UPDATE_CELL":
      const temp = Object.keys(draft.data[action.row]);
      draft.data[action.row][temp[action.col]].value = action.value;
      return draft;
    case "DRAG_COLUMN":
      const draggedColumn = draft.headers[action.dragId];

      draft.headers.splice(action.dragId, 1);

      // Find the index where the dragged column should be inserted

      // Insert the dragged column at the new position
      draft.headers.splice(action.hoverId, 0, draggedColumn);
      return draft;
    case "UPDATE_COLUMN_ORDER":
      const targetColumn =
        draft.headers[action.targetColumn.replace(/column/, "") - 1];
      const draggedColumnDND =
        draft.headers[action.draggedColumn.replace(/column/, "") - 1];

      draft.headers[action.targetColumn.replace(/column/, "") - 1] =
        draggedColumnDND;
      draft.headers[action.draggedColumn.replace(/column/, "") - 1] =
        targetColumn;
      return draft;
    case "UPDATE_ROW":
      draft.data.splice(
        action.targetRowIndex,
        0,
        draft.data.splice(action.draggedRowIndex, 1)[0]
      );
      return draft;
    case "ADD_ROW":
      draft.data = action.data;
      return draft;
    case "ADD_COL":
      draft.headers = action.headers;
      draft.data = action.data;
      return draft;
    case "DELETE_COLUMN":
      draft.headers = action.headers;
      draft.data = action.data;
      return draft;
    case "DELETE_ROW":
      draft.headers = action.headers;
      draft.data = action.data;
      return draft;
    case "MOVE_ROW_DOWN":
      draft.headers = action.headers;
      draft.data = action.data;
      return draft;
    case "MOVE_ROW_UP":
      draft.headers = action.headers;
      draft.data = action.data;
      return draft;
    case "UPDATE_ROWS_COLUMN":
      draft.headers = action.headers;
      draft.data = action.data;
      return draft;
    case "MOVE_LEFT_COLUMN":
      const currentCol = draft.headers[action.currCol];
      const prevCol = draft.headers[action.prevCol];
      draft.headers[action.prevCol] = currentCol;
      draft.headers[action.currCol] = prevCol;
      return draft;
    case "MOVE_RIGHT_COLUMN":
      const currCol = draft.headers[action.currCol];
      const nextCol = draft.headers[action.nextCol];
      draft.headers[action.nextCol] = currCol;
      draft.headers[action.currCol] = nextCol;
      return draft;
    case "UPDATE_STRIP":
      draft.showStripes = action.showStripes;
      return draft;
    case "UPDATE_TOPHEADER":
      draft.hideTopHeader = action.hideTopHeader;
      return draft;
    case "UPDATE_SIDEHEADER":
      draft.hideSideHeader = action.hideSideHeader;
      return draft;
    case "CHANGE_ALIGNMENT":
      draft.data[action.selectedCell.row][
        `column${action.selectedCell.col + 1}`
      ].horizontalAlignment = action.activeHorizontalAlignment;
      draft.data[action.selectedCell.row][
        `column${action.selectedCell.col + 1}`
      ].verticalAlignment = action.activeVerticalAlignment;
      return draft;
    default:
      return draft;
  }
};

// layout provider wraps the tab component to access reducer
export const LayoutProvider = ({ children, setProp, layoutState }) => {
  const [state, dispatch] = useReducer(produce(layoutConfig), layoutState);
  const diff = JSON.stringify(state) !== JSON.stringify(layoutState);

  const updatingRef = useRef(false);

  useEffect(() => {
    dispatch({ func: "UPDATE_STATE", data: layoutState });
    updatingRef.current = true;
  }, []);

  useEffect(() => {
    if (updatingRef.current) {
      updatingRef.current = false;
      return;
    }

    if (diff) {
      setProp({ layoutState: state });
      updatingRef.current = true;
    }
  }, [state]);

  useEffect(() => {
    if (updatingRef.current) {
      updatingRef.current = false;
      return;
    }
    if (diff) {
      dispatch({ func: "UPDATE_STATE", data: layoutState });
      updatingRef.current = true;
    }
  }, [layoutState]);

  return (
    <LayoutContext.Provider
      value={useMemo(() => [state, dispatch], [state, dispatch])}
    >
      {children}
    </LayoutContext.Provider>
  );
};
