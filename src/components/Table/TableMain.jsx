import React, { useMemo } from "react";
import { v4 as uuidv4 } from "uuid";
import { LayoutProvider } from "./TableContext";

import Table from "./subcomponents/Table";
import { MainWrapper } from "../../theme/styledComponents/globalComponent";

export const defaultProps = {
  layoutState: {
    headers: [],
    data: [],
    headerType: null,
    hideTopHeader: false,
    hideSideHeader: false,
    showStripes: false,
  },
};

const TableMain = ({
  layoutState = {},
  setProp = () => {},
  toolbarContainerRef,
  showSelf = null,
  isActiveComponent = null,
}) => {
  const tableId = useMemo(() => `unique-id-${uuidv4()}`, []);

  return (
    <LayoutProvider layoutState={layoutState} setProp={setProp}>
      <MainWrapper>
        <Table
          tableId={tableId}
          toolbarContainerRef={toolbarContainerRef}
          isActiveComponent={isActiveComponent}
          showSelf={showSelf}
        />
      </MainWrapper>
    </LayoutProvider>
  );
};

export default TableMain;
