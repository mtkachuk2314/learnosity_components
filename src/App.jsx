import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import { WidgetContextProvider } from "./Utility/mockWrapper";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
// import Header from "./components/Header";
// import FormattedText from "./components/FormattedText";
import Home from "./pages/Home";
import AccessibilityKeysPage from "./components/FormattedText/AccessibilityKeysPage";
import exposedVersion from "../exposedStage";
import Text from "./components/Text/Text";
import componentIndex from "./components/componentIndex";

import NewHome from "./pages/NewHome";
import { SingleComponentTestPage } from "./Utility/MockAuthoringExperiencePage";
import AuthoringExperienceFormatProvider, {
  FORMATS,
} from "./authoring/context/AuthoringExperienceFormatContext";

import "./index.css";

const App = () => {
  console.log(
    `Stage is ${exposedVersion.stage} and version of the app is ${exposedVersion.version}`
  );

  const componentNames = Object.keys(componentIndex);

  return (
    <DndProvider backend={HTML5Backend}>
      <Suspense fallback="loading">
        <WidgetContextProvider>
          <AuthoringExperienceFormatProvider
            initSelectedFormat={FORMATS.DIGITAL}
          >
            <BrowserRouter>
              <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/newhome" element={<NewHome />} />
                <Route
                  path="/AccessibilityKeysPage"
                  element={<AccessibilityKeysPage />}
                />
                {/* <Route path="/ImagePage" element={<ImagePage />} /> */}
                <Route path="/text-component" element={<Text />} />
                {componentNames.map((componentName) => (
                  <Route
                    path={`/${componentName.toLowerCase()}`}
                    key={componentName}
                    element={
                      <SingleComponentTestPage componentName={componentName} />
                    }
                  />
                ))}
                <Route path="*" element={<Home />} />
              </Routes>
            </BrowserRouter>
          </AuthoringExperienceFormatProvider>
        </WidgetContextProvider>
      </Suspense>
    </DndProvider>
  );
};

ReactDOM.render(<App />, document.getElementById("app"));
