import React, { useContext } from "react";
import { unmountComponentAtNode } from "react-dom";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";

import ImageMain from "../components/Image/ImageMain";
import TenantContext from "../Context/TenantContext";

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

//Mock props/data

const mockProps = {
  imgSize: {
    width: 100,
    height: 100,
  },
  uploadedImg:
    "https://www.tvo.org/files/s3fs-public/styles/hero_image/public/media-library/2_3_juno_1.jpg",
  imgLink: null,
  alt: "Temporary",
  longDesc: "Test long description",
  customAltTextOptionSelected: "Custom alt Text",
  caption: null,
  credit: "Test credit",
  imageTextSettings: {
    description: true,
    credit: true,
    fullWidth: false,
  },
};
jest.mock("mathpix-markdown-it", () => jest.fn(() => {}));
// jest.mock("openAssetManager", () => jest.fn(() => {}));
// jest.mock("mediaRootURL", () => jest.fn(() => {}));
jest.mock("../Context/TenantContext", () => ({
  openAssetManager: jest.fn(),
  mediaRootURL: jest.fn(),
}));
jest.mock("react", () => {
  const ActualReact = jest.requireActual("react");
  return {
    ...ActualReact,
    useContext: () => (
      { openAssetManager: () => {} }, { ImageContext: () => {} }
    ), // what you want to return when useContext get fired goes here
  };
});
// jest.mock('mathpix-markdown-it', () => () => ({
//   name: 'MathpixMarkdownModel',
// }))
xdescribe("<ImageMain/>", () => {
  it("Image component renders with ", () => {
    render(
      <ImageMain
        setProp={() => {}}
        parentComponent={null}
        setTabActive={() => {}}
        setActiveComponent={() => {}}
        imgSize={{
          width: 100,
          height: 100,
        }}
        uploadedImg={""}
        imgLink={null}
        alt={""}
        customAltTextOptionSelected={""}
        longDesc={""}
        caption={null}
        credit={null}
        imageTextSettings={{
          description: true,
          credit: true,
          fullWidth: false,
        }}
      />
    );
    expect(screen.getByTestId("image-component-container")).toBeInTheDocument();
    expect(screen.getByTestId("image-wrapper")).toBeInTheDocument();
    expect(screen.getByTestId("image-toolbar")).toBeInTheDocument();
    expect(screen.getByTestId("image-text-container")).toBeInTheDocument();
  });

  // it("renders Image component with data", () => {
  //   render(
  //     <Image
  //       imgSize={mockProps.imgSize}
  //       uploadedImg={mockProps.uploadedImg}
  //       imgLink={mockProps.imgLink}
  //       alt={mockProps.alt}
  //       longDesc={mockProps.longDesc}
  //     />
  //   );

  //   expect(screen.getByTestId("image")).toHaveAttribute("alt", "dog");
  //   expect(screen.getByTestId("image")).toHaveAttribute(
  //     "src",
  //     "https://www.tvo.org/files/s3fs-public/styles/hero_image/public/media-library/2_3_juno_1.jpg"
  //   );
  //   expect(screen.getByTestId("image-link")).toHaveAttribute(
  //     "href",
  //     "https://www.tvo.org/files/s3fs-public/styles/hero_image/public/media-library/2_3_juno_1.jpg"
  //   );
  // });
});
