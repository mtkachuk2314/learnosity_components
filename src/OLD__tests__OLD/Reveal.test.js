import React from "react";
import { unmountComponentAtNode } from "react-dom";
import { render, screen, fireEvent } from "@testing-library/react";
import produce from "immer";
import "@testing-library/jest-dom";
import RevealMain from "../components/Reveal/RevealMain";
import "../Icons/componentIcons/Vector.svg";
import { getByTestId } from "@testing-library/dom";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

const testLayout = [
  {
    id: 0,
    title: "Close Pane",
    closedTitle: "",
    activeTitle: "close reveal",
    inactiveTitle: "",
    components: [],
    expanded: true,
  },
  {
    id: 1,
    title: "",
    closedTitle: "Open Pane",
    activeTitle: "",
    inactiveTitle: "open reveal",
    components: [],
    expanded: false,
  },
];

// Mock Data muated with immer
const singleComponent = produce(testLayout, (draft) => {
  draft[0].components.push({
    componentName: "Text",
    componentProps: {
      body: {
        ops: [{ insert: "Polkaroo\n" }],
      },
    },
  });
});

const multipleComponents = produce(testLayout, (draft) => {
  draft[0].components.push(
    {
      componentName: "Text",
      componentProps: {
        body: {
          ops: [{ insert: "Polkaroo\n" }],
        },
      },
    },
    {
      componentName: "Text",
      componentProps: {
        body: {
          ops: [{ insert: "Polkaroo 2\n" }],
        },
      },
    }
  );
});
jest.mock("mathpix-markdown-it", () => jest.fn(() => {}));

describe("Renders Reveal Component with default props", () => {
  it("Renders Reveal Panes with default data", async () => {
    render(<RevealMain layoutState={testLayout} setProp={() => {}} />);
    expect(screen.getByText(/Close Pane/i)).toBeInTheDocument();
    expect(screen.getByText(/Open Pane/i)).toBeInTheDocument();
  });
  it("Displays placeholder text within Reveal", () => {
    render(<RevealMain layoutState={testLayout} />);
    expect(screen.getAllByText(/Add a component here/i)[0]).toBeInTheDocument();
    expect(
      screen.getAllByText(
        /Drag and drop a component from the left panel or use your keyboard to insert a component./i
      )[0]
    ).toBeInTheDocument();
    expect(
      screen.getAllByText(
        /Accepted components: header, text, image, video, and table/i
      )[0]
    ).toBeInTheDocument();
  });
});
describe("Expanding and Collapse Panes", () => {
  it("Chevron is rendered for each pane", async () => {
    render(<RevealMain layoutState={testLayout} setProp={() => {}} />);
    expect(screen.getAllByTestId("ExpandMoreIcon")[0]).toBeInTheDocument();
    expect(screen.getAllByTestId("ExpandMoreIcon")[1]).toBeInTheDocument();
  });
  it("On click of chevron pane opens and closes", async () => {
    render(<RevealMain layoutState={testLayout} setProp={() => {}} />);
    const expandChevron = screen.getAllByTestId("ExpandMoreIcon")[0];
    // Expect one expanded and not expanded
    expect(screen.getByRole("button", { expanded: true })).toBeInTheDocument();
    expect(screen.getByRole("button", { expanded: false })).toBeInTheDocument();
    fireEvent.click(expandChevron);
  });
});

// Mock Data, can't test actual drag and drop since we are rendering only the Reveal component and there is nothing to drag
describe("Testing Rendering components when added", () => {
  it("Dropping text component into a Accordion is registered and Text comp is rendered", async () => {
    render(<RevealMain layoutState={singleComponent} setProp={() => {}} />);
    const textComponent = screen.getByTestId("text-editor-component");
    expect(textComponent).toBeInTheDocument();
    expect(screen.getByText(/Polkaroo/i)).toBeInTheDocument();
  });

  it("Multiple components render", async () => {
    render(<RevealMain layoutState={multipleComponents} setProp={() => {}} />);
    const textComponent = screen.getAllByTestId("text-editor-component");
    expect(textComponent[0]).toBeInTheDocument();
    expect(screen.getByText("Polkaroo")).toBeInTheDocument();
    expect(screen.getByText("Polkaroo 2")).toBeInTheDocument();
  });
});

// Component Wrapper label only Testing, Renders with correct label
describe("Testing Component Wrapper", () => {
  it("Wrapper Renders for Text Component", async () => {
    render(<RevealMain layoutState={singleComponent} setProp={() => {}} />);
    const textComponent = screen.getByTestId("text-editor-component");
    expect(textComponent).toBeInTheDocument();
    expect(screen.getByText("Polkaroo")).toBeInTheDocument();
    fireEvent.click(textComponent);
    const ComponentLabel = screen.getByTestId("component-label-name");
    expect(ComponentLabel).toBeInTheDocument;
    expect(ComponentLabel).toHaveTextContent("Text");
  });
});
