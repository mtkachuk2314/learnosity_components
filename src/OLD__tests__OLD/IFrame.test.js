import React from "react";
import { unmountComponentAtNode } from "react-dom";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";

import IFrameMain from "../components/IFrame/IFrameMain";

// Setup/teardown
// https://reactjs.org/docs/testing-recipes.html#setup--teardown
let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

// Mock data
const mockData = {
  title: "",
  src: "",
  urlToValidate: "",
  height: "500",
  width: "900",
};

describe("IFrame", () => {
  // Test that iFrame component renders when no data saved, and test that
  // modal appears on page when user clicks "Add iFrame button"
  it("renders iFrame without any given data", async () => {
    const user = userEvent.setup();
    render(<IFrameMain />);

    expect(screen.getByTestId("iFrameComponent")).toBeInTheDocument();
    expect(
      screen.getByRole("button", { name: "Add iFrame" })
    ).toBeInTheDocument();

    // User clicks Add iFrame button to launch modal
    await user.click(screen.getByRole("button", { name: "Add iFrame" }));
    await expect(screen.getByTestId("iFrameModal")).toBeInTheDocument();
    // // Checking for the close button (x in corner of modal)
    await expect(
      screen.getByLabelText("Close iFrame modal")
    ).toBeInTheDocument();
    await expect(
      screen.getByRole("button", { name: "Close iFrame modal" })
    ).toBeInTheDocument();
    // // Checking for width/height inputs
    await expect(
      screen.getByRole("spinbutton", { name: "Width px" })
    ).toBeInTheDocument();
    await expect(
      screen.getByRole("spinbutton", { name: "Height px" })
    ).toBeInTheDocument();
    // // Checking for title and iFrame link inputs
    await expect(
      screen.getByLabelText("Type Title attribute *")
    ).toBeInTheDocument();
    await expect(
      screen.getByRole("textbox", {
        name: "Type Title attribute",
      })
    ).toBeInTheDocument();
    await expect(
      screen.getByLabelText("Paste iFrame link *")
    ).toBeInTheDocument();
    await expect(
      screen.getByRole("textbox", {
        name: "Paste iFrame link",
      })
    ).toBeInTheDocument();
    // // Check for add button to be disabled
    await expect(screen.getByRole("button", { name: "Add" })).toBeDisabled();
  });

  // Test that iFrame component renders when data provided
  it("renders iFrame with given data", () => {
    render(
      <IFrameMain
        title="TVO ILO"
        src="https://dcc.ilc.org/olc4o/08/comics_strip/index.html"
        width={850}
        height={600}
      />
    );

    // Check if iFrame is displaying on page
    expect(screen.getByTestId("iFrameComponent")).toBeInTheDocument();
    // Check if title prop has been properly serialized
    expect(screen.getByTitle("TVO ILO")).toBeInTheDocument();
    // Check if src prop has been properly serialized
    expect(screen.getByTitle("TVO ILO")).toHaveAttribute(
      "src",
      "https://dcc.ilc.org/olc4o/08/comics_strip/index.html"
    );
    // Check if width & height props have been serialized
    expect(screen.getByTitle("TVO ILO")).toHaveAttribute("width", "850");
    expect(screen.getByTitle("TVO ILO")).toHaveAttribute("height", "600");
  });
});
