import React from "react";
import { unmountComponentAtNode } from "react-dom";
import { render, screen, fireEvent } from "@testing-library/react";
import produce from "immer";
import "@testing-library/jest-dom";
import HeaderMain from "../components/Header/HeaderMain";
import "../Icons/componentIcons/Vector.svg";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

export const defaultProps = {
   // headerState: {
      size: "large",
      alignment: "left-align",
      heading: "",
   // },
  };

  describe("Header Main, placeholder data. Test Rendering.", () => {
    it("Renders Info Box Component, with placeholder data", async () => {
      render(<HeaderMain layoutState={defaultProps} />);
    });
    it("Renders Header, with placeholder data", async () => {
      render(<HeaderMain layoutState={defaultProps} />);
      expect(
        screen.getByPlaceholderText(/Type your header here.../i)
      ).toBeInTheDocument();
    });
  });

  describe("Header Main, test reducers. Rendering props.", () => {
    
    it("Update Header", async () => {
      const newHeader = "Test Header";
      const newState = produce(defaultProps, (draft) => {
        draft.heading = newHeader;
      });
      render(<HeaderMain headerState={newState} />);
      expect(screen.getByDisplayValue("Test Header")).toBeInTheDocument();
      
    });
   
  });

