import React, { useContext } from "react";
import PropTypes from "prop-types";

// TODO: The only solid way for handling this is dividing the meta for a component from the component index and creating an exportable component meta index
// eslint-disable-next-line import/no-cycle
import componentIndex from "../../components/componentIndex";

import { AuthoringExperienceFormatContext } from "../context/AuthoringExperienceFormatContext";

const ComponentWithFormat = ({ componentName, formats, children }) => {
  const formatContext = useContext(AuthoringExperienceFormatContext);

  // If the format context is not being rendered as a parent, this falls back to the normal experience
  if (!formatContext) return children;

  const [selectedFormat] = formatContext;

  const componentDetails = componentIndex[componentName];

  if (!componentDetails) {
    // eslint-disable-next-line no-console
    console.error("ComponentWithFormat: Invalid component name passed");
    return children;
  }

  // Get the compatible formats for the component
  const { compatibleFormats } = componentDetails;

  // If component isn't valid in the selected format or if it's being explicitly hidden, show replacements dropzone
  // For now this is going to replace the component wrapper, in future it will potentially be a child to retain controls for the component

  // TODO: Show callout if a component is incompatible
  if (compatibleFormats?.[selectedFormat] === false) return null;

  // TODO: Show replacements here if a component is hidden
  if (formats?.[selectedFormat]?.shown === false) return null;

  // If the component is compatible and being shown, render it
  return children;
};

const ComponentWithFormatMemo = React.memo(ComponentWithFormat);

ComponentWithFormat.propTypes = {
  componentName: PropTypes.string.isRequired,
  formats: PropTypes.shape({
    digital: PropTypes.shape({
      shown: PropTypes.bool,
    }),
    print: PropTypes.shape({
      shown: PropTypes.bool,
    }),
  }),
  children: PropTypes.node.isRequired,
};

ComponentWithFormat.defaultProps = {
  formats: {},
};

export default ComponentWithFormatMemo;
