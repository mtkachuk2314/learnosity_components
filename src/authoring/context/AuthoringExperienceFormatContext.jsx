import React, { createContext, useState } from "react";
import PropTypes from "prop-types";

// Extensible list of formats, this cannot be changed.
export const FORMATS = {
  DIGITAL: "digital",
  PRINT: "print",
};

export const AuthoringExperienceFormatContext = createContext();

const AuthoringExperienceFormatProvider = ({
  children,
  // Creating an initial selected format to allow for overriding; defaults to print
  initSelectedFormat = FORMATS.PRINT,
}) => {
  const selectedFormatState = useState(initSelectedFormat);

  return (
    <AuthoringExperienceFormatContext.Provider value={selectedFormatState}>
      {children}
    </AuthoringExperienceFormatContext.Provider>
  );
};

AuthoringExperienceFormatProvider.propTypes = {
  children: PropTypes.node.isRequired,
  initSelectedFormat: PropTypes.oneOf(
    Object.keys(FORMATS).map((key) => FORMATS[key])
  ),
};

AuthoringExperienceFormatProvider.defaultProps = {
  initSelectedFormat: FORMATS.PRINT,
};

export default AuthoringExperienceFormatProvider;
