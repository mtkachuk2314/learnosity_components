module.exports = (results) => {
  // accumulate the errors and warnings
  const summary = results.reduce(
    (seq, current) => ({
      errors: seq.errors + current.errorCount,
      warnings: seq.warnings + current.warningCount,
    }),
    { errors: 0, warnings: 0 }
  );

  if (summary.errors > 0 || summary.warnings > 0) {
    return `eslint_errors ${summary.errors}\neslint_warnings ${summary.warnings}\n`;
  }

  return "";
};
