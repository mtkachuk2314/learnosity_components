// locally test programatic 11ty api - simulates lambda trigger
// cd src
// run cli: node -e 'require("./test/local.js")'
const compiler = require('../index.local.js');
const tokenGenerator = require("./generate-local-token");
const fs = require('fs');
const setters = require('../utils/setters');

(async () => {

  let awsCredentialsConfig = {
    region: process.env.AWS_REGION,
    credentials: {
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
      sessionToken: process.env.AWS_SESSION_TOKEN,
    },
  }

  const _permissionPublisher = 'course:edit-pubversion,course:publish-course,course:view-courses,course:create-course,course:create-lesson,course:edit-lesson,course:edit-section,course:delete-lesson,course:update-structure,course:delete-unit,course:create-unit,course:duplicate-unpubversion,course:validate-version,course:edit-meta,course:delete-course,course:delete-version,course:create-edition,course:edit-state,course:move-inreview';
  const _permissionEditior = 'course:edit-meta,course:delete-course,course:delete-version,course:create-edition,course:create-course,course:create-lesson,course:edit-lesson,course:edit-section,course:delete-lesson,course:update-structure,course:delete-unit,course:create-unit,course:duplicate-unpubversion,course:validate-version,course:view-courses,course:edit-state,course:move-inreview';

  const evt = {
    headers: {
      Host: "api-sbx.tvopublishing.com",
    },
    queryStringParameters: {
      id: process.env.ID,
    },
    requestContext: {
      resourceId: '5o83qlb5ak',
      authorizer: {
        tenantName: 'tvo',
        permissions: _permissionPublisher,
        tenantId: '76876a56',
        name: 'platypusPublisher',
        principalId: 'platypusPublisher@tvo.org',
        integrationLatency: 276,
        userId: 'b4875517-bb4c-488a-b9e5-e38706f68c44',
      },
      domainName: "api-sbx.tvopublishing.com",
    },
    awsCredentialsConfig: awsCredentialsConfig,
  };
  console.log("process.env.MODE === 'preview': ", process.env.MODE === 'preview')
  if (process.env.MODE === 'preview') {
    const STAGE_NAME_MAP = new Map([
      ["sbx01", "sbx"],
      ["qa01", "qa"],
      ["staging01", "stage"],
      ["prod01", "prod"],
    ]);
    evt.ampParams = {
      stage: STAGE_NAME_MAP.get(process.env.STAGE_ENV), // get corresponding stage name
      courseId: 'previewCourseId',
      groupId: 'previewGroupId',
      edition: 'previewEdition',
      mode: 'preview',
      tenantID: '76876a56',
      parsedDatafile: process.env.PREVIEW_PARSED_DATA_FILE,
      htmlFilename: process.env.PREVIEW_HTML_FILENAME,
      htmlFilepath: process.env.PREVIEW_HTML_PATH,
      bucketName: process.env.PREVIEW_BUCKET,
      awsCredentialsConfig: awsCredentialsConfig,
    };
  } else {
    const token = await tokenGenerator.getCredentials();
    evt.requestContext.authorizer.authorizationToken = token;
    setters.setAuthToken(token);
  }

  console.time("luke.compile()");
  new Promise(async function (resolve, reject) {
    evt.resolve = resolve;
    evt.reject = reject;
  }).then((result) => {
    console.log("'lambda handler:'", result.statusCode, result.message);
    // write files locally to dist
    let _files = result.data;
    _files.forEach((file, index) => {
      console.log(`queue => ${index}: ${file.url}`);
      fs.writeFileSync(file.outputPath, file.content);
    });

  }).catch((err) => {
    console.log("catch err:", err);
    return err;
  }).finally(() => {
    console.timeEnd("luke.compile()");
  });
  await compiler.compile(evt);
})();
