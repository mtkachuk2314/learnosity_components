/* eslint-disable no-param-reassign */
// TODO: find a way to eliminate no-param-reassign error without above flag ^^
const handleImageRequest = (
  entity,
  tenantData,
  componentName,
  infoboxHasIcon
) => {
  const aspectRatio = entity.imgSize.height / entity.imgSize.width;
  let imageRequest = {};

  switch (componentName) {
    // For individual, unnested Image components in a lesson:
    case "Image":
      // Setting image base64-encoded imageRequest to be passed to serverless image handler URL
      imageRequest = {
        375: JSON.stringify({
          bucket: tenantData.bucketName,
          key: `${tenantData.imgKey}${entity.uploadedImg}`,
          edits: {
            resize: {
              width: 375,
              height: aspectRatio * 375,
              fit: "cover",
            },
          },
        }),
        560: JSON.stringify({
          bucket: tenantData.bucketName,
          key: `${tenantData.imgKey}${entity.uploadedImg}`,
          edits: {
            resize: {
              width: 560,
              height: aspectRatio * 560,
              fit: "cover",
            },
          },
        }),
        759: JSON.stringify({
          bucket: tenantData.bucketName,
          key: `${tenantData.imgKey}${entity.uploadedImg}`,
          edits: {
            resize: {
              width: 759,
              height: aspectRatio * 759,
              fit: "cover",
            },
          },
        }),
        960: JSON.stringify({
          bucket: tenantData.bucketName,
          key: `${tenantData.imgKey}${entity.uploadedImg}`,
          edits: {
            resize: {
              width: 960,
              height: aspectRatio * 960,
              fit: "cover",
            },
          },
        }),
        1024: JSON.stringify({
          bucket: tenantData.bucketName,
          key: `${tenantData.imgKey}${entity.uploadedImg}`,
          edits: {
            resize: {
              width: 1024,
              height: aspectRatio * 1024,
              fit: "cover",
            },
          },
        }),
        [entity.imgSize.width]: JSON.stringify({
          bucket: tenantData.bucketName,
          key: `${tenantData.imgKey}${entity.uploadedImg}`,
          edits: {
            resize: {
              width: entity.imgSize.width,
              height: aspectRatio * entity.imgSize.width,
              fit: "cover",
            },
          },
        }),
      };
      // Setting entity.uploadedImg to its respective URL based on imageRequest sizes listed above:
      entity.uploadedImg = {
        375: `${tenantData.serviceUrl}/${Buffer.from(
          imageRequest[375],
          "binary"
        ).toString("base64")}`,
        560: `${tenantData.serviceUrl}/${Buffer.from(
          imageRequest[560],
          "binary"
        ).toString("base64")}`,
        759: `${tenantData.serviceUrl}/${Buffer.from(
          imageRequest[759],
          "binary"
        ).toString("base64")}`,
        960: `${tenantData.serviceUrl}/${Buffer.from(
          imageRequest[960],
          "binary"
        ).toString("base64")}`,
        1024: `${tenantData.serviceUrl}/${Buffer.from(
          imageRequest[1024],
          "binary"
        ).toString("base64")}`,
        [entity.imgSize.width]: `${tenantData.serviceUrl}/${Buffer.from(
          imageRequest[entity.imgSize.width],
          "binary"
        ).toString("base64")}`,
      };
      // Setting entity.srcset for standalone image components:
      entity.srcset = `
        ${entity.uploadedImg[375]} 375w,
        ${entity.uploadedImg[560]} 560w,
        ${entity.uploadedImg[759]} 759w,
        ${entity.uploadedImg[960]} 960w,
        ${entity.uploadedImg[1024]} 1024w,
        ${entity.uploadedImg[entity.imgSize.width]} ${entity.imgSize.width}w
      `;
      // Setting the sizes attribute for Image components when stand-alone:
      if (entity.imgSize.width > 1024 || entity.imageTextSettings.fullWidth) {
        entity.sizes = `(max-width: 375px) 375px, (max-width: 560px) 560px, (max-width: 759px) 759px, (max-width: 960px) 960px, (max-width: 1024px) 1024px, 64rem`;
      } else if (entity.imgSize.width > 960) {
        entity.sizes = `(max-width: 375px) 375px, (max-width: 560px) 560px, (max-width: 759px) 759px, (max-width: 960px) 960px`;
      } else if (entity.imgSize.width > 759) {
        entity.sizes = `(max-width: 375px) 375px, (max-width: 560px) 560px, (max-width: 759px) 759px`;
      } else if (entity.imgSize.width > 560) {
        entity.sizes = `(max-width: 375px) 375px, (max-width: 560px) 560px, `;
      } else if (entity.imgSize.width > 375) {
        entity.sizes = `(max-width: 375px) 375px, 375px`;
      }
      if (entity.imgSize.width < 375) {
        entity.sizes = `(max-width: 375px) ${entity.imgSize.width}px, ${entity.imgSize.width}px`;
      } else if (entity.imgSize.width < 560) {
        entity.sizes = `(max-width: 375px) 375px, (max-width: 560px) ${entity.imgSize.width}px, ${entity.imgSize.width}px`;
      } else if (entity.imgSize.width < 759) {
        entity.sizes = `(max-width: 375px) 375px, (max-width: 560px) 560px, (max-width: 759px) ${entity.imgSize.width}px, ${entity.imgSize.width}px`;
      } else if (entity.imgSize.width < 960) {
        entity.sizes = `(max-width: 375px) 375px, (max-width: 560px) 560px, (max-width: 759px) 759px, (max-width: 960px) ${entity.imgSize.width}px, ${entity.imgSize.width}px`;
      } else if (entity.imgSize.width < 1024) {
        entity.sizes = `(max-width: 375px) 375px, (max-width: 560px) 560px, (max-width: 759px) 759px, (max-width: 960px) 960px, (max-width: 1024px) ${entity.imgSize.width}px, ${entity.imgSize.width}px`;
      }
      break;
    // "Tab" covers images nested within Tabs, Accordion and Reveal components as they have very similar layouts/image sizing restrictions at varying viewpoints:
    case "Tab":
      imageRequest = {
        295: JSON.stringify({
          bucket: tenantData.bucketName,
          key: `${tenantData.imgKey}${entity.uploadedImg}`,
          edits: {
            resize: {
              width: 295,
              height: aspectRatio * 295,
              fit: "cover",
            },
          },
        }),
        664: JSON.stringify({
          bucket: tenantData.bucketName,
          key: `${tenantData.imgKey}${entity.uploadedImg}`,
          edits: {
            resize: {
              width: 664,
              height: aspectRatio * 664,
              fit: "cover",
            },
          },
        }),
        710: JSON.stringify({
          bucket: tenantData.bucketName,
          key: `${tenantData.imgKey}${entity.uploadedImg}`,
          edits: {
            resize: {
              width: 710,
              height: aspectRatio * 710,
              fit: "cover",
            },
          },
        }),
        [entity.imgSize.width]: JSON.stringify({
          bucket: tenantData.bucketName,
          key: `${tenantData.imgKey}${entity.uploadedImg}`,
          edits: {
            resize: {
              width: entity.imgSize.width,
              height: aspectRatio * entity.imgSize.width,
              fit: "cover",
            },
          },
        }),
      };

      entity.uploadedImg = {
        295: `${tenantData.serviceUrl}/${Buffer.from(
          imageRequest[295],
          "binary"
        ).toString("base64")}`,
        664: `${tenantData.serviceUrl}/${Buffer.from(
          imageRequest[664],
          "binary"
        ).toString("base64")}`,
        710: `${tenantData.serviceUrl}/${Buffer.from(
          imageRequest[710],
          "binary"
        ).toString("base64")}`,
        [entity.imgSize.width]: `${tenantData.serviceUrl}/${Buffer.from(
          imageRequest[entity.imgSize.width],
          "binary"
        ).toString("base64")}`,
      };

      entity.srcset = `
        ${entity.uploadedImg[295]} 295w,
        ${entity.uploadedImg[664]} 664w,
        ${entity.uploadedImg[710]} 710w,
        ${entity.uploadedImg[entity.imgSize.width]} ${entity.imgSize.width}w
      `;

      if (entity.imgSize.width > 710) {
        entity.sizes = `(max-width: 375px) 295px, (max-width: 759px) 664px, (max-width: 810px) 664px, ${entity.imgSize.width}px`;
      } else if (entity.imgSize.width > 664) {
        entity.sizes = `(max-width: 375px) 295px, (max-width: 759px) 664px, ${entity.imgSize.width}px`;
      } else if (entity.imgSize.width > 295) {
        entity.sizes = `(max-width: 375px) 295px, ${entity.imgSize.width}px`;
      }
      if (entity.imgSize.width < 295) {
        entity.sizes = `(max-width: 375px) ${entity.imgSize.width}px, ${entity.imgSize.width}px`;
      } else if (entity.imgSize.width < 664) {
        entity.sizes = `(max-width: 375px) 295px, (max-width: 759px) ${entity.imgSize.width}}px, ${entity.imgSize.width}px`;
      } else if (entity.imgSize.width < 710) {
        entity.sizes = `(max-width: 375px) 295px, (max-width: 759px) 664px, (max-width: 810px) ${entity.imgSize.width}px, ${entity.imgSize.width}px`;
      }
      break;
    // For Image components nested within Infobox:
    case "InfoBox":
      imageRequest = {
        295: JSON.stringify({
          bucket: tenantData.bucketName,
          key: `${tenantData.imgKey}${entity.uploadedImg}`,
          edits: {
            resize: {
              width: 295,
              height: aspectRatio * 295,
              fit: "cover",
            },
          },
        }),
        480: JSON.stringify({
          bucket: tenantData.bucketName,
          key: `${tenantData.imgKey}${entity.uploadedImg}`,
          edits: {
            resize: {
              width: 480,
              height: aspectRatio * 480,
              fit: "cover",
            },
          },
        }),
        680: JSON.stringify({
          bucket: tenantData.bucketName,
          key: `${tenantData.imgKey}${entity.uploadedImg}`,
          edits: {
            resize: {
              width: 680,
              height: aspectRatio * 680,
              fit: "cover",
            },
          },
        }),
        [entity.imgSize.width]: JSON.stringify({
          bucket: tenantData.bucketName,
          key: `${tenantData.imgKey}${entity.uploadedImg}`,
          edits: {
            resize: {
              width: entity.imgSize.width,
              height: aspectRatio * entity.imgSize.width,
              fit: "cover",
            },
          },
        }),
      };

      entity.uploadedImg = {
        295: `${tenantData.serviceUrl}/${Buffer.from(
          imageRequest[295],
          "binary"
        ).toString("base64")}`,
        480: `${tenantData.serviceUrl}/${Buffer.from(
          imageRequest[480],
          "binary"
        ).toString("base64")}`,
        680: `${tenantData.serviceUrl}/${Buffer.from(
          imageRequest[680],
          "binary"
        ).toString("base64")}`,
        [entity.imgSize.width]: `${tenantData.serviceUrl}/${Buffer.from(
          imageRequest[entity.imgSize.width],
          "binary"
        ).toString("base64")}`,
      };

      entity.srcset = `
        ${entity.uploadedImg[295]} 295w,
        ${entity.uploadedImg[480]} 480w,
        ${entity.uploadedImg[680]} 680w,
        ${entity.uploadedImg[entity.imgSize.width]} ${entity.imgSize.width}w
      `;

      // KEEP below commented code for now (TODO: connect Infobox images to SIH):
      // if (infoboxHasIcon === true && entity.imgSize.width < 482) {
      //   entity.sizes = `(max-width: 375px) 295px, (max-width: 560px) 480px, (max-width: 759px) ${entity.imgSize.width}px, ${entity.imgSize.width}px`;
      // } else if (infoboxHasIcon === true && entity.imgSize.width > 482) {
      //   entity.sizes = `(max-width: 375px) 295px, (max-width: 560px) 480px, (max-width: 759px) 482px, ${entity.imgSize.width}px`;
      // } else if (infoboxHasIcon === false && entity.imgSize.width < 680) {
      //   entity.sizes = `(max-width: 375px) 295px, (max-width: 560px) 480px, (max-width: 759px) ${entity.imgSize.width}px, ${entity.imgSize.width}px`;
      // } else if (infoboxHasIcon === false && entity.imgSize.width > 680) {
      //   entity.sizes = `(max-width: 375px) 295px, (max-width: 560px) 480px, (max-width: 759px) 656px, ${entity.imgSize.width}px`;
      // }
      if (entity.imgSize.width > 295) {
        entity.sizes = `(max-width: 375px) 295px, ${entity.imgSize.width}px`;
      } else {
        entity.sizes = `(max-width: 375px) ${entity.imgSize.width}px, ${entity.imgSize.width}px`;
      }
      if (entity.imgSize.width > 480 && infoboxHasIcon === true) {
        entity.sizes = `(max-width: 375px) 295px, (min-width: 759px) 480px, ${entity.imgSize.width}px`;
      } else if (entity.imgSize.width < 480 && infoboxHasIcon === true) {
        entity.sizes = `(max-width: 375px) 295px, (min-width: 759px) ${entity.imgSize.width}}px, ${entity.imgSize.width}px`;
      } else if (entity.imgSize.width > 680 && infoboxHasIcon === false) {
        entity.sizes = `(max-width: 375px) 295px, (min-width: 759px) 680px, ${entity.imgSize.width}px`;
      } else if (entity.imgSize.width < 680 && infoboxHasIcon === false) {
        entity.sizes = `(max-width: 375px) 295px, (min-width: 759px) ${entity.imgSize.width}px, ${entity.imgSize.width}px`;
      }
      break;
    // For Image components nested within MultiColumn:
    case "MultiColumn":
      imageRequest = {
        368: JSON.stringify({
          bucket: tenantData.bucketName,
          key: `${tenantData.imgKey}${entity.uploadedImg}`,
          edits: {
            resize: {
              width: 368,
              height: aspectRatio * 368,
              fit: "cover",
            },
          },
        }),
        515: JSON.stringify({
          bucket: tenantData.bucketName,
          key: `${tenantData.imgKey}${entity.uploadedImg}`,
          edits: {
            resize: {
              width: 515,
              height: aspectRatio * 515,
              fit: "cover",
            },
          },
        }),
        560: JSON.stringify({
          bucket: tenantData.bucketName,
          key: `${tenantData.imgKey}${entity.uploadedImg}`,
          edits: {
            resize: {
              width: 560,
              height: aspectRatio * 560,
              fit: "cover",
            },
          },
        }),
        [entity.imgSize.width]: JSON.stringify({
          bucket: tenantData.bucketName,
          key: `${tenantData.imgKey}${entity.uploadedImg}`,
          edits: {
            resize: {
              width: entity.imgSize.width,
              height: aspectRatio * entity.imgSize.width,
              fit: "cover",
            },
          },
        }),
      };

      entity.uploadedImg = {
        368: `${tenantData.serviceUrl}/${Buffer.from(
          imageRequest[368],
          "binary"
        ).toString("base64")}`,
        515: `${tenantData.serviceUrl}/${Buffer.from(
          imageRequest[515],
          "binary"
        ).toString("base64")}`,
        560: `${tenantData.serviceUrl}/${Buffer.from(
          imageRequest[560],
          "binary"
        ).toString("base64")}`,
        [entity.imgSize.width]: `${tenantData.serviceUrl}/${Buffer.from(
          imageRequest[entity.imgSize.width],
          "binary"
        ).toString("base64")}`,
      };
      break;
    default:
  }

  return entity.uploadedImg;
};

module.exports = { handleImageRequest };
