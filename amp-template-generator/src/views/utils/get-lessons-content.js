const { S3Client, GetObjectCommand } = require("@aws-sdk/client-s3");
const graphql = require("./graphql");
const queries = require("../scripts/queries");
const setters = require("../../utils/setters");

/**
 *
 * @returns
 * onSuccess: query response - data.getLessons;
 * onError: boolean - false;
 *
 */
async function getLessonsContentByQuery() {
  // if _ampParams is undefined, use 'getCourseAndLessons' for get course info as well.
  const _queryName = setters.getParams().ampParams
    ? "getLessons"
    : "getCourseAndLessons";
  const _courseID = setters.getParams().id;

  console.info(
    `--- utils/get-lessons-content - by query; Course ID: ${_courseID}`
  );
  console.time(
    `--- utils/get-lessons-content - query execution - ${_queryName}`
  );

  try {
    const data = await graphql.run(queries[_queryName], { id: _courseID });

    console.log("--- utils/get-lessons-content - byQuery; result: ", data);

    // if data empty, return false
    if (Object.keys(data).length === 0) {
      return false;
    }

    // if getCourseDetails exists, then build ampParams
    if (data?.getCourseDetails) {
      const STAGE_NAME_MAP = new Map([
        ["sbx01", "sbx"],
        ["qa01", "qa"],
        ["staging01", "stage"],
        ["prod01", "prod"],
      ]);
      const _obj = {
        stage: STAGE_NAME_MAP.get(process.env.STAGE_ENV), // get corresponding stage name
        courseId: _courseID,
        groupId: data.getCourseDetails.groupID,
        edition: data.getCourseDetails.versionNumber.major,
      };
      setters.setAmpParams(_obj);
    }
    if (data?.getLessons) return data.getLessons;
    return false;
  } catch (err) {
    console.error(
      "-- utils/query-getLessonsContent() error: client request catch err",
      err
    );
    return false;
  } finally {
    console.timeEnd(
      `--- utils/get-lessons-content - query execution - ${_queryName}`
    );
  }
}

// get content for preview, reading s3 json file
async function getLessonsContentByS3(
  bucketName,
  filename,
  awsCredentialsConfig
) {
  console.time(`-- call s3.getObject()`);
  const _params = {
    Bucket: bucketName,
    Key: filename,
  };

  // sdk v3:
  const s3Client = new S3Client(awsCredentialsConfig); // Provide AWS credentials configuration

  try {
    const _resp = await s3Client.send(new GetObjectCommand(_params));
    console.timeEnd(`-- call s3.getObject()`);

    // log the response
    console.info("-- s3.getObject() response: ", _resp);

    // sdk v3
    // if there is an error, return false
    if (_resp.$metadata.httpStatusCode !== 200) {
      console.error("Error response from S3:", _resp.$metadata.httpStatusCode);
      return false;
    }

    const _contentBody = await streamToString(_resp.Body);

    console.log("----S3: lesson content: ", _contentBody);

    return [JSON.parse(_contentBody)];
  } catch (err) {
    console.error("-- utils/getLessonsContentByS3() error:", err);
    return false;
  }
}

const streamToString = (stream) =>
  new Promise((resolve, reject) => {
    const chunks = [];
    stream.on("data", (chunk) => chunks.push(chunk));
    stream.on("error", reject);
    stream.on("end", () => resolve(Buffer.concat(chunks).toString()));
  });

module.exports = { getLessonsContentByQuery, getLessonsContentByS3 };
