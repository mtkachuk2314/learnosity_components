const { QuillDeltaToHtmlConverter } = require("quill-delta-to-html");
const katex = require("katex");
const imageHandler = require("./image-handler");

function parse(entity) {
  /**
   * Internal callback to recursively call self at the next level. Called by
   * forEach loops.
   *
   * @private
   * @param {Object} element
   * @returns {Object}
   * @throws {Error}
   */
  function __parseElement(element) {
    return (element = parse(element));
  }

  // I am passing over the course
  // I have access to componentContainers
  // Inside componentContainers there is sections
  // Sections hold non-learning and learning sections
  // Within the sections are the components
  // Inside of the components array is the { componentName: 'Text', props: { body: { ops: [Array] } } }
  // I need access to props.body to be able to convert it to a html element

  const _entity = entity.__typename ? entity.__typename : entity.componentName;

  // TODO: leave comment about passing that tenantData down
  switch (_entity) {
    case "CourseStructureContainer":
      entity.componentContainers.forEach(__parseElement);
      break;
    case "LessonStructureContainer":
      __parseElement({
        ...entity.componentContainer,
        tenantData: entity.tenantData,
      });
      break;
    case "ComponentContainer":
      entity.sections.forEach((element) =>
        __parseElement({ ...element, tenantData: entity.tenantData })
      );
      break;
    case "Section":
      entity.components.forEach((element) =>
        __parseElement({ ...element, tenantData: entity.tenantData })
      );
      break;
    case "Text":
      _convertText(entity.props);
      break;
    case "Tab":
    case "Accordion":
    case "Reveal":
      // Running a for loop through the first array we hit, which is the layoutState
      for (let i = 0; i < entity.props.layoutState.length; i++) {
        // Running another for loop to then iterate through the components in the tab and convert anything with componentProps.body
        for (
          let j = 0;
          j < entity.props.layoutState[i].components.length;
          j++
        ) {
          switch (entity.props.layoutState[i].components[j].componentName) {
            case "Text":
              _convertText(
                entity.props.layoutState[i].components[j].componentProps
              );
              break;
            case "Video":
              _convertVideo(
                entity.props.layoutState[i].components[j].componentProps
                  .videoState
              );
              break;
            case "Image":
              _convertImage(
                entity.props.layoutState[i].components[j].componentProps,
                entity.tenantData,
                "Tab"
              );
              break;
          }
        }
      }
      break;
    case "MultiColumn":
      // Running a for loop through the first array we hit, which is the layoutState
      for (let i = 0; i < entity.props.layoutState.length; i++) {
        // Running another for loop to then iterate through the components in the tab and convert anything with componentProps.body
        for (
          let j = 0;
          j < entity.props.layoutState[i].components.length;
          j++
        ) {
          switch (entity.props.layoutState[i].components[j].componentName) {
            case "Text":
              _convertText(
                entity.props.layoutState[i].components[j].componentProps
              );
              break;
            case "Video":
              _convertVideo(
                entity.props.layoutState[i].components[j].componentProps
                  .videoState
              );
              break;
            case "Image":
              _convertImage(
                entity.props.layoutState[i].components[j].componentProps,
                entity.tenantData,
                "MultiColumn"
              );
              break;
          }
        }
      }
      break;
    case "InfoBox":
      // Retained the old data flow for old courses.
      entity.props.infoBoxState.body = _setHtml(entity.props.infoBoxState.body);
      if (!entity.props.infoBoxState.components) return;

      for (let i = 0; i < entity.props.infoBoxState.components.length; i++) {
        switch (entity.props.infoBoxState.components[i].componentName) {
          case "Text":
            _convertText(
              entity.props.infoBoxState.components[i].componentProps
            );
            break;
          case "Video":
            _convertVideo(
              entity.props.infoBoxState.components[i].componentProps.videoState
            );
            break;
          case "Image":
            // TODO: use SIH/_convertImage() here in place of previously used function:
            _convertImageOld(
              entity.props.infoBoxState.components[i].componentProps,
              entity.tenantData
            );
            break;
        }
      }
      break;
    case "Video":
      _convertVideo(entity.props.videoState);
      break;
    case "Image":
      _convertImage(entity.props, entity.tenantData, "Image");
      break;
    default:
    // nothing to do here, because anything else doesn't have a fancy heading.
  }
  return entity;
}

const _convertText = (convertTextState) => {
  convertTextState.body = _setHtml(convertTextState.body);
  return convertTextState;
};

const _convertVideo = (convertVideoState) => {
  convertVideoState.videoDescription = _setHtml(
    convertVideoState.videoDescription
  );
  noBreakCredit = _replaceLineBreak(convertVideoState.videoCredit);
  convertVideoState.videoCredit = _setHtml(noBreakCredit);
  return convertVideoState;
};

const _convertImage = (convertImageState, tenantData, componentName) => {
  if (
    !convertImageState.uploadedImg ||
    convertImageState.uploadedImg?.includes(".gif")
  ) {
    convertImageState.credit = _setHtml(convertImageState.credit);
    convertImageState.longDesc = _setHtml(convertImageState.longDesc);
  } else {
    convertImageState.credit = _setHtml(convertImageState.credit);
    convertImageState.longDesc = _setHtml(convertImageState.longDesc);
    convertImageState.uploadedImg = imageHandler.handleImageRequest(
      convertImageState,
      tenantData,
      componentName
    );
  }

  return convertImageState;
};

// Keeping this in case needed for nested components rollback
const _convertImageOld = (convertImageState) => {
  convertImageState.credit = _setHtml(convertImageState.credit);
  convertImageState.longDesc = _setHtml(convertImageState.longDesc);
  return convertImageState;
};

const _replaceLineBreak = (text) => {
  if (text === "<br>") {
    text.replace("<br>", null);
  }
  return text;
};

// Converts the quill data into a html element
function _setHtml(quill) {
  // Converting and storing the quill data for easy access

  const item = quill?.ops;
  let converter = new QuillDeltaToHtmlConverter(item);
  converter.renderCustomWith((datas, ctxop) => {
    if (datas.insert.type === "mathpix") {
      try {
        const mathFormula = katex.renderToString(datas.insert.value.replace(/\\\[/g, "").replace(/\\\]/g, ""));
        return `<span>${mathFormula}</span>`;
      } catch (e) {
        if (e instanceof katex.ParseError) {
          // KaTeX can't parse the expression
          mathFormula = datas.insert.value;
          return `<span class="math-formula">${mathFormula}</span>`;
        } else {
          throw e; // other error
        }
      }
    }
  });

  html = converter.convert();

  // Adding sr-only text to links after quill conversion
  html = html.replace(
    /<\/a>/g,
    "<span><i class='anchor-icon'>&nbsp;&#xe802;</i></span><span class='sr-only'>(Opens in a new window)</span></a>"
  );

  // Replacing nest <br> tags
  html = html.replace(/<p><br><\/p>/g, "<br>");
  html = html.replace(/contenteditable="false"/g, "");
  // The below .replace() handles the replacement of &nbsp; that previously were replacing normal spaces only in the Video component description
  html = html.replace(/\u00A0/g, " ");

  // Wrapping the contents of li tags with span tags
  html = html.replace(/<li>/g, "<li><span>");

  return html;
}

module.exports = {
  parse: parse,
};
