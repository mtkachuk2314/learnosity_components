const parseData = require("./parse-props");
const quillConverter = require("./quill-converter");
const headingsConverter = require("./headings-converter");

// Array to hold all the lessons
const lessons = [];

const dataConversionFunction = (data, tenantData, isPreview) => {
  // reset lessons array on each request
  lessons.length = 0;
  // Parsing the original data object to convert parts of it that we received as strings into JSON format
  parseData.transformComponentPropsRecursive(data);

  // Looping over each lesson and convering the raw quill data and headings
  for (let lessonIndex = 0; lessonIndex < data.length; lessonIndex++) {
    // Storing a lesson in a variable on each iteration for easy access/manipulation
    const lesson = data[lessonIndex];
    console.log("lesson:", lesson);

    // lesson path is different between preview and published html
    let lessonPath = '';
    if (isPreview) {
      lessonPath = [lesson.HTMLFilename];
    } else {
      const twoDigits = lesson.indexInParent < 9 ? "0" : "";
      lessonPath = [
        ...lesson.path.map(
          (container) =>
            `${container.type.substring(0, 1).toUpperCase()}${container.indexInParent < 9 ? "0" : ""
            }${container.indexInParent + 1}`
        ),
        `L${twoDigits}${lesson.indexInParent + 1}`
      ].join("_");
    }

    // Getting all the quiz references for the lesson
    function getActivityIDs(lesson) {
      const activityIDs = [];

      function extractActivityIDs(components) {
        components.forEach((component) => {
          if (component.props?.multipleChoiceState?.activityID) {
            activityIDs.push(component.props.multipleChoiceState.activityID);
          }

          if (component.components) {
            extractActivityIDs(component.components);
          }
        });
      }

      if (lesson.componentContainer) {
        lesson.componentContainer.sections.forEach((section) => {
          extractActivityIDs(section.components);
        });
      }

      return activityIDs;
    }

    const quizReferences = getActivityIDs(lesson);

    lesson.quizReferences = quizReferences;
    lesson.studentAPI = process.env.StudentAssessmentURL;
    lesson.HTMLFilename = lesson.HTMLFilename || lessonPath;
    lesson.pageTitle = lesson.componentContainer.title;

    lessons.push(
      // Converting any headings found
      // Converting the raw quill data into a html format
      // Finally pushing each lesson into an array on each iteration
      headingsConverter.parse(
        quillConverter.parse({
          ...lesson,
          lessonPath,
          tenantData,
        })
      )
    );
  }
};

module.exports = {
  dataConversionFunction,
  lessons,
};
