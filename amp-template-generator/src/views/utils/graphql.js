const GraphQLClient = require("graphql-request").GraphQLClient;
const dotenv = require("dotenv");
const path = require("path");
const setters = require("../../utils/setters");
dotenv.config();

function _client() {

  if (process.env.NODE_ENV === "local") {
    if (setters.getParams()?.ampParams?.AUTH_TOKEN) return;
    console.log("Auth Token Empty. Running in local mode, getting token from env file");
    dotenv.config({ path: path.join(__dirname, "token.env") });
    console.log("Token", process.env.AUTH_TOKEN);
    setters.setAuthToken(process.env.AUTH_TOKEN);
  }

  let _params = setters.getParams();
  console.log('setters.getParams.gqlEndpoint', _params.gqlEndpoint);
  console.log('setters.getParams.authToken', _params.authToken);
  return new GraphQLClient(_params.gqlEndpoint, {
    headers: {
      "Content-Type": "application/json",
      // "x-api-key": process.env.X_API_KEY,
      authorizationToken: `Bearer ${_params.authToken}`,
    },
  });
}


/**
 * Returns an Object representing the entity id provided. If an error is 
 * encountered, an empty Object will be returned.
 * 
 * @param {String} query graphQL query to run
 * @param {Object} params query vars
 * @returns {Object}
 */
async function run(query, params) {
  let _gqlClient = _client();
  return _gqlClient.request(query, params)
    .catch(err => {
      console.log("--@-- (utils/index.js) err: ", err);
      if (err.hasOwnProperty('response')) {
        if (err.response.hasOwnProperty('errors')) {
          // error but probably not graphql specifically
          console.error('gql client errors', err.response.errors);
        } else {
          // graphql error
          console.error('gql err.error', err.error); // GraphQL response errors
          console.error('gql err.request.query', err.request.query); // GraphQL query errors
          console.error('gql err.response.data', err.response.data); // Response data if available
        }
      } else {
        // general error
        console.error('gql general err', err);
      }

      return {};
    });
}

// let _params = setters.getParams();
// console.log('setters.getParams', _params.gqlEndpoint);
// module.exports = {
//   client: new GraphQLClient(_params.gqlEndpoint, {
//     headers: {
//       "Content-Type": "application/json",
//       // "x-api-key": process.env.X_API_KEY,
//       authorizationToken: `Bearer ${_params.authToken}`,
//     },
//   }),
// };

module.exports = {
  client: _client,
  run: run
};
