const axios = require("axios");
const convertData = require("../utils/convert-data");
const setters = require("../../utils/setters");
const { getLessonsContentByQuery, getLessonsContentByS3 } = require("../utils/get-lessons-content");

async function _getTenantFetch(tenantAPI) {
  let _resp;
  try {
    console.time("contentResults.tenantFetch()");
    _resp = await axios({ method: "get", url: tenantAPI })
  } catch (err) {
    console.error(err.message, err.response?.data, err);
    throw new Error("fail to call axios to get tenant info");
  } finally {
    console.info(`--- utils/contentResults - _getTenantFetch() axios response: `, _resp);
    console.timeEnd("contentResults.tenantFetch()");
  }
  let _ampParams = setters.getParams().ampParams;
  let _domain = _ampParams.stage !== 'prod' ? `sih-${_ampParams.stage}` : `sih`;
  let _isMediaImgSrc = _ampParams?.mode === 'preview' || _ampParams?.imageSource === 'media';

  return {
    brightcoveAccountId: _resp.data.brightcoveAccountId,
    brightcovePolicyKey: _resp.data.brightcovePolicyKey,
    brightcovePlayer: _resp.data.brightcovePlayer,
    imageTenantUrl: _resp.data.courseMediaCFrontUrl,
    bucketName: _isMediaImgSrc 
      ? `s3-coursemedia-${_resp.data.tenantId}-${process.env.STAGE_ENV}-cac1-01` 
      : `s3-coursepublished-${_resp.data.tenantName}-${process.env.STAGE_ENV}-cac1-01`,
    imgKey: _isMediaImgSrc 
      ? ``
      : `${_ampParams.groupId}/${_ampParams.edition}/amp/lessons/assets/`,
    serviceUrl: `https://${_domain}.tvopublishing.com`,
  }
}

/**
 * Returns an array of lessons, based on the Node ID. Even if you are
 * requesting a singular lesson, it will be returned as an array of one
 * lesson. If an error is encountered, an empty Array will be returned.
 *
 * @returns {Array}
 */
module.exports = async function () {
  console.time("contentResults.js");

  let _ampParams = setters.getParams().ampParams;
  let _lessonContent;
  let _isPreview = _ampParams?.mode === 'preview';

  // If there is an existence of queryResult, then use it
  // Otherwise, getLessonContent by reading S3 (for preview) or by obtaining from apollo
  if (_ampParams?.queryResultCourseAndLessons && _ampParams.queryResultCourseAndLessons?.getLessons) {
    _lessonContent = _ampParams.queryResultCourseAndLessons.getLessons;
  } else if (_isPreview) {
    let _bucketName = _ampParams.bucketName;
    let _jsonFilePath = _ampParams.parsedDatafile;
    let _htmlFilename = _ampParams.htmlFilename;
    if (!_bucketName || !_jsonFilePath || !_htmlFilename) {
      console.error(`--- utils/contentResults - getLessonsContent() failed. Expected field is missing; _ampParams: `, _ampParams);
      return false
    }
    _lessonContent = await getLessonsContentByS3(_bucketName, _jsonFilePath, _ampParams.awsCredentialsConfig);
  } else {
    _lessonContent = await getLessonsContentByQuery();
  }

  if (!_lessonContent) { // false or undefined
    console.error(`--- utils/contentResults - getLessonsContent() failed. No data. Preview: ${_isPreview}`);
    return false
  }

  // provide the filename for 11ty
  if (_isPreview) {
    let _htmlFilename = _ampParams.htmlFilename;
    if (_htmlFilename.endsWith(".html")) {
      _htmlFilename = _htmlFilename.slice(0, -5);
    }
    _lessonContent[0].HTMLFilename = _htmlFilename;
  }

  // get tenant info
  let _tenantAPI = setters.getParams().tenantAPI;
  let _tenantFetch;
  try {
    _tenantFetch = await _getTenantFetch(_tenantAPI);
    console.info(`--- utils/contentResults - _tenantFetch: `, _tenantFetch);
  } catch (err) {
    return false;
  }

  if (
    // If the returns is an array of LessonStructureContainers (getLessons)
    Array.isArray(_lessonContent) &&
    _lessonContent[0]?.__typename === "LessonStructureContainer"
  ) {
    // Before conversion log
    // console.log("before conversion data", JSON.stringify(_lessonContent, null, 4));
    console.time("contentResults.dataConversionFunction()");
    convertData.dataConversionFunction(_lessonContent, _tenantFetch, _isPreview);
    console.timeEnd("contentResults.dataConversionFunction()");
    // Afer conversion log
    // console.log("after conversion data", JSON.stringify(_lessonContent, null, 4));
  } else {
    // TODO: may need to check for type if __typename does not exist eg. getLesson
  }
  // TODO: handle promise rejection if something goes wrong

  console.timeEnd("contentResults.js");
  // Returning the array that we set in convertData.js
  return convertData.lessons;
};
