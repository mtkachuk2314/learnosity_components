const Eleventy = require("@11ty/eleventy");
const path = require("path");
require("dotenv").config();
const csAmpOptimizer = require("./utils/ampoptimize");
const csPurgeCss = require("./utils/purgecss");
const pjson = require('./package.json');
const AmpOptimizer = require('@ampproject/toolbox-optimizer');
const setters = require("./utils/setters");

const compile = async function (promise) {
  const ts = Date.now();
  console.log("luke.compile() called", "v" + pjson.version, ts);
  const _configpath = path.join(__dirname, ".eleventy.js");
  const _input = path.join(__dirname, 'views/node.liquid');
  const _output = "/";
  const _css = path.join(__dirname, "/styles/styles.css");

  let _isPreview = promise?.ampParams?.mode === 'preview';

  // Fetching Tenant api and ID from event; when it is preview, get from it ampParams
  let domainURL, tenantId = "";
  if (_isPreview) {
    tenantId = promise.ampParams.tenantID;
    domainURL = promise.ampParams.stage !== "prod" ? `api-${promise.ampParams.stage}.tvopublishing.com` : "api.tvopublishing.com";
  } else {
    domainURL = promise.requestContext.domainName ? promise.requestContext.domainName : "api.tvopublishing.com";
    tenantId = promise.requestContext.authorizer.tenantId;
  }

  var _files = new Array();
  let elev = new Eleventy(_input, _output, {
    // --quiet
    // quietMode: true,
    // --config
    configPath: _configpath,
  });


  // set params usage for contentResults
  setters.setTenantAPI(`https://${domainURL}/tenants/id/${tenantId}`);
  setters.setAmpParams(promise.ampParams);
  process.env.StudentAssessmentURL = `https://${domainURL}/assessments/student`;

  if (!_isPreview) {
    setters.setCourseID(promise.queryStringParameters.id);
    let _requestContextToken = promise.requestContext.authorizer.authorizationToken;
    let _authToken = _requestContextToken.replace("Bearer ", "");
    let _host = promise.headers.Host ? promise.headers.Host : promise.headers.host;
    setters.setAuthToken(_authToken);
    setters.setGraphqlEndpoint(`https://${_host}/apollo/graphql`);
  }

  // start 11ty process programatically
  let stream = await elev.toNDJSON();

  stream.on("data", (entry) => {
    let json = JSON.parse(entry.toString());
    _files.push(json);
  });

  stream.on("end", () => {
    // console.log("11ty stream process end", _files.length);
  });

  stream.on("close", async () => {
    console.log("11ty stream process closed - post 11ty process");
    var _promises = new Array();
    if (_files.length > 0) {
      _files.forEach((file, index, array) => {
        _promises.push(queueFile(file, _css));
      });
      return await promiseAll(_promises).then((result) => {
        // console.log("respond to handler", result.statusCode);
        if (result.statusCode === 200) {
          promise.resolve(result);
        } else {
          promise.reject(result);
        }
      });
    } else {
      // no files
      promise.reject({
        statusCode: 400,
        message: "An error occured and the files could not be processed",
      });
    }
  });
};

async function queueFile(file, _css) {
  return new Promise(async function (resolve, reject) {
    await csPurgeCss(file.content, [_css]).then(async (result) => {
      const config = {
        verbose: false,
        extensionVersions: {
          "amp-carousel": "0.1",
        },
        transformations: [...AmpOptimizer.TRANSFORMATIONS_AMP_FIRST],
        minify: true,
      };

      await csAmpOptimizer(result, config).then((result) => {
        file.content = result;
        console.log("csPurgeCss => csAmpOptimizer =>", file.outputPath);
        // console.log("file.content", file.content);
      });
    });
    resolve(file);
  });
}

async function promiseAll(promises) {
  // console.log("promiseAll", promises);
  let _postfiles = new Array();
  return Promise.allSettled(promises).then(
    async (files) => {
      files.forEach(file => {
        _postfiles.push(file.value);
      });
      console.log('promiseAll files: ' + _postfiles.length);
      return {
        statusCode: 200,
        headers: { "Access-Control-Allow-Origin": "*" },
        message: `Success from luke.compile(); ${_postfiles.length} file(s) generated`,
        data: _postfiles,
      };
    },
    (reason) => {
      console.log(reason);
      return {
        statusCode: 400,
        headers: { "Access-Control-Allow-Origin": "*" },
        body: JSON.stringify({
          message: reason,
        }),
      };
    }
  );
}

module.exports = { compile };
