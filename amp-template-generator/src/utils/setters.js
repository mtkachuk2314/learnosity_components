"use strict";

let _id, _tenantAPI = "";
let _authToken, _gqlEndpoint = "";
let _ampParams;

function setCourseID(id) {
  _id = id;
}

function setTenantAPI(api) {
  _tenantAPI = api;
}

function setAuthToken(token) {
  _authToken = token;
}

function setGraphqlEndpoint(endpoint) {
  _gqlEndpoint = endpoint;
}

function setAmpParams(obj) {
  if (obj) _ampParams = obj;
}

function getParams() {
  if (process.env.NODE_ENV === "local") {
    _id = process.env.ID;
    _tenantAPI = process.env.TENANT_API;
    _gqlEndpoint = process.env.GQL_ENDPOINT;
  }
  return {
    id: _id,
    tenantAPI: _tenantAPI,
    gqlEndpoint: _gqlEndpoint,
    authToken: _authToken,
    ampParams: _ampParams,
  };
}

module.exports = { getParams, setCourseID, setTenantAPI, setAuthToken, setGraphqlEndpoint, setAmpParams }
